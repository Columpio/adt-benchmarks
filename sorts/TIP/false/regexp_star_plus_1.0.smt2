(set-logic ALL)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-sort Nat@0 0)
(declare-fun Z@0 () Nat@0)
(declare-fun S@0 (Nat@0) Nat@0)
(declare-fun diseqNat@0@54 (Nat@0 Nat@0) Bool)
(assert (forall ((x@1602 Nat@0)) (diseqNat@0@54 Z@0 (S@0 x@1602))))
(assert (forall ((x@1603 Nat@0)) (diseqNat@0@54 (S@0 x@1603) Z@0)))
(assert (forall ((x@1604 Nat@0) (x@1605 Nat@0)) (=> (diseqNat@0@54 x@1604 x@1605) (diseqNat@0@54 (S@0 x@1604) (S@0 x@1605)))))
(declare-sort T 0)
(declare-fun A () T)
(declare-fun B () T)
(declare-fun C () T)
(declare-fun diseqT@2 (T T) Bool)
(assert (diseqT@2 A B))
(assert (diseqT@2 A C))
(assert (diseqT@2 B A))
(assert (diseqT@2 C A))
(assert (diseqT@2 B C))
(assert (diseqT@2 C B))
(declare-sort list 0)
(declare-fun nil () list)
(declare-fun cons (T list) list)
(declare-fun diseqlist@45 (list list) Bool)
(assert (forall ((x@1606 T) (x@1607 list)) (diseqlist@45 nil (cons x@1606 x@1607))))
(assert (forall ((x@1608 T) (x@1609 list)) (diseqlist@45 (cons x@1608 x@1609) nil)))
(assert (forall ((x@1610 T) (x@1611 list) (x@1612 T) (x@1613 list)) (=> (diseqT@2 x@1610 x@1612) (diseqlist@45 (cons x@1610 x@1611) (cons x@1612 x@1613)))))
(assert (forall ((x@1610 T) (x@1611 list) (x@1612 T) (x@1613 list)) (=> (diseqlist@45 x@1611 x@1613) (diseqlist@45 (cons x@1610 x@1611) (cons x@1612 x@1613)))))
(declare-sort R 0)
(declare-fun Nil () R)
(declare-fun Eps () R)
(declare-fun Atom (T) R)
(declare-fun |:+:| (R R) R)
(declare-fun |:>:| (R R) R)
(declare-fun Star (R) R)
(declare-fun diseqR@2 (R R) Bool)
(assert (diseqR@2 Nil Eps))
(assert (forall ((x@1614 T)) (diseqR@2 Nil (Atom x@1614))))
(assert (forall ((x@1615 R) (x@1616 R)) (diseqR@2 Nil (|:+:| x@1615 x@1616))))
(assert (forall ((x@1617 R) (x@1618 R)) (diseqR@2 Nil (|:>:| x@1617 x@1618))))
(assert (forall ((x@1619 R)) (diseqR@2 Nil (Star x@1619))))
(assert (diseqR@2 Eps Nil))
(assert (forall ((x@1620 T)) (diseqR@2 (Atom x@1620) Nil)))
(assert (forall ((x@1621 R) (x@1622 R)) (diseqR@2 (|:+:| x@1621 x@1622) Nil)))
(assert (forall ((x@1623 R) (x@1624 R)) (diseqR@2 (|:>:| x@1623 x@1624) Nil)))
(assert (forall ((x@1625 R)) (diseqR@2 (Star x@1625) Nil)))
(assert (forall ((x@1626 T)) (diseqR@2 Eps (Atom x@1626))))
(assert (forall ((x@1627 R) (x@1628 R)) (diseqR@2 Eps (|:+:| x@1627 x@1628))))
(assert (forall ((x@1629 R) (x@1630 R)) (diseqR@2 Eps (|:>:| x@1629 x@1630))))
(assert (forall ((x@1631 R)) (diseqR@2 Eps (Star x@1631))))
(assert (forall ((x@1632 T)) (diseqR@2 (Atom x@1632) Eps)))
(assert (forall ((x@1633 R) (x@1634 R)) (diseqR@2 (|:+:| x@1633 x@1634) Eps)))
(assert (forall ((x@1635 R) (x@1636 R)) (diseqR@2 (|:>:| x@1635 x@1636) Eps)))
(assert (forall ((x@1637 R)) (diseqR@2 (Star x@1637) Eps)))
(assert (forall ((x@1638 T) (x@1639 R) (x@1640 R)) (diseqR@2 (Atom x@1638) (|:+:| x@1639 x@1640))))
(assert (forall ((x@1641 T) (x@1642 R) (x@1643 R)) (diseqR@2 (Atom x@1641) (|:>:| x@1642 x@1643))))
(assert (forall ((x@1644 T) (x@1645 R)) (diseqR@2 (Atom x@1644) (Star x@1645))))
(assert (forall ((x@1646 R) (x@1647 R) (x@1648 T)) (diseqR@2 (|:+:| x@1646 x@1647) (Atom x@1648))))
(assert (forall ((x@1649 R) (x@1650 R) (x@1651 T)) (diseqR@2 (|:>:| x@1649 x@1650) (Atom x@1651))))
(assert (forall ((x@1652 R) (x@1653 T)) (diseqR@2 (Star x@1652) (Atom x@1653))))
(assert (forall ((x@1654 R) (x@1655 R) (x@1656 R) (x@1657 R)) (diseqR@2 (|:+:| x@1654 x@1655) (|:>:| x@1656 x@1657))))
(assert (forall ((x@1658 R) (x@1659 R) (x@1660 R)) (diseqR@2 (|:+:| x@1658 x@1659) (Star x@1660))))
(assert (forall ((x@1661 R) (x@1662 R) (x@1663 R) (x@1664 R)) (diseqR@2 (|:>:| x@1661 x@1662) (|:+:| x@1663 x@1664))))
(assert (forall ((x@1665 R) (x@1666 R) (x@1667 R)) (diseqR@2 (Star x@1665) (|:+:| x@1666 x@1667))))
(assert (forall ((x@1668 R) (x@1669 R) (x@1670 R)) (diseqR@2 (|:>:| x@1668 x@1669) (Star x@1670))))
(assert (forall ((x@1671 R) (x@1672 R) (x@1673 R)) (diseqR@2 (Star x@1671) (|:>:| x@1672 x@1673))))
(assert (forall ((x@1674 T) (x@1675 T)) (=> (diseqT@2 x@1674 x@1675) (diseqR@2 (Atom x@1674) (Atom x@1675)))))
(assert (forall ((x@1676 R) (x@1677 R) (x@1678 R) (x@1679 R)) (=> (diseqR@2 x@1676 x@1678) (diseqR@2 (|:+:| x@1676 x@1677) (|:+:| x@1678 x@1679)))))
(assert (forall ((x@1676 R) (x@1677 R) (x@1678 R) (x@1679 R)) (=> (diseqR@2 x@1677 x@1679) (diseqR@2 (|:+:| x@1676 x@1677) (|:+:| x@1678 x@1679)))))
(assert (forall ((x@1680 R) (x@1681 R) (x@1682 R) (x@1683 R)) (=> (diseqR@2 x@1680 x@1682) (diseqR@2 (|:>:| x@1680 x@1681) (|:>:| x@1682 x@1683)))))
(assert (forall ((x@1680 R) (x@1681 R) (x@1682 R) (x@1683 R)) (=> (diseqR@2 x@1681 x@1683) (diseqR@2 (|:>:| x@1680 x@1681) (|:>:| x@1682 x@1683)))))
(assert (forall ((x@1684 R) (x@1685 R)) (=> (diseqR@2 x@1684 x@1685) (diseqR@2 (Star x@1684) (Star x@1685)))))
(declare-fun eps (Bool R) Bool)
(assert (forall ((x@1583 Bool) (x R) (x@1581 R)) (=> (and (= x@1583 false) (= x x@1581)) (eps x@1583 x))))
(assert (forall ((x@1583 Bool) (x R)) (=> (and (= x@1583 true) (= x Eps)) (eps x@1583 x))))
(assert (forall ((x@1583 Bool) (x R) (p R) (q R) (x@1585 Bool) (x@1584 Bool)) (=> (and (= x@1583 (or x@1585 x@1584)) (= x (|:+:| p q)) (eps x@1585 p) (eps x@1584 q)) (eps x@1583 x))))
(assert (forall ((x@1583 Bool) (x R) (r R) (q2 R) (x@1587 Bool) (x@1586 Bool)) (=> (and (= x@1583 (and x@1587 x@1586)) (= x (|:>:| r q2)) (eps x@1587 r) (eps x@1586 q2)) (eps x@1583 x))))
(assert (forall ((x@1583 Bool) (x R) (y R)) (=> (and (= x@1583 true) (= x (Star y))) (eps x@1583 x))))
(declare-fun step (R R T) Bool)
(assert (forall ((x@1588 R) (x R) (y T) (x@1582 R)) (=> (and (= x@1588 Nil) (= x x@1582)) (step x@1588 x y))))
(assert (forall ((x@1588 R) (x R) (y T) (b T)) (=> (and (= x@1588 (ite (= b y) Eps Nil)) (= x (Atom b))) (step x@1588 x y))))
(assert (forall ((x@1588 R) (x R) (y T) (p R) (q R) (x@1590 R) (x@1589 R)) (=> (and (= x@1588 (|:+:| x@1590 x@1589)) (= x (|:+:| p q)) (step x@1590 p y) (step x@1589 q y)) (step x@1588 x y))))
(assert (forall ((x@1588 R) (x R) (y T) (r R) (q2 R) (x@1591 Bool) (x@1593 R) (x@1592 R) (x@1594 R)) (=> (and (= x@1588 (ite x@1591 (|:+:| (|:>:| x@1593 q2) x@1592) (|:+:| (|:>:| x@1594 q2) Nil))) (= x (|:>:| r q2)) (eps x@1591 r) (step x@1593 r y) (step x@1592 q2 y) (step x@1594 r y)) (step x@1588 x y))))
(assert (forall ((x@1588 R) (x R) (y T) (p2 R) (x@1595 R)) (=> (and (= x@1588 (|:>:| x@1595 (Star p2))) (= x (Star p2)) (step x@1595 p2 y)) (step x@1588 x y))))
(declare-fun rec (Bool R list) Bool)
(assert (forall ((x@1596 Bool) (x R) (y list) (x@1597 Bool)) (=> (and (= x@1596 x@1597) (= y nil) (eps x@1597 x)) (rec x@1596 x y))))
(assert (forall ((x@1596 Bool) (x R) (y list) (z T) (xs list) (x@1599 Bool) (x@1598 R)) (=> (and (= x@1596 x@1599) (= y (cons z xs)) (rec x@1599 x@1598 xs) (step x@1598 x z)) (rec x@1596 x y))))
(assert (forall ((p R) (q R) (s list)) (forall ((x@1600 Bool) (x@1601 Bool)) (=> (and (not x@1601) x@1600 (rec x@1600 (Star (|:+:| p q)) s) (rec x@1601 (|:+:| (Star p) (Star q)) s)) false))))
(check-sat)
(get-info :reason-unknown)
