(set-logic ALL)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-sort Nat@0 0)
(declare-fun Z@0 () Nat@0)
(declare-fun S@0 (Nat@0) Nat@0)
(declare-fun diseqNat@0@98 (Nat@0 Nat@0) Bool)
(assert (forall ((x@5803 Nat@0)) (diseqNat@0@98 Z@0 (S@0 x@5803))))
(assert (forall ((x@5804 Nat@0)) (diseqNat@0@98 (S@0 x@5804) Z@0)))
(assert (forall ((x@5805 Nat@0) (x@5806 Nat@0)) (=> (diseqNat@0@98 x@5805 x@5806) (diseqNat@0@98 (S@0 x@5805) (S@0 x@5806)))))
(declare-sort Nat 0)
(declare-fun S (Nat) Nat)
(declare-fun Z () Nat)
(declare-fun diseqNat@54 (Nat Nat) Bool)
(assert (forall ((x@5807 Nat)) (diseqNat@54 (S x@5807) Z)))
(assert (forall ((x@5808 Nat)) (diseqNat@54 Z (S x@5808))))
(assert (forall ((x@5809 Nat) (x@5810 Nat)) (=> (diseqNat@54 x@5809 x@5810) (diseqNat@54 (S x@5809) (S x@5810)))))
(declare-sort list 0)
(declare-fun nil () list)
(declare-fun cons (Nat list) list)
(declare-fun diseqlist@89 (list list) Bool)
(assert (forall ((x@5811 Nat) (x@5812 list)) (diseqlist@89 nil (cons x@5811 x@5812))))
(assert (forall ((x@5813 Nat) (x@5814 list)) (diseqlist@89 (cons x@5813 x@5814) nil)))
(assert (forall ((x@5815 Nat) (x@5816 list) (x@5817 Nat) (x@5818 list)) (=> (diseqNat@54 x@5815 x@5817) (diseqlist@89 (cons x@5815 x@5816) (cons x@5817 x@5818)))))
(assert (forall ((x@5815 Nat) (x@5816 list) (x@5817 Nat) (x@5818 list)) (=> (diseqlist@89 x@5816 x@5818) (diseqlist@89 (cons x@5815 x@5816) (cons x@5817 x@5818)))))
(declare-fun ++ (list list list) Bool)
(assert (forall ((x@5797 list) (x list) (y list)) (=> (and (= x@5797 y) (= x nil)) (++ x@5797 x y))))
(assert (forall ((x@5797 list) (x list) (y list) (z Nat) (xs list) (x@5798 list)) (=> (and (= x@5797 (cons z x@5798)) (= x (cons z xs)) (++ x@5798 xs y)) (++ x@5797 x y))))
(declare-fun rotate (list Nat list) Bool)
(assert (forall ((x@5799 list) (x Nat) (y list) (z Nat)) (=> (and (= x@5799 nil) (= x (S z)) (= y nil)) (rotate x@5799 x y))))
(assert (forall ((x@5799 list) (x Nat) (y list) (z Nat) (x2 Nat) (x3 list) (x@5801 list) (x@5800 list)) (=> (and (= x@5799 x@5801) (= x (S z)) (= y (cons x2 x3)) (rotate x@5801 z x@5800) (++ x@5800 x3 (cons x2 nil))) (rotate x@5799 x y))))
(assert (forall ((x@5799 list) (x Nat) (y list)) (=> (and (= x@5799 y) (= x Z)) (rotate x@5799 x y))))
(assert (forall ((n Nat) (xs list)) (forall ((x@5802 list)) (=> (and (diseqlist@89 xs x@5802) (rotate x@5802 n xs)) false))))
(check-sat)
(get-info :reason-unknown)
