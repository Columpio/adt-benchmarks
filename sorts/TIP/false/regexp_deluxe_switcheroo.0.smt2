(set-logic ALL)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-sort Nat@0 0)
(declare-fun Z@0 () Nat@0)
(declare-fun S@0 (Nat@0) Nat@0)
(declare-fun diseqNat@0@71 (Nat@0 Nat@0) Bool)
(assert (forall ((x@3162 Nat@0)) (diseqNat@0@71 Z@0 (S@0 x@3162))))
(assert (forall ((x@3163 Nat@0)) (diseqNat@0@71 (S@0 x@3163) Z@0)))
(assert (forall ((x@3164 Nat@0) (x@3165 Nat@0)) (=> (diseqNat@0@71 x@3164 x@3165) (diseqNat@0@71 (S@0 x@3164) (S@0 x@3165)))))
(declare-sort T 0)
(declare-fun A () T)
(declare-fun B () T)
(declare-fun C () T)
(declare-fun diseqT@13 (T T) Bool)
(assert (diseqT@13 A B))
(assert (diseqT@13 A C))
(assert (diseqT@13 B A))
(assert (diseqT@13 C A))
(assert (diseqT@13 B C))
(assert (diseqT@13 C B))
(declare-sort list 0)
(declare-fun nil () list)
(declare-fun cons (T list) list)
(declare-fun diseqlist@62 (list list) Bool)
(assert (forall ((x@3166 T) (x@3167 list)) (diseqlist@62 nil (cons x@3166 x@3167))))
(assert (forall ((x@3168 T) (x@3169 list)) (diseqlist@62 (cons x@3168 x@3169) nil)))
(assert (forall ((x@3170 T) (x@3171 list) (x@3172 T) (x@3173 list)) (=> (diseqT@13 x@3170 x@3172) (diseqlist@62 (cons x@3170 x@3171) (cons x@3172 x@3173)))))
(assert (forall ((x@3170 T) (x@3171 list) (x@3172 T) (x@3173 list)) (=> (diseqlist@62 x@3171 x@3173) (diseqlist@62 (cons x@3170 x@3171) (cons x@3172 x@3173)))))
(declare-sort R 0)
(declare-fun Nil () R)
(declare-fun Eps () R)
(declare-fun Atom (T) R)
(declare-fun |:+:| (R R) R)
(declare-fun |:&:| (R R) R)
(declare-fun |:>:| (R R) R)
(declare-fun Star (R) R)
(declare-fun diseqR@13 (R R) Bool)
(assert (diseqR@13 Nil Eps))
(assert (forall ((x@3174 T)) (diseqR@13 Nil (Atom x@3174))))
(assert (forall ((x@3175 R) (x@3176 R)) (diseqR@13 Nil (|:+:| x@3175 x@3176))))
(assert (forall ((x@3177 R) (x@3178 R)) (diseqR@13 Nil (|:&:| x@3177 x@3178))))
(assert (forall ((x@3179 R) (x@3180 R)) (diseqR@13 Nil (|:>:| x@3179 x@3180))))
(assert (forall ((x@3181 R)) (diseqR@13 Nil (Star x@3181))))
(assert (diseqR@13 Eps Nil))
(assert (forall ((x@3182 T)) (diseqR@13 (Atom x@3182) Nil)))
(assert (forall ((x@3183 R) (x@3184 R)) (diseqR@13 (|:+:| x@3183 x@3184) Nil)))
(assert (forall ((x@3185 R) (x@3186 R)) (diseqR@13 (|:&:| x@3185 x@3186) Nil)))
(assert (forall ((x@3187 R) (x@3188 R)) (diseqR@13 (|:>:| x@3187 x@3188) Nil)))
(assert (forall ((x@3189 R)) (diseqR@13 (Star x@3189) Nil)))
(assert (forall ((x@3190 T)) (diseqR@13 Eps (Atom x@3190))))
(assert (forall ((x@3191 R) (x@3192 R)) (diseqR@13 Eps (|:+:| x@3191 x@3192))))
(assert (forall ((x@3193 R) (x@3194 R)) (diseqR@13 Eps (|:&:| x@3193 x@3194))))
(assert (forall ((x@3195 R) (x@3196 R)) (diseqR@13 Eps (|:>:| x@3195 x@3196))))
(assert (forall ((x@3197 R)) (diseqR@13 Eps (Star x@3197))))
(assert (forall ((x@3198 T)) (diseqR@13 (Atom x@3198) Eps)))
(assert (forall ((x@3199 R) (x@3200 R)) (diseqR@13 (|:+:| x@3199 x@3200) Eps)))
(assert (forall ((x@3201 R) (x@3202 R)) (diseqR@13 (|:&:| x@3201 x@3202) Eps)))
(assert (forall ((x@3203 R) (x@3204 R)) (diseqR@13 (|:>:| x@3203 x@3204) Eps)))
(assert (forall ((x@3205 R)) (diseqR@13 (Star x@3205) Eps)))
(assert (forall ((x@3206 T) (x@3207 R) (x@3208 R)) (diseqR@13 (Atom x@3206) (|:+:| x@3207 x@3208))))
(assert (forall ((x@3209 T) (x@3210 R) (x@3211 R)) (diseqR@13 (Atom x@3209) (|:&:| x@3210 x@3211))))
(assert (forall ((x@3212 T) (x@3213 R) (x@3214 R)) (diseqR@13 (Atom x@3212) (|:>:| x@3213 x@3214))))
(assert (forall ((x@3215 T) (x@3216 R)) (diseqR@13 (Atom x@3215) (Star x@3216))))
(assert (forall ((x@3217 R) (x@3218 R) (x@3219 T)) (diseqR@13 (|:+:| x@3217 x@3218) (Atom x@3219))))
(assert (forall ((x@3220 R) (x@3221 R) (x@3222 T)) (diseqR@13 (|:&:| x@3220 x@3221) (Atom x@3222))))
(assert (forall ((x@3223 R) (x@3224 R) (x@3225 T)) (diseqR@13 (|:>:| x@3223 x@3224) (Atom x@3225))))
(assert (forall ((x@3226 R) (x@3227 T)) (diseqR@13 (Star x@3226) (Atom x@3227))))
(assert (forall ((x@3228 R) (x@3229 R) (x@3230 R) (x@3231 R)) (diseqR@13 (|:+:| x@3228 x@3229) (|:&:| x@3230 x@3231))))
(assert (forall ((x@3232 R) (x@3233 R) (x@3234 R) (x@3235 R)) (diseqR@13 (|:+:| x@3232 x@3233) (|:>:| x@3234 x@3235))))
(assert (forall ((x@3236 R) (x@3237 R) (x@3238 R)) (diseqR@13 (|:+:| x@3236 x@3237) (Star x@3238))))
(assert (forall ((x@3239 R) (x@3240 R) (x@3241 R) (x@3242 R)) (diseqR@13 (|:&:| x@3239 x@3240) (|:+:| x@3241 x@3242))))
(assert (forall ((x@3243 R) (x@3244 R) (x@3245 R) (x@3246 R)) (diseqR@13 (|:>:| x@3243 x@3244) (|:+:| x@3245 x@3246))))
(assert (forall ((x@3247 R) (x@3248 R) (x@3249 R)) (diseqR@13 (Star x@3247) (|:+:| x@3248 x@3249))))
(assert (forall ((x@3250 R) (x@3251 R) (x@3252 R) (x@3253 R)) (diseqR@13 (|:&:| x@3250 x@3251) (|:>:| x@3252 x@3253))))
(assert (forall ((x@3254 R) (x@3255 R) (x@3256 R)) (diseqR@13 (|:&:| x@3254 x@3255) (Star x@3256))))
(assert (forall ((x@3257 R) (x@3258 R) (x@3259 R) (x@3260 R)) (diseqR@13 (|:>:| x@3257 x@3258) (|:&:| x@3259 x@3260))))
(assert (forall ((x@3261 R) (x@3262 R) (x@3263 R)) (diseqR@13 (Star x@3261) (|:&:| x@3262 x@3263))))
(assert (forall ((x@3264 R) (x@3265 R) (x@3266 R)) (diseqR@13 (|:>:| x@3264 x@3265) (Star x@3266))))
(assert (forall ((x@3267 R) (x@3268 R) (x@3269 R)) (diseqR@13 (Star x@3267) (|:>:| x@3268 x@3269))))
(assert (forall ((x@3270 T) (x@3271 T)) (=> (diseqT@13 x@3270 x@3271) (diseqR@13 (Atom x@3270) (Atom x@3271)))))
(assert (forall ((x@3272 R) (x@3273 R) (x@3274 R) (x@3275 R)) (=> (diseqR@13 x@3272 x@3274) (diseqR@13 (|:+:| x@3272 x@3273) (|:+:| x@3274 x@3275)))))
(assert (forall ((x@3272 R) (x@3273 R) (x@3274 R) (x@3275 R)) (=> (diseqR@13 x@3273 x@3275) (diseqR@13 (|:+:| x@3272 x@3273) (|:+:| x@3274 x@3275)))))
(assert (forall ((x@3276 R) (x@3277 R) (x@3278 R) (x@3279 R)) (=> (diseqR@13 x@3276 x@3278) (diseqR@13 (|:&:| x@3276 x@3277) (|:&:| x@3278 x@3279)))))
(assert (forall ((x@3276 R) (x@3277 R) (x@3278 R) (x@3279 R)) (=> (diseqR@13 x@3277 x@3279) (diseqR@13 (|:&:| x@3276 x@3277) (|:&:| x@3278 x@3279)))))
(assert (forall ((x@3280 R) (x@3281 R) (x@3282 R) (x@3283 R)) (=> (diseqR@13 x@3280 x@3282) (diseqR@13 (|:>:| x@3280 x@3281) (|:>:| x@3282 x@3283)))))
(assert (forall ((x@3280 R) (x@3281 R) (x@3282 R) (x@3283 R)) (=> (diseqR@13 x@3281 x@3283) (diseqR@13 (|:>:| x@3280 x@3281) (|:>:| x@3282 x@3283)))))
(assert (forall ((x@3284 R) (x@3285 R)) (=> (diseqR@13 x@3284 x@3285) (diseqR@13 (Star x@3284) (Star x@3285)))))
(declare-fun x.>. (R R R) Bool)
(assert (forall ((x@3131 R) (x R) (y R) (x@3121 R) (x@3122 R) (x@3123 R) (x@3124 R)) (=> (and (= x@3131 (|:>:| x y)) (= x x@3121) (= y x@3122) (= x x@3123) (= y x@3124)) (x.>. x@3131 x y))))
(assert (forall ((x@3131 R) (x R) (y R) (x@3121 R) (x@3122 R) (x@3123 R)) (=> (and (= x@3131 x) (= x x@3121) (= y x@3122) (= x x@3123) (= y Eps)) (x.>. x@3131 x y))))
(assert (forall ((x@3131 R) (x R) (y R) (x@3121 R) (x@3122 R)) (=> (and (= x@3131 y) (= x x@3121) (= y x@3122) (= x Eps)) (x.>. x@3131 x y))))
(assert (forall ((x@3131 R) (x R) (y R) (x@3121 R)) (=> (and (= x@3131 Nil) (= x x@3121) (= y Nil)) (x.>. x@3131 x y))))
(assert (forall ((x@3131 R) (x R) (y R)) (=> (and (= x@3131 Nil) (= x Nil)) (x.>. x@3131 x y))))
(declare-fun x.+. (R R R) Bool)
(assert (forall ((x@3132 R) (x R) (y R) (x@3125 R) (x@3126 R)) (=> (and (= x@3132 (|:+:| x y)) (= x x@3125) (= y x@3126)) (x.+. x@3132 x y))))
(assert (forall ((x@3132 R) (x R) (y R) (x@3125 R)) (=> (and (= x@3132 x) (= x x@3125) (= y Nil)) (x.+. x@3132 x y))))
(assert (forall ((x@3132 R) (x R) (y R)) (=> (and (= x@3132 y) (= x Nil)) (x.+. x@3132 x y))))
(declare-fun eps (Bool R) Bool)
(assert (forall ((x@3133 Bool) (x R) (x@3127 R)) (=> (and (= x@3133 false) (= x x@3127)) (eps x@3133 x))))
(assert (forall ((x@3133 Bool) (x R)) (=> (and (= x@3133 true) (= x Eps)) (eps x@3133 x))))
(assert (forall ((x@3133 Bool) (x R) (p R) (q R) (x@3135 Bool) (x@3134 Bool)) (=> (and (= x@3133 (or x@3135 x@3134)) (= x (|:+:| p q)) (eps x@3135 p) (eps x@3134 q)) (eps x@3133 x))))
(assert (forall ((x@3133 Bool) (x R) (r R) (q2 R) (x@3137 Bool) (x@3136 Bool)) (=> (and (= x@3133 (and x@3137 x@3136)) (= x (|:&:| r q2)) (eps x@3137 r) (eps x@3136 q2)) (eps x@3133 x))))
(assert (forall ((x@3133 Bool) (x R) (p2 R) (q3 R) (x@3139 Bool) (x@3138 Bool)) (=> (and (= x@3133 (and x@3139 x@3138)) (= x (|:>:| p2 q3)) (eps x@3139 p2) (eps x@3138 q3)) (eps x@3133 x))))
(assert (forall ((x@3133 Bool) (x R) (y R)) (=> (and (= x@3133 true) (= x (Star y))) (eps x@3133 x))))
(declare-fun step (R R T) Bool)
(assert (forall ((x@3140 R) (x R) (y T) (x@3128 R)) (=> (and (= x@3140 Nil) (= x x@3128)) (step x@3140 x y))))
(assert (forall ((x@3140 R) (x R) (y T) (b T)) (=> (and (= x@3140 (ite (= b y) Eps Nil)) (= x (Atom b))) (step x@3140 x y))))
(assert (forall ((x@3140 R) (x R) (y T) (p R) (q R) (x@3143 R) (x@3142 R) (x@3141 R)) (=> (and (= x@3140 x@3143) (= x (|:+:| p q)) (x.+. x@3143 x@3142 x@3141) (step x@3142 p y) (step x@3141 q y)) (step x@3140 x y))))
(assert (forall ((x@3140 R) (x R) (y T) (r R) (q2 R) (wild1 R) (x@3144 R) (x@3129 R) (wild2 R) (x@3145 R) (x@3130 R)) (=> (and (= x@3140 (|:&:| wild1 wild2)) (= x (|:&:| r q2)) (= wild1 x@3144) (step x@3144 r y) (= wild1 x@3129) (= wild2 x@3145) (step x@3145 q2 y) (= wild2 x@3130)) (step x@3140 x y))))
(assert (forall ((x@3140 R) (x R) (y T) (r R) (q2 R) (wild1 R) (x@3144 R) (x@3129 R) (wild2 R) (x@3145 R)) (=> (and (= x@3140 Nil) (= x (|:&:| r q2)) (= wild1 x@3144) (step x@3144 r y) (= wild1 x@3129) (= wild2 x@3145) (step x@3145 q2 y) (= wild2 Nil)) (step x@3140 x y))))
(assert (forall ((x@3140 R) (x R) (y T) (r R) (q2 R) (wild1 R) (x@3144 R)) (=> (and (= x@3140 Nil) (= x (|:&:| r q2)) (= wild1 x@3144) (step x@3144 r y) (= wild1 Nil)) (step x@3140 x y))))
(assert (forall ((x@3140 R) (x R) (y T) (p2 R) (q3 R) (x@3146 Bool) (x@3150 R) (x@3149 R) (x@3148 R) (x@3147 R) (x@3153 R) (x@3152 R) (x@3151 R)) (=> (and (= x@3140 (ite x@3146 x@3150 x@3153)) (= x (|:>:| p2 q3)) (eps x@3146 p2) (x.+. x@3150 x@3149 x@3147) (x.>. x@3149 x@3148 q3) (step x@3148 p2 y) (step x@3147 q3 y) (x.+. x@3153 x@3152 Nil) (x.>. x@3152 x@3151 q3) (step x@3151 p2 y)) (step x@3140 x y))))
(assert (forall ((x@3140 R) (x R) (y T) (p3 R) (x@3155 R) (x@3154 R)) (=> (and (= x@3140 x@3155) (= x (Star p3)) (x.>. x@3155 x@3154 (Star p3)) (step x@3154 p3 y)) (step x@3140 x y))))
(declare-fun rec (Bool R list) Bool)
(assert (forall ((x@3156 Bool) (x R) (y list) (x@3157 Bool)) (=> (and (= x@3156 x@3157) (= y nil) (eps x@3157 x)) (rec x@3156 x y))))
(assert (forall ((x@3156 Bool) (x R) (y list) (z T) (xs list) (x@3159 Bool) (x@3158 R)) (=> (and (= x@3156 x@3159) (= y (cons z xs)) (rec x@3159 x@3158 xs) (step x@3158 x z)) (rec x@3156 x y))))
(assert (forall ((p R) (q R) (s list)) (forall ((x@3161 Bool) (x@3160 Bool)) (=> (and (diseqBool@0 x@3161 x@3160) (rec x@3161 (|:+:| p q) s) (rec x@3160 (|:>:| p q) s)) false))))
(check-sat)
(get-info :reason-unknown)
