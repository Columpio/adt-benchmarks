(set-logic ALL)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-sort Nat@0 0)
(declare-fun Z@0 () Nat@0)
(declare-fun S@0 (Nat@0) Nat@0)
(declare-fun diseqNat@0@91 (Nat@0 Nat@0) Bool)
(assert (forall ((x@5087 Nat@0)) (diseqNat@0@91 Z@0 (S@0 x@5087))))
(assert (forall ((x@5088 Nat@0)) (diseqNat@0@91 (S@0 x@5088) Z@0)))
(assert (forall ((x@5089 Nat@0) (x@5090 Nat@0)) (=> (diseqNat@0@91 x@5089 x@5090) (diseqNat@0@91 (S@0 x@5089) (S@0 x@5090)))))
(declare-sort T 0)
(declare-fun A () T)
(declare-fun B () T)
(declare-fun C () T)
(declare-fun diseqT@26 (T T) Bool)
(assert (diseqT@26 A B))
(assert (diseqT@26 A C))
(assert (diseqT@26 B A))
(assert (diseqT@26 C A))
(assert (diseqT@26 B C))
(assert (diseqT@26 C B))
(declare-sort list 0)
(declare-fun nil () list)
(declare-fun cons (T list) list)
(declare-fun diseqlist@82 (list list) Bool)
(assert (forall ((x@5091 T) (x@5092 list)) (diseqlist@82 nil (cons x@5091 x@5092))))
(assert (forall ((x@5093 T) (x@5094 list)) (diseqlist@82 (cons x@5093 x@5094) nil)))
(assert (forall ((x@5095 T) (x@5096 list) (x@5097 T) (x@5098 list)) (=> (diseqT@26 x@5095 x@5097) (diseqlist@82 (cons x@5095 x@5096) (cons x@5097 x@5098)))))
(assert (forall ((x@5095 T) (x@5096 list) (x@5097 T) (x@5098 list)) (=> (diseqlist@82 x@5096 x@5098) (diseqlist@82 (cons x@5095 x@5096) (cons x@5097 x@5098)))))
(declare-sort R 0)
(declare-fun Nil () R)
(declare-fun Eps () R)
(declare-fun Atom (T) R)
(declare-fun |:+:| (R R) R)
(declare-fun |:&:| (R R) R)
(declare-fun |:>:| (R R) R)
(declare-fun Star (R) R)
(declare-fun diseqR@26 (R R) Bool)
(assert (diseqR@26 Nil Eps))
(assert (forall ((x@5099 T)) (diseqR@26 Nil (Atom x@5099))))
(assert (forall ((x@5100 R) (x@5101 R)) (diseqR@26 Nil (|:+:| x@5100 x@5101))))
(assert (forall ((x@5102 R) (x@5103 R)) (diseqR@26 Nil (|:&:| x@5102 x@5103))))
(assert (forall ((x@5104 R) (x@5105 R)) (diseqR@26 Nil (|:>:| x@5104 x@5105))))
(assert (forall ((x@5106 R)) (diseqR@26 Nil (Star x@5106))))
(assert (diseqR@26 Eps Nil))
(assert (forall ((x@5107 T)) (diseqR@26 (Atom x@5107) Nil)))
(assert (forall ((x@5108 R) (x@5109 R)) (diseqR@26 (|:+:| x@5108 x@5109) Nil)))
(assert (forall ((x@5110 R) (x@5111 R)) (diseqR@26 (|:&:| x@5110 x@5111) Nil)))
(assert (forall ((x@5112 R) (x@5113 R)) (diseqR@26 (|:>:| x@5112 x@5113) Nil)))
(assert (forall ((x@5114 R)) (diseqR@26 (Star x@5114) Nil)))
(assert (forall ((x@5115 T)) (diseqR@26 Eps (Atom x@5115))))
(assert (forall ((x@5116 R) (x@5117 R)) (diseqR@26 Eps (|:+:| x@5116 x@5117))))
(assert (forall ((x@5118 R) (x@5119 R)) (diseqR@26 Eps (|:&:| x@5118 x@5119))))
(assert (forall ((x@5120 R) (x@5121 R)) (diseqR@26 Eps (|:>:| x@5120 x@5121))))
(assert (forall ((x@5122 R)) (diseqR@26 Eps (Star x@5122))))
(assert (forall ((x@5123 T)) (diseqR@26 (Atom x@5123) Eps)))
(assert (forall ((x@5124 R) (x@5125 R)) (diseqR@26 (|:+:| x@5124 x@5125) Eps)))
(assert (forall ((x@5126 R) (x@5127 R)) (diseqR@26 (|:&:| x@5126 x@5127) Eps)))
(assert (forall ((x@5128 R) (x@5129 R)) (diseqR@26 (|:>:| x@5128 x@5129) Eps)))
(assert (forall ((x@5130 R)) (diseqR@26 (Star x@5130) Eps)))
(assert (forall ((x@5131 T) (x@5132 R) (x@5133 R)) (diseqR@26 (Atom x@5131) (|:+:| x@5132 x@5133))))
(assert (forall ((x@5134 T) (x@5135 R) (x@5136 R)) (diseqR@26 (Atom x@5134) (|:&:| x@5135 x@5136))))
(assert (forall ((x@5137 T) (x@5138 R) (x@5139 R)) (diseqR@26 (Atom x@5137) (|:>:| x@5138 x@5139))))
(assert (forall ((x@5140 T) (x@5141 R)) (diseqR@26 (Atom x@5140) (Star x@5141))))
(assert (forall ((x@5142 R) (x@5143 R) (x@5144 T)) (diseqR@26 (|:+:| x@5142 x@5143) (Atom x@5144))))
(assert (forall ((x@5145 R) (x@5146 R) (x@5147 T)) (diseqR@26 (|:&:| x@5145 x@5146) (Atom x@5147))))
(assert (forall ((x@5148 R) (x@5149 R) (x@5150 T)) (diseqR@26 (|:>:| x@5148 x@5149) (Atom x@5150))))
(assert (forall ((x@5151 R) (x@5152 T)) (diseqR@26 (Star x@5151) (Atom x@5152))))
(assert (forall ((x@5153 R) (x@5154 R) (x@5155 R) (x@5156 R)) (diseqR@26 (|:+:| x@5153 x@5154) (|:&:| x@5155 x@5156))))
(assert (forall ((x@5157 R) (x@5158 R) (x@5159 R) (x@5160 R)) (diseqR@26 (|:+:| x@5157 x@5158) (|:>:| x@5159 x@5160))))
(assert (forall ((x@5161 R) (x@5162 R) (x@5163 R)) (diseqR@26 (|:+:| x@5161 x@5162) (Star x@5163))))
(assert (forall ((x@5164 R) (x@5165 R) (x@5166 R) (x@5167 R)) (diseqR@26 (|:&:| x@5164 x@5165) (|:+:| x@5166 x@5167))))
(assert (forall ((x@5168 R) (x@5169 R) (x@5170 R) (x@5171 R)) (diseqR@26 (|:>:| x@5168 x@5169) (|:+:| x@5170 x@5171))))
(assert (forall ((x@5172 R) (x@5173 R) (x@5174 R)) (diseqR@26 (Star x@5172) (|:+:| x@5173 x@5174))))
(assert (forall ((x@5175 R) (x@5176 R) (x@5177 R) (x@5178 R)) (diseqR@26 (|:&:| x@5175 x@5176) (|:>:| x@5177 x@5178))))
(assert (forall ((x@5179 R) (x@5180 R) (x@5181 R)) (diseqR@26 (|:&:| x@5179 x@5180) (Star x@5181))))
(assert (forall ((x@5182 R) (x@5183 R) (x@5184 R) (x@5185 R)) (diseqR@26 (|:>:| x@5182 x@5183) (|:&:| x@5184 x@5185))))
(assert (forall ((x@5186 R) (x@5187 R) (x@5188 R)) (diseqR@26 (Star x@5186) (|:&:| x@5187 x@5188))))
(assert (forall ((x@5189 R) (x@5190 R) (x@5191 R)) (diseqR@26 (|:>:| x@5189 x@5190) (Star x@5191))))
(assert (forall ((x@5192 R) (x@5193 R) (x@5194 R)) (diseqR@26 (Star x@5192) (|:>:| x@5193 x@5194))))
(assert (forall ((x@5195 T) (x@5196 T)) (=> (diseqT@26 x@5195 x@5196) (diseqR@26 (Atom x@5195) (Atom x@5196)))))
(assert (forall ((x@5197 R) (x@5198 R) (x@5199 R) (x@5200 R)) (=> (diseqR@26 x@5197 x@5199) (diseqR@26 (|:+:| x@5197 x@5198) (|:+:| x@5199 x@5200)))))
(assert (forall ((x@5197 R) (x@5198 R) (x@5199 R) (x@5200 R)) (=> (diseqR@26 x@5198 x@5200) (diseqR@26 (|:+:| x@5197 x@5198) (|:+:| x@5199 x@5200)))))
(assert (forall ((x@5201 R) (x@5202 R) (x@5203 R) (x@5204 R)) (=> (diseqR@26 x@5201 x@5203) (diseqR@26 (|:&:| x@5201 x@5202) (|:&:| x@5203 x@5204)))))
(assert (forall ((x@5201 R) (x@5202 R) (x@5203 R) (x@5204 R)) (=> (diseqR@26 x@5202 x@5204) (diseqR@26 (|:&:| x@5201 x@5202) (|:&:| x@5203 x@5204)))))
(assert (forall ((x@5205 R) (x@5206 R) (x@5207 R) (x@5208 R)) (=> (diseqR@26 x@5205 x@5207) (diseqR@26 (|:>:| x@5205 x@5206) (|:>:| x@5207 x@5208)))))
(assert (forall ((x@5205 R) (x@5206 R) (x@5207 R) (x@5208 R)) (=> (diseqR@26 x@5206 x@5208) (diseqR@26 (|:>:| x@5205 x@5206) (|:>:| x@5207 x@5208)))))
(assert (forall ((x@5209 R) (x@5210 R)) (=> (diseqR@26 x@5209 x@5210) (diseqR@26 (Star x@5209) (Star x@5210)))))
(declare-fun x.>. (R R R) Bool)
(assert (forall ((x@5056 R) (x R) (y R) (x@5046 R) (x@5047 R) (x@5048 R) (x@5049 R)) (=> (and (= x@5056 (|:>:| x y)) (= x x@5046) (= y x@5047) (= x x@5048) (= y x@5049)) (x.>. x@5056 x y))))
(assert (forall ((x@5056 R) (x R) (y R) (x@5046 R) (x@5047 R) (x@5048 R)) (=> (and (= x@5056 x) (= x x@5046) (= y x@5047) (= x x@5048) (= y Eps)) (x.>. x@5056 x y))))
(assert (forall ((x@5056 R) (x R) (y R) (x@5046 R) (x@5047 R)) (=> (and (= x@5056 y) (= x x@5046) (= y x@5047) (= x Eps)) (x.>. x@5056 x y))))
(assert (forall ((x@5056 R) (x R) (y R) (x@5046 R)) (=> (and (= x@5056 Nil) (= x x@5046) (= y Nil)) (x.>. x@5056 x y))))
(assert (forall ((x@5056 R) (x R) (y R)) (=> (and (= x@5056 Nil) (= x Nil)) (x.>. x@5056 x y))))
(declare-fun x.+. (R R R) Bool)
(assert (forall ((x@5057 R) (x R) (y R) (x@5050 R) (x@5051 R)) (=> (and (= x@5057 (|:+:| x y)) (= x x@5050) (= y x@5051)) (x.+. x@5057 x y))))
(assert (forall ((x@5057 R) (x R) (y R) (x@5050 R)) (=> (and (= x@5057 x) (= x x@5050) (= y Nil)) (x.+. x@5057 x y))))
(assert (forall ((x@5057 R) (x R) (y R)) (=> (and (= x@5057 y) (= x Nil)) (x.+. x@5057 x y))))
(declare-fun eps (Bool R) Bool)
(assert (forall ((x@5058 Bool) (x R) (x@5052 R)) (=> (and (= x@5058 false) (= x x@5052)) (eps x@5058 x))))
(assert (forall ((x@5058 Bool) (x R)) (=> (and (= x@5058 true) (= x Eps)) (eps x@5058 x))))
(assert (forall ((x@5058 Bool) (x R) (p R) (q R) (x@5060 Bool) (x@5059 Bool)) (=> (and (= x@5058 (or x@5060 x@5059)) (= x (|:+:| p q)) (eps x@5060 p) (eps x@5059 q)) (eps x@5058 x))))
(assert (forall ((x@5058 Bool) (x R) (r R) (q2 R) (x@5062 Bool) (x@5061 Bool)) (=> (and (= x@5058 (and x@5062 x@5061)) (= x (|:&:| r q2)) (eps x@5062 r) (eps x@5061 q2)) (eps x@5058 x))))
(assert (forall ((x@5058 Bool) (x R) (p2 R) (q3 R) (x@5064 Bool) (x@5063 Bool)) (=> (and (= x@5058 (and x@5064 x@5063)) (= x (|:>:| p2 q3)) (eps x@5064 p2) (eps x@5063 q3)) (eps x@5058 x))))
(assert (forall ((x@5058 Bool) (x R) (y R)) (=> (and (= x@5058 true) (= x (Star y))) (eps x@5058 x))))
(declare-fun step (R R T) Bool)
(assert (forall ((x@5065 R) (x R) (y T) (x@5053 R)) (=> (and (= x@5065 Nil) (= x x@5053)) (step x@5065 x y))))
(assert (forall ((x@5065 R) (x R) (y T) (b T)) (=> (and (= x@5065 (ite (= b y) Eps Nil)) (= x (Atom b))) (step x@5065 x y))))
(assert (forall ((x@5065 R) (x R) (y T) (p R) (q R) (x@5068 R) (x@5067 R) (x@5066 R)) (=> (and (= x@5065 x@5068) (= x (|:+:| p q)) (x.+. x@5068 x@5067 x@5066) (step x@5067 p y) (step x@5066 q y)) (step x@5065 x y))))
(assert (forall ((x@5065 R) (x R) (y T) (r R) (q2 R) (wild1 R) (x@5069 R) (x@5054 R) (wild2 R) (x@5070 R) (x@5055 R)) (=> (and (= x@5065 (|:&:| wild1 wild2)) (= x (|:&:| r q2)) (= wild1 x@5069) (step x@5069 r y) (= wild1 x@5054) (= wild2 x@5070) (step x@5070 q2 y) (= wild2 x@5055)) (step x@5065 x y))))
(assert (forall ((x@5065 R) (x R) (y T) (r R) (q2 R) (wild1 R) (x@5069 R) (x@5054 R) (wild2 R) (x@5070 R)) (=> (and (= x@5065 Nil) (= x (|:&:| r q2)) (= wild1 x@5069) (step x@5069 r y) (= wild1 x@5054) (= wild2 x@5070) (step x@5070 q2 y) (= wild2 Nil)) (step x@5065 x y))))
(assert (forall ((x@5065 R) (x R) (y T) (r R) (q2 R) (wild1 R) (x@5069 R)) (=> (and (= x@5065 Nil) (= x (|:&:| r q2)) (= wild1 x@5069) (step x@5069 r y) (= wild1 Nil)) (step x@5065 x y))))
(assert (forall ((x@5065 R) (x R) (y T) (p2 R) (q3 R) (x@5071 Bool) (x@5075 R) (x@5074 R) (x@5073 R) (x@5072 R) (x@5078 R) (x@5077 R) (x@5076 R)) (=> (and (= x@5065 (ite x@5071 x@5075 x@5078)) (= x (|:>:| p2 q3)) (eps x@5071 p2) (x.+. x@5075 x@5074 x@5072) (x.>. x@5074 x@5073 q3) (step x@5073 p2 y) (step x@5072 q3 y) (x.+. x@5078 x@5077 Nil) (x.>. x@5077 x@5076 q3) (step x@5076 p2 y)) (step x@5065 x y))))
(assert (forall ((x@5065 R) (x R) (y T) (p3 R) (x@5080 R) (x@5079 R)) (=> (and (= x@5065 x@5080) (= x (Star p3)) (x.>. x@5080 x@5079 (Star p3)) (step x@5079 p3 y)) (step x@5065 x y))))
(declare-fun rec (Bool R list) Bool)
(assert (forall ((x@5081 Bool) (x R) (y list) (x@5082 Bool)) (=> (and (= x@5081 x@5082) (= y nil) (eps x@5082 x)) (rec x@5081 x y))))
(assert (forall ((x@5081 Bool) (x R) (y list) (z T) (xs list) (x@5084 Bool) (x@5083 R)) (=> (and (= x@5081 x@5084) (= y (cons z xs)) (rec x@5084 x@5083 xs) (step x@5083 x z)) (rec x@5081 x y))))
(assert (forall ((p R) (q R) (s list)) (forall ((x@5086 Bool) (x@5085 Bool)) (=> (and (diseqBool@0 x@5086 x@5085) (rec x@5086 (|:>:| p q) s) (rec x@5085 (|:>:| q p) s)) false))))
(check-sat)
(get-info :reason-unknown)
