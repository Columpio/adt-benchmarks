(set-logic ALL)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-sort Nat@0 0)
(declare-fun Z@0 () Nat@0)
(declare-fun S@0 (Nat@0) Nat@0)
(declare-fun diseqNat@0@315 (Nat@0 Nat@0) Bool)
(assert (forall ((x@15189 Nat@0)) (diseqNat@0@315 Z@0 (S@0 x@15189))))
(assert (forall ((x@15190 Nat@0)) (diseqNat@0@315 (S@0 x@15190) Z@0)))
(assert (forall ((x@15191 Nat@0) (x@15192 Nat@0)) (=> (diseqNat@0@315 x@15191 x@15192) (diseqNat@0@315 (S@0 x@15191) (S@0 x@15192)))))
(declare-sort list 0)
(declare-fun nil () list)
(declare-fun cons (Nat@0 list) list)
(declare-fun diseqlist@192 (list list) Bool)
(assert (forall ((x@15193 Nat@0) (x@15194 list)) (diseqlist@192 nil (cons x@15193 x@15194))))
(assert (forall ((x@15195 Nat@0) (x@15196 list)) (diseqlist@192 (cons x@15195 x@15196) nil)))
(assert (forall ((x@15197 Nat@0) (x@15198 list) (x@15199 Nat@0) (x@15200 list)) (=> (diseqNat@0@315 x@15197 x@15199) (diseqlist@192 (cons x@15197 x@15198) (cons x@15199 x@15200)))))
(assert (forall ((x@15197 Nat@0) (x@15198 list) (x@15199 Nat@0) (x@15200 list)) (=> (diseqlist@192 x@15198 x@15200) (diseqlist@192 (cons x@15197 x@15198) (cons x@15199 x@15200)))))
(declare-sort Nat 0)
(declare-fun Z () Nat)
(declare-fun S (Nat) Nat)
(declare-fun diseqNat@222 (Nat Nat) Bool)
(assert (forall ((x@15201 Nat)) (diseqNat@222 Z (S x@15201))))
(assert (forall ((x@15202 Nat)) (diseqNat@222 (S x@15202) Z)))
(assert (forall ((x@15203 Nat) (x@15204 Nat)) (=> (diseqNat@222 x@15203 x@15204) (diseqNat@222 (S x@15203) (S x@15204)))))
(declare-fun take (list Nat list) Bool)
(assert (forall ((x@15170 list) (x Nat) (y list)) (=> (and (= x@15170 nil) (= x Z)) (take x@15170 x y))))
(assert (forall ((x@15170 list) (x Nat) (y list) (z Nat)) (=> (and (= x@15170 nil) (= x (S z)) (= y nil)) (take x@15170 x y))))
(assert (forall ((x@15170 list) (x Nat) (y list) (z Nat) (x2 Nat@0) (x3 list) (x@15171 list)) (=> (and (= x@15170 (cons x2 x@15171)) (= x (S z)) (= y (cons x2 x3)) (take x@15171 z x3)) (take x@15170 x y))))
(declare-fun len (Nat list) Bool)
(assert (forall ((x@15172 Nat) (x list)) (=> (and (= x@15172 Z) (= x nil)) (len x@15172 x))))
(assert (forall ((x@15172 Nat) (x list) (y Nat@0) (xs list) (x@15173 Nat)) (=> (and (= x@15172 (S x@15173)) (= x (cons y xs)) (len x@15173 xs)) (len x@15172 x))))
(declare-fun drop (list Nat list) Bool)
(assert (forall ((x@15174 list) (x Nat) (y list)) (=> (and (= x@15174 y) (= x Z)) (drop x@15174 x y))))
(assert (forall ((x@15174 list) (x Nat) (y list) (z Nat)) (=> (and (= x@15174 nil) (= x (S z)) (= y nil)) (drop x@15174 x y))))
(assert (forall ((x@15174 list) (x Nat) (y list) (z Nat) (x2 Nat@0) (x3 list) (x@15175 list)) (=> (and (= x@15174 x@15175) (= x (S z)) (= y (cons x2 x3)) (drop x@15175 z x3)) (drop x@15174 x y))))
(declare-fun |-2| (Nat Nat Nat) Bool)
(assert (forall ((x@15176 Nat) (x Nat) (y Nat)) (=> (and (= x@15176 Z) (= x Z)) (|-2| x@15176 x y))))
(assert (forall ((x@15176 Nat) (x Nat) (y Nat) (z Nat)) (=> (and (= x@15176 (S z)) (= x (S z)) (= y Z)) (|-2| x@15176 x y))))
(assert (forall ((x@15176 Nat) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@15177 Nat)) (=> (and (= x@15176 x@15177) (= x (S z)) (= y (S x2)) (|-2| x@15177 z x2)) (|-2| x@15176 x y))))
(declare-fun ++ (list list list) Bool)
(assert (forall ((x@15178 list) (x list) (y list)) (=> (and (= x@15178 y) (= x nil)) (++ x@15178 x y))))
(assert (forall ((x@15178 list) (x list) (y list) (z Nat@0) (xs list) (x@15179 list)) (=> (and (= x@15178 (cons z x@15179)) (= x (cons z xs)) (++ x@15179 xs y)) (++ x@15178 x y))))
(declare-fun rev (list list) Bool)
(assert (forall ((x@15180 list) (x list)) (=> (and (= x@15180 nil) (= x nil)) (rev x@15180 x))))
(assert (forall ((x@15180 list) (x list) (y Nat@0) (xs list) (x@15182 list) (x@15181 list)) (=> (and (= x@15180 x@15182) (= x (cons y xs)) (++ x@15182 x@15181 (cons y nil)) (rev x@15181 xs)) (rev x@15180 x))))
(assert (forall ((i Nat) (xs list)) (forall ((x@15188 list) (x@15187 list) (x@15186 list) (x@15185 Nat) (x@15184 Nat) (x@15183 list)) (=> (and (diseqlist@192 x@15188 x@15186) (rev x@15188 x@15187) (take x@15187 i xs) (drop x@15186 x@15185 x@15183) (|-2| x@15185 x@15184 i) (len x@15184 xs) (rev x@15183 xs)) false))))
(check-sat)
(get-info :reason-unknown)
