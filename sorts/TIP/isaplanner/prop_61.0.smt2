(set-logic ALL)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-sort Nat@0 0)
(declare-fun Z@0 () Nat@0)
(declare-fun S@0 (Nat@0) Nat@0)
(declare-fun diseqNat@0@297 (Nat@0 Nat@0) Bool)
(assert (forall ((x@14661 Nat@0)) (diseqNat@0@297 Z@0 (S@0 x@14661))))
(assert (forall ((x@14662 Nat@0)) (diseqNat@0@297 (S@0 x@14662) Z@0)))
(assert (forall ((x@14663 Nat@0) (x@14664 Nat@0)) (=> (diseqNat@0@297 x@14663 x@14664) (diseqNat@0@297 (S@0 x@14663) (S@0 x@14664)))))
(declare-sort Nat 0)
(declare-fun Z () Nat)
(declare-fun S (Nat) Nat)
(declare-fun diseqNat@207 (Nat Nat) Bool)
(assert (forall ((x@14665 Nat)) (diseqNat@207 Z (S x@14665))))
(assert (forall ((x@14666 Nat)) (diseqNat@207 (S x@14666) Z)))
(assert (forall ((x@14667 Nat) (x@14668 Nat)) (=> (diseqNat@207 x@14667 x@14668) (diseqNat@207 (S x@14667) (S x@14668)))))
(declare-sort list 0)
(declare-fun nil () list)
(declare-fun cons (Nat list) list)
(declare-fun diseqlist@177 (list list) Bool)
(assert (forall ((x@14669 Nat) (x@14670 list)) (diseqlist@177 nil (cons x@14669 x@14670))))
(assert (forall ((x@14671 Nat) (x@14672 list)) (diseqlist@177 (cons x@14671 x@14672) nil)))
(assert (forall ((x@14673 Nat) (x@14674 list) (x@14675 Nat) (x@14676 list)) (=> (diseqNat@207 x@14673 x@14675) (diseqlist@177 (cons x@14673 x@14674) (cons x@14675 x@14676)))))
(assert (forall ((x@14673 Nat) (x@14674 list) (x@14675 Nat) (x@14676 list)) (=> (diseqlist@177 x@14674 x@14676) (diseqlist@177 (cons x@14673 x@14674) (cons x@14675 x@14676)))))
(declare-fun last (Nat list) Bool)
(assert (forall ((x@14651 Nat) (x list)) (=> (and (= x@14651 Z) (= x nil)) (last x@14651 x))))
(assert (forall ((x@14651 Nat) (x list) (y Nat) (z list)) (=> (and (= x@14651 y) (= x (cons y z)) (= z nil)) (last x@14651 x))))
(assert (forall ((x@14651 Nat) (x list) (y Nat) (z list) (x2 Nat) (x3 list) (x@14652 Nat)) (=> (and (= x@14651 x@14652) (= x (cons y z)) (= z (cons x2 x3)) (last x@14652 (cons x2 x3))) (last x@14651 x))))
(declare-fun lastOfTwo (Nat list list) Bool)
(assert (forall ((x@14653 Nat) (x list) (y list) (x@14654 Nat)) (=> (and (= x@14653 x@14654) (= y nil) (last x@14654 x)) (lastOfTwo x@14653 x y))))
(assert (forall ((x@14653 Nat) (x list) (y list) (z Nat) (x2 list) (x@14655 Nat)) (=> (and (= x@14653 x@14655) (= y (cons z x2)) (last x@14655 (cons z x2))) (lastOfTwo x@14653 x y))))
(declare-fun ++ (list list list) Bool)
(assert (forall ((x@14656 list) (x list) (y list)) (=> (and (= x@14656 y) (= x nil)) (++ x@14656 x y))))
(assert (forall ((x@14656 list) (x list) (y list) (z Nat) (xs list) (x@14657 list)) (=> (and (= x@14656 (cons z x@14657)) (= x (cons z xs)) (++ x@14657 xs y)) (++ x@14656 x y))))
(assert (forall ((xs list) (ys list)) (forall ((x@14660 Nat) (x@14659 list) (x@14658 Nat)) (=> (and (diseqNat@207 x@14660 x@14658) (last x@14660 x@14659) (++ x@14659 xs ys) (lastOfTwo x@14658 xs ys)) false))))
(check-sat)
(get-info :reason-unknown)
