(set-logic ALL)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-sort Nat@0 0)
(declare-fun Z@0 () Nat@0)
(declare-fun S@0 (Nat@0) Nat@0)
(declare-fun diseqNat@0@354 (Nat@0 Nat@0) Bool)
(assert (forall ((x@16085 Nat@0)) (diseqNat@0@354 Z@0 (S@0 x@16085))))
(assert (forall ((x@16086 Nat@0)) (diseqNat@0@354 (S@0 x@16086) Z@0)))
(assert (forall ((x@16087 Nat@0) (x@16088 Nat@0)) (=> (diseqNat@0@354 x@16087 x@16088) (diseqNat@0@354 (S@0 x@16087) (S@0 x@16088)))))
(declare-sort list 0)
(declare-fun nil () list)
(declare-fun cons (Nat@0 list) list)
(declare-fun diseqlist@219 (list list) Bool)
(assert (forall ((x@16089 Nat@0) (x@16090 list)) (diseqlist@219 nil (cons x@16089 x@16090))))
(assert (forall ((x@16091 Nat@0) (x@16092 list)) (diseqlist@219 (cons x@16091 x@16092) nil)))
(assert (forall ((x@16093 Nat@0) (x@16094 list) (x@16095 Nat@0) (x@16096 list)) (=> (diseqNat@0@354 x@16093 x@16095) (diseqlist@219 (cons x@16093 x@16094) (cons x@16095 x@16096)))))
(assert (forall ((x@16093 Nat@0) (x@16094 list) (x@16095 Nat@0) (x@16096 list)) (=> (diseqlist@219 x@16094 x@16096) (diseqlist@219 (cons x@16093 x@16094) (cons x@16095 x@16096)))))
(declare-sort Nat 0)
(declare-fun Z () Nat)
(declare-fun S (Nat) Nat)
(declare-fun diseqNat@260 (Nat Nat) Bool)
(assert (forall ((x@16097 Nat)) (diseqNat@260 Z (S x@16097))))
(assert (forall ((x@16098 Nat)) (diseqNat@260 (S x@16098) Z)))
(assert (forall ((x@16099 Nat) (x@16100 Nat)) (=> (diseqNat@260 x@16099 x@16100) (diseqNat@260 (S x@16099) (S x@16100)))))
(declare-fun len (Nat list) Bool)
(assert (forall ((x@16070 Nat) (x list)) (=> (and (= x@16070 Z) (= x nil)) (len x@16070 x))))
(assert (forall ((x@16070 Nat) (x list) (y Nat@0) (xs list) (x@16071 Nat)) (=> (and (= x@16070 (S x@16071)) (= x (cons y xs)) (len x@16071 xs)) (len x@16070 x))))
(declare-fun drop (list Nat list) Bool)
(assert (forall ((x@16072 list) (x Nat) (y list)) (=> (and (= x@16072 y) (= x Z)) (drop x@16072 x y))))
(assert (forall ((x@16072 list) (x Nat) (y list) (z Nat)) (=> (and (= x@16072 nil) (= x (S z)) (= y nil)) (drop x@16072 x y))))
(assert (forall ((x@16072 list) (x Nat) (y list) (z Nat) (x2 Nat@0) (x3 list) (x@16073 list)) (=> (and (= x@16072 x@16073) (= x (S z)) (= y (cons x2 x3)) (drop x@16073 z x3)) (drop x@16072 x y))))
(declare-fun |-2| (Nat Nat Nat) Bool)
(assert (forall ((x@16074 Nat) (x Nat) (y Nat)) (=> (and (= x@16074 Z) (= x Z)) (|-2| x@16074 x y))))
(assert (forall ((x@16074 Nat) (x Nat) (y Nat) (z Nat)) (=> (and (= x@16074 (S z)) (= x (S z)) (= y Z)) (|-2| x@16074 x y))))
(assert (forall ((x@16074 Nat) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@16075 Nat)) (=> (and (= x@16074 x@16075) (= x (S z)) (= y (S x2)) (|-2| x@16075 z x2)) (|-2| x@16074 x y))))
(declare-fun ++ (list list list) Bool)
(assert (forall ((x@16076 list) (x list) (y list)) (=> (and (= x@16076 y) (= x nil)) (++ x@16076 x y))))
(assert (forall ((x@16076 list) (x list) (y list) (z Nat@0) (xs list) (x@16077 list)) (=> (and (= x@16076 (cons z x@16077)) (= x (cons z xs)) (++ x@16077 xs y)) (++ x@16076 x y))))
(assert (forall ((n Nat) (xs list) (ys list)) (forall ((x@16084 list) (x@16083 list) (x@16082 list) (x@16081 list) (x@16080 list) (x@16079 Nat) (x@16078 Nat)) (=> (and (diseqlist@219 x@16084 x@16082) (drop x@16084 n x@16083) (++ x@16083 xs ys) (++ x@16082 x@16081 x@16080) (drop x@16081 n xs) (drop x@16080 x@16079 ys) (|-2| x@16079 n x@16078) (len x@16078 xs)) false))))
(check-sat)
(get-info :reason-unknown)
