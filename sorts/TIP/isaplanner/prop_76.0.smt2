(set-logic ALL)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-sort Nat@0 0)
(declare-fun Z@0 () Nat@0)
(declare-fun S@0 (Nat@0) Nat@0)
(declare-fun diseqNat@0@359 (Nat@0 Nat@0) Bool)
(assert (forall ((x@16194 Nat@0)) (diseqNat@0@359 Z@0 (S@0 x@16194))))
(assert (forall ((x@16195 Nat@0)) (diseqNat@0@359 (S@0 x@16195) Z@0)))
(assert (forall ((x@16196 Nat@0) (x@16197 Nat@0)) (=> (diseqNat@0@359 x@16196 x@16197) (diseqNat@0@359 (S@0 x@16196) (S@0 x@16197)))))
(declare-sort Nat 0)
(declare-fun Z () Nat)
(declare-fun S (Nat) Nat)
(declare-fun diseqNat@265 (Nat Nat) Bool)
(assert (forall ((x@16198 Nat)) (diseqNat@265 Z (S x@16198))))
(assert (forall ((x@16199 Nat)) (diseqNat@265 (S x@16199) Z)))
(assert (forall ((x@16200 Nat) (x@16201 Nat)) (=> (diseqNat@265 x@16200 x@16201) (diseqNat@265 (S x@16200) (S x@16201)))))
(declare-sort list 0)
(declare-fun nil () list)
(declare-fun cons (Nat list) list)
(declare-fun diseqlist@223 (list list) Bool)
(assert (forall ((x@16202 Nat) (x@16203 list)) (diseqlist@223 nil (cons x@16202 x@16203))))
(assert (forall ((x@16204 Nat) (x@16205 list)) (diseqlist@223 (cons x@16204 x@16205) nil)))
(assert (forall ((x@16206 Nat) (x@16207 list) (x@16208 Nat) (x@16209 list)) (=> (diseqNat@265 x@16206 x@16208) (diseqlist@223 (cons x@16206 x@16207) (cons x@16208 x@16209)))))
(assert (forall ((x@16206 Nat) (x@16207 list) (x@16208 Nat) (x@16209 list)) (=> (diseqlist@223 x@16207 x@16209) (diseqlist@223 (cons x@16206 x@16207) (cons x@16208 x@16209)))))
(declare-fun == (Bool Nat Nat) Bool)
(assert (forall ((x@16182 Bool) (x Nat) (y Nat)) (=> (and (= x@16182 true) (= x Z) (= y Z)) (== x@16182 x y))))
(assert (forall ((x@16182 Bool) (x Nat) (y Nat) (z Nat)) (=> (and (= x@16182 false) (= x Z) (= y (S z))) (== x@16182 x y))))
(assert (forall ((x@16182 Bool) (x Nat) (y Nat) (x2 Nat)) (=> (and (= x@16182 false) (= x (S x2)) (= y Z)) (== x@16182 x y))))
(assert (forall ((x@16182 Bool) (x Nat) (y Nat) (x2 Nat) (y2 Nat) (x@16183 Bool)) (=> (and (= x@16182 x@16183) (= x (S x2)) (= y (S y2)) (== x@16183 x2 y2)) (== x@16182 x y))))
(declare-fun count (Nat Nat list) Bool)
(assert (forall ((x@16184 Nat) (x Nat) (y list)) (=> (and (= x@16184 Z) (= y nil)) (count x@16184 x y))))
(assert (forall ((x@16184 Nat) (x Nat) (y list) (z Nat) (ys list) (x@16185 Bool) (x@16186 Nat) (x@16187 Nat)) (=> (and (= x@16184 (ite x@16185 (S x@16186) x@16187)) (= y (cons z ys)) (== x@16185 x z) (count x@16186 x ys) (count x@16187 x ys)) (count x@16184 x y))))
(declare-fun ++ (list list list) Bool)
(assert (forall ((x@16188 list) (x list) (y list)) (=> (and (= x@16188 y) (= x nil)) (++ x@16188 x y))))
(assert (forall ((x@16188 list) (x list) (y list) (z Nat) (xs list) (x@16189 list)) (=> (and (= x@16188 (cons z x@16189)) (= x (cons z xs)) (++ x@16189 xs y)) (++ x@16188 x y))))
(assert (forall ((n Nat) (m Nat) (xs list)) (forall ((x@16190 Bool) (x@16193 Nat) (x@16192 list) (x@16191 Nat)) (=> (and (diseqNat@265 x@16193 x@16191) (not x@16190) (== x@16190 n m) (count x@16193 n x@16192) (++ x@16192 xs (cons m nil)) (count x@16191 n xs)) false))))
(check-sat)
(get-info :reason-unknown)
