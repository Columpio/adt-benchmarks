(set-logic ALL)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-sort Nat@0 0)
(declare-fun Z@0 () Nat@0)
(declare-fun S@0 (Nat@0) Nat@0)
(declare-fun diseqNat@0@341 (Nat@0 Nat@0) Bool)
(assert (forall ((x@15775 Nat@0)) (diseqNat@0@341 Z@0 (S@0 x@15775))))
(assert (forall ((x@15776 Nat@0)) (diseqNat@0@341 (S@0 x@15776) Z@0)))
(assert (forall ((x@15777 Nat@0) (x@15778 Nat@0)) (=> (diseqNat@0@341 x@15777 x@15778) (diseqNat@0@341 (S@0 x@15777) (S@0 x@15778)))))
(declare-sort Nat 0)
(declare-fun Z () Nat)
(declare-fun S (Nat) Nat)
(declare-fun diseqNat@247 (Nat Nat) Bool)
(assert (forall ((x@15779 Nat)) (diseqNat@247 Z (S x@15779))))
(assert (forall ((x@15780 Nat)) (diseqNat@247 (S x@15780) Z)))
(assert (forall ((x@15781 Nat) (x@15782 Nat)) (=> (diseqNat@247 x@15781 x@15782) (diseqNat@247 (S x@15781) (S x@15782)))))
(declare-fun |-2| (Nat Nat Nat) Bool)
(assert (forall ((x@15769 Nat) (x Nat) (y Nat)) (=> (and (= x@15769 Z) (= x Z)) (|-2| x@15769 x y))))
(assert (forall ((x@15769 Nat) (x Nat) (y Nat) (z Nat)) (=> (and (= x@15769 (S z)) (= x (S z)) (= y Z)) (|-2| x@15769 x y))))
(assert (forall ((x@15769 Nat) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@15770 Nat)) (=> (and (= x@15769 x@15770) (= x (S z)) (= y (S x2)) (|-2| x@15770 z x2)) (|-2| x@15769 x y))))
(declare-fun +2 (Nat Nat Nat) Bool)
(assert (forall ((x@15771 Nat) (x Nat) (y Nat)) (=> (and (= x@15771 y) (= x Z)) (+2 x@15771 x y))))
(assert (forall ((x@15771 Nat) (x Nat) (y Nat) (z Nat) (x@15772 Nat)) (=> (and (= x@15771 (S x@15772)) (= x (S z)) (+2 x@15772 z y)) (+2 x@15771 x y))))
(assert (forall ((n Nat) (m Nat)) (forall ((x@15774 Nat) (x@15773 Nat)) (=> (and (diseqNat@247 x@15774 m) (|-2| x@15774 x@15773 n) (+2 x@15773 n m)) false))))
(check-sat)
(get-info :reason-unknown)
