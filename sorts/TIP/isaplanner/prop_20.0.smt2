(set-logic ALL)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-sort Nat@0 0)
(declare-fun Z@0 () Nat@0)
(declare-fun S@0 (Nat@0) Nat@0)
(declare-fun diseqNat@0@344 (Nat@0 Nat@0) Bool)
(assert (forall ((x@15832 Nat@0)) (diseqNat@0@344 Z@0 (S@0 x@15832))))
(assert (forall ((x@15833 Nat@0)) (diseqNat@0@344 (S@0 x@15833) Z@0)))
(assert (forall ((x@15834 Nat@0) (x@15835 Nat@0)) (=> (diseqNat@0@344 x@15834 x@15835) (diseqNat@0@344 (S@0 x@15834) (S@0 x@15835)))))
(declare-sort Nat 0)
(declare-fun Z () Nat)
(declare-fun S (Nat) Nat)
(declare-fun diseqNat@250 (Nat Nat) Bool)
(assert (forall ((x@15836 Nat)) (diseqNat@250 Z (S x@15836))))
(assert (forall ((x@15837 Nat)) (diseqNat@250 (S x@15837) Z)))
(assert (forall ((x@15838 Nat) (x@15839 Nat)) (=> (diseqNat@250 x@15838 x@15839) (diseqNat@250 (S x@15838) (S x@15839)))))
(declare-sort list 0)
(declare-fun nil () list)
(declare-fun cons (Nat list) list)
(declare-fun diseqlist@211 (list list) Bool)
(assert (forall ((x@15840 Nat) (x@15841 list)) (diseqlist@211 nil (cons x@15840 x@15841))))
(assert (forall ((x@15842 Nat) (x@15843 list)) (diseqlist@211 (cons x@15842 x@15843) nil)))
(assert (forall ((x@15844 Nat) (x@15845 list) (x@15846 Nat) (x@15847 list)) (=> (diseqNat@250 x@15844 x@15846) (diseqlist@211 (cons x@15844 x@15845) (cons x@15846 x@15847)))))
(assert (forall ((x@15844 Nat) (x@15845 list) (x@15846 Nat) (x@15847 list)) (=> (diseqlist@211 x@15845 x@15847) (diseqlist@211 (cons x@15844 x@15845) (cons x@15846 x@15847)))))
(declare-fun len (Nat list) Bool)
(assert (forall ((x@15819 Nat) (x list)) (=> (and (= x@15819 Z) (= x nil)) (len x@15819 x))))
(assert (forall ((x@15819 Nat) (x list) (y Nat) (xs list) (x@15820 Nat)) (=> (and (= x@15819 (S x@15820)) (= x (cons y xs)) (len x@15820 xs)) (len x@15819 x))))
(declare-fun <=2 (Bool Nat Nat) Bool)
(assert (forall ((x@15821 Bool) (x Nat) (y Nat)) (=> (and (= x@15821 true) (= x Z)) (<=2 x@15821 x y))))
(assert (forall ((x@15821 Bool) (x Nat) (y Nat) (z Nat)) (=> (and (= x@15821 false) (= x (S z)) (= y Z)) (<=2 x@15821 x y))))
(assert (forall ((x@15821 Bool) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@15822 Bool)) (=> (and (= x@15821 x@15822) (= x (S z)) (= y (S x2)) (<=2 x@15822 z x2)) (<=2 x@15821 x y))))
(declare-fun insort (list Nat list) Bool)
(assert (forall ((x@15823 list) (x Nat) (y list)) (=> (and (= x@15823 (cons x nil)) (= y nil)) (insort x@15823 x y))))
(assert (forall ((x@15823 list) (x Nat) (y list) (z Nat) (xs list) (x@15824 Bool) (x@15825 list)) (=> (and (= x@15823 (ite x@15824 (cons x (cons z xs)) (cons z x@15825))) (= y (cons z xs)) (<=2 x@15824 x z) (insort x@15825 x xs)) (insort x@15823 x y))))
(declare-fun sort (list list) Bool)
(assert (forall ((x@15826 list) (x list)) (=> (and (= x@15826 nil) (= x nil)) (sort x@15826 x))))
(assert (forall ((x@15826 list) (x list) (y Nat) (xs list) (x@15828 list) (x@15827 list)) (=> (and (= x@15826 x@15828) (= x (cons y xs)) (insort x@15828 y x@15827) (sort x@15827 xs)) (sort x@15826 x))))
(assert (forall ((xs list)) (forall ((x@15831 Nat) (x@15830 list) (x@15829 Nat)) (=> (and (diseqNat@250 x@15831 x@15829) (len x@15831 x@15830) (sort x@15830 xs) (len x@15829 xs)) false))))
(check-sat)
(get-info :reason-unknown)
