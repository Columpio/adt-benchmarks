(set-logic ALL)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-sort Nat@0 0)
(declare-fun Z@0 () Nat@0)
(declare-fun S@0 (Nat@0) Nat@0)
(declare-fun diseqNat@0@304 (Nat@0 Nat@0) Bool)
(assert (forall ((x@14841 Nat@0)) (diseqNat@0@304 Z@0 (S@0 x@14841))))
(assert (forall ((x@14842 Nat@0)) (diseqNat@0@304 (S@0 x@14842) Z@0)))
(assert (forall ((x@14843 Nat@0) (x@14844 Nat@0)) (=> (diseqNat@0@304 x@14843 x@14844) (diseqNat@0@304 (S@0 x@14843) (S@0 x@14844)))))
(declare-sort Nat 0)
(declare-fun Z () Nat)
(declare-fun S (Nat) Nat)
(declare-fun diseqNat@212 (Nat Nat) Bool)
(assert (forall ((x@14845 Nat)) (diseqNat@212 Z (S x@14845))))
(assert (forall ((x@14846 Nat)) (diseqNat@212 (S x@14846) Z)))
(assert (forall ((x@14847 Nat) (x@14848 Nat)) (=> (diseqNat@212 x@14847 x@14848) (diseqNat@212 (S x@14847) (S x@14848)))))
(declare-sort list 0)
(declare-fun nil () list)
(declare-fun cons (Nat list) list)
(declare-fun diseqlist@183 (list list) Bool)
(assert (forall ((x@14849 Nat) (x@14850 list)) (diseqlist@183 nil (cons x@14849 x@14850))))
(assert (forall ((x@14851 Nat) (x@14852 list)) (diseqlist@183 (cons x@14851 x@14852) nil)))
(assert (forall ((x@14853 Nat) (x@14854 list) (x@14855 Nat) (x@14856 list)) (=> (diseqNat@212 x@14853 x@14855) (diseqlist@183 (cons x@14853 x@14854) (cons x@14855 x@14856)))))
(assert (forall ((x@14853 Nat) (x@14854 list) (x@14855 Nat) (x@14856 list)) (=> (diseqlist@183 x@14854 x@14856) (diseqlist@183 (cons x@14853 x@14854) (cons x@14855 x@14856)))))
(declare-fun == (Bool Nat Nat) Bool)
(assert (forall ((x@14829 Bool) (x Nat) (y Nat)) (=> (and (= x@14829 true) (= x Z) (= y Z)) (== x@14829 x y))))
(assert (forall ((x@14829 Bool) (x Nat) (y Nat) (z Nat)) (=> (and (= x@14829 false) (= x Z) (= y (S z))) (== x@14829 x y))))
(assert (forall ((x@14829 Bool) (x Nat) (y Nat) (x2 Nat)) (=> (and (= x@14829 false) (= x (S x2)) (= y Z)) (== x@14829 x y))))
(assert (forall ((x@14829 Bool) (x Nat) (y Nat) (x2 Nat) (y2 Nat) (x@14830 Bool)) (=> (and (= x@14829 x@14830) (= x (S x2)) (= y (S y2)) (== x@14830 x2 y2)) (== x@14829 x y))))
(declare-fun elem (Bool Nat list) Bool)
(assert (forall ((x@14831 Bool) (x Nat) (y list)) (=> (and (= x@14831 false) (= y nil)) (elem x@14831 x y))))
(assert (forall ((x@14831 Bool) (x Nat) (y list) (z Nat) (xs list) (x@14832 Bool) (x@14833 Bool)) (=> (and (= x@14831 (ite x@14832 true x@14833)) (= y (cons z xs)) (== x@14832 x z) (elem x@14833 x xs)) (elem x@14831 x y))))
(declare-fun <2 (Bool Nat Nat) Bool)
(assert (forall ((x@14834 Bool) (x Nat) (y Nat)) (=> (and (= x@14834 false) (= y Z)) (<2 x@14834 x y))))
(assert (forall ((x@14834 Bool) (x Nat) (y Nat) (z Nat)) (=> (and (= x@14834 true) (= y (S z)) (= x Z)) (<2 x@14834 x y))))
(assert (forall ((x@14834 Bool) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@14835 Bool)) (=> (and (= x@14834 x@14835) (= y (S z)) (= x (S x2)) (<2 x@14835 x2 z)) (<2 x@14834 x y))))
(declare-fun ins (list Nat list) Bool)
(assert (forall ((x@14836 list) (x Nat) (y list)) (=> (and (= x@14836 (cons x nil)) (= y nil)) (ins x@14836 x y))))
(assert (forall ((x@14836 list) (x Nat) (y list) (z Nat) (xs list) (x@14837 Bool) (x@14838 list)) (=> (and (= x@14836 (ite x@14837 (cons x (cons z xs)) (cons z x@14838))) (= y (cons z xs)) (<2 x@14837 x z) (ins x@14838 x xs)) (ins x@14836 x y))))
(assert (forall ((x Nat) (xs list)) (forall ((x@14840 Bool) (x@14839 list)) (=> (and (not x@14840) (elem x@14840 x x@14839) (ins x@14839 x xs)) false))))
(check-sat)
(get-info :reason-unknown)
