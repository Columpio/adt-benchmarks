(set-logic ALL)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-sort Nat@0 0)
(declare-fun Z@0 () Nat@0)
(declare-fun S@0 (Nat@0) Nat@0)
(declare-fun diseqNat@0@367 (Nat@0 Nat@0) Bool)
(assert (forall ((x@16386 Nat@0)) (diseqNat@0@367 Z@0 (S@0 x@16386))))
(assert (forall ((x@16387 Nat@0)) (diseqNat@0@367 (S@0 x@16387) Z@0)))
(assert (forall ((x@16388 Nat@0) (x@16389 Nat@0)) (=> (diseqNat@0@367 x@16388 x@16389) (diseqNat@0@367 (S@0 x@16388) (S@0 x@16389)))))
(declare-sort Nat 0)
(declare-fun Z () Nat)
(declare-fun S (Nat) Nat)
(declare-fun diseqNat@273 (Nat Nat) Bool)
(assert (forall ((x@16390 Nat)) (diseqNat@273 Z (S x@16390))))
(assert (forall ((x@16391 Nat)) (diseqNat@273 (S x@16391) Z)))
(assert (forall ((x@16392 Nat) (x@16393 Nat)) (=> (diseqNat@273 x@16392 x@16393) (diseqNat@273 (S x@16392) (S x@16393)))))
(declare-fun |-2| (Nat Nat Nat) Bool)
(assert (forall ((x@16378 Nat) (x Nat) (y Nat)) (=> (and (= x@16378 Z) (= x Z)) (|-2| x@16378 x y))))
(assert (forall ((x@16378 Nat) (x Nat) (y Nat) (z Nat)) (=> (and (= x@16378 (S z)) (= x (S z)) (= y Z)) (|-2| x@16378 x y))))
(assert (forall ((x@16378 Nat) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@16379 Nat)) (=> (and (= x@16378 x@16379) (= x (S z)) (= y (S x2)) (|-2| x@16379 z x2)) (|-2| x@16378 x y))))
(declare-fun +2 (Nat Nat Nat) Bool)
(assert (forall ((x@16380 Nat) (x Nat) (y Nat)) (=> (and (= x@16380 y) (= x Z)) (+2 x@16380 x y))))
(assert (forall ((x@16380 Nat) (x Nat) (y Nat) (z Nat) (x@16381 Nat)) (=> (and (= x@16380 (S x@16381)) (= x (S z)) (+2 x@16381 z y)) (+2 x@16380 x y))))
(assert (forall ((i Nat) (j Nat) (k Nat)) (forall ((x@16385 Nat) (x@16384 Nat) (x@16383 Nat) (x@16382 Nat)) (=> (and (diseqNat@273 x@16385 x@16383) (|-2| x@16385 x@16384 k) (|-2| x@16384 i j) (|-2| x@16383 i x@16382) (+2 x@16382 j k)) false))))
(check-sat)
(get-info :reason-unknown)
