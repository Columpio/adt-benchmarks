(set-logic ALL)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-sort Nat@0 0)
(declare-fun Z@0 () Nat@0)
(declare-fun S@0 (Nat@0) Nat@0)
(declare-fun diseqNat@0@299 (Nat@0 Nat@0) Bool)
(assert (forall ((x@14709 Nat@0)) (diseqNat@0@299 Z@0 (S@0 x@14709))))
(assert (forall ((x@14710 Nat@0)) (diseqNat@0@299 (S@0 x@14710) Z@0)))
(assert (forall ((x@14711 Nat@0) (x@14712 Nat@0)) (=> (diseqNat@0@299 x@14711 x@14712) (diseqNat@0@299 (S@0 x@14711) (S@0 x@14712)))))
(declare-sort list 0)
(declare-fun nil () list)
(declare-fun cons (Nat@0 list) list)
(declare-fun diseqlist@179 (list list) Bool)
(assert (forall ((x@14713 Nat@0) (x@14714 list)) (diseqlist@179 nil (cons x@14713 x@14714))))
(assert (forall ((x@14715 Nat@0) (x@14716 list)) (diseqlist@179 (cons x@14715 x@14716) nil)))
(assert (forall ((x@14717 Nat@0) (x@14718 list) (x@14719 Nat@0) (x@14720 list)) (=> (diseqNat@0@299 x@14717 x@14719) (diseqlist@179 (cons x@14717 x@14718) (cons x@14719 x@14720)))))
(assert (forall ((x@14717 Nat@0) (x@14718 list) (x@14719 Nat@0) (x@14720 list)) (=> (diseqlist@179 x@14718 x@14720) (diseqlist@179 (cons x@14717 x@14718) (cons x@14719 x@14720)))))
(declare-fun butlast (list list) Bool)
(assert (forall ((x@14703 list) (x list)) (=> (and (= x@14703 nil) (= x nil)) (butlast x@14703 x))))
(assert (forall ((x@14703 list) (x list) (y Nat@0) (z list)) (=> (and (= x@14703 nil) (= x (cons y z)) (= z nil)) (butlast x@14703 x))))
(assert (forall ((x@14703 list) (x list) (y Nat@0) (z list) (x2 Nat@0) (x3 list) (x@14704 list)) (=> (and (= x@14703 (cons y x@14704)) (= x (cons y z)) (= z (cons x2 x3)) (butlast x@14704 (cons x2 x3))) (butlast x@14703 x))))
(declare-fun ++ (list list list) Bool)
(assert (forall ((x@14705 list) (x list) (y list)) (=> (and (= x@14705 y) (= x nil)) (++ x@14705 x y))))
(assert (forall ((x@14705 list) (x list) (y list) (z Nat@0) (xs list) (x@14706 list)) (=> (and (= x@14705 (cons z x@14706)) (= x (cons z xs)) (++ x@14706 xs y)) (++ x@14705 x y))))
(assert (forall ((xs list) (x Nat@0)) (forall ((x@14708 list) (x@14707 list)) (=> (and (diseqlist@179 x@14708 xs) (butlast x@14708 x@14707) (++ x@14707 xs (cons x nil))) false))))
(check-sat)
(get-info :reason-unknown)
