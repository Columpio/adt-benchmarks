(set-logic ALL)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-sort Nat@0 0)
(declare-fun Z@0 () Nat@0)
(declare-fun S@0 (Nat@0) Nat@0)
(declare-fun diseqNat@0@254 (Nat@0 Nat@0) Bool)
(assert (forall ((x@13160 Nat@0)) (diseqNat@0@254 Z@0 (S@0 x@13160))))
(assert (forall ((x@13161 Nat@0)) (diseqNat@0@254 (S@0 x@13161) Z@0)))
(assert (forall ((x@13162 Nat@0) (x@13163 Nat@0)) (=> (diseqNat@0@254 x@13162 x@13163) (diseqNat@0@254 (S@0 x@13162) (S@0 x@13163)))))
(declare-sort Nat 0)
(declare-fun zero () Nat)
(declare-fun succ (Nat) Nat)
(declare-fun diseqNat@171 (Nat Nat) Bool)
(assert (forall ((x@13164 Nat)) (diseqNat@171 zero (succ x@13164))))
(assert (forall ((x@13165 Nat)) (diseqNat@171 (succ x@13165) zero)))
(assert (forall ((x@13166 Nat) (x@13167 Nat)) (=> (diseqNat@171 x@13166 x@13167) (diseqNat@171 (succ x@13166) (succ x@13167)))))
(declare-sort list2 0)
(declare-fun nil2 () list2)
(declare-fun cons2 (Nat list2) list2)
(declare-fun diseqlist2@16 (list2 list2) Bool)
(assert (forall ((x@13168 Nat) (x@13169 list2)) (diseqlist2@16 nil2 (cons2 x@13168 x@13169))))
(assert (forall ((x@13170 Nat) (x@13171 list2)) (diseqlist2@16 (cons2 x@13170 x@13171) nil2)))
(assert (forall ((x@13172 Nat) (x@13173 list2) (x@13174 Nat) (x@13175 list2)) (=> (diseqNat@171 x@13172 x@13174) (diseqlist2@16 (cons2 x@13172 x@13173) (cons2 x@13174 x@13175)))))
(assert (forall ((x@13172 Nat) (x@13173 list2) (x@13174 Nat) (x@13175 list2)) (=> (diseqlist2@16 x@13173 x@13175) (diseqlist2@16 (cons2 x@13172 x@13173) (cons2 x@13174 x@13175)))))
(declare-sort list 0)
(declare-fun nil () list)
(declare-fun cons (list2 list) list)
(declare-fun diseqlist@159 (list list) Bool)
(assert (forall ((x@13176 list2) (x@13177 list)) (diseqlist@159 nil (cons x@13176 x@13177))))
(assert (forall ((x@13178 list2) (x@13179 list)) (diseqlist@159 (cons x@13178 x@13179) nil)))
(assert (forall ((x@13180 list2) (x@13181 list) (x@13182 list2) (x@13183 list)) (=> (diseqlist2@16 x@13180 x@13182) (diseqlist@159 (cons x@13180 x@13181) (cons x@13182 x@13183)))))
(assert (forall ((x@13180 list2) (x@13181 list) (x@13182 list2) (x@13183 list)) (=> (diseqlist@159 x@13181 x@13183) (diseqlist@159 (cons x@13180 x@13181) (cons x@13182 x@13183)))))
(declare-fun leq (Bool Nat Nat) Bool)
(assert (forall ((x@13133 Bool) (x Nat) (y Nat)) (=> (and (= x@13133 true) (= x zero)) (leq x@13133 x y))))
(assert (forall ((x@13133 Bool) (x Nat) (y Nat) (z Nat)) (=> (and (= x@13133 false) (= x (succ z)) (= y zero)) (leq x@13133 x y))))
(assert (forall ((x@13133 Bool) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@13134 Bool)) (=> (and (= x@13133 x@13134) (= x (succ z)) (= y (succ x2)) (leq x@13134 z x2)) (leq x@13133 x y))))
(declare-fun lmerge (list2 list2 list2) Bool)
(assert (forall ((x@13135 list2) (x list2) (y list2)) (=> (and (= x@13135 y) (= x nil2)) (lmerge x@13135 x y))))
(assert (forall ((x@13135 list2) (x list2) (y list2) (z Nat) (x2 list2)) (=> (and (= x@13135 (cons2 z x2)) (= x (cons2 z x2)) (= y nil2)) (lmerge x@13135 x y))))
(assert (forall ((x@13135 list2) (x list2) (y list2) (z Nat) (x2 list2) (x3 Nat) (x4 list2) (x@13136 Bool) (x@13137 list2) (x@13138 list2)) (=> (and (= x@13135 (ite x@13136 (cons2 z x@13137) (cons2 x3 x@13138))) (= x (cons2 z x2)) (= y (cons2 x3 x4)) (leq x@13136 z x3) (lmerge x@13137 x2 (cons2 x3 x4)) (lmerge x@13138 (cons2 z x2) x4)) (lmerge x@13135 x y))))
(declare-fun pairwise (list list) Bool)
(assert (forall ((x@13139 list) (x list)) (=> (and (= x@13139 nil) (= x nil)) (pairwise x@13139 x))))
(assert (forall ((x@13139 list) (x list) (xs list2) (y list)) (=> (and (= x@13139 (cons xs nil)) (= x (cons xs y)) (= y nil)) (pairwise x@13139 x))))
(assert (forall ((x@13139 list) (x list) (xs list2) (y list) (ys list2) (xss list) (x@13141 list2) (x@13140 list)) (=> (and (= x@13139 (cons x@13141 x@13140)) (= x (cons xs y)) (= y (cons ys xss)) (lmerge x@13141 xs ys) (pairwise x@13140 xss)) (pairwise x@13139 x))))
(declare-fun mergingbu2 (list2 list) Bool)
(assert (forall ((x@13142 list2) (x list)) (=> (and (= x@13142 nil2) (= x nil)) (mergingbu2 x@13142 x))))
(assert (forall ((x@13142 list2) (x list) (xs list2) (y list)) (=> (and (= x@13142 xs) (= x (cons xs y)) (= y nil)) (mergingbu2 x@13142 x))))
(assert (forall ((x@13142 list2) (x list) (xs list2) (y list) (z list2) (x2 list) (x@13144 list2) (x@13143 list)) (=> (and (= x@13142 x@13144) (= x (cons xs y)) (= y (cons z x2)) (mergingbu2 x@13144 x@13143) (pairwise x@13143 (cons xs (cons z x2)))) (mergingbu2 x@13142 x))))
(declare-fun risers (list list2) Bool)
(assert (forall ((x@13145 list) (x list2)) (=> (and (= x@13145 nil) (= x nil2)) (risers x@13145 x))))
(assert (forall ((x@13145 list) (x list2) (y Nat) (z list2)) (=> (and (= x@13145 (cons (cons2 y nil2) nil)) (= x (cons2 y z)) (= z nil2)) (risers x@13145 x))))
(assert (forall ((x@13145 list) (x list2) (y Nat) (z list2) (y2 Nat) (xs list2) (x@13146 Bool) (x@13147 list) (x@13148 list)) (=> (and (= x@13145 (ite x@13146 nil (cons (cons2 y nil2) x@13148))) (= x (cons2 y z)) (= z (cons2 y2 xs)) (leq x@13146 y y2) (= x@13147 nil) (risers x@13147 (cons2 y2 xs)) (risers x@13148 (cons2 y2 xs))) (risers x@13145 x))))
(assert (forall ((x@13145 list) (x list2) (y Nat) (z list2) (y2 Nat) (xs list2) (x@13146 Bool) (x@13147 list) (ys list2) (yss list) (x@13148 list)) (=> (and (= x@13145 (ite x@13146 (cons (cons2 y ys) yss) (cons (cons2 y nil2) x@13148))) (= x (cons2 y z)) (= z (cons2 y2 xs)) (leq x@13146 y y2) (= x@13147 (cons ys yss)) (risers x@13147 (cons2 y2 xs)) (risers x@13148 (cons2 y2 xs))) (risers x@13145 x))))
(declare-fun msortbu2 (list2 list2) Bool)
(assert (forall ((x@13149 list2) (x list2) (x@13151 list2) (x@13150 list)) (=> (and (= x@13149 x@13151) (mergingbu2 x@13151 x@13150) (risers x@13150 x)) (msortbu2 x@13149 x))))
(declare-fun insert2 (list2 Nat list2) Bool)
(assert (forall ((x@13152 list2) (x Nat) (y list2)) (=> (and (= x@13152 (cons2 x nil2)) (= y nil2)) (insert2 x@13152 x y))))
(assert (forall ((x@13152 list2) (x Nat) (y list2) (z Nat) (xs list2) (x@13153 Bool) (x@13154 list2)) (=> (and (= x@13152 (ite x@13153 (cons2 x (cons2 z xs)) (cons2 z x@13154))) (= y (cons2 z xs)) (leq x@13153 x z) (insert2 x@13154 x xs)) (insert2 x@13152 x y))))
(declare-fun isort (list2 list2) Bool)
(assert (forall ((x@13155 list2) (x list2)) (=> (and (= x@13155 nil2) (= x nil2)) (isort x@13155 x))))
(assert (forall ((x@13155 list2) (x list2) (y Nat) (xs list2) (x@13157 list2) (x@13156 list2)) (=> (and (= x@13155 x@13157) (= x (cons2 y xs)) (insert2 x@13157 y x@13156) (isort x@13156 xs)) (isort x@13155 x))))
(assert (forall ((xs list2)) (forall ((x@13159 list2) (x@13158 list2)) (=> (and (diseqlist2@16 x@13159 x@13158) (msortbu2 x@13159 xs) (isort x@13158 xs)) false))))
(check-sat)
(get-info :reason-unknown)
