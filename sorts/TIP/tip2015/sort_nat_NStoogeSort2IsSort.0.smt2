(set-logic ALL)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-sort Nat@0 0)
(declare-fun Z@0 () Nat@0)
(declare-fun S@0 (Nat@0) Nat@0)
(declare-fun diseqNat@0@251 (Nat@0 Nat@0) Bool)
(assert (forall ((x@12980 Nat@0)) (diseqNat@0@251 Z@0 (S@0 x@12980))))
(assert (forall ((x@12981 Nat@0)) (diseqNat@0@251 (S@0 x@12981) Z@0)))
(assert (forall ((x@12982 Nat@0) (x@12983 Nat@0)) (=> (diseqNat@0@251 x@12982 x@12983) (diseqNat@0@251 (S@0 x@12982) (S@0 x@12983)))))
(declare-sort Nat 0)
(declare-fun zero () Nat)
(declare-fun succ (Nat) Nat)
(declare-fun diseqNat@169 (Nat Nat) Bool)
(assert (forall ((x@12984 Nat)) (diseqNat@169 zero (succ x@12984))))
(assert (forall ((x@12985 Nat)) (diseqNat@169 (succ x@12985) zero)))
(assert (forall ((x@12986 Nat) (x@12987 Nat)) (=> (diseqNat@169 x@12986 x@12987) (diseqNat@169 (succ x@12986) (succ x@12987)))))
(declare-sort list 0)
(declare-fun nil () list)
(declare-fun cons (Nat list) list)
(declare-fun diseqlist@157 (list list) Bool)
(assert (forall ((x@12988 Nat) (x@12989 list)) (diseqlist@157 nil (cons x@12988 x@12989))))
(assert (forall ((x@12990 Nat) (x@12991 list)) (diseqlist@157 (cons x@12990 x@12991) nil)))
(assert (forall ((x@12992 Nat) (x@12993 list) (x@12994 Nat) (x@12995 list)) (=> (diseqNat@169 x@12992 x@12994) (diseqlist@157 (cons x@12992 x@12993) (cons x@12994 x@12995)))))
(assert (forall ((x@12992 Nat) (x@12993 list) (x@12994 Nat) (x@12995 list)) (=> (diseqlist@157 x@12993 x@12995) (diseqlist@157 (cons x@12992 x@12993) (cons x@12994 x@12995)))))
(declare-sort pair 0)
(declare-fun pair2 (list list) pair)
(declare-fun diseqpair@21 (pair pair) Bool)
(assert (forall ((x@12996 list) (x@12997 list) (x@12998 list) (x@12999 list)) (=> (diseqlist@157 x@12996 x@12998) (diseqpair@21 (pair2 x@12996 x@12997) (pair2 x@12998 x@12999)))))
(assert (forall ((x@12996 list) (x@12997 list) (x@12998 list) (x@12999 list)) (=> (diseqlist@157 x@12997 x@12999) (diseqpair@21 (pair2 x@12996 x@12997) (pair2 x@12998 x@12999)))))
(declare-fun take (list Nat list) Bool)
(assert (forall ((x@12919 list) (x Nat) (y list)) (=> (and (= x@12919 nil) (= x zero)) (take x@12919 x y))))
(assert (forall ((x@12919 list) (x Nat) (y list) (z Nat)) (=> (and (= x@12919 nil) (= x (succ z)) (= y nil)) (take x@12919 x y))))
(assert (forall ((x@12919 list) (x Nat) (y list) (z Nat) (z2 Nat) (xs list) (x@12920 list)) (=> (and (= x@12919 (cons z2 x@12920)) (= x (succ z)) (= y (cons z2 xs)) (take x@12920 z xs)) (take x@12919 x y))))
(declare-fun plus (Nat Nat Nat) Bool)
(assert (forall ((x@12921 Nat) (x Nat) (y Nat)) (=> (and (= x@12921 y) (= x zero)) (plus x@12921 x y))))
(assert (forall ((x@12921 Nat) (x Nat) (y Nat) (z Nat) (x@12922 Nat)) (=> (and (= x@12921 (succ x@12922)) (= x (succ z)) (plus x@12922 z y)) (plus x@12921 x y))))
(declare-fun minus (Nat Nat Nat) Bool)
(assert (forall ((x@12923 Nat) (x Nat) (y Nat)) (=> (and (= x@12923 zero) (= x zero)) (minus x@12923 x y))))
(assert (forall ((x@12923 Nat) (x Nat) (y Nat) (z Nat)) (=> (and (= x@12923 zero) (= x (succ z)) (= y zero)) (minus x@12923 x y))))
(assert (forall ((x@12923 Nat) (x Nat) (y Nat) (z Nat) (y2 Nat) (x@12924 Nat)) (=> (and (= x@12923 x@12924) (= x (succ z)) (= y (succ y2)) (minus x@12924 z y2)) (minus x@12923 x y))))
(declare-fun third (Nat Nat) Bool)
(assert (forall ((x@12925 Nat) (x Nat)) (=> (and (= x@12925 (ite (= x (succ (succ zero))) zero (ite (= x (succ zero)) zero zero))) (= x zero)) (third x@12925 x))))
(assert (forall ((x@12925 Nat) (x Nat) (y Nat) (x@12928 Nat) (x@12927 Nat) (x@12926 Nat)) (=> (and (= x@12925 (ite (= x (succ (succ zero))) zero (ite (= x (succ zero)) zero x@12928))) (= x (succ y)) (plus x@12928 (succ zero) x@12927) (third x@12927 x@12926) (minus x@12926 (succ y) (succ (succ (succ zero))))) (third x@12925 x))))
(declare-fun twoThirds (Nat Nat) Bool)
(assert (forall ((x@12929 Nat) (x Nat)) (=> (and (= x@12929 (ite (= x (succ (succ zero))) (succ zero) (ite (= x (succ zero)) (succ zero) zero))) (= x zero)) (twoThirds x@12929 x))))
(assert (forall ((x@12929 Nat) (x Nat) (y Nat) (x@12932 Nat) (x@12931 Nat) (x@12930 Nat)) (=> (and (= x@12929 (ite (= x (succ (succ zero))) (succ zero) (ite (= x (succ zero)) (succ zero) x@12932))) (= x (succ y)) (plus x@12932 (succ (succ zero)) x@12931) (twoThirds x@12931 x@12930) (minus x@12930 (succ y) (succ (succ (succ zero))))) (twoThirds x@12929 x))))
(declare-fun leq (Bool Nat Nat) Bool)
(assert (forall ((x@12933 Bool) (x Nat) (y Nat)) (=> (and (= x@12933 true) (= x zero)) (leq x@12933 x y))))
(assert (forall ((x@12933 Bool) (x Nat) (y Nat) (z Nat)) (=> (and (= x@12933 false) (= x (succ z)) (= y zero)) (leq x@12933 x y))))
(assert (forall ((x@12933 Bool) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@12934 Bool)) (=> (and (= x@12933 x@12934) (= x (succ z)) (= y (succ x2)) (leq x@12934 z x2)) (leq x@12933 x y))))
(declare-fun sort2 (list Nat Nat) Bool)
(assert (forall ((x@12935 list) (x Nat) (y Nat) (x@12936 Bool)) (=> (and (= x@12935 (ite x@12936 (cons x (cons y nil)) (cons y (cons x nil)))) (leq x@12936 x y)) (sort2 x@12935 x y))))
(declare-fun length (Nat list) Bool)
(assert (forall ((x@12937 Nat) (x list)) (=> (and (= x@12937 zero) (= x nil)) (length x@12937 x))))
(assert (forall ((x@12937 Nat) (x list) (y Nat) (l list) (x@12939 Nat) (x@12938 Nat)) (=> (and (= x@12937 x@12939) (= x (cons y l)) (plus x@12939 (succ zero) x@12938) (length x@12938 l)) (length x@12937 x))))
(declare-fun insert2 (list Nat list) Bool)
(assert (forall ((x@12940 list) (x Nat) (y list)) (=> (and (= x@12940 (cons x nil)) (= y nil)) (insert2 x@12940 x y))))
(assert (forall ((x@12940 list) (x Nat) (y list) (z Nat) (xs list) (x@12941 Bool) (x@12942 list)) (=> (and (= x@12940 (ite x@12941 (cons x (cons z xs)) (cons z x@12942))) (= y (cons z xs)) (leq x@12941 x z) (insert2 x@12942 x xs)) (insert2 x@12940 x y))))
(declare-fun isort (list list) Bool)
(assert (forall ((x@12943 list) (x list)) (=> (and (= x@12943 nil) (= x nil)) (isort x@12943 x))))
(assert (forall ((x@12943 list) (x list) (y Nat) (xs list) (x@12945 list) (x@12944 list)) (=> (and (= x@12943 x@12945) (= x (cons y xs)) (insert2 x@12945 y x@12944) (isort x@12944 xs)) (isort x@12943 x))))
(declare-fun drop (list Nat list) Bool)
(assert (forall ((x@12946 list) (x Nat) (y list)) (=> (and (= x@12946 y) (= x zero)) (drop x@12946 x y))))
(assert (forall ((x@12946 list) (x Nat) (y list) (z Nat)) (=> (and (= x@12946 nil) (= x (succ z)) (= y nil)) (drop x@12946 x y))))
(assert (forall ((x@12946 list) (x Nat) (y list) (z Nat) (z2 Nat) (xs1 list) (x@12947 list)) (=> (and (= x@12946 x@12947) (= x (succ z)) (= y (cons z2 xs1)) (drop x@12947 z xs1)) (drop x@12946 x y))))
(declare-fun splitAt (pair Nat list) Bool)
(assert (forall ((x@12948 pair) (x Nat) (y list) (x@12950 list) (x@12949 list)) (=> (and (= x@12948 (pair2 x@12950 x@12949)) (take x@12950 x y) (drop x@12949 x y)) (splitAt x@12948 x y))))
(declare-fun ++ (list list list) Bool)
(assert (forall ((x@12951 list) (x list) (y list)) (=> (and (= x@12951 y) (= x nil)) (++ x@12951 x y))))
(assert (forall ((x@12951 list) (x list) (y list) (z Nat) (xs list) (x@12952 list)) (=> (and (= x@12951 (cons z x@12952)) (= x (cons z xs)) (++ x@12952 xs y)) (++ x@12951 x y))))
(declare-fun nstooge2sort2 (list list) Bool)
(declare-fun nstoogesort2 (list list) Bool)
(declare-fun nstooge2sort1 (list list) Bool)
(assert (forall ((x@12953 list) (x list) (x@12956 pair) (x@12955 Nat) (x@12954 Nat) (ys1 list) (zs list) (x@12958 list) (x@12957 list)) (=> (and (= x@12953 x@12958) (= x@12956 (pair2 ys1 zs)) (splitAt x@12956 x@12955 x) (twoThirds x@12955 x@12954) (length x@12954 x) (++ x@12958 x@12957 zs) (nstoogesort2 x@12957 ys1)) (nstooge2sort2 x@12953 x))))
(assert (forall ((x@12959 list) (x list)) (=> (and (= x@12959 nil) (= x nil)) (nstoogesort2 x@12959 x))))
(assert (forall ((x@12959 list) (x list) (y Nat) (z list)) (=> (and (= x@12959 (cons y nil)) (= x (cons y z)) (= z nil)) (nstoogesort2 x@12959 x))))
(assert (forall ((x@12959 list) (x list) (y Nat) (z list) (y2 Nat) (x2 list) (x@12960 list)) (=> (and (= x@12959 x@12960) (= x (cons y z)) (= z (cons y2 x2)) (= x2 nil) (sort2 x@12960 y y2)) (nstoogesort2 x@12959 x))))
(assert (forall ((x@12959 list) (x list) (y Nat) (z list) (y2 Nat) (x2 list) (x3 Nat) (x4 list) (x@12963 list) (x@12962 list) (x@12961 list)) (=> (and (= x@12959 x@12963) (= x (cons y z)) (= z (cons y2 x2)) (= x2 (cons x3 x4)) (nstooge2sort2 x@12963 x@12962) (nstooge2sort1 x@12962 x@12961) (nstooge2sort2 x@12961 (cons y (cons y2 (cons x3 x4))))) (nstoogesort2 x@12959 x))))
(assert (forall ((x@12964 list) (x list) (x@12967 pair) (x@12966 Nat) (x@12965 Nat) (ys1 list) (zs list) (x@12969 list) (x@12968 list)) (=> (and (= x@12964 x@12969) (= x@12967 (pair2 ys1 zs)) (splitAt x@12967 x@12966 x) (third x@12966 x@12965) (length x@12965 x) (++ x@12969 ys1 x@12968) (nstoogesort2 x@12968 zs)) (nstooge2sort1 x@12964 x))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@12973 Nat) (x@12972 Nat) (x@12971 Nat) (x@12970 Nat)) (=> (and (diseqNat@169 x@12973 x@12971) (plus x@12973 x x@12972) (plus x@12972 y z) (plus x@12971 x@12970 z) (plus x@12970 x y)) false))))
(assert (forall ((x Nat) (y Nat)) (forall ((x@12975 Nat) (x@12974 Nat)) (=> (and (diseqNat@169 x@12975 x@12974) (plus x@12975 x y) (plus x@12974 y x)) false))))
(assert (forall ((x Nat)) (forall ((x@12976 Nat)) (=> (and (diseqNat@169 x@12976 x) (plus x@12976 x zero)) false))))
(assert (forall ((x Nat)) (forall ((x@12977 Nat)) (=> (and (diseqNat@169 x@12977 x) (plus x@12977 zero x)) false))))
(assert (forall ((xs list)) (forall ((x@12979 list) (x@12978 list)) (=> (and (diseqlist@157 x@12979 x@12978) (nstoogesort2 x@12979 xs) (isort x@12978 xs)) false))))
(check-sat)
(get-info :reason-unknown)
