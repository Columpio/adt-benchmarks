(set-logic ALL)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-sort Nat@0 0)
(declare-fun Z@0 () Nat@0)
(declare-fun S@0 (Nat@0) Nat@0)
(declare-fun diseqNat@0@106 (Nat@0 Nat@0) Bool)
(assert (forall ((x@6365 Nat@0)) (diseqNat@0@106 Z@0 (S@0 x@6365))))
(assert (forall ((x@6366 Nat@0)) (diseqNat@0@106 (S@0 x@6366) Z@0)))
(assert (forall ((x@6367 Nat@0) (x@6368 Nat@0)) (=> (diseqNat@0@106 x@6367 x@6368) (diseqNat@0@106 (S@0 x@6367) (S@0 x@6368)))))
(declare-sort Sign 0)
(declare-fun Pos () Sign)
(declare-fun Neg () Sign)
(declare-fun diseqSign@0 (Sign Sign) Bool)
(assert (diseqSign@0 Pos Neg))
(assert (diseqSign@0 Neg Pos))
(declare-sort Nat 0)
(declare-fun zero () Nat)
(declare-fun succ (Nat) Nat)
(declare-fun diseqNat@58 (Nat Nat) Bool)
(assert (forall ((x@6369 Nat)) (diseqNat@58 zero (succ x@6369))))
(assert (forall ((x@6370 Nat)) (diseqNat@58 (succ x@6370) zero)))
(assert (forall ((x@6371 Nat) (x@6372 Nat)) (=> (diseqNat@58 x@6371 x@6372) (diseqNat@58 (succ x@6371) (succ x@6372)))))
(declare-sort Integer 0)
(declare-fun P (Nat) Integer)
(declare-fun N (Nat) Integer)
(declare-fun diseqInteger@0 (Integer Integer) Bool)
(assert (forall ((x@6373 Nat) (x@6374 Nat)) (diseqInteger@0 (P x@6373) (N x@6374))))
(assert (forall ((x@6375 Nat) (x@6376 Nat)) (diseqInteger@0 (N x@6375) (P x@6376))))
(assert (forall ((x@6377 Nat) (x@6378 Nat)) (=> (diseqNat@58 x@6377 x@6378) (diseqInteger@0 (P x@6377) (P x@6378)))))
(assert (forall ((x@6379 Nat) (x@6380 Nat)) (=> (diseqNat@58 x@6379 x@6380) (diseqInteger@0 (N x@6379) (N x@6380)))))
(declare-fun toInteger (Integer Sign Nat) Bool)
(assert (forall ((x@6302 Integer) (x Sign) (y Nat)) (=> (and (= x@6302 (P y)) (= x Pos)) (toInteger x@6302 x y))))
(assert (forall ((x@6302 Integer) (x Sign) (y Nat)) (=> (and (= x@6302 (P zero)) (= x Neg) (= y zero)) (toInteger x@6302 x y))))
(assert (forall ((x@6302 Integer) (x Sign) (y Nat) (z Nat)) (=> (and (= x@6302 (N z)) (= x Neg) (= y (succ z))) (toInteger x@6302 x y))))
(declare-fun sign (Sign Integer) Bool)
(assert (forall ((x@6303 Sign) (x Integer) (y Nat)) (=> (and (= x@6303 Pos) (= x (P y))) (sign x@6303 x))))
(assert (forall ((x@6303 Sign) (x Integer) (z Nat)) (=> (and (= x@6303 Neg) (= x (N z))) (sign x@6303 x))))
(declare-fun plus (Nat Nat Nat) Bool)
(assert (forall ((x@6304 Nat) (x Nat) (y Nat)) (=> (and (= x@6304 y) (= x zero)) (plus x@6304 x y))))
(assert (forall ((x@6304 Nat) (x Nat) (y Nat) (z Nat) (x@6305 Nat)) (=> (and (= x@6304 (succ x@6305)) (= x (succ z)) (plus x@6305 z y)) (plus x@6304 x y))))
(declare-fun times2 (Nat Nat Nat) Bool)
(assert (forall ((x@6306 Nat) (x Nat) (y Nat)) (=> (and (= x@6306 zero) (= x zero)) (times2 x@6306 x y))))
(assert (forall ((x@6306 Nat) (x Nat) (y Nat) (z Nat) (x@6308 Nat) (x@6307 Nat)) (=> (and (= x@6306 x@6308) (= x (succ z)) (plus x@6308 y x@6307) (times2 x@6307 z y)) (times2 x@6306 x y))))
(declare-fun opposite (Sign Sign) Bool)
(assert (forall ((x@6309 Sign) (x Sign)) (=> (and (= x@6309 Neg) (= x Pos)) (opposite x@6309 x))))
(assert (forall ((x@6309 Sign) (x Sign)) (=> (and (= x@6309 Pos) (= x Neg)) (opposite x@6309 x))))
(declare-fun timesSign (Sign Sign Sign) Bool)
(assert (forall ((x@6310 Sign) (x Sign) (y Sign)) (=> (and (= x@6310 y) (= x Pos)) (timesSign x@6310 x y))))
(assert (forall ((x@6310 Sign) (x Sign) (y Sign) (x@6311 Sign)) (=> (and (= x@6310 x@6311) (= x Neg) (opposite x@6311 y)) (timesSign x@6310 x y))))
(declare-fun absVal (Nat Integer) Bool)
(assert (forall ((x@6312 Nat) (x Integer) (n Nat)) (=> (and (= x@6312 n) (= x (P n))) (absVal x@6312 x))))
(assert (forall ((x@6312 Nat) (x Integer) (m Nat) (x@6313 Nat)) (=> (and (= x@6312 x@6313) (= x (N m)) (plus x@6313 (succ zero) m)) (absVal x@6312 x))))
(declare-fun times (Integer Integer Integer) Bool)
(assert (forall ((x@6314 Integer) (x Integer) (y Integer) (x@6321 Integer) (x@6320 Sign) (x@6319 Sign) (x@6318 Sign) (x@6317 Nat) (x@6316 Nat) (x@6315 Nat)) (=> (and (= x@6314 x@6321) (toInteger x@6321 x@6320 x@6317) (timesSign x@6320 x@6319 x@6318) (sign x@6319 x) (sign x@6318 y) (times2 x@6317 x@6316 x@6315) (absVal x@6316 x) (absVal x@6315 y)) (times x@6314 x y))))
(declare-fun |-2| (Integer Nat Nat) Bool)
(assert (forall ((x@6322 Integer) (x Nat) (y Nat) (fail Integer)) (=> (and (= x@6322 (P zero)) (= fail (P x)) (= y zero) (= x zero) (= y zero)) (|-2| x@6322 x y))))
(assert (forall ((x@6322 Integer) (x Nat) (y Nat) (fail Integer) (x4 Nat)) (=> (and (= x@6322 fail) (= fail (P x)) (= y zero) (= x zero) (= y (succ x4))) (|-2| x@6322 x y))))
(assert (forall ((x@6322 Integer) (x Nat) (y Nat) (fail Integer) (x3 Nat)) (=> (and (= x@6322 fail) (= fail (P x)) (= y zero) (= x (succ x3))) (|-2| x@6322 x y))))
(assert (forall ((x@6322 Integer) (x Nat) (y Nat) (fail Integer) (z Nat)) (=> (and (= x@6322 (P zero)) (= fail (N (succ z))) (= y (succ z)) (= x zero) (= x zero) (= y zero)) (|-2| x@6322 x y))))
(assert (forall ((x@6322 Integer) (x Nat) (y Nat) (fail Integer) (z Nat) (x4 Nat)) (=> (and (= x@6322 fail) (= fail (N (succ z))) (= y (succ z)) (= x zero) (= x zero) (= y (succ x4))) (|-2| x@6322 x y))))
(assert (forall ((x@6322 Integer) (x Nat) (y Nat) (fail Integer) (z Nat) (x3 Nat)) (=> (and (= x@6322 fail) (= fail (N (succ z))) (= y (succ z)) (= x zero) (= x (succ x3))) (|-2| x@6322 x y))))
(assert (forall ((x@6322 Integer) (x Nat) (y Nat) (fail Integer) (z Nat) (x2 Nat) (x@6323 Integer)) (=> (and (= x@6322 (P zero)) (= fail x@6323) (= y (succ z)) (= x (succ x2)) (|-2| x@6323 x2 z) (= x zero) (= y zero)) (|-2| x@6322 x y))))
(assert (forall ((x@6322 Integer) (x Nat) (y Nat) (fail Integer) (z Nat) (x2 Nat) (x@6323 Integer) (x4 Nat)) (=> (and (= x@6322 fail) (= fail x@6323) (= y (succ z)) (= x (succ x2)) (|-2| x@6323 x2 z) (= x zero) (= y (succ x4))) (|-2| x@6322 x y))))
(assert (forall ((x@6322 Integer) (x Nat) (y Nat) (fail Integer) (z Nat) (x2 Nat) (x@6323 Integer) (x3 Nat)) (=> (and (= x@6322 fail) (= fail x@6323) (= y (succ z)) (= x (succ x2)) (|-2| x@6323 x2 z) (= x (succ x3))) (|-2| x@6322 x y))))
(declare-fun plus2 (Integer Integer Integer) Bool)
(assert (forall ((x@6324 Integer) (x Integer) (y Integer) (m Nat) (n Nat) (x@6325 Nat)) (=> (and (= x@6324 (P x@6325)) (= x (P m)) (= y (P n)) (plus x@6325 m n)) (plus2 x@6324 x y))))
(assert (forall ((x@6324 Integer) (x Integer) (y Integer) (m Nat) (o Nat) (x@6327 Integer) (x@6326 Nat)) (=> (and (= x@6324 x@6327) (= x (P m)) (= y (N o)) (|-2| x@6327 m x@6326) (plus x@6326 (succ zero) o)) (plus2 x@6324 x y))))
(assert (forall ((x@6324 Integer) (x Integer) (y Integer) (m2 Nat) (n2 Nat) (x@6329 Integer) (x@6328 Nat)) (=> (and (= x@6324 x@6329) (= x (N m2)) (= y (P n2)) (|-2| x@6329 n2 x@6328) (plus x@6328 (succ zero) m2)) (plus2 x@6324 x y))))
(assert (forall ((x@6324 Integer) (x Integer) (y Integer) (m2 Nat) (n3 Nat) (x@6331 Nat) (x@6330 Nat)) (=> (and (= x@6324 (N x@6331)) (= x (N m2)) (= y (N n3)) (plus x@6331 x@6330 n3) (plus x@6330 (succ zero) m2)) (plus2 x@6324 x y))))
(assert (forall ((x Integer) (y Integer) (z Integer)) (forall ((x@6336 Integer) (x@6335 Integer) (x@6334 Integer) (x@6333 Integer) (x@6332 Integer)) (=> (and (diseqInteger@0 x@6336 x@6334) (times x@6336 x@6335 z) (plus2 x@6335 x y) (plus2 x@6334 x@6333 x@6332) (times x@6333 x z) (times x@6332 y z)) false))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@6340 Nat) (x@6339 Nat) (x@6338 Nat) (x@6337 Nat)) (=> (and (diseqNat@58 x@6340 x@6338) (times2 x@6340 x x@6339) (times2 x@6339 y z) (times2 x@6338 x@6337 z) (times2 x@6337 x y)) false))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@6344 Nat) (x@6343 Nat) (x@6342 Nat) (x@6341 Nat)) (=> (and (diseqNat@58 x@6344 x@6342) (plus x@6344 x x@6343) (plus x@6343 y z) (plus x@6342 x@6341 z) (plus x@6341 x y)) false))))
(assert (forall ((x Nat) (y Nat)) (forall ((x@6346 Nat) (x@6345 Nat)) (=> (and (diseqNat@58 x@6346 x@6345) (times2 x@6346 x y) (times2 x@6345 y x)) false))))
(assert (forall ((x Nat) (y Nat)) (forall ((x@6348 Nat) (x@6347 Nat)) (=> (and (diseqNat@58 x@6348 x@6347) (plus x@6348 x y) (plus x@6347 y x)) false))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@6353 Nat) (x@6352 Nat) (x@6351 Nat) (x@6350 Nat) (x@6349 Nat)) (=> (and (diseqNat@58 x@6353 x@6351) (times2 x@6353 x x@6352) (plus x@6352 y z) (plus x@6351 x@6350 x@6349) (times2 x@6350 x y) (times2 x@6349 x z)) false))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@6358 Nat) (x@6357 Nat) (x@6356 Nat) (x@6355 Nat) (x@6354 Nat)) (=> (and (diseqNat@58 x@6358 x@6356) (times2 x@6358 x@6357 z) (plus x@6357 x y) (plus x@6356 x@6355 x@6354) (times2 x@6355 x z) (times2 x@6354 y z)) false))))
(assert (forall ((x Nat)) (forall ((x@6359 Nat)) (=> (and (diseqNat@58 x@6359 x) (times2 x@6359 x (succ zero))) false))))
(assert (forall ((x Nat)) (forall ((x@6360 Nat)) (=> (and (diseqNat@58 x@6360 x) (times2 x@6360 (succ zero) x)) false))))
(assert (forall ((x Nat)) (forall ((x@6361 Nat)) (=> (and (diseqNat@58 x@6361 x) (plus x@6361 x zero)) false))))
(assert (forall ((x Nat)) (forall ((x@6362 Nat)) (=> (and (diseqNat@58 x@6362 x) (plus x@6362 zero x)) false))))
(assert (forall ((x Nat)) (forall ((x@6363 Nat)) (=> (and (diseqNat@58 x@6363 zero) (times2 x@6363 x zero)) false))))
(assert (forall ((x Nat)) (forall ((x@6364 Nat)) (=> (and (diseqNat@58 x@6364 zero) (times2 x@6364 zero x)) false))))
(check-sat)
(get-info :reason-unknown)
