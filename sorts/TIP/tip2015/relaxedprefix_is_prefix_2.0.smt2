(set-logic ALL)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-sort Nat@0 0)
(declare-fun Z@0 () Nat@0)
(declare-fun S@0 (Nat@0) Nat@0)
(declare-fun diseqNat@0@105 (Nat@0 Nat@0) Bool)
(assert (forall ((x@6290 Nat@0)) (diseqNat@0@105 Z@0 (S@0 x@6290))))
(assert (forall ((x@6291 Nat@0)) (diseqNat@0@105 (S@0 x@6291) Z@0)))
(assert (forall ((x@6292 Nat@0) (x@6293 Nat@0)) (=> (diseqNat@0@105 x@6292 x@6293) (diseqNat@0@105 (S@0 x@6292) (S@0 x@6293)))))
(declare-sort It 0)
(declare-fun A () It)
(declare-fun B () It)
(declare-fun C () It)
(declare-fun diseqIt@0 (It It) Bool)
(assert (diseqIt@0 A B))
(assert (diseqIt@0 A C))
(assert (diseqIt@0 B A))
(assert (diseqIt@0 C A))
(assert (diseqIt@0 B C))
(assert (diseqIt@0 C B))
(declare-sort list 0)
(declare-fun nil () list)
(declare-fun cons (It list) list)
(declare-fun diseqlist@96 (list list) Bool)
(assert (forall ((x@6294 It) (x@6295 list)) (diseqlist@96 nil (cons x@6294 x@6295))))
(assert (forall ((x@6296 It) (x@6297 list)) (diseqlist@96 (cons x@6296 x@6297) nil)))
(assert (forall ((x@6298 It) (x@6299 list) (x@6300 It) (x@6301 list)) (=> (diseqIt@0 x@6298 x@6300) (diseqlist@96 (cons x@6298 x@6299) (cons x@6300 x@6301)))))
(assert (forall ((x@6298 It) (x@6299 list) (x@6300 It) (x@6301 list)) (=> (diseqlist@96 x@6299 x@6301) (diseqlist@96 (cons x@6298 x@6299) (cons x@6300 x@6301)))))
(declare-fun isPrefix (Bool list list) Bool)
(assert (forall ((x@6278 Bool) (x list) (y list)) (=> (and (= x@6278 true) (= x nil)) (isPrefix x@6278 x y))))
(assert (forall ((x@6278 Bool) (x list) (y list) (z It) (x2 list)) (=> (and (= x@6278 false) (= x (cons z x2)) (= y nil)) (isPrefix x@6278 x y))))
(assert (forall ((x@6278 Bool) (x list) (y list) (z It) (x2 list) (x3 It) (x4 list) (x@6279 Bool)) (=> (and (= x@6278 (and (= z x3) x@6279)) (= x (cons z x2)) (= y (cons x3 x4)) (isPrefix x@6279 x2 x4)) (isPrefix x@6278 x y))))
(declare-fun isRelaxedPrefix (Bool list list) Bool)
(assert (forall ((x@6280 Bool) (x list) (y list)) (=> (and (= x@6280 true) (= x nil)) (isRelaxedPrefix x@6280 x y))))
(assert (forall ((x@6280 Bool) (x list) (y list) (z It) (x2 list)) (=> (and (= x@6280 true) (= x (cons z x2)) (= x2 nil)) (isRelaxedPrefix x@6280 x y))))
(assert (forall ((x@6280 Bool) (x list) (y list) (z It) (x2 list) (x3 It) (x4 list)) (=> (and (= x@6280 false) (= x (cons z x2)) (= x2 (cons x3 x4)) (= y nil)) (isRelaxedPrefix x@6280 x y))))
(assert (forall ((x@6280 Bool) (x list) (y list) (z It) (x2 list) (x3 It) (x4 list) (x5 It) (x6 list) (x@6281 Bool) (x@6282 Bool)) (=> (and (= x@6280 (ite (= z x5) x@6281 x@6282)) (= x (cons z x2)) (= x2 (cons x3 x4)) (= y (cons x5 x6)) (isRelaxedPrefix x@6281 (cons x3 x4) x6) (isPrefix x@6282 (cons x3 x4) (cons x5 x6))) (isRelaxedPrefix x@6280 x y))))
(declare-fun ++ (list list list) Bool)
(assert (forall ((x@6283 list) (x list) (y list)) (=> (and (= x@6283 y) (= x nil)) (++ x@6283 x y))))
(assert (forall ((x@6283 list) (x list) (y list) (z It) (xs list) (x@6284 list)) (=> (and (= x@6283 (cons z x@6284)) (= x (cons z xs)) (++ x@6284 xs y)) (++ x@6283 x y))))
(assert (forall ((x It) (xs list) (ys list) (zs list)) (forall ((x@6289 Bool) (x@6288 list) (x@6287 list) (x@6286 list) (x@6285 list)) (=> (and (not x@6289) (isRelaxedPrefix x@6289 x@6288 x@6286) (++ x@6288 xs x@6287) (++ x@6287 (cons x nil) ys) (++ x@6286 xs x@6285) (++ x@6285 ys zs)) false))))
(check-sat)
(get-info :reason-unknown)
