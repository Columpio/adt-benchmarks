(set-logic ALL)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-sort Nat@0 0)
(declare-fun Z@0 () Nat@0)
(declare-fun S@0 (Nat@0) Nat@0)
(declare-fun diseqNat@0@215 (Nat@0 Nat@0) Bool)
(assert (forall ((x@11064 Nat@0)) (diseqNat@0@215 Z@0 (S@0 x@11064))))
(assert (forall ((x@11065 Nat@0)) (diseqNat@0@215 (S@0 x@11065) Z@0)))
(assert (forall ((x@11066 Nat@0) (x@11067 Nat@0)) (=> (diseqNat@0@215 x@11066 x@11067) (diseqNat@0@215 (S@0 x@11066) (S@0 x@11067)))))
(declare-sort Nat 0)
(declare-fun zero () Nat)
(declare-fun succ (Nat) Nat)
(declare-fun diseqNat@141 (Nat Nat) Bool)
(assert (forall ((x@11068 Nat)) (diseqNat@141 zero (succ x@11068))))
(assert (forall ((x@11069 Nat)) (diseqNat@141 (succ x@11069) zero)))
(assert (forall ((x@11070 Nat) (x@11071 Nat)) (=> (diseqNat@141 x@11070 x@11071) (diseqNat@141 (succ x@11070) (succ x@11071)))))
(declare-sort Tree 0)
(declare-fun TNode (Tree Nat Tree) Tree)
(declare-fun TNil () Tree)
(declare-fun diseqTree@4 (Tree Tree) Bool)
(assert (forall ((x@11072 Tree) (x@11073 Nat) (x@11074 Tree)) (diseqTree@4 (TNode x@11072 x@11073 x@11074) TNil)))
(assert (forall ((x@11075 Tree) (x@11076 Nat) (x@11077 Tree)) (diseqTree@4 TNil (TNode x@11075 x@11076 x@11077))))
(assert (forall ((x@11078 Tree) (x@11079 Nat) (x@11080 Tree) (x@11081 Tree) (x@11082 Nat) (x@11083 Tree)) (=> (diseqTree@4 x@11078 x@11081) (diseqTree@4 (TNode x@11078 x@11079 x@11080) (TNode x@11081 x@11082 x@11083)))))
(assert (forall ((x@11078 Tree) (x@11079 Nat) (x@11080 Tree) (x@11081 Tree) (x@11082 Nat) (x@11083 Tree)) (=> (diseqNat@141 x@11079 x@11082) (diseqTree@4 (TNode x@11078 x@11079 x@11080) (TNode x@11081 x@11082 x@11083)))))
(assert (forall ((x@11078 Tree) (x@11079 Nat) (x@11080 Tree) (x@11081 Tree) (x@11082 Nat) (x@11083 Tree)) (=> (diseqTree@4 x@11080 x@11083) (diseqTree@4 (TNode x@11078 x@11079 x@11080) (TNode x@11081 x@11082 x@11083)))))
(declare-sort list 0)
(declare-fun nil () list)
(declare-fun cons (Nat list) list)
(declare-fun diseqlist@138 (list list) Bool)
(assert (forall ((x@11084 Nat) (x@11085 list)) (diseqlist@138 nil (cons x@11084 x@11085))))
(assert (forall ((x@11086 Nat) (x@11087 list)) (diseqlist@138 (cons x@11086 x@11087) nil)))
(assert (forall ((x@11088 Nat) (x@11089 list) (x@11090 Nat) (x@11091 list)) (=> (diseqNat@141 x@11088 x@11090) (diseqlist@138 (cons x@11088 x@11089) (cons x@11090 x@11091)))))
(assert (forall ((x@11088 Nat) (x@11089 list) (x@11090 Nat) (x@11091 list)) (=> (diseqlist@138 x@11089 x@11091) (diseqlist@138 (cons x@11088 x@11089) (cons x@11090 x@11091)))))
(declare-fun plus (Nat Nat Nat) Bool)
(assert (forall ((x@11032 Nat) (x Nat) (y Nat)) (=> (and (= x@11032 y) (= x zero)) (plus x@11032 x y))))
(assert (forall ((x@11032 Nat) (x Nat) (y Nat) (z Nat) (x@11033 Nat)) (=> (and (= x@11032 (succ x@11033)) (= x (succ z)) (plus x@11033 z y)) (plus x@11032 x y))))
(declare-fun leq (Bool Nat Nat) Bool)
(assert (forall ((x@11034 Bool) (x Nat) (y Nat)) (=> (and (= x@11034 true) (= x zero)) (leq x@11034 x y))))
(assert (forall ((x@11034 Bool) (x Nat) (y Nat) (z Nat)) (=> (and (= x@11034 false) (= x (succ z)) (= y zero)) (leq x@11034 x y))))
(assert (forall ((x@11034 Bool) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@11035 Bool)) (=> (and (= x@11034 x@11035) (= x (succ z)) (= y (succ x2)) (leq x@11035 z x2)) (leq x@11034 x y))))
(declare-fun flatten (list Tree list) Bool)
(assert (forall ((x@11036 list) (x Tree) (y list) (q Tree) (z Nat) (r Tree) (x@11038 list) (x@11037 list)) (=> (and (= x@11036 x@11038) (= x (TNode q z r)) (flatten x@11038 q (cons z x@11037)) (flatten x@11037 r y)) (flatten x@11036 x y))))
(assert (forall ((x@11036 list) (x Tree) (y list)) (=> (and (= x@11036 y) (= x TNil)) (flatten x@11036 x y))))
(declare-fun count (Nat Nat list) Bool)
(assert (forall ((x@11039 Nat) (x Nat) (y list)) (=> (and (= x@11039 zero) (= y nil)) (count x@11039 x y))))
(assert (forall ((x@11039 Nat) (x Nat) (y list) (z Nat) (ys list) (x@11041 Nat) (x@11040 Nat) (x@11042 Nat)) (=> (and (= x@11039 (ite (= x z) x@11041 x@11042)) (= y (cons z ys)) (plus x@11041 (succ zero) x@11040) (count x@11040 x ys) (count x@11042 x ys)) (count x@11039 x y))))
(declare-fun add (Tree Nat Tree) Bool)
(assert (forall ((x@11043 Tree) (x Nat) (y Tree) (q Tree) (z Nat) (r Tree) (x@11044 Bool) (x@11045 Tree) (x@11046 Tree)) (=> (and (= x@11043 (ite x@11044 (TNode x@11045 z r) (TNode q z x@11046))) (= y (TNode q z r)) (leq x@11044 x z) (add x@11045 x q) (add x@11046 x r)) (add x@11043 x y))))
(assert (forall ((x@11043 Tree) (x Nat) (y Tree)) (=> (and (= x@11043 (TNode TNil x TNil)) (= y TNil)) (add x@11043 x y))))
(declare-fun toTree (Tree list) Bool)
(assert (forall ((x@11047 Tree) (x list)) (=> (and (= x@11047 TNil) (= x nil)) (toTree x@11047 x))))
(assert (forall ((x@11047 Tree) (x list) (y Nat) (xs list) (x@11049 Tree) (x@11048 Tree)) (=> (and (= x@11047 x@11049) (= x (cons y xs)) (add x@11049 y x@11048) (toTree x@11048 xs)) (toTree x@11047 x))))
(declare-fun tsort (list list) Bool)
(assert (forall ((x@11050 list) (x list) (x@11052 list) (x@11051 Tree)) (=> (and (= x@11050 x@11052) (flatten x@11052 x@11051 nil) (toTree x@11051 x)) (tsort x@11050 x))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@11056 Nat) (x@11055 Nat) (x@11054 Nat) (x@11053 Nat)) (=> (and (diseqNat@141 x@11056 x@11054) (plus x@11056 x x@11055) (plus x@11055 y z) (plus x@11054 x@11053 z) (plus x@11053 x y)) false))))
(assert (forall ((x Nat) (y Nat)) (forall ((x@11058 Nat) (x@11057 Nat)) (=> (and (diseqNat@141 x@11058 x@11057) (plus x@11058 x y) (plus x@11057 y x)) false))))
(assert (forall ((x Nat)) (forall ((x@11059 Nat)) (=> (and (diseqNat@141 x@11059 x) (plus x@11059 x zero)) false))))
(assert (forall ((x Nat)) (forall ((x@11060 Nat)) (=> (and (diseqNat@141 x@11060 x) (plus x@11060 zero x)) false))))
(assert (forall ((x Nat) (xs list)) (forall ((x@11063 Nat) (x@11062 list) (x@11061 Nat)) (=> (and (diseqNat@141 x@11063 x@11061) (count x@11063 x x@11062) (tsort x@11062 xs) (count x@11061 x xs)) false))))
(check-sat)
(get-info :reason-unknown)
