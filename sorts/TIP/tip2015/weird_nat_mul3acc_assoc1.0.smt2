(set-logic ALL)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-sort Nat@0 0)
(declare-fun Z@0 () Nat@0)
(declare-fun S@0 (Nat@0) Nat@0)
(declare-fun diseqNat@0@149 (Nat@0 Nat@0) Bool)
(assert (forall ((x@8268 Nat@0)) (diseqNat@0@149 Z@0 (S@0 x@8268))))
(assert (forall ((x@8269 Nat@0)) (diseqNat@0@149 (S@0 x@8269) Z@0)))
(assert (forall ((x@8270 Nat@0) (x@8271 Nat@0)) (=> (diseqNat@0@149 x@8270 x@8271) (diseqNat@0@149 (S@0 x@8270) (S@0 x@8271)))))
(declare-sort Nat 0)
(declare-fun zero () Nat)
(declare-fun succ (Nat) Nat)
(declare-fun diseqNat@88 (Nat Nat) Bool)
(assert (forall ((x@8272 Nat)) (diseqNat@88 zero (succ x@8272))))
(assert (forall ((x@8273 Nat)) (diseqNat@88 (succ x@8273) zero)))
(assert (forall ((x@8274 Nat) (x@8275 Nat)) (=> (diseqNat@88 x@8274 x@8275) (diseqNat@88 (succ x@8274) (succ x@8275)))))
(declare-fun plus (Nat Nat Nat) Bool)
(assert (forall ((x@8242 Nat) (x Nat) (y Nat)) (=> (and (= x@8242 y) (= x zero)) (plus x@8242 x y))))
(assert (forall ((x@8242 Nat) (x Nat) (y Nat) (z Nat) (x@8243 Nat)) (=> (and (= x@8242 (succ x@8243)) (= x (succ z)) (plus x@8243 z y)) (plus x@8242 x y))))
(declare-fun add3acc (Nat Nat Nat Nat) Bool)
(assert (forall ((x@8244 Nat) (x Nat) (y Nat) (z Nat)) (=> (and (= x@8244 z) (= x zero) (= y zero)) (add3acc x@8244 x y z))))
(assert (forall ((x@8244 Nat) (x Nat) (y Nat) (z Nat) (x3 Nat) (x@8245 Nat)) (=> (and (= x@8244 x@8245) (= x zero) (= y (succ x3)) (add3acc x@8245 zero x3 (succ z))) (add3acc x@8244 x y z))))
(assert (forall ((x@8244 Nat) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@8246 Nat)) (=> (and (= x@8244 x@8246) (= x (succ x2)) (add3acc x@8246 x2 (succ y) z)) (add3acc x@8244 x y z))))
(declare-fun mul3acc (Nat Nat Nat Nat) Bool)
(assert (forall ((x@8247 Nat) (x Nat) (y Nat) (z Nat)) (=> (and (= x@8247 zero) (= x zero)) (mul3acc x@8247 x y z))))
(assert (forall ((x@8247 Nat) (x Nat) (y Nat) (z Nat) (x2 Nat)) (=> (and (= x@8247 zero) (= x (succ x2)) (= y zero)) (mul3acc x@8247 x y z))))
(assert (forall ((x@8247 Nat) (x Nat) (y Nat) (z Nat) (x2 Nat) (x3 Nat)) (=> (and (= x@8247 zero) (= x (succ x2)) (= y (succ x3)) (= z zero)) (mul3acc x@8247 x y z))))
(assert (forall ((x@8247 Nat) (x Nat) (y Nat) (z Nat) (x2 Nat) (x3 Nat) (x4 Nat) (fail Nat) (x@8255 Nat) (x@8254 Nat) (x@8253 Nat) (x@8252 Nat) (x@8251 Nat) (x@8250 Nat) (x@8249 Nat) (x@8248 Nat)) (=> (and (= x@8247 (ite (= x2 zero) (ite (= x3 zero) (ite (= x4 zero) (succ zero) fail) fail) fail)) (= x (succ x2)) (= y (succ x3)) (= z (succ x4)) (= fail x@8255) (plus x@8255 (succ zero) x@8254) (add3acc x@8254 x@8253 x@8252 x@8248) (mul3acc x@8253 x2 x3 x4) (add3acc x@8252 x@8251 x@8250 x@8249) (mul3acc x@8251 (succ zero) x3 x4) (mul3acc x@8250 x2 (succ zero) x4) (mul3acc x@8249 x2 x3 (succ zero)) (add3acc x@8248 (succ x2) (succ x3) (succ x4))) (mul3acc x@8247 x y z))))
(assert (forall ((x1 Nat) (x2 Nat) (x3acc Nat) (x4 Nat) (x5 Nat)) (forall ((x@8259 Nat) (x@8258 Nat) (x@8257 Nat) (x@8256 Nat)) (=> (and (diseqNat@88 x@8259 x@8257) (mul3acc x@8259 x@8258 x4 x5) (mul3acc x@8258 x1 x2 x3acc) (mul3acc x@8257 x1 x2 x@8256) (mul3acc x@8256 x3acc x4 x5)) false))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@8263 Nat) (x@8262 Nat) (x@8261 Nat) (x@8260 Nat)) (=> (and (diseqNat@88 x@8263 x@8261) (plus x@8263 x x@8262) (plus x@8262 y z) (plus x@8261 x@8260 z) (plus x@8260 x y)) false))))
(assert (forall ((x Nat) (y Nat)) (forall ((x@8265 Nat) (x@8264 Nat)) (=> (and (diseqNat@88 x@8265 x@8264) (plus x@8265 x y) (plus x@8264 y x)) false))))
(assert (forall ((x Nat)) (forall ((x@8266 Nat)) (=> (and (diseqNat@88 x@8266 x) (plus x@8266 x zero)) false))))
(assert (forall ((x Nat)) (forall ((x@8267 Nat)) (=> (and (diseqNat@88 x@8267 x) (plus x@8267 zero x)) false))))
(check-sat)
(get-info :reason-unknown)
