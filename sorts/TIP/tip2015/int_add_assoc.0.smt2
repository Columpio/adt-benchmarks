(set-logic ALL)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-sort Nat@0 0)
(declare-fun Z@0 () Nat@0)
(declare-fun S@0 (Nat@0) Nat@0)
(declare-fun diseqNat@0@131 (Nat@0 Nat@0) Bool)
(assert (forall ((x@7397 Nat@0)) (diseqNat@0@131 Z@0 (S@0 x@7397))))
(assert (forall ((x@7398 Nat@0)) (diseqNat@0@131 (S@0 x@7398) Z@0)))
(assert (forall ((x@7399 Nat@0) (x@7400 Nat@0)) (=> (diseqNat@0@131 x@7399 x@7400) (diseqNat@0@131 (S@0 x@7399) (S@0 x@7400)))))
(declare-sort Nat 0)
(declare-fun zero () Nat)
(declare-fun succ (Nat) Nat)
(declare-fun diseqNat@76 (Nat Nat) Bool)
(assert (forall ((x@7401 Nat)) (diseqNat@76 zero (succ x@7401))))
(assert (forall ((x@7402 Nat)) (diseqNat@76 (succ x@7402) zero)))
(assert (forall ((x@7403 Nat) (x@7404 Nat)) (=> (diseqNat@76 x@7403 x@7404) (diseqNat@76 (succ x@7403) (succ x@7404)))))
(declare-sort Integer 0)
(declare-fun P (Nat) Integer)
(declare-fun N (Nat) Integer)
(declare-fun diseqInteger@2 (Integer Integer) Bool)
(assert (forall ((x@7405 Nat) (x@7406 Nat)) (diseqInteger@2 (P x@7405) (N x@7406))))
(assert (forall ((x@7407 Nat) (x@7408 Nat)) (diseqInteger@2 (N x@7407) (P x@7408))))
(assert (forall ((x@7409 Nat) (x@7410 Nat)) (=> (diseqNat@76 x@7409 x@7410) (diseqInteger@2 (P x@7409) (P x@7410)))))
(assert (forall ((x@7411 Nat) (x@7412 Nat)) (=> (diseqNat@76 x@7411 x@7412) (diseqInteger@2 (N x@7411) (N x@7412)))))
(declare-fun plus2 (Nat Nat Nat) Bool)
(assert (forall ((x@7373 Nat) (x Nat) (y Nat)) (=> (and (= x@7373 y) (= x zero)) (plus2 x@7373 x y))))
(assert (forall ((x@7373 Nat) (x Nat) (y Nat) (z Nat) (x@7374 Nat)) (=> (and (= x@7373 (succ x@7374)) (= x (succ z)) (plus2 x@7374 z y)) (plus2 x@7373 x y))))
(declare-fun |-2| (Integer Nat Nat) Bool)
(assert (forall ((x@7375 Integer) (x Nat) (y Nat) (fail Integer)) (=> (and (= x@7375 (P zero)) (= fail (P x)) (= y zero) (= x zero) (= y zero)) (|-2| x@7375 x y))))
(assert (forall ((x@7375 Integer) (x Nat) (y Nat) (fail Integer) (x4 Nat)) (=> (and (= x@7375 fail) (= fail (P x)) (= y zero) (= x zero) (= y (succ x4))) (|-2| x@7375 x y))))
(assert (forall ((x@7375 Integer) (x Nat) (y Nat) (fail Integer) (x3 Nat)) (=> (and (= x@7375 fail) (= fail (P x)) (= y zero) (= x (succ x3))) (|-2| x@7375 x y))))
(assert (forall ((x@7375 Integer) (x Nat) (y Nat) (fail Integer) (z Nat)) (=> (and (= x@7375 (P zero)) (= fail (N (succ z))) (= y (succ z)) (= x zero) (= x zero) (= y zero)) (|-2| x@7375 x y))))
(assert (forall ((x@7375 Integer) (x Nat) (y Nat) (fail Integer) (z Nat) (x4 Nat)) (=> (and (= x@7375 fail) (= fail (N (succ z))) (= y (succ z)) (= x zero) (= x zero) (= y (succ x4))) (|-2| x@7375 x y))))
(assert (forall ((x@7375 Integer) (x Nat) (y Nat) (fail Integer) (z Nat) (x3 Nat)) (=> (and (= x@7375 fail) (= fail (N (succ z))) (= y (succ z)) (= x zero) (= x (succ x3))) (|-2| x@7375 x y))))
(assert (forall ((x@7375 Integer) (x Nat) (y Nat) (fail Integer) (z Nat) (x2 Nat) (x@7376 Integer)) (=> (and (= x@7375 (P zero)) (= fail x@7376) (= y (succ z)) (= x (succ x2)) (|-2| x@7376 x2 z) (= x zero) (= y zero)) (|-2| x@7375 x y))))
(assert (forall ((x@7375 Integer) (x Nat) (y Nat) (fail Integer) (z Nat) (x2 Nat) (x@7376 Integer) (x4 Nat)) (=> (and (= x@7375 fail) (= fail x@7376) (= y (succ z)) (= x (succ x2)) (|-2| x@7376 x2 z) (= x zero) (= y (succ x4))) (|-2| x@7375 x y))))
(assert (forall ((x@7375 Integer) (x Nat) (y Nat) (fail Integer) (z Nat) (x2 Nat) (x@7376 Integer) (x3 Nat)) (=> (and (= x@7375 fail) (= fail x@7376) (= y (succ z)) (= x (succ x2)) (|-2| x@7376 x2 z) (= x (succ x3))) (|-2| x@7375 x y))))
(declare-fun plus (Integer Integer Integer) Bool)
(assert (forall ((x@7377 Integer) (x Integer) (y Integer) (m Nat) (n Nat) (x@7378 Nat)) (=> (and (= x@7377 (P x@7378)) (= x (P m)) (= y (P n)) (plus2 x@7378 m n)) (plus x@7377 x y))))
(assert (forall ((x@7377 Integer) (x Integer) (y Integer) (m Nat) (o Nat) (x@7380 Integer) (x@7379 Nat)) (=> (and (= x@7377 x@7380) (= x (P m)) (= y (N o)) (|-2| x@7380 m x@7379) (plus2 x@7379 (succ zero) o)) (plus x@7377 x y))))
(assert (forall ((x@7377 Integer) (x Integer) (y Integer) (m2 Nat) (n2 Nat) (x@7382 Integer) (x@7381 Nat)) (=> (and (= x@7377 x@7382) (= x (N m2)) (= y (P n2)) (|-2| x@7382 n2 x@7381) (plus2 x@7381 (succ zero) m2)) (plus x@7377 x y))))
(assert (forall ((x@7377 Integer) (x Integer) (y Integer) (m2 Nat) (n3 Nat) (x@7384 Nat) (x@7383 Nat)) (=> (and (= x@7377 (N x@7384)) (= x (N m2)) (= y (N n3)) (plus2 x@7384 x@7383 n3) (plus2 x@7383 (succ zero) m2)) (plus x@7377 x y))))
(assert (forall ((x Integer) (y Integer) (z Integer)) (forall ((x@7388 Integer) (x@7387 Integer) (x@7386 Integer) (x@7385 Integer)) (=> (and (diseqInteger@2 x@7388 x@7386) (plus x@7388 x x@7387) (plus x@7387 y z) (plus x@7386 x@7385 z) (plus x@7385 x y)) false))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@7392 Nat) (x@7391 Nat) (x@7390 Nat) (x@7389 Nat)) (=> (and (diseqNat@76 x@7392 x@7390) (plus2 x@7392 x x@7391) (plus2 x@7391 y z) (plus2 x@7390 x@7389 z) (plus2 x@7389 x y)) false))))
(assert (forall ((x Nat) (y Nat)) (forall ((x@7394 Nat) (x@7393 Nat)) (=> (and (diseqNat@76 x@7394 x@7393) (plus2 x@7394 x y) (plus2 x@7393 y x)) false))))
(assert (forall ((x Nat)) (forall ((x@7395 Nat)) (=> (and (diseqNat@76 x@7395 x) (plus2 x@7395 x zero)) false))))
(assert (forall ((x Nat)) (forall ((x@7396 Nat)) (=> (and (diseqNat@76 x@7396 x) (plus2 x@7396 zero x)) false))))
(check-sat)
(get-info :reason-unknown)
