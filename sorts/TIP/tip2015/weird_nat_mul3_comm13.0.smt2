(set-logic ALL)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-sort Nat@0 0)
(declare-fun Z@0 () Nat@0)
(declare-fun S@0 (Nat@0) Nat@0)
(declare-fun diseqNat@0@245 (Nat@0 Nat@0) Bool)
(assert (forall ((x@12505 Nat@0)) (diseqNat@0@245 Z@0 (S@0 x@12505))))
(assert (forall ((x@12506 Nat@0)) (diseqNat@0@245 (S@0 x@12506) Z@0)))
(assert (forall ((x@12507 Nat@0) (x@12508 Nat@0)) (=> (diseqNat@0@245 x@12507 x@12508) (diseqNat@0@245 (S@0 x@12507) (S@0 x@12508)))))
(declare-sort Nat 0)
(declare-fun zero () Nat)
(declare-fun succ (Nat) Nat)
(declare-fun diseqNat@166 (Nat Nat) Bool)
(assert (forall ((x@12509 Nat)) (diseqNat@166 zero (succ x@12509))))
(assert (forall ((x@12510 Nat)) (diseqNat@166 (succ x@12510) zero)))
(assert (forall ((x@12511 Nat) (x@12512 Nat)) (=> (diseqNat@166 x@12511 x@12512) (diseqNat@166 (succ x@12511) (succ x@12512)))))
(declare-fun plus (Nat Nat Nat) Bool)
(assert (forall ((x@12479 Nat) (x Nat) (y Nat)) (=> (and (= x@12479 y) (= x zero)) (plus x@12479 x y))))
(assert (forall ((x@12479 Nat) (x Nat) (y Nat) (z Nat) (x@12480 Nat)) (=> (and (= x@12479 (succ x@12480)) (= x (succ z)) (plus x@12480 z y)) (plus x@12479 x y))))
(declare-fun add3 (Nat Nat Nat Nat) Bool)
(assert (forall ((x@12481 Nat) (x Nat) (y Nat) (z Nat)) (=> (and (= x@12481 z) (= x zero) (= y zero)) (add3 x@12481 x y z))))
(assert (forall ((x@12481 Nat) (x Nat) (y Nat) (z Nat) (x3 Nat) (x@12483 Nat) (x@12482 Nat)) (=> (and (= x@12481 x@12483) (= x zero) (= y (succ x3)) (plus x@12483 (succ zero) x@12482) (add3 x@12482 zero x3 z)) (add3 x@12481 x y z))))
(assert (forall ((x@12481 Nat) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@12485 Nat) (x@12484 Nat)) (=> (and (= x@12481 x@12485) (= x (succ x2)) (plus x@12485 (succ zero) x@12484) (add3 x@12484 x2 y z)) (add3 x@12481 x y z))))
(declare-fun mul3 (Nat Nat Nat Nat) Bool)
(assert (forall ((x@12486 Nat) (x Nat) (y Nat) (z Nat)) (=> (and (= x@12486 zero) (= x zero)) (mul3 x@12486 x y z))))
(assert (forall ((x@12486 Nat) (x Nat) (y Nat) (z Nat) (x2 Nat)) (=> (and (= x@12486 zero) (= x (succ x2)) (= y zero)) (mul3 x@12486 x y z))))
(assert (forall ((x@12486 Nat) (x Nat) (y Nat) (z Nat) (x2 Nat) (x3 Nat)) (=> (and (= x@12486 zero) (= x (succ x2)) (= y (succ x3)) (= z zero)) (mul3 x@12486 x y z))))
(assert (forall ((x@12486 Nat) (x Nat) (y Nat) (z Nat) (x2 Nat) (x3 Nat) (x4 Nat) (fail Nat) (x@12494 Nat) (x@12493 Nat) (x@12492 Nat) (x@12491 Nat) (x@12490 Nat) (x@12489 Nat) (x@12488 Nat) (x@12487 Nat)) (=> (and (= x@12486 (ite (= x2 zero) (ite (= x3 zero) (ite (= x4 zero) (succ zero) fail) fail) fail)) (= x (succ x2)) (= y (succ x3)) (= z (succ x4)) (= fail x@12494) (plus x@12494 (succ zero) x@12493) (add3 x@12493 x@12492 x@12491 x@12487) (mul3 x@12492 x2 x3 x4) (add3 x@12491 x@12490 x@12489 x@12488) (mul3 x@12490 (succ zero) x3 x4) (mul3 x@12489 x2 (succ zero) x4) (mul3 x@12488 x2 x3 (succ zero)) (add3 x@12487 x2 x3 x4)) (mul3 x@12486 x y z))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@12496 Nat) (x@12495 Nat)) (=> (and (diseqNat@166 x@12496 x@12495) (mul3 x@12496 x y z) (mul3 x@12495 z y x)) false))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@12500 Nat) (x@12499 Nat) (x@12498 Nat) (x@12497 Nat)) (=> (and (diseqNat@166 x@12500 x@12498) (plus x@12500 x x@12499) (plus x@12499 y z) (plus x@12498 x@12497 z) (plus x@12497 x y)) false))))
(assert (forall ((x Nat) (y Nat)) (forall ((x@12502 Nat) (x@12501 Nat)) (=> (and (diseqNat@166 x@12502 x@12501) (plus x@12502 x y) (plus x@12501 y x)) false))))
(assert (forall ((x Nat)) (forall ((x@12503 Nat)) (=> (and (diseqNat@166 x@12503 x) (plus x@12503 x zero)) false))))
(assert (forall ((x Nat)) (forall ((x@12504 Nat)) (=> (and (diseqNat@166 x@12504 x) (plus x@12504 zero x)) false))))
(check-sat)
(get-info :reason-unknown)
