(set-logic ALL)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-sort Nat@0 0)
(declare-fun Z@0 () Nat@0)
(declare-fun S@0 (Nat@0) Nat@0)
(declare-fun diseqNat@0@143 (Nat@0 Nat@0) Bool)
(assert (forall ((x@8069 Nat@0)) (diseqNat@0@143 Z@0 (S@0 x@8069))))
(assert (forall ((x@8070 Nat@0)) (diseqNat@0@143 (S@0 x@8070) Z@0)))
(assert (forall ((x@8071 Nat@0) (x@8072 Nat@0)) (=> (diseqNat@0@143 x@8071 x@8072) (diseqNat@0@143 (S@0 x@8071) (S@0 x@8072)))))
(declare-sort Nat 0)
(declare-fun zero () Nat)
(declare-fun succ (Nat) Nat)
(declare-fun diseqNat@84 (Nat Nat) Bool)
(assert (forall ((x@8073 Nat)) (diseqNat@84 zero (succ x@8073))))
(assert (forall ((x@8074 Nat)) (diseqNat@84 (succ x@8074) zero)))
(assert (forall ((x@8075 Nat) (x@8076 Nat)) (=> (diseqNat@84 x@8075 x@8076) (diseqNat@84 (succ x@8075) (succ x@8076)))))
(declare-sort list 0)
(declare-fun nil () list)
(declare-fun cons (Nat list) list)
(declare-fun diseqlist@113 (list list) Bool)
(assert (forall ((x@8077 Nat) (x@8078 list)) (diseqlist@113 nil (cons x@8077 x@8078))))
(assert (forall ((x@8079 Nat) (x@8080 list)) (diseqlist@113 (cons x@8079 x@8080) nil)))
(assert (forall ((x@8081 Nat) (x@8082 list) (x@8083 Nat) (x@8084 list)) (=> (diseqNat@84 x@8081 x@8083) (diseqlist@113 (cons x@8081 x@8082) (cons x@8083 x@8084)))))
(assert (forall ((x@8081 Nat) (x@8082 list) (x@8083 Nat) (x@8084 list)) (=> (diseqlist@113 x@8082 x@8084) (diseqlist@113 (cons x@8081 x@8082) (cons x@8083 x@8084)))))
(declare-sort pair 0)
(declare-fun pair2 (list list) pair)
(declare-fun diseqpair@9 (pair pair) Bool)
(assert (forall ((x@8085 list) (x@8086 list) (x@8087 list) (x@8088 list)) (=> (diseqlist@113 x@8085 x@8087) (diseqpair@9 (pair2 x@8085 x@8086) (pair2 x@8087 x@8088)))))
(assert (forall ((x@8085 list) (x@8086 list) (x@8087 list) (x@8088 list)) (=> (diseqlist@113 x@8086 x@8088) (diseqpair@9 (pair2 x@8085 x@8086) (pair2 x@8087 x@8088)))))
(declare-fun take (list Nat list) Bool)
(assert (forall ((x@8010 list) (x Nat) (y list)) (=> (and (= x@8010 nil) (= x zero)) (take x@8010 x y))))
(assert (forall ((x@8010 list) (x Nat) (y list) (z Nat)) (=> (and (= x@8010 nil) (= x (succ z)) (= y nil)) (take x@8010 x y))))
(assert (forall ((x@8010 list) (x Nat) (y list) (z Nat) (z2 Nat) (xs list) (x@8011 list)) (=> (and (= x@8010 (cons z2 x@8011)) (= x (succ z)) (= y (cons z2 xs)) (take x@8011 z xs)) (take x@8010 x y))))
(declare-fun plus (Nat Nat Nat) Bool)
(assert (forall ((x@8012 Nat) (x Nat) (y Nat)) (=> (and (= x@8012 y) (= x zero)) (plus x@8012 x y))))
(assert (forall ((x@8012 Nat) (x Nat) (y Nat) (z Nat) (x@8013 Nat)) (=> (and (= x@8012 (succ x@8013)) (= x (succ z)) (plus x@8013 z y)) (plus x@8012 x y))))
(declare-fun minus (Nat Nat Nat) Bool)
(assert (forall ((x@8014 Nat) (x Nat) (y Nat)) (=> (and (= x@8014 zero) (= x zero)) (minus x@8014 x y))))
(assert (forall ((x@8014 Nat) (x Nat) (y Nat) (z Nat)) (=> (and (= x@8014 zero) (= x (succ z)) (= y zero)) (minus x@8014 x y))))
(assert (forall ((x@8014 Nat) (x Nat) (y Nat) (z Nat) (y2 Nat) (x@8015 Nat)) (=> (and (= x@8014 x@8015) (= x (succ z)) (= y (succ y2)) (minus x@8015 z y2)) (minus x@8014 x y))))
(declare-fun third (Nat Nat) Bool)
(assert (forall ((x@8016 Nat) (x Nat)) (=> (and (= x@8016 (ite (= x (succ (succ zero))) zero (ite (= x (succ zero)) zero zero))) (= x zero)) (third x@8016 x))))
(assert (forall ((x@8016 Nat) (x Nat) (y Nat) (x@8019 Nat) (x@8018 Nat) (x@8017 Nat)) (=> (and (= x@8016 (ite (= x (succ (succ zero))) zero (ite (= x (succ zero)) zero x@8019))) (= x (succ y)) (plus x@8019 (succ zero) x@8018) (third x@8018 x@8017) (minus x@8017 (succ y) (succ (succ (succ zero))))) (third x@8016 x))))
(declare-fun leq (Bool Nat Nat) Bool)
(assert (forall ((x@8020 Bool) (x Nat) (y Nat)) (=> (and (= x@8020 true) (= x zero)) (leq x@8020 x y))))
(assert (forall ((x@8020 Bool) (x Nat) (y Nat) (z Nat)) (=> (and (= x@8020 false) (= x (succ z)) (= y zero)) (leq x@8020 x y))))
(assert (forall ((x@8020 Bool) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@8021 Bool)) (=> (and (= x@8020 x@8021) (= x (succ z)) (= y (succ x2)) (leq x@8021 z x2)) (leq x@8020 x y))))
(declare-fun ordered (Bool list) Bool)
(assert (forall ((x@8022 Bool) (x list)) (=> (and (= x@8022 true) (= x nil)) (ordered x@8022 x))))
(assert (forall ((x@8022 Bool) (x list) (y Nat) (z list)) (=> (and (= x@8022 true) (= x (cons y z)) (= z nil)) (ordered x@8022 x))))
(assert (forall ((x@8022 Bool) (x list) (y Nat) (z list) (y2 Nat) (xs list) (x@8024 Bool) (x@8023 Bool)) (=> (and (= x@8022 (and x@8024 x@8023)) (= x (cons y z)) (= z (cons y2 xs)) (leq x@8024 y y2) (ordered x@8023 (cons y2 xs))) (ordered x@8022 x))))
(declare-fun sort2 (list Nat Nat) Bool)
(assert (forall ((x@8025 list) (x Nat) (y Nat) (x@8026 Bool)) (=> (and (= x@8025 (ite x@8026 (cons x (cons y nil)) (cons y (cons x nil)))) (leq x@8026 x y)) (sort2 x@8025 x y))))
(declare-fun length (Nat list) Bool)
(assert (forall ((x@8027 Nat) (x list)) (=> (and (= x@8027 zero) (= x nil)) (length x@8027 x))))
(assert (forall ((x@8027 Nat) (x list) (y Nat) (l list) (x@8029 Nat) (x@8028 Nat)) (=> (and (= x@8027 x@8029) (= x (cons y l)) (plus x@8029 (succ zero) x@8028) (length x@8028 l)) (length x@8027 x))))
(declare-fun drop (list Nat list) Bool)
(assert (forall ((x@8030 list) (x Nat) (y list)) (=> (and (= x@8030 y) (= x zero)) (drop x@8030 x y))))
(assert (forall ((x@8030 list) (x Nat) (y list) (z Nat)) (=> (and (= x@8030 nil) (= x (succ z)) (= y nil)) (drop x@8030 x y))))
(assert (forall ((x@8030 list) (x Nat) (y list) (z Nat) (z2 Nat) (xs1 list) (x@8031 list)) (=> (and (= x@8030 x@8031) (= x (succ z)) (= y (cons z2 xs1)) (drop x@8031 z xs1)) (drop x@8030 x y))))
(declare-fun splitAt (pair Nat list) Bool)
(assert (forall ((x@8032 pair) (x Nat) (y list) (x@8034 list) (x@8033 list)) (=> (and (= x@8032 (pair2 x@8034 x@8033)) (take x@8034 x y) (drop x@8033 x y)) (splitAt x@8032 x y))))
(declare-fun ++ (list list list) Bool)
(assert (forall ((x@8035 list) (x list) (y list)) (=> (and (= x@8035 y) (= x nil)) (++ x@8035 x y))))
(assert (forall ((x@8035 list) (x list) (y list) (z Nat) (xs list) (x@8036 list)) (=> (and (= x@8035 (cons z x@8036)) (= x (cons z xs)) (++ x@8036 xs y)) (++ x@8035 x y))))
(declare-fun reverse (list list) Bool)
(assert (forall ((x@8037 list) (x list)) (=> (and (= x@8037 nil) (= x nil)) (reverse x@8037 x))))
(assert (forall ((x@8037 list) (x list) (y Nat) (xs list) (x@8039 list) (x@8038 list)) (=> (and (= x@8037 x@8039) (= x (cons y xs)) (++ x@8039 x@8038 (cons y nil)) (reverse x@8038 xs)) (reverse x@8037 x))))
(declare-fun nstooge1sort2 (list list) Bool)
(declare-fun nstoogesort (list list) Bool)
(declare-fun nstooge1sort1 (list list) Bool)
(assert (forall ((x@8040 list) (x list) (x@8044 pair) (x@8043 Nat) (x@8042 Nat) (x@8041 list) (ys1 list) (zs1 list) (x@8047 list) (x@8046 list) (x@8045 list)) (=> (and (= x@8040 x@8047) (= x@8044 (pair2 ys1 zs1)) (splitAt x@8044 x@8043 x@8041) (third x@8043 x@8042) (length x@8042 x) (reverse x@8041 x) (++ x@8047 x@8046 x@8045) (nstoogesort x@8046 zs1) (reverse x@8045 ys1)) (nstooge1sort2 x@8040 x))))
(assert (forall ((x@8048 list) (x list)) (=> (and (= x@8048 nil) (= x nil)) (nstoogesort x@8048 x))))
(assert (forall ((x@8048 list) (x list) (y Nat) (z list)) (=> (and (= x@8048 (cons y nil)) (= x (cons y z)) (= z nil)) (nstoogesort x@8048 x))))
(assert (forall ((x@8048 list) (x list) (y Nat) (z list) (y2 Nat) (x2 list) (x@8049 list)) (=> (and (= x@8048 x@8049) (= x (cons y z)) (= z (cons y2 x2)) (= x2 nil) (sort2 x@8049 y y2)) (nstoogesort x@8048 x))))
(assert (forall ((x@8048 list) (x list) (y Nat) (z list) (y2 Nat) (x2 list) (x3 Nat) (x4 list) (x@8052 list) (x@8051 list) (x@8050 list)) (=> (and (= x@8048 x@8052) (= x (cons y z)) (= z (cons y2 x2)) (= x2 (cons x3 x4)) (nstooge1sort2 x@8052 x@8051) (nstooge1sort1 x@8051 x@8050) (nstooge1sort2 x@8050 (cons y (cons y2 (cons x3 x4))))) (nstoogesort x@8048 x))))
(assert (forall ((x@8053 list) (x list) (x@8056 pair) (x@8055 Nat) (x@8054 Nat) (ys1 list) (zs list) (x@8058 list) (x@8057 list)) (=> (and (= x@8053 x@8058) (= x@8056 (pair2 ys1 zs)) (splitAt x@8056 x@8055 x) (third x@8055 x@8054) (length x@8054 x) (++ x@8058 ys1 x@8057) (nstoogesort x@8057 zs)) (nstooge1sort1 x@8053 x))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@8062 Nat) (x@8061 Nat) (x@8060 Nat) (x@8059 Nat)) (=> (and (diseqNat@84 x@8062 x@8060) (plus x@8062 x x@8061) (plus x@8061 y z) (plus x@8060 x@8059 z) (plus x@8059 x y)) false))))
(assert (forall ((x Nat) (y Nat)) (forall ((x@8064 Nat) (x@8063 Nat)) (=> (and (diseqNat@84 x@8064 x@8063) (plus x@8064 x y) (plus x@8063 y x)) false))))
(assert (forall ((x Nat)) (forall ((x@8065 Nat)) (=> (and (diseqNat@84 x@8065 x) (plus x@8065 x zero)) false))))
(assert (forall ((x Nat)) (forall ((x@8066 Nat)) (=> (and (diseqNat@84 x@8066 x) (plus x@8066 zero x)) false))))
(assert (forall ((xs list)) (forall ((x@8068 Bool) (x@8067 list)) (=> (and (not x@8068) (ordered x@8068 x@8067) (nstoogesort x@8067 xs)) false))))
(check-sat)
(get-info :reason-unknown)
