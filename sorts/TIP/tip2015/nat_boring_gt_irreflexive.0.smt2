(set-logic ALL)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-sort Nat@0 0)
(declare-fun Z@0 () Nat@0)
(declare-fun S@0 (Nat@0) Nat@0)
(declare-fun diseqNat@0@188 (Nat@0 Nat@0) Bool)
(assert (forall ((x@9940 Nat@0)) (diseqNat@0@188 Z@0 (S@0 x@9940))))
(assert (forall ((x@9941 Nat@0)) (diseqNat@0@188 (S@0 x@9941) Z@0)))
(assert (forall ((x@9942 Nat@0) (x@9943 Nat@0)) (=> (diseqNat@0@188 x@9942 x@9943) (diseqNat@0@188 (S@0 x@9942) (S@0 x@9943)))))
(declare-sort Nat 0)
(declare-fun zero () Nat)
(declare-fun succ (Nat) Nat)
(declare-fun diseqNat@118 (Nat Nat) Bool)
(assert (forall ((x@9944 Nat)) (diseqNat@118 zero (succ x@9944))))
(assert (forall ((x@9945 Nat)) (diseqNat@118 (succ x@9945) zero)))
(assert (forall ((x@9946 Nat) (x@9947 Nat)) (=> (diseqNat@118 x@9946 x@9947) (diseqNat@118 (succ x@9946) (succ x@9947)))))
(declare-fun lt (Bool Nat Nat) Bool)
(assert (forall ((x@9935 Bool) (x Nat) (y Nat)) (=> (and (= x@9935 false) (= y zero)) (lt x@9935 x y))))
(assert (forall ((x@9935 Bool) (x Nat) (y Nat) (z Nat)) (=> (and (= x@9935 true) (= y (succ z)) (= x zero)) (lt x@9935 x y))))
(assert (forall ((x@9935 Bool) (x Nat) (y Nat) (z Nat) (n Nat) (x@9936 Bool)) (=> (and (= x@9935 x@9936) (= y (succ z)) (= x (succ n)) (lt x@9936 n z)) (lt x@9935 x y))))
(declare-fun gt (Bool Nat Nat) Bool)
(assert (forall ((x@9937 Bool) (x Nat) (y Nat) (x@9938 Bool)) (=> (and (= x@9937 x@9938) (lt x@9938 y x)) (gt x@9937 x y))))
(assert (forall ((x Nat)) (forall ((x@9939 Bool)) (=> (and x@9939 (gt x@9939 x x)) false))))
(check-sat)
(get-info :reason-unknown)
