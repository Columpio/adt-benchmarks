(set-logic ALL)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-sort Nat@0 0)
(declare-fun Z@0 () Nat@0)
(declare-fun S@0 (Nat@0) Nat@0)
(declare-fun diseqNat@0@217 (Nat@0 Nat@0) Bool)
(assert (forall ((x@11162 Nat@0)) (diseqNat@0@217 Z@0 (S@0 x@11162))))
(assert (forall ((x@11163 Nat@0)) (diseqNat@0@217 (S@0 x@11163) Z@0)))
(assert (forall ((x@11164 Nat@0) (x@11165 Nat@0)) (=> (diseqNat@0@217 x@11164 x@11165) (diseqNat@0@217 (S@0 x@11164) (S@0 x@11165)))))
(declare-sort Nat 0)
(declare-fun zero () Nat)
(declare-fun succ (Nat) Nat)
(declare-fun diseqNat@143 (Nat Nat) Bool)
(assert (forall ((x@11166 Nat)) (diseqNat@143 zero (succ x@11166))))
(assert (forall ((x@11167 Nat)) (diseqNat@143 (succ x@11167) zero)))
(assert (forall ((x@11168 Nat) (x@11169 Nat)) (=> (diseqNat@143 x@11168 x@11169) (diseqNat@143 (succ x@11168) (succ x@11169)))))
(declare-fun leq (Bool Nat Nat) Bool)
(assert (forall ((x@11159 Bool) (x Nat) (y Nat)) (=> (and (= x@11159 true) (= x zero)) (leq x@11159 x y))))
(assert (forall ((x@11159 Bool) (x Nat) (y Nat) (z Nat)) (=> (and (= x@11159 false) (= x (succ z)) (= y zero)) (leq x@11159 x y))))
(assert (forall ((x@11159 Bool) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@11160 Bool)) (=> (and (= x@11159 x@11160) (= x (succ z)) (= y (succ x2)) (leq x@11160 z x2)) (leq x@11159 x y))))
(assert (forall ((x Nat)) (forall ((x@11161 Bool)) (=> (and (not x@11161) (leq x@11161 x x)) false))))
(check-sat)
(get-info :reason-unknown)
