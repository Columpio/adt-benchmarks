(set-logic ALL)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-sort Nat@0 0)
(declare-fun Z@0 () Nat@0)
(declare-fun S@0 (Nat@0) Nat@0)
(declare-fun diseqNat@0@228 (Nat@0 Nat@0) Bool)
(assert (forall ((x@11658 Nat@0)) (diseqNat@0@228 Z@0 (S@0 x@11658))))
(assert (forall ((x@11659 Nat@0)) (diseqNat@0@228 (S@0 x@11659) Z@0)))
(assert (forall ((x@11660 Nat@0) (x@11661 Nat@0)) (=> (diseqNat@0@228 x@11660 x@11661) (diseqNat@0@228 (S@0 x@11660) (S@0 x@11661)))))
(declare-sort Nat 0)
(declare-fun zero () Nat)
(declare-fun succ (Nat) Nat)
(declare-fun diseqNat@150 (Nat Nat) Bool)
(assert (forall ((x@11662 Nat)) (diseqNat@150 zero (succ x@11662))))
(assert (forall ((x@11663 Nat)) (diseqNat@150 (succ x@11663) zero)))
(assert (forall ((x@11664 Nat) (x@11665 Nat)) (=> (diseqNat@150 x@11664 x@11665) (diseqNat@150 (succ x@11664) (succ x@11665)))))
(declare-fun leq (Bool Nat Nat) Bool)
(assert (forall ((x@11651 Bool) (x Nat) (y Nat)) (=> (and (= x@11651 true) (= x zero)) (leq x@11651 x y))))
(assert (forall ((x@11651 Bool) (x Nat) (y Nat) (z Nat)) (=> (and (= x@11651 false) (= x (succ z)) (= y zero)) (leq x@11651 x y))))
(assert (forall ((x@11651 Bool) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@11652 Bool)) (=> (and (= x@11651 x@11652) (= x (succ z)) (= y (succ x2)) (leq x@11652 z x2)) (leq x@11651 x y))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((y2 Nat) (x@11653 Bool) (x2 Nat) (x@11654 Bool) (y3 Nat) (x@11655 Bool) (x@11657 Bool) (x@11656 Bool)) (=> (and (diseqNat@150 (ite x@11657 y2 x) (ite x@11656 x2 y3)) (= y2 (ite x@11653 y z)) (leq x@11653 y z) (= x2 (ite x@11654 y x)) (leq x@11654 x y) (= y3 (ite x@11655 z x)) (leq x@11655 x z) (leq x@11657 x y2) (leq x@11656 x2 y3)) false))))
(check-sat)
(get-info :reason-unknown)
