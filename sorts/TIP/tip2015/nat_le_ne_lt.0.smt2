(set-logic ALL)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-sort Nat@0 0)
(declare-fun Z@0 () Nat@0)
(declare-fun S@0 (Nat@0) Nat@0)
(declare-fun diseqNat@0@266 (Nat@0 Nat@0) Bool)
(assert (forall ((x@13636 Nat@0)) (diseqNat@0@266 Z@0 (S@0 x@13636))))
(assert (forall ((x@13637 Nat@0)) (diseqNat@0@266 (S@0 x@13637) Z@0)))
(assert (forall ((x@13638 Nat@0) (x@13639 Nat@0)) (=> (diseqNat@0@266 x@13638 x@13639) (diseqNat@0@266 (S@0 x@13638) (S@0 x@13639)))))
(declare-sort Nat 0)
(declare-fun zero () Nat)
(declare-fun succ (Nat) Nat)
(declare-fun diseqNat@181 (Nat Nat) Bool)
(assert (forall ((x@13640 Nat)) (diseqNat@181 zero (succ x@13640))))
(assert (forall ((x@13641 Nat)) (diseqNat@181 (succ x@13641) zero)))
(assert (forall ((x@13642 Nat) (x@13643 Nat)) (=> (diseqNat@181 x@13642 x@13643) (diseqNat@181 (succ x@13642) (succ x@13643)))))
(declare-fun lt (Bool Nat Nat) Bool)
(assert (forall ((x@13630 Bool) (x Nat) (y Nat)) (=> (and (= x@13630 false) (= y zero)) (lt x@13630 x y))))
(assert (forall ((x@13630 Bool) (x Nat) (y Nat) (z Nat)) (=> (and (= x@13630 true) (= y (succ z)) (= x zero)) (lt x@13630 x y))))
(assert (forall ((x@13630 Bool) (x Nat) (y Nat) (z Nat) (n Nat) (x@13631 Bool)) (=> (and (= x@13630 x@13631) (= y (succ z)) (= x (succ n)) (lt x@13631 n z)) (lt x@13630 x y))))
(declare-fun leq (Bool Nat Nat) Bool)
(assert (forall ((x@13632 Bool) (x Nat) (y Nat)) (=> (and (= x@13632 true) (= x zero)) (leq x@13632 x y))))
(assert (forall ((x@13632 Bool) (x Nat) (y Nat) (z Nat)) (=> (and (= x@13632 false) (= x (succ z)) (= y zero)) (leq x@13632 x y))))
(assert (forall ((x@13632 Bool) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@13633 Bool)) (=> (and (= x@13632 x@13633) (= x (succ z)) (= y (succ x2)) (leq x@13633 z x2)) (leq x@13632 x y))))
(assert (forall ((x Nat) (y Nat)) (forall ((x@13634 Bool) (x@13635 Bool)) (=> (and (not x@13635) x@13634 (leq x@13634 x y) (diseqNat@181 x y) (lt x@13635 x y)) false))))
(check-sat)
(get-info :reason-unknown)
