(set-logic ALL)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-sort Nat@0 0)
(declare-fun Z@0 () Nat@0)
(declare-fun S@0 (Nat@0) Nat@0)
(declare-fun diseqNat@0@237 (Nat@0 Nat@0) Bool)
(assert (forall ((x@12151 Nat@0)) (diseqNat@0@237 Z@0 (S@0 x@12151))))
(assert (forall ((x@12152 Nat@0)) (diseqNat@0@237 (S@0 x@12152) Z@0)))
(assert (forall ((x@12153 Nat@0) (x@12154 Nat@0)) (=> (diseqNat@0@237 x@12153 x@12154) (diseqNat@0@237 (S@0 x@12153) (S@0 x@12154)))))
(declare-sort Nat 0)
(declare-fun zero () Nat)
(declare-fun succ (Nat) Nat)
(declare-fun diseqNat@159 (Nat Nat) Bool)
(assert (forall ((x@12155 Nat)) (diseqNat@159 zero (succ x@12155))))
(assert (forall ((x@12156 Nat)) (diseqNat@159 (succ x@12156) zero)))
(assert (forall ((x@12157 Nat) (x@12158 Nat)) (=> (diseqNat@159 x@12157 x@12158) (diseqNat@159 (succ x@12157) (succ x@12158)))))
(declare-sort list 0)
(declare-fun nil () list)
(declare-fun cons (Nat list) list)
(declare-fun diseqlist@148 (list list) Bool)
(assert (forall ((x@12159 Nat) (x@12160 list)) (diseqlist@148 nil (cons x@12159 x@12160))))
(assert (forall ((x@12161 Nat) (x@12162 list)) (diseqlist@148 (cons x@12161 x@12162) nil)))
(assert (forall ((x@12163 Nat) (x@12164 list) (x@12165 Nat) (x@12166 list)) (=> (diseqNat@159 x@12163 x@12165) (diseqlist@148 (cons x@12163 x@12164) (cons x@12165 x@12166)))))
(assert (forall ((x@12163 Nat) (x@12164 list) (x@12165 Nat) (x@12166 list)) (=> (diseqlist@148 x@12164 x@12166) (diseqlist@148 (cons x@12163 x@12164) (cons x@12165 x@12166)))))
(declare-fun take (list Nat list) Bool)
(assert (forall ((x@12106 list) (x Nat) (y list)) (=> (and (= x@12106 nil) (= x zero)) (take x@12106 x y))))
(assert (forall ((x@12106 list) (x Nat) (y list) (z Nat)) (=> (and (= x@12106 nil) (= x (succ z)) (= y nil)) (take x@12106 x y))))
(assert (forall ((x@12106 list) (x Nat) (y list) (z Nat) (z2 Nat) (xs list) (x@12107 list)) (=> (and (= x@12106 (cons z2 x@12107)) (= x (succ z)) (= y (cons z2 xs)) (take x@12107 z xs)) (take x@12106 x y))))
(declare-fun plus (Nat Nat Nat) Bool)
(assert (forall ((x@12108 Nat) (x Nat) (y Nat)) (=> (and (= x@12108 y) (= x zero)) (plus x@12108 x y))))
(assert (forall ((x@12108 Nat) (x Nat) (y Nat) (z Nat) (x@12109 Nat)) (=> (and (= x@12108 (succ x@12109)) (= x (succ z)) (plus x@12109 z y)) (plus x@12108 x y))))
(declare-fun minus (Nat Nat Nat) Bool)
(assert (forall ((x@12110 Nat) (x Nat) (y Nat)) (=> (and (= x@12110 zero) (= x zero)) (minus x@12110 x y))))
(assert (forall ((x@12110 Nat) (x Nat) (y Nat) (z Nat)) (=> (and (= x@12110 zero) (= x (succ z)) (= y zero)) (minus x@12110 x y))))
(assert (forall ((x@12110 Nat) (x Nat) (y Nat) (z Nat) (y2 Nat) (x@12111 Nat)) (=> (and (= x@12110 x@12111) (= x (succ z)) (= y (succ y2)) (minus x@12111 z y2)) (minus x@12110 x y))))
(declare-fun nmsorttd-half1 (Nat Nat) Bool)
(assert (forall ((x@12112 Nat) (x Nat)) (=> (and (= x@12112 (ite (= x (succ zero)) zero zero)) (= x zero)) (nmsorttd-half1 x@12112 x))))
(assert (forall ((x@12112 Nat) (x Nat) (y Nat) (x@12115 Nat) (x@12114 Nat) (x@12113 Nat)) (=> (and (= x@12112 (ite (= x (succ zero)) zero x@12115)) (= x (succ y)) (plus x@12115 (succ zero) x@12114) (nmsorttd-half1 x@12114 x@12113) (minus x@12113 (succ y) (succ (succ zero)))) (nmsorttd-half1 x@12112 x))))
(declare-fun leq (Bool Nat Nat) Bool)
(assert (forall ((x@12116 Bool) (x Nat) (y Nat)) (=> (and (= x@12116 true) (= x zero)) (leq x@12116 x y))))
(assert (forall ((x@12116 Bool) (x Nat) (y Nat) (z Nat)) (=> (and (= x@12116 false) (= x (succ z)) (= y zero)) (leq x@12116 x y))))
(assert (forall ((x@12116 Bool) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@12117 Bool)) (=> (and (= x@12116 x@12117) (= x (succ z)) (= y (succ x2)) (leq x@12117 z x2)) (leq x@12116 x y))))
(declare-fun lmerge (list list list) Bool)
(assert (forall ((x@12118 list) (x list) (y list)) (=> (and (= x@12118 y) (= x nil)) (lmerge x@12118 x y))))
(assert (forall ((x@12118 list) (x list) (y list) (z Nat) (x2 list)) (=> (and (= x@12118 (cons z x2)) (= x (cons z x2)) (= y nil)) (lmerge x@12118 x y))))
(assert (forall ((x@12118 list) (x list) (y list) (z Nat) (x2 list) (x3 Nat) (x4 list) (x@12119 Bool) (x@12120 list) (x@12121 list)) (=> (and (= x@12118 (ite x@12119 (cons z x@12120) (cons x3 x@12121))) (= x (cons z x2)) (= y (cons x3 x4)) (leq x@12119 z x3) (lmerge x@12120 x2 (cons x3 x4)) (lmerge x@12121 (cons z x2) x4)) (lmerge x@12118 x y))))
(declare-fun length (Nat list) Bool)
(assert (forall ((x@12122 Nat) (x list)) (=> (and (= x@12122 zero) (= x nil)) (length x@12122 x))))
(assert (forall ((x@12122 Nat) (x list) (y Nat) (l list) (x@12124 Nat) (x@12123 Nat)) (=> (and (= x@12122 x@12124) (= x (cons y l)) (plus x@12124 (succ zero) x@12123) (length x@12123 l)) (length x@12122 x))))
(declare-fun insert2 (list Nat list) Bool)
(assert (forall ((x@12125 list) (x Nat) (y list)) (=> (and (= x@12125 (cons x nil)) (= y nil)) (insert2 x@12125 x y))))
(assert (forall ((x@12125 list) (x Nat) (y list) (z Nat) (xs list) (x@12126 Bool) (x@12127 list)) (=> (and (= x@12125 (ite x@12126 (cons x (cons z xs)) (cons z x@12127))) (= y (cons z xs)) (leq x@12126 x z) (insert2 x@12127 x xs)) (insert2 x@12125 x y))))
(declare-fun isort (list list) Bool)
(assert (forall ((x@12128 list) (x list)) (=> (and (= x@12128 nil) (= x nil)) (isort x@12128 x))))
(assert (forall ((x@12128 list) (x list) (y Nat) (xs list) (x@12130 list) (x@12129 list)) (=> (and (= x@12128 x@12130) (= x (cons y xs)) (insert2 x@12130 y x@12129) (isort x@12129 xs)) (isort x@12128 x))))
(declare-fun drop (list Nat list) Bool)
(assert (forall ((x@12131 list) (x Nat) (y list)) (=> (and (= x@12131 y) (= x zero)) (drop x@12131 x y))))
(assert (forall ((x@12131 list) (x Nat) (y list) (z Nat)) (=> (and (= x@12131 nil) (= x (succ z)) (= y nil)) (drop x@12131 x y))))
(assert (forall ((x@12131 list) (x Nat) (y list) (z Nat) (z2 Nat) (xs1 list) (x@12132 list)) (=> (and (= x@12131 x@12132) (= x (succ z)) (= y (cons z2 xs1)) (drop x@12132 z xs1)) (drop x@12131 x y))))
(declare-fun nmsorttd (list list) Bool)
(assert (forall ((x@12133 list) (x list)) (=> (and (= x@12133 nil) (= x nil)) (nmsorttd x@12133 x))))
(assert (forall ((x@12133 list) (x list) (y Nat) (z list)) (=> (and (= x@12133 (cons y nil)) (= x (cons y z)) (= z nil)) (nmsorttd x@12133 x))))
(assert (forall ((x@12133 list) (x list) (y Nat) (z list) (x2 Nat) (x3 list) (k Nat) (x@12135 Nat) (x@12134 Nat) (x@12140 list) (x@12139 list) (x@12138 list) (x@12137 list) (x@12136 list)) (=> (and (= x@12133 x@12140) (= x (cons y z)) (= z (cons x2 x3)) (= k x@12135) (nmsorttd-half1 x@12135 x@12134) (length x@12134 (cons y (cons x2 x3))) (lmerge x@12140 x@12139 x@12137) (nmsorttd x@12139 x@12138) (take x@12138 k (cons y (cons x2 x3))) (nmsorttd x@12137 x@12136) (drop x@12136 k (cons y (cons x2 x3)))) (nmsorttd x@12133 x))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@12144 Nat) (x@12143 Nat) (x@12142 Nat) (x@12141 Nat)) (=> (and (diseqNat@159 x@12144 x@12142) (plus x@12144 x x@12143) (plus x@12143 y z) (plus x@12142 x@12141 z) (plus x@12141 x y)) false))))
(assert (forall ((x Nat) (y Nat)) (forall ((x@12146 Nat) (x@12145 Nat)) (=> (and (diseqNat@159 x@12146 x@12145) (plus x@12146 x y) (plus x@12145 y x)) false))))
(assert (forall ((x Nat)) (forall ((x@12147 Nat)) (=> (and (diseqNat@159 x@12147 x) (plus x@12147 x zero)) false))))
(assert (forall ((x Nat)) (forall ((x@12148 Nat)) (=> (and (diseqNat@159 x@12148 x) (plus x@12148 zero x)) false))))
(assert (forall ((xs list)) (forall ((x@12150 list) (x@12149 list)) (=> (and (diseqlist@148 x@12150 x@12149) (nmsorttd x@12150 xs) (isort x@12149 xs)) false))))
(check-sat)
(get-info :reason-unknown)
