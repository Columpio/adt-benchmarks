(set-logic ALL)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-sort Nat@0 0)
(declare-fun Z@0 () Nat@0)
(declare-fun S@0 (Nat@0) Nat@0)
(declare-fun diseqNat@0@166 (Nat@0 Nat@0) Bool)
(assert (forall ((x@9026 Nat@0)) (diseqNat@0@166 Z@0 (S@0 x@9026))))
(assert (forall ((x@9027 Nat@0)) (diseqNat@0@166 (S@0 x@9027) Z@0)))
(assert (forall ((x@9028 Nat@0) (x@9029 Nat@0)) (=> (diseqNat@0@166 x@9028 x@9029) (diseqNat@0@166 (S@0 x@9028) (S@0 x@9029)))))
(declare-sort Nat 0)
(declare-fun zero () Nat)
(declare-fun succ (Nat) Nat)
(declare-fun diseqNat@100 (Nat Nat) Bool)
(assert (forall ((x@9030 Nat)) (diseqNat@100 zero (succ x@9030))))
(assert (forall ((x@9031 Nat)) (diseqNat@100 (succ x@9031) zero)))
(assert (forall ((x@9032 Nat) (x@9033 Nat)) (=> (diseqNat@100 x@9032 x@9033) (diseqNat@100 (succ x@9032) (succ x@9033)))))
(declare-fun plus (Nat Nat Nat) Bool)
(assert (forall ((x@9000 Nat) (x Nat) (y Nat)) (=> (and (= x@9000 y) (= x zero)) (plus x@9000 x y))))
(assert (forall ((x@9000 Nat) (x Nat) (y Nat) (z Nat) (x@9001 Nat)) (=> (and (= x@9000 (succ x@9001)) (= x (succ z)) (plus x@9001 z y)) (plus x@9000 x y))))
(declare-fun add3 (Nat Nat Nat Nat) Bool)
(assert (forall ((x@9002 Nat) (x Nat) (y Nat) (z Nat)) (=> (and (= x@9002 z) (= x zero) (= y zero)) (add3 x@9002 x y z))))
(assert (forall ((x@9002 Nat) (x Nat) (y Nat) (z Nat) (x3 Nat) (x@9004 Nat) (x@9003 Nat)) (=> (and (= x@9002 x@9004) (= x zero) (= y (succ x3)) (plus x@9004 (succ zero) x@9003) (add3 x@9003 zero x3 z)) (add3 x@9002 x y z))))
(assert (forall ((x@9002 Nat) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@9006 Nat) (x@9005 Nat)) (=> (and (= x@9002 x@9006) (= x (succ x2)) (plus x@9006 (succ zero) x@9005) (add3 x@9005 x2 y z)) (add3 x@9002 x y z))))
(declare-fun mul3 (Nat Nat Nat Nat) Bool)
(assert (forall ((x@9007 Nat) (x Nat) (y Nat) (z Nat)) (=> (and (= x@9007 zero) (= x zero)) (mul3 x@9007 x y z))))
(assert (forall ((x@9007 Nat) (x Nat) (y Nat) (z Nat) (x2 Nat)) (=> (and (= x@9007 zero) (= x (succ x2)) (= y zero)) (mul3 x@9007 x y z))))
(assert (forall ((x@9007 Nat) (x Nat) (y Nat) (z Nat) (x2 Nat) (x3 Nat)) (=> (and (= x@9007 zero) (= x (succ x2)) (= y (succ x3)) (= z zero)) (mul3 x@9007 x y z))))
(assert (forall ((x@9007 Nat) (x Nat) (y Nat) (z Nat) (x2 Nat) (x3 Nat) (x4 Nat) (fail Nat) (x@9015 Nat) (x@9014 Nat) (x@9013 Nat) (x@9012 Nat) (x@9011 Nat) (x@9010 Nat) (x@9009 Nat) (x@9008 Nat)) (=> (and (= x@9007 (ite (= x2 zero) (ite (= x3 zero) (ite (= x4 zero) (succ zero) fail) fail) fail)) (= x (succ x2)) (= y (succ x3)) (= z (succ x4)) (= fail x@9015) (plus x@9015 (succ zero) x@9014) (add3 x@9014 x@9013 x@9012 x@9008) (mul3 x@9013 x2 x3 x4) (add3 x@9012 x@9011 x@9010 x@9009) (mul3 x@9011 (succ zero) x3 x4) (mul3 x@9010 x2 (succ zero) x4) (mul3 x@9009 x2 x3 (succ zero)) (add3 x@9008 x2 x3 x4)) (mul3 x@9007 x y z))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@9017 Nat) (x@9016 Nat)) (=> (and (diseqNat@100 x@9017 x@9016) (mul3 x@9017 x y z) (mul3 x@9016 y x z)) false))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@9021 Nat) (x@9020 Nat) (x@9019 Nat) (x@9018 Nat)) (=> (and (diseqNat@100 x@9021 x@9019) (plus x@9021 x x@9020) (plus x@9020 y z) (plus x@9019 x@9018 z) (plus x@9018 x y)) false))))
(assert (forall ((x Nat) (y Nat)) (forall ((x@9023 Nat) (x@9022 Nat)) (=> (and (diseqNat@100 x@9023 x@9022) (plus x@9023 x y) (plus x@9022 y x)) false))))
(assert (forall ((x Nat)) (forall ((x@9024 Nat)) (=> (and (diseqNat@100 x@9024 x) (plus x@9024 x zero)) false))))
(assert (forall ((x Nat)) (forall ((x@9025 Nat)) (=> (and (diseqNat@100 x@9025 x) (plus x@9025 zero x)) false))))
(check-sat)
(get-info :reason-unknown)
