(set-logic ALL)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-sort Nat@0 0)
(declare-fun Z@0 () Nat@0)
(declare-fun S@0 (Nat@0) Nat@0)
(declare-fun diseqNat@0@203 (Nat@0 Nat@0) Bool)
(assert (forall ((x@10528 Nat@0)) (diseqNat@0@203 Z@0 (S@0 x@10528))))
(assert (forall ((x@10529 Nat@0)) (diseqNat@0@203 (S@0 x@10529) Z@0)))
(assert (forall ((x@10530 Nat@0) (x@10531 Nat@0)) (=> (diseqNat@0@203 x@10530 x@10531) (diseqNat@0@203 (S@0 x@10530) (S@0 x@10531)))))
(declare-sort Nat 0)
(declare-fun zero () Nat)
(declare-fun succ (Nat) Nat)
(declare-fun diseqNat@133 (Nat Nat) Bool)
(assert (forall ((x@10532 Nat)) (diseqNat@133 zero (succ x@10532))))
(assert (forall ((x@10533 Nat)) (diseqNat@133 (succ x@10533) zero)))
(assert (forall ((x@10534 Nat) (x@10535 Nat)) (=> (diseqNat@133 x@10534 x@10535) (diseqNat@133 (succ x@10534) (succ x@10535)))))
(declare-fun plus (Nat Nat Nat) Bool)
(assert (forall ((x@10482 Nat) (x Nat) (y Nat)) (=> (and (= x@10482 y) (= x zero)) (plus x@10482 x y))))
(assert (forall ((x@10482 Nat) (x Nat) (y Nat) (z Nat) (x@10483 Nat)) (=> (and (= x@10482 (succ x@10483)) (= x (succ z)) (plus x@10483 z y)) (plus x@10482 x y))))
(declare-fun times (Nat Nat Nat) Bool)
(assert (forall ((x@10484 Nat) (x Nat) (y Nat)) (=> (and (= x@10484 zero) (= x zero)) (times x@10484 x y))))
(assert (forall ((x@10484 Nat) (x Nat) (y Nat) (z Nat) (x@10486 Nat) (x@10485 Nat)) (=> (and (= x@10484 x@10486) (= x (succ z)) (plus x@10486 y x@10485) (times x@10485 z y)) (times x@10484 x y))))
(declare-fun lt (Bool Nat Nat) Bool)
(assert (forall ((x@10487 Bool) (x Nat) (y Nat)) (=> (and (= x@10487 false) (= y zero)) (lt x@10487 x y))))
(assert (forall ((x@10487 Bool) (x Nat) (y Nat) (z Nat)) (=> (and (= x@10487 true) (= y (succ z)) (= x zero)) (lt x@10487 x y))))
(assert (forall ((x@10487 Bool) (x Nat) (y Nat) (z Nat) (n Nat) (x@10488 Bool)) (=> (and (= x@10487 x@10488) (= y (succ z)) (= x (succ n)) (lt x@10488 n z)) (lt x@10487 x y))))
(declare-fun formula-pow (Nat Nat Nat) Bool)
(assert (forall ((x@10489 Nat) (x Nat) (y Nat)) (=> (and (= x@10489 (succ zero)) (= y zero)) (formula-pow x@10489 x y))))
(assert (forall ((x@10489 Nat) (x Nat) (y Nat) (z Nat) (x@10491 Nat) (x@10490 Nat)) (=> (and (= x@10489 x@10491) (= y (succ z)) (times x@10491 x x@10490) (formula-pow x@10490 x z)) (formula-pow x@10489 x y))))
(declare-fun factorial (Nat Nat) Bool)
(assert (forall ((x@10492 Nat) (x Nat)) (=> (and (= x@10492 (succ zero)) (= x zero)) (factorial x@10492 x))))
(assert (forall ((x@10492 Nat) (x Nat) (y Nat) (x@10494 Nat) (x@10493 Nat)) (=> (and (= x@10492 x@10494) (= x (succ y)) (times x@10494 (succ y) x@10493) (factorial x@10493 y)) (factorial x@10492 x))))
(assert (forall ((n Nat)) (forall ((x@10499 Bool) (x@10498 Nat) (x@10497 Nat) (x@10496 Nat) (x@10495 Nat)) (=> (and (not x@10499) (lt x@10499 x@10498 x@10496) (formula-pow x@10498 (succ (succ zero)) x@10497) (plus x@10497 (succ (succ (succ (succ zero)))) n) (factorial x@10496 x@10495) (plus x@10495 (succ (succ (succ (succ zero)))) n)) false))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@10503 Nat) (x@10502 Nat) (x@10501 Nat) (x@10500 Nat)) (=> (and (diseqNat@133 x@10503 x@10501) (times x@10503 x x@10502) (times x@10502 y z) (times x@10501 x@10500 z) (times x@10500 x y)) false))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@10507 Nat) (x@10506 Nat) (x@10505 Nat) (x@10504 Nat)) (=> (and (diseqNat@133 x@10507 x@10505) (plus x@10507 x x@10506) (plus x@10506 y z) (plus x@10505 x@10504 z) (plus x@10504 x y)) false))))
(assert (forall ((x Nat) (y Nat)) (forall ((x@10509 Nat) (x@10508 Nat)) (=> (and (diseqNat@133 x@10509 x@10508) (times x@10509 x y) (times x@10508 y x)) false))))
(assert (forall ((x Nat) (y Nat)) (forall ((x@10511 Nat) (x@10510 Nat)) (=> (and (diseqNat@133 x@10511 x@10510) (plus x@10511 x y) (plus x@10510 y x)) false))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@10516 Nat) (x@10515 Nat) (x@10514 Nat) (x@10513 Nat) (x@10512 Nat)) (=> (and (diseqNat@133 x@10516 x@10514) (times x@10516 x x@10515) (plus x@10515 y z) (plus x@10514 x@10513 x@10512) (times x@10513 x y) (times x@10512 x z)) false))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@10521 Nat) (x@10520 Nat) (x@10519 Nat) (x@10518 Nat) (x@10517 Nat)) (=> (and (diseqNat@133 x@10521 x@10519) (times x@10521 x@10520 z) (plus x@10520 x y) (plus x@10519 x@10518 x@10517) (times x@10518 x z) (times x@10517 y z)) false))))
(assert (forall ((x Nat)) (forall ((x@10522 Nat)) (=> (and (diseqNat@133 x@10522 x) (times x@10522 x (succ zero))) false))))
(assert (forall ((x Nat)) (forall ((x@10523 Nat)) (=> (and (diseqNat@133 x@10523 x) (times x@10523 (succ zero) x)) false))))
(assert (forall ((x Nat)) (forall ((x@10524 Nat)) (=> (and (diseqNat@133 x@10524 x) (plus x@10524 x zero)) false))))
(assert (forall ((x Nat)) (forall ((x@10525 Nat)) (=> (and (diseqNat@133 x@10525 x) (plus x@10525 zero x)) false))))
(assert (forall ((x Nat)) (forall ((x@10526 Nat)) (=> (and (diseqNat@133 x@10526 zero) (times x@10526 x zero)) false))))
(assert (forall ((x Nat)) (forall ((x@10527 Nat)) (=> (and (diseqNat@133 x@10527 zero) (times x@10527 zero x)) false))))
(check-sat)
(get-info :reason-unknown)
