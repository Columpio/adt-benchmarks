(set-logic ALL)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-sort Nat@0 0)
(declare-fun Z@0 () Nat@0)
(declare-fun S@0 (Nat@0) Nat@0)
(declare-fun diseqNat@0@281 (Nat@0 Nat@0) Bool)
(assert (forall ((x@14121 Nat@0)) (diseqNat@0@281 Z@0 (S@0 x@14121))))
(assert (forall ((x@14122 Nat@0)) (diseqNat@0@281 (S@0 x@14122) Z@0)))
(assert (forall ((x@14123 Nat@0) (x@14124 Nat@0)) (=> (diseqNat@0@281 x@14123 x@14124) (diseqNat@0@281 (S@0 x@14123) (S@0 x@14124)))))
(declare-sort Nat 0)
(declare-fun zero () Nat)
(declare-fun succ (Nat) Nat)
(declare-fun diseqNat@196 (Nat Nat) Bool)
(assert (forall ((x@14125 Nat)) (diseqNat@196 zero (succ x@14125))))
(assert (forall ((x@14126 Nat)) (diseqNat@196 (succ x@14126) zero)))
(assert (forall ((x@14127 Nat) (x@14128 Nat)) (=> (diseqNat@196 x@14127 x@14128) (diseqNat@196 (succ x@14127) (succ x@14128)))))
(declare-fun lt (Bool Nat Nat) Bool)
(assert (forall ((x@14118 Bool) (x Nat) (y Nat)) (=> (and (= x@14118 false) (= y zero)) (lt x@14118 x y))))
(assert (forall ((x@14118 Bool) (x Nat) (y Nat) (z Nat)) (=> (and (= x@14118 true) (= y (succ z)) (= x zero)) (lt x@14118 x y))))
(assert (forall ((x@14118 Bool) (x Nat) (y Nat) (z Nat) (n Nat) (x@14119 Bool)) (=> (and (= x@14118 x@14119) (= y (succ z)) (= x (succ n)) (lt x@14119 n z)) (lt x@14118 x y))))
(assert (forall ((x Nat) (y Nat)) (forall ((x@14120 Bool)) (=> (and (= x y) x@14120 (lt x@14120 y x)) false))))
(check-sat)
(get-info :reason-unknown)
