(set-logic ALL)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-sort Nat@0 0)
(declare-fun Z@0 () Nat@0)
(declare-fun S@0 (Nat@0) Nat@0)
(declare-fun diseqNat@0@226 (Nat@0 Nat@0) Bool)
(assert (forall ((x@11521 Nat@0)) (diseqNat@0@226 Z@0 (S@0 x@11521))))
(assert (forall ((x@11522 Nat@0)) (diseqNat@0@226 (S@0 x@11522) Z@0)))
(assert (forall ((x@11523 Nat@0) (x@11524 Nat@0)) (=> (diseqNat@0@226 x@11523 x@11524) (diseqNat@0@226 (S@0 x@11523) (S@0 x@11524)))))
(declare-sort A 0)
(declare-fun X () A)
(declare-fun Y () A)
(declare-fun diseqA@9 (A A) Bool)
(assert (diseqA@9 X Y))
(assert (diseqA@9 Y X))
(declare-sort R 0)
(declare-fun Nil () R)
(declare-fun Eps () R)
(declare-fun Atom (A) R)
(declare-fun Plus (R R) R)
(declare-fun Seq (R R) R)
(declare-fun Star (R) R)
(declare-fun diseqR@43 (R R) Bool)
(assert (diseqR@43 Nil Eps))
(assert (forall ((x@11525 A)) (diseqR@43 Nil (Atom x@11525))))
(assert (forall ((x@11526 R) (x@11527 R)) (diseqR@43 Nil (Plus x@11526 x@11527))))
(assert (forall ((x@11528 R) (x@11529 R)) (diseqR@43 Nil (Seq x@11528 x@11529))))
(assert (forall ((x@11530 R)) (diseqR@43 Nil (Star x@11530))))
(assert (diseqR@43 Eps Nil))
(assert (forall ((x@11531 A)) (diseqR@43 (Atom x@11531) Nil)))
(assert (forall ((x@11532 R) (x@11533 R)) (diseqR@43 (Plus x@11532 x@11533) Nil)))
(assert (forall ((x@11534 R) (x@11535 R)) (diseqR@43 (Seq x@11534 x@11535) Nil)))
(assert (forall ((x@11536 R)) (diseqR@43 (Star x@11536) Nil)))
(assert (forall ((x@11537 A)) (diseqR@43 Eps (Atom x@11537))))
(assert (forall ((x@11538 R) (x@11539 R)) (diseqR@43 Eps (Plus x@11538 x@11539))))
(assert (forall ((x@11540 R) (x@11541 R)) (diseqR@43 Eps (Seq x@11540 x@11541))))
(assert (forall ((x@11542 R)) (diseqR@43 Eps (Star x@11542))))
(assert (forall ((x@11543 A)) (diseqR@43 (Atom x@11543) Eps)))
(assert (forall ((x@11544 R) (x@11545 R)) (diseqR@43 (Plus x@11544 x@11545) Eps)))
(assert (forall ((x@11546 R) (x@11547 R)) (diseqR@43 (Seq x@11546 x@11547) Eps)))
(assert (forall ((x@11548 R)) (diseqR@43 (Star x@11548) Eps)))
(assert (forall ((x@11549 A) (x@11550 R) (x@11551 R)) (diseqR@43 (Atom x@11549) (Plus x@11550 x@11551))))
(assert (forall ((x@11552 A) (x@11553 R) (x@11554 R)) (diseqR@43 (Atom x@11552) (Seq x@11553 x@11554))))
(assert (forall ((x@11555 A) (x@11556 R)) (diseqR@43 (Atom x@11555) (Star x@11556))))
(assert (forall ((x@11557 R) (x@11558 R) (x@11559 A)) (diseqR@43 (Plus x@11557 x@11558) (Atom x@11559))))
(assert (forall ((x@11560 R) (x@11561 R) (x@11562 A)) (diseqR@43 (Seq x@11560 x@11561) (Atom x@11562))))
(assert (forall ((x@11563 R) (x@11564 A)) (diseqR@43 (Star x@11563) (Atom x@11564))))
(assert (forall ((x@11565 R) (x@11566 R) (x@11567 R) (x@11568 R)) (diseqR@43 (Plus x@11565 x@11566) (Seq x@11567 x@11568))))
(assert (forall ((x@11569 R) (x@11570 R) (x@11571 R)) (diseqR@43 (Plus x@11569 x@11570) (Star x@11571))))
(assert (forall ((x@11572 R) (x@11573 R) (x@11574 R) (x@11575 R)) (diseqR@43 (Seq x@11572 x@11573) (Plus x@11574 x@11575))))
(assert (forall ((x@11576 R) (x@11577 R) (x@11578 R)) (diseqR@43 (Star x@11576) (Plus x@11577 x@11578))))
(assert (forall ((x@11579 R) (x@11580 R) (x@11581 R)) (diseqR@43 (Seq x@11579 x@11580) (Star x@11581))))
(assert (forall ((x@11582 R) (x@11583 R) (x@11584 R)) (diseqR@43 (Star x@11582) (Seq x@11583 x@11584))))
(assert (forall ((x@11585 A) (x@11586 A)) (=> (diseqA@9 x@11585 x@11586) (diseqR@43 (Atom x@11585) (Atom x@11586)))))
(assert (forall ((x@11587 R) (x@11588 R) (x@11589 R) (x@11590 R)) (=> (diseqR@43 x@11587 x@11589) (diseqR@43 (Plus x@11587 x@11588) (Plus x@11589 x@11590)))))
(assert (forall ((x@11587 R) (x@11588 R) (x@11589 R) (x@11590 R)) (=> (diseqR@43 x@11588 x@11590) (diseqR@43 (Plus x@11587 x@11588) (Plus x@11589 x@11590)))))
(assert (forall ((x@11591 R) (x@11592 R) (x@11593 R) (x@11594 R)) (=> (diseqR@43 x@11591 x@11593) (diseqR@43 (Seq x@11591 x@11592) (Seq x@11593 x@11594)))))
(assert (forall ((x@11591 R) (x@11592 R) (x@11593 R) (x@11594 R)) (=> (diseqR@43 x@11592 x@11594) (diseqR@43 (Seq x@11591 x@11592) (Seq x@11593 x@11594)))))
(assert (forall ((x@11595 R) (x@11596 R)) (=> (diseqR@43 x@11595 x@11596) (diseqR@43 (Star x@11595) (Star x@11596)))))
(declare-sort list 0)
(declare-fun nil () list)
(declare-fun cons (A list) list)
(declare-fun diseqlist@143 (list list) Bool)
(assert (forall ((x@11597 A) (x@11598 list)) (diseqlist@143 nil (cons x@11597 x@11598))))
(assert (forall ((x@11599 A) (x@11600 list)) (diseqlist@143 (cons x@11599 x@11600) nil)))
(assert (forall ((x@11601 A) (x@11602 list) (x@11603 A) (x@11604 list)) (=> (diseqA@9 x@11601 x@11603) (diseqlist@143 (cons x@11601 x@11602) (cons x@11603 x@11604)))))
(assert (forall ((x@11601 A) (x@11602 list) (x@11603 A) (x@11604 list)) (=> (diseqlist@143 x@11602 x@11604) (diseqlist@143 (cons x@11601 x@11602) (cons x@11603 x@11604)))))
(declare-fun seq (R R R) Bool)
(assert (forall ((x@11408 R) (x R) (y R) (x@11400 R) (x@11401 R) (x@11402 R) (x@11403 R)) (=> (and (= x@11408 (Seq x y)) (= x x@11400) (= y x@11401) (= x x@11402) (= y x@11403)) (seq x@11408 x y))))
(assert (forall ((x@11408 R) (x R) (y R) (x@11400 R) (x@11401 R) (x@11402 R)) (=> (and (= x@11408 x) (= x x@11400) (= y x@11401) (= x x@11402) (= y Eps)) (seq x@11408 x y))))
(assert (forall ((x@11408 R) (x R) (y R) (x@11400 R) (x@11401 R)) (=> (and (= x@11408 y) (= x x@11400) (= y x@11401) (= x Eps)) (seq x@11408 x y))))
(assert (forall ((x@11408 R) (x R) (y R) (x@11400 R)) (=> (and (= x@11408 Nil) (= x x@11400) (= y Nil)) (seq x@11408 x y))))
(assert (forall ((x@11408 R) (x R) (y R)) (=> (and (= x@11408 Nil) (= x Nil)) (seq x@11408 x y))))
(declare-fun plus (R R R) Bool)
(assert (forall ((x@11409 R) (x R) (y R) (x@11404 R) (x@11405 R)) (=> (and (= x@11409 (Plus x y)) (= x x@11404) (= y x@11405)) (plus x@11409 x y))))
(assert (forall ((x@11409 R) (x R) (y R) (x@11404 R)) (=> (and (= x@11409 x) (= x x@11404) (= y Nil)) (plus x@11409 x y))))
(assert (forall ((x@11409 R) (x R) (y R)) (=> (and (= x@11409 y) (= x Nil)) (plus x@11409 x y))))
(declare-fun eqA (Bool A A) Bool)
(assert (forall ((x@11410 Bool) (x A) (y A)) (=> (and (= x@11410 true) (= x X) (= y X)) (eqA x@11410 x y))))
(assert (forall ((x@11410 Bool) (x A) (y A)) (=> (and (= x@11410 false) (= x X) (= y Y)) (eqA x@11410 x y))))
(assert (forall ((x@11410 Bool) (x A) (y A)) (=> (and (= x@11410 false) (= x Y) (= y X)) (eqA x@11410 x y))))
(assert (forall ((x@11410 Bool) (x A) (y A)) (=> (and (= x@11410 true) (= x Y) (= y Y)) (eqA x@11410 x y))))
(declare-fun eps (Bool R) Bool)
(assert (forall ((x@11411 Bool) (x R) (x@11406 R)) (=> (and (= x@11411 false) (= x x@11406)) (eps x@11411 x))))
(assert (forall ((x@11411 Bool) (x R)) (=> (and (= x@11411 true) (= x Eps)) (eps x@11411 x))))
(assert (forall ((x@11411 Bool) (x R) (p R) (q R) (x@11413 Bool) (x@11412 Bool)) (=> (and (= x@11411 (or x@11413 x@11412)) (= x (Plus p q)) (eps x@11413 p) (eps x@11412 q)) (eps x@11411 x))))
(assert (forall ((x@11411 Bool) (x R) (r R) (q2 R) (x@11415 Bool) (x@11414 Bool)) (=> (and (= x@11411 (and x@11415 x@11414)) (= x (Seq r q2)) (eps x@11415 r) (eps x@11414 q2)) (eps x@11411 x))))
(assert (forall ((x@11411 Bool) (x R) (y R)) (=> (and (= x@11411 true) (= x (Star y))) (eps x@11411 x))))
(declare-fun epsR (R R) Bool)
(assert (forall ((x@11416 R) (x R) (x@11417 Bool)) (=> (and (= x@11416 (ite x@11417 Eps Nil)) (eps x@11417 x)) (epsR x@11416 x))))
(declare-fun step (R R A) Bool)
(assert (forall ((x@11418 R) (x R) (y A) (x@11407 R)) (=> (and (= x@11418 Nil) (= x x@11407)) (step x@11418 x y))))
(assert (forall ((x@11418 R) (x R) (y A) (a A) (x@11419 Bool)) (=> (and (= x@11418 (ite x@11419 Eps Nil)) (= x (Atom a)) (eqA x@11419 a y)) (step x@11418 x y))))
(assert (forall ((x@11418 R) (x R) (y A) (p R) (q R) (x@11422 R) (x@11421 R) (x@11420 R)) (=> (and (= x@11418 x@11422) (= x (Plus p q)) (plus x@11422 x@11421 x@11420) (step x@11421 p y) (step x@11420 q y)) (step x@11418 x y))))
(assert (forall ((x@11418 R) (x R) (y A) (r R) (q2 R) (x@11428 R) (x@11427 R) (x@11426 R) (x@11425 R) (x@11424 R) (x@11423 R)) (=> (and (= x@11418 x@11428) (= x (Seq r q2)) (plus x@11428 x@11427 x@11425) (seq x@11427 x@11426 q2) (step x@11426 r y) (seq x@11425 x@11424 x@11423) (epsR x@11424 r) (step x@11423 q2 y)) (step x@11418 x y))))
(assert (forall ((x@11418 R) (x R) (y A) (p2 R) (x@11430 R) (x@11429 R)) (=> (and (= x@11418 x@11430) (= x (Star p2)) (seq x@11430 x@11429 (Star p2)) (step x@11429 p2 y)) (step x@11418 x y))))
(declare-fun recognise (Bool R list) Bool)
(assert (forall ((x@11431 Bool) (x R) (y list) (x@11432 Bool)) (=> (and (= x@11431 x@11432) (= y nil) (eps x@11432 x)) (recognise x@11431 x y))))
(assert (forall ((x@11431 Bool) (x R) (y list) (z A) (xs list) (x@11434 Bool) (x@11433 R)) (=> (and (= x@11431 x@11434) (= y (cons z xs)) (recognise x@11434 x@11433 xs) (step x@11433 x z)) (recognise x@11431 x y))))
(assert (forall ((p R) (s list)) (forall ((x@11436 Bool) (x A) (y list) (x@11435 Bool)) (=> (and (diseqBool@0 x@11436 x@11435) (recognise x@11436 (Star p) s) (= s (cons x y)) (recognise x@11435 (Seq p (Star p)) s)) false))))
(check-sat)
(get-info :reason-unknown)
