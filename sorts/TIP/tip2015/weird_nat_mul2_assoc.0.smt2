(set-logic ALL)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-sort Nat@0 0)
(declare-fun Z@0 () Nat@0)
(declare-fun S@0 (Nat@0) Nat@0)
(declare-fun diseqNat@0@235 (Nat@0 Nat@0) Bool)
(assert (forall ((x@12038 Nat@0)) (diseqNat@0@235 Z@0 (S@0 x@12038))))
(assert (forall ((x@12039 Nat@0)) (diseqNat@0@235 (S@0 x@12039) Z@0)))
(assert (forall ((x@12040 Nat@0) (x@12041 Nat@0)) (=> (diseqNat@0@235 x@12040 x@12041) (diseqNat@0@235 (S@0 x@12040) (S@0 x@12041)))))
(declare-sort Nat 0)
(declare-fun zero () Nat)
(declare-fun succ (Nat) Nat)
(declare-fun diseqNat@157 (Nat Nat) Bool)
(assert (forall ((x@12042 Nat)) (diseqNat@157 zero (succ x@12042))))
(assert (forall ((x@12043 Nat)) (diseqNat@157 (succ x@12043) zero)))
(assert (forall ((x@12044 Nat) (x@12045 Nat)) (=> (diseqNat@157 x@12044 x@12045) (diseqNat@157 (succ x@12044) (succ x@12045)))))
(declare-fun plus (Nat Nat Nat) Bool)
(assert (forall ((x@12017 Nat) (x Nat) (y Nat)) (=> (and (= x@12017 y) (= x zero)) (plus x@12017 x y))))
(assert (forall ((x@12017 Nat) (x Nat) (y Nat) (z Nat) (x@12018 Nat)) (=> (and (= x@12017 (succ x@12018)) (= x (succ z)) (plus x@12018 z y)) (plus x@12017 x y))))
(declare-fun add3acc (Nat Nat Nat Nat) Bool)
(assert (forall ((x@12019 Nat) (x Nat) (y Nat) (z Nat)) (=> (and (= x@12019 z) (= x zero) (= y zero)) (add3acc x@12019 x y z))))
(assert (forall ((x@12019 Nat) (x Nat) (y Nat) (z Nat) (x3 Nat) (x@12020 Nat)) (=> (and (= x@12019 x@12020) (= x zero) (= y (succ x3)) (add3acc x@12020 zero x3 (succ z))) (add3acc x@12019 x y z))))
(assert (forall ((x@12019 Nat) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@12021 Nat)) (=> (and (= x@12019 x@12021) (= x (succ x2)) (add3acc x@12021 x2 (succ y) z)) (add3acc x@12019 x y z))))
(declare-fun mul2 (Nat Nat Nat) Bool)
(assert (forall ((x@12022 Nat) (x Nat) (y Nat)) (=> (and (= x@12022 zero) (= x zero)) (mul2 x@12022 x y))))
(assert (forall ((x@12022 Nat) (x Nat) (y Nat) (z Nat)) (=> (and (= x@12022 zero) (= x (succ z)) (= y zero)) (mul2 x@12022 x y))))
(assert (forall ((x@12022 Nat) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@12025 Nat) (x@12024 Nat) (x@12023 Nat)) (=> (and (= x@12022 x@12025) (= x (succ z)) (= y (succ x2)) (plus x@12025 (succ zero) x@12024) (add3acc x@12024 z x2 x@12023) (mul2 x@12023 z x2)) (mul2 x@12022 x y))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@12029 Nat) (x@12028 Nat) (x@12027 Nat) (x@12026 Nat)) (=> (and (diseqNat@157 x@12029 x@12027) (mul2 x@12029 x x@12028) (mul2 x@12028 y z) (mul2 x@12027 x@12026 z) (mul2 x@12026 x y)) false))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@12033 Nat) (x@12032 Nat) (x@12031 Nat) (x@12030 Nat)) (=> (and (diseqNat@157 x@12033 x@12031) (plus x@12033 x x@12032) (plus x@12032 y z) (plus x@12031 x@12030 z) (plus x@12030 x y)) false))))
(assert (forall ((x Nat) (y Nat)) (forall ((x@12035 Nat) (x@12034 Nat)) (=> (and (diseqNat@157 x@12035 x@12034) (plus x@12035 x y) (plus x@12034 y x)) false))))
(assert (forall ((x Nat)) (forall ((x@12036 Nat)) (=> (and (diseqNat@157 x@12036 x) (plus x@12036 x zero)) false))))
(assert (forall ((x Nat)) (forall ((x@12037 Nat)) (=> (and (diseqNat@157 x@12037 x) (plus x@12037 zero x)) false))))
(check-sat)
(get-info :reason-unknown)
