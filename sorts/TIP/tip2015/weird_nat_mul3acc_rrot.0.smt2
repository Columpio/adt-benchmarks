(set-logic ALL)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-sort Nat@0 0)
(declare-fun Z@0 () Nat@0)
(declare-fun S@0 (Nat@0) Nat@0)
(declare-fun diseqNat@0@146 (Nat@0 Nat@0) Bool)
(assert (forall ((x@8195 Nat@0)) (diseqNat@0@146 Z@0 (S@0 x@8195))))
(assert (forall ((x@8196 Nat@0)) (diseqNat@0@146 (S@0 x@8196) Z@0)))
(assert (forall ((x@8197 Nat@0) (x@8198 Nat@0)) (=> (diseqNat@0@146 x@8197 x@8198) (diseqNat@0@146 (S@0 x@8197) (S@0 x@8198)))))
(declare-sort Nat 0)
(declare-fun zero () Nat)
(declare-fun succ (Nat) Nat)
(declare-fun diseqNat@85 (Nat Nat) Bool)
(assert (forall ((x@8199 Nat)) (diseqNat@85 zero (succ x@8199))))
(assert (forall ((x@8200 Nat)) (diseqNat@85 (succ x@8200) zero)))
(assert (forall ((x@8201 Nat) (x@8202 Nat)) (=> (diseqNat@85 x@8201 x@8202) (diseqNat@85 (succ x@8201) (succ x@8202)))))
(declare-fun plus (Nat Nat Nat) Bool)
(assert (forall ((x@8171 Nat) (x Nat) (y Nat)) (=> (and (= x@8171 y) (= x zero)) (plus x@8171 x y))))
(assert (forall ((x@8171 Nat) (x Nat) (y Nat) (z Nat) (x@8172 Nat)) (=> (and (= x@8171 (succ x@8172)) (= x (succ z)) (plus x@8172 z y)) (plus x@8171 x y))))
(declare-fun add3acc (Nat Nat Nat Nat) Bool)
(assert (forall ((x@8173 Nat) (x Nat) (y Nat) (z Nat)) (=> (and (= x@8173 z) (= x zero) (= y zero)) (add3acc x@8173 x y z))))
(assert (forall ((x@8173 Nat) (x Nat) (y Nat) (z Nat) (x3 Nat) (x@8174 Nat)) (=> (and (= x@8173 x@8174) (= x zero) (= y (succ x3)) (add3acc x@8174 zero x3 (succ z))) (add3acc x@8173 x y z))))
(assert (forall ((x@8173 Nat) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@8175 Nat)) (=> (and (= x@8173 x@8175) (= x (succ x2)) (add3acc x@8175 x2 (succ y) z)) (add3acc x@8173 x y z))))
(declare-fun mul3acc (Nat Nat Nat Nat) Bool)
(assert (forall ((x@8176 Nat) (x Nat) (y Nat) (z Nat)) (=> (and (= x@8176 zero) (= x zero)) (mul3acc x@8176 x y z))))
(assert (forall ((x@8176 Nat) (x Nat) (y Nat) (z Nat) (x2 Nat)) (=> (and (= x@8176 zero) (= x (succ x2)) (= y zero)) (mul3acc x@8176 x y z))))
(assert (forall ((x@8176 Nat) (x Nat) (y Nat) (z Nat) (x2 Nat) (x3 Nat)) (=> (and (= x@8176 zero) (= x (succ x2)) (= y (succ x3)) (= z zero)) (mul3acc x@8176 x y z))))
(assert (forall ((x@8176 Nat) (x Nat) (y Nat) (z Nat) (x2 Nat) (x3 Nat) (x4 Nat) (fail Nat) (x@8184 Nat) (x@8183 Nat) (x@8182 Nat) (x@8181 Nat) (x@8180 Nat) (x@8179 Nat) (x@8178 Nat) (x@8177 Nat)) (=> (and (= x@8176 (ite (= x2 zero) (ite (= x3 zero) (ite (= x4 zero) (succ zero) fail) fail) fail)) (= x (succ x2)) (= y (succ x3)) (= z (succ x4)) (= fail x@8184) (plus x@8184 (succ zero) x@8183) (add3acc x@8183 x@8182 x@8181 x@8177) (mul3acc x@8182 x2 x3 x4) (add3acc x@8181 x@8180 x@8179 x@8178) (mul3acc x@8180 (succ zero) x3 x4) (mul3acc x@8179 x2 (succ zero) x4) (mul3acc x@8178 x2 x3 (succ zero)) (add3acc x@8177 (succ x2) (succ x3) (succ x4))) (mul3acc x@8176 x y z))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@8186 Nat) (x@8185 Nat)) (=> (and (diseqNat@85 x@8186 x@8185) (mul3acc x@8186 x y z) (mul3acc x@8185 z x y)) false))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@8190 Nat) (x@8189 Nat) (x@8188 Nat) (x@8187 Nat)) (=> (and (diseqNat@85 x@8190 x@8188) (plus x@8190 x x@8189) (plus x@8189 y z) (plus x@8188 x@8187 z) (plus x@8187 x y)) false))))
(assert (forall ((x Nat) (y Nat)) (forall ((x@8192 Nat) (x@8191 Nat)) (=> (and (diseqNat@85 x@8192 x@8191) (plus x@8192 x y) (plus x@8191 y x)) false))))
(assert (forall ((x Nat)) (forall ((x@8193 Nat)) (=> (and (diseqNat@85 x@8193 x) (plus x@8193 x zero)) false))))
(assert (forall ((x Nat)) (forall ((x@8194 Nat)) (=> (and (diseqNat@85 x@8194 x) (plus x@8194 zero x)) false))))
(check-sat)
(get-info :reason-unknown)
