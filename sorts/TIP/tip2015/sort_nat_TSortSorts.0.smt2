(set-logic ALL)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-sort Nat@0 0)
(declare-fun Z@0 () Nat@0)
(declare-fun S@0 (Nat@0) Nat@0)
(declare-fun diseqNat@0@133 (Nat@0 Nat@0) Bool)
(assert (forall ((x@7445 Nat@0)) (diseqNat@0@133 Z@0 (S@0 x@7445))))
(assert (forall ((x@7446 Nat@0)) (diseqNat@0@133 (S@0 x@7446) Z@0)))
(assert (forall ((x@7447 Nat@0) (x@7448 Nat@0)) (=> (diseqNat@0@133 x@7447 x@7448) (diseqNat@0@133 (S@0 x@7447) (S@0 x@7448)))))
(declare-sort Nat 0)
(declare-fun zero () Nat)
(declare-fun succ (Nat) Nat)
(declare-fun diseqNat@78 (Nat Nat) Bool)
(assert (forall ((x@7449 Nat)) (diseqNat@78 zero (succ x@7449))))
(assert (forall ((x@7450 Nat)) (diseqNat@78 (succ x@7450) zero)))
(assert (forall ((x@7451 Nat) (x@7452 Nat)) (=> (diseqNat@78 x@7451 x@7452) (diseqNat@78 (succ x@7451) (succ x@7452)))))
(declare-sort Tree 0)
(declare-fun TNode (Tree Nat Tree) Tree)
(declare-fun TNil () Tree)
(declare-fun diseqTree@0 (Tree Tree) Bool)
(assert (forall ((x@7453 Tree) (x@7454 Nat) (x@7455 Tree)) (diseqTree@0 (TNode x@7453 x@7454 x@7455) TNil)))
(assert (forall ((x@7456 Tree) (x@7457 Nat) (x@7458 Tree)) (diseqTree@0 TNil (TNode x@7456 x@7457 x@7458))))
(assert (forall ((x@7459 Tree) (x@7460 Nat) (x@7461 Tree) (x@7462 Tree) (x@7463 Nat) (x@7464 Tree)) (=> (diseqTree@0 x@7459 x@7462) (diseqTree@0 (TNode x@7459 x@7460 x@7461) (TNode x@7462 x@7463 x@7464)))))
(assert (forall ((x@7459 Tree) (x@7460 Nat) (x@7461 Tree) (x@7462 Tree) (x@7463 Nat) (x@7464 Tree)) (=> (diseqNat@78 x@7460 x@7463) (diseqTree@0 (TNode x@7459 x@7460 x@7461) (TNode x@7462 x@7463 x@7464)))))
(assert (forall ((x@7459 Tree) (x@7460 Nat) (x@7461 Tree) (x@7462 Tree) (x@7463 Nat) (x@7464 Tree)) (=> (diseqTree@0 x@7461 x@7464) (diseqTree@0 (TNode x@7459 x@7460 x@7461) (TNode x@7462 x@7463 x@7464)))))
(declare-sort list 0)
(declare-fun nil () list)
(declare-fun cons (Nat list) list)
(declare-fun diseqlist@107 (list list) Bool)
(assert (forall ((x@7465 Nat) (x@7466 list)) (diseqlist@107 nil (cons x@7465 x@7466))))
(assert (forall ((x@7467 Nat) (x@7468 list)) (diseqlist@107 (cons x@7467 x@7468) nil)))
(assert (forall ((x@7469 Nat) (x@7470 list) (x@7471 Nat) (x@7472 list)) (=> (diseqNat@78 x@7469 x@7471) (diseqlist@107 (cons x@7469 x@7470) (cons x@7471 x@7472)))))
(assert (forall ((x@7469 Nat) (x@7470 list) (x@7471 Nat) (x@7472 list)) (=> (diseqlist@107 x@7470 x@7472) (diseqlist@107 (cons x@7469 x@7470) (cons x@7471 x@7472)))))
(declare-fun leq (Bool Nat Nat) Bool)
(assert (forall ((x@7425 Bool) (x Nat) (y Nat)) (=> (and (= x@7425 true) (= x zero)) (leq x@7425 x y))))
(assert (forall ((x@7425 Bool) (x Nat) (y Nat) (z Nat)) (=> (and (= x@7425 false) (= x (succ z)) (= y zero)) (leq x@7425 x y))))
(assert (forall ((x@7425 Bool) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@7426 Bool)) (=> (and (= x@7425 x@7426) (= x (succ z)) (= y (succ x2)) (leq x@7426 z x2)) (leq x@7425 x y))))
(declare-fun ordered (Bool list) Bool)
(assert (forall ((x@7427 Bool) (x list)) (=> (and (= x@7427 true) (= x nil)) (ordered x@7427 x))))
(assert (forall ((x@7427 Bool) (x list) (y Nat) (z list)) (=> (and (= x@7427 true) (= x (cons y z)) (= z nil)) (ordered x@7427 x))))
(assert (forall ((x@7427 Bool) (x list) (y Nat) (z list) (y2 Nat) (xs list) (x@7429 Bool) (x@7428 Bool)) (=> (and (= x@7427 (and x@7429 x@7428)) (= x (cons y z)) (= z (cons y2 xs)) (leq x@7429 y y2) (ordered x@7428 (cons y2 xs))) (ordered x@7427 x))))
(declare-fun flatten (list Tree list) Bool)
(assert (forall ((x@7430 list) (x Tree) (y list) (q Tree) (z Nat) (r Tree) (x@7432 list) (x@7431 list)) (=> (and (= x@7430 x@7432) (= x (TNode q z r)) (flatten x@7432 q (cons z x@7431)) (flatten x@7431 r y)) (flatten x@7430 x y))))
(assert (forall ((x@7430 list) (x Tree) (y list)) (=> (and (= x@7430 y) (= x TNil)) (flatten x@7430 x y))))
(declare-fun add (Tree Nat Tree) Bool)
(assert (forall ((x@7433 Tree) (x Nat) (y Tree) (q Tree) (z Nat) (r Tree) (x@7434 Bool) (x@7435 Tree) (x@7436 Tree)) (=> (and (= x@7433 (ite x@7434 (TNode x@7435 z r) (TNode q z x@7436))) (= y (TNode q z r)) (leq x@7434 x z) (add x@7435 x q) (add x@7436 x r)) (add x@7433 x y))))
(assert (forall ((x@7433 Tree) (x Nat) (y Tree)) (=> (and (= x@7433 (TNode TNil x TNil)) (= y TNil)) (add x@7433 x y))))
(declare-fun toTree (Tree list) Bool)
(assert (forall ((x@7437 Tree) (x list)) (=> (and (= x@7437 TNil) (= x nil)) (toTree x@7437 x))))
(assert (forall ((x@7437 Tree) (x list) (y Nat) (xs list) (x@7439 Tree) (x@7438 Tree)) (=> (and (= x@7437 x@7439) (= x (cons y xs)) (add x@7439 y x@7438) (toTree x@7438 xs)) (toTree x@7437 x))))
(declare-fun tsort (list list) Bool)
(assert (forall ((x@7440 list) (x list) (x@7442 list) (x@7441 Tree)) (=> (and (= x@7440 x@7442) (flatten x@7442 x@7441 nil) (toTree x@7441 x)) (tsort x@7440 x))))
(assert (forall ((xs list)) (forall ((x@7444 Bool) (x@7443 list)) (=> (and (not x@7444) (ordered x@7444 x@7443) (tsort x@7443 xs)) false))))
(check-sat)
(get-info :reason-unknown)
