(set-logic ALL)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-sort Nat@0 0)
(declare-fun Z@0 () Nat@0)
(declare-fun S@0 (Nat@0) Nat@0)
(declare-fun diseqNat@0@116 (Nat@0 Nat@0) Bool)
(assert (forall ((x@6785 Nat@0)) (diseqNat@0@116 Z@0 (S@0 x@6785))))
(assert (forall ((x@6786 Nat@0)) (diseqNat@0@116 (S@0 x@6786) Z@0)))
(assert (forall ((x@6787 Nat@0) (x@6788 Nat@0)) (=> (diseqNat@0@116 x@6787 x@6788) (diseqNat@0@116 (S@0 x@6787) (S@0 x@6788)))))
(declare-sort Nat 0)
(declare-fun zero () Nat)
(declare-fun succ (Nat) Nat)
(declare-fun diseqNat@67 (Nat Nat) Bool)
(assert (forall ((x@6789 Nat)) (diseqNat@67 zero (succ x@6789))))
(assert (forall ((x@6790 Nat)) (diseqNat@67 (succ x@6790) zero)))
(assert (forall ((x@6791 Nat) (x@6792 Nat)) (=> (diseqNat@67 x@6791 x@6792) (diseqNat@67 (succ x@6791) (succ x@6792)))))
(declare-fun add3acc (Nat Nat Nat Nat) Bool)
(assert (forall ((x@6780 Nat) (x Nat) (y Nat) (z Nat)) (=> (and (= x@6780 z) (= x zero) (= y zero)) (add3acc x@6780 x y z))))
(assert (forall ((x@6780 Nat) (x Nat) (y Nat) (z Nat) (x3 Nat) (x@6781 Nat)) (=> (and (= x@6780 x@6781) (= x zero) (= y (succ x3)) (add3acc x@6781 zero x3 (succ z))) (add3acc x@6780 x y z))))
(assert (forall ((x@6780 Nat) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@6782 Nat)) (=> (and (= x@6780 x@6782) (= x (succ x2)) (add3acc x@6782 x2 (succ y) z)) (add3acc x@6780 x y z))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@6784 Nat) (x@6783 Nat)) (=> (and (diseqNat@67 x@6784 x@6783) (add3acc x@6784 x y z) (add3acc x@6783 z x y)) false))))
(check-sat)
(get-info :reason-unknown)
