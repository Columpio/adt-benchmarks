(set-logic ALL)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-sort Nat@0 0)
(declare-fun Z@0 () Nat@0)
(declare-fun S@0 (Nat@0) Nat@0)
(declare-fun diseqNat@0@121 (Nat@0 Nat@0) Bool)
(assert (forall ((x@7024 Nat@0)) (diseqNat@0@121 Z@0 (S@0 x@7024))))
(assert (forall ((x@7025 Nat@0)) (diseqNat@0@121 (S@0 x@7025) Z@0)))
(assert (forall ((x@7026 Nat@0) (x@7027 Nat@0)) (=> (diseqNat@0@121 x@7026 x@7027) (diseqNat@0@121 (S@0 x@7026) (S@0 x@7027)))))
(declare-sort list 0)
(declare-fun nil () list)
(declare-fun cons (Nat@0 list) list)
(declare-fun diseqlist@103 (list list) Bool)
(assert (forall ((x@7028 Nat@0) (x@7029 list)) (diseqlist@103 nil (cons x@7028 x@7029))))
(assert (forall ((x@7030 Nat@0) (x@7031 list)) (diseqlist@103 (cons x@7030 x@7031) nil)))
(assert (forall ((x@7032 Nat@0) (x@7033 list) (x@7034 Nat@0) (x@7035 list)) (=> (diseqNat@0@121 x@7032 x@7034) (diseqlist@103 (cons x@7032 x@7033) (cons x@7034 x@7035)))))
(assert (forall ((x@7032 Nat@0) (x@7033 list) (x@7034 Nat@0) (x@7035 list)) (=> (diseqlist@103 x@7033 x@7035) (diseqlist@103 (cons x@7032 x@7033) (cons x@7034 x@7035)))))
(declare-fun interleave (list list list) Bool)
(assert (forall ((x@7015 list) (x list) (y list)) (=> (and (= x@7015 y) (= x nil)) (interleave x@7015 x y))))
(assert (forall ((x@7015 list) (x list) (y list) (z Nat@0) (xs list) (x@7016 list)) (=> (and (= x@7015 (cons z x@7016)) (= x (cons z xs)) (interleave x@7016 y xs)) (interleave x@7015 x y))))
(declare-fun evens (list list) Bool)
(declare-fun odds (list list) Bool)
(assert (forall ((x@7017 list) (x list)) (=> (and (= x@7017 nil) (= x nil)) (evens x@7017 x))))
(assert (forall ((x@7017 list) (x list) (y Nat@0) (xs list) (x@7018 list)) (=> (and (= x@7017 (cons y x@7018)) (= x (cons y xs)) (odds x@7018 xs)) (evens x@7017 x))))
(assert (forall ((x@7019 list) (x list)) (=> (and (= x@7019 nil) (= x nil)) (odds x@7019 x))))
(assert (forall ((x@7019 list) (x list) (y Nat@0) (xs list) (x@7020 list)) (=> (and (= x@7019 x@7020) (= x (cons y xs)) (evens x@7020 xs)) (odds x@7019 x))))
(assert (forall ((xs list)) (forall ((x@7023 list) (x@7022 list) (x@7021 list)) (=> (and (diseqlist@103 x@7023 xs) (interleave x@7023 x@7022 x@7021) (evens x@7022 xs) (odds x@7021 xs)) false))))
(check-sat)
(get-info :reason-unknown)
