(set-logic ALL)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-sort Nat@0 0)
(declare-fun Z@0 () Nat@0)
(declare-fun S@0 (Nat@0) Nat@0)
(declare-fun diseqNat@0@183 (Nat@0 Nat@0) Bool)
(assert (forall ((x@9783 Nat@0)) (diseqNat@0@183 Z@0 (S@0 x@9783))))
(assert (forall ((x@9784 Nat@0)) (diseqNat@0@183 (S@0 x@9784) Z@0)))
(assert (forall ((x@9785 Nat@0) (x@9786 Nat@0)) (=> (diseqNat@0@183 x@9785 x@9786) (diseqNat@0@183 (S@0 x@9785) (S@0 x@9786)))))
(declare-sort Bin 0)
(declare-fun One () Bin)
(declare-fun ZeroAnd (Bin) Bin)
(declare-fun OneAnd (Bin) Bin)
(declare-fun diseqBin@10 (Bin Bin) Bool)
(assert (forall ((x@9787 Bin)) (diseqBin@10 One (ZeroAnd x@9787))))
(assert (forall ((x@9788 Bin)) (diseqBin@10 One (OneAnd x@9788))))
(assert (forall ((x@9789 Bin)) (diseqBin@10 (ZeroAnd x@9789) One)))
(assert (forall ((x@9790 Bin)) (diseqBin@10 (OneAnd x@9790) One)))
(assert (forall ((x@9791 Bin) (x@9792 Bin)) (diseqBin@10 (ZeroAnd x@9791) (OneAnd x@9792))))
(assert (forall ((x@9793 Bin) (x@9794 Bin)) (diseqBin@10 (OneAnd x@9793) (ZeroAnd x@9794))))
(assert (forall ((x@9795 Bin) (x@9796 Bin)) (=> (diseqBin@10 x@9795 x@9796) (diseqBin@10 (ZeroAnd x@9795) (ZeroAnd x@9796)))))
(assert (forall ((x@9797 Bin) (x@9798 Bin)) (=> (diseqBin@10 x@9797 x@9798) (diseqBin@10 (OneAnd x@9797) (OneAnd x@9798)))))
(declare-fun s (Bin Bin) Bool)
(assert (forall ((x@9768 Bin) (x Bin)) (=> (and (= x@9768 (ZeroAnd One)) (= x One)) (s x@9768 x))))
(assert (forall ((x@9768 Bin) (x Bin) (xs Bin)) (=> (and (= x@9768 (OneAnd xs)) (= x (ZeroAnd xs))) (s x@9768 x))))
(assert (forall ((x@9768 Bin) (x Bin) (ys Bin) (x@9769 Bin)) (=> (and (= x@9768 (ZeroAnd x@9769)) (= x (OneAnd ys)) (s x@9769 ys)) (s x@9768 x))))
(declare-fun plus (Bin Bin Bin) Bool)
(assert (forall ((x@9770 Bin) (x Bin) (y Bin) (x@9771 Bin)) (=> (and (= x@9770 x@9771) (= x One) (s x@9771 y)) (plus x@9770 x y))))
(assert (forall ((x@9770 Bin) (x Bin) (y Bin) (z Bin) (x@9772 Bin)) (=> (and (= x@9770 x@9772) (= x (ZeroAnd z)) (= y One) (s x@9772 (ZeroAnd z))) (plus x@9770 x y))))
(assert (forall ((x@9770 Bin) (x Bin) (y Bin) (z Bin) (ys Bin) (x@9773 Bin)) (=> (and (= x@9770 (ZeroAnd x@9773)) (= x (ZeroAnd z)) (= y (ZeroAnd ys)) (plus x@9773 z ys)) (plus x@9770 x y))))
(assert (forall ((x@9770 Bin) (x Bin) (y Bin) (z Bin) (xs Bin) (x@9774 Bin)) (=> (and (= x@9770 (OneAnd x@9774)) (= x (ZeroAnd z)) (= y (OneAnd xs)) (plus x@9774 z xs)) (plus x@9770 x y))))
(assert (forall ((x@9770 Bin) (x Bin) (y Bin) (x2 Bin) (x@9775 Bin)) (=> (and (= x@9770 x@9775) (= x (OneAnd x2)) (= y One) (s x@9775 (OneAnd x2))) (plus x@9770 x y))))
(assert (forall ((x@9770 Bin) (x Bin) (y Bin) (x2 Bin) (zs Bin) (x@9776 Bin)) (=> (and (= x@9770 (OneAnd x@9776)) (= x (OneAnd x2)) (= y (ZeroAnd zs)) (plus x@9776 x2 zs)) (plus x@9770 x y))))
(assert (forall ((x@9770 Bin) (x Bin) (y Bin) (x2 Bin) (ys2 Bin) (x@9778 Bin) (x@9777 Bin)) (=> (and (= x@9770 (ZeroAnd x@9778)) (= x (OneAnd x2)) (= y (OneAnd ys2)) (s x@9778 x@9777) (plus x@9777 x2 ys2)) (plus x@9770 x y))))
(assert (forall ((x Bin) (y Bin) (z Bin)) (forall ((x@9782 Bin) (x@9781 Bin) (x@9780 Bin) (x@9779 Bin)) (=> (and (diseqBin@10 x@9782 x@9780) (plus x@9782 x x@9781) (plus x@9781 y z) (plus x@9780 x@9779 z) (plus x@9779 x y)) false))))
(check-sat)
(get-info :reason-unknown)
