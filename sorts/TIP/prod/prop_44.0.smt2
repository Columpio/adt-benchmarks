(set-logic ALL)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-sort Nat@0 0)
(declare-fun Z@0 () Nat@0)
(declare-fun S@0 (Nat@0) Nat@0)
(declare-fun diseqNat@0@2 (Nat@0 Nat@0) Bool)
(assert (forall ((x@61 Nat@0)) (diseqNat@0@2 Z@0 (S@0 x@61))))
(assert (forall ((x@62 Nat@0)) (diseqNat@0@2 (S@0 x@62) Z@0)))
(assert (forall ((x@63 Nat@0) (x@64 Nat@0)) (=> (diseqNat@0@2 x@63 x@64) (diseqNat@0@2 (S@0 x@63) (S@0 x@64)))))
(declare-sort Nat 0)
(declare-fun Z () Nat)
(declare-fun S (Nat) Nat)
(declare-fun diseqNat@2 (Nat Nat) Bool)
(assert (forall ((x@65 Nat)) (diseqNat@2 Z (S x@65))))
(assert (forall ((x@66 Nat)) (diseqNat@2 (S x@66) Z)))
(assert (forall ((x@67 Nat) (x@68 Nat)) (=> (diseqNat@2 x@67 x@68) (diseqNat@2 (S x@67) (S x@68)))))
(declare-sort list 0)
(declare-fun nil () list)
(declare-fun cons (Nat list) list)
(declare-fun diseqlist@1 (list list) Bool)
(assert (forall ((x@69 Nat) (x@70 list)) (diseqlist@1 nil (cons x@69 x@70))))
(assert (forall ((x@71 Nat) (x@72 list)) (diseqlist@1 (cons x@71 x@72) nil)))
(assert (forall ((x@73 Nat) (x@74 list) (x@75 Nat) (x@76 list)) (=> (diseqNat@2 x@73 x@75) (diseqlist@1 (cons x@73 x@74) (cons x@75 x@76)))))
(assert (forall ((x@73 Nat) (x@74 list) (x@75 Nat) (x@76 list)) (=> (diseqlist@1 x@74 x@76) (diseqlist@1 (cons x@73 x@74) (cons x@75 x@76)))))
(declare-fun barbar (Bool Bool Bool) Bool)
(assert (forall ((x@46 Bool) (x Bool) (y Bool)) (=> (= x@46 (ite x true y)) (barbar x@46 x y))))
(declare-fun == (Bool Nat Nat) Bool)
(assert (forall ((x@47 Bool) (x Nat) (y Nat)) (=> (and (= x@47 true) (= x Z) (= y Z)) (== x@47 x y))))
(assert (forall ((x@47 Bool) (x Nat) (y Nat) (z Nat)) (=> (and (= x@47 false) (= x Z) (= y (S z))) (== x@47 x y))))
(assert (forall ((x@47 Bool) (x Nat) (y Nat) (x2 Nat)) (=> (and (= x@47 false) (= x (S x2)) (= y Z)) (== x@47 x y))))
(assert (forall ((x@47 Bool) (x Nat) (y Nat) (x2 Nat) (y2 Nat) (x@48 Bool)) (=> (and (= x@47 x@48) (= x (S x2)) (= y (S y2)) (== x@48 x2 y2)) (== x@47 x y))))
(declare-fun elem (Bool Nat list) Bool)
(assert (forall ((x@49 Bool) (x Nat) (y list)) (=> (and (= x@49 false) (= y nil)) (elem x@49 x y))))
(assert (forall ((x@49 Bool) (x Nat) (y list) (z Nat) (xs list) (x@52 Bool) (x@51 Bool) (x@50 Bool)) (=> (and (= x@49 x@52) (= y (cons z xs)) (barbar x@52 x@51 x@50) (== x@51 x z) (elem x@50 x xs)) (elem x@49 x y))))
(declare-fun intersect2 (list list list) Bool)
(assert (forall ((x@53 list) (x list) (y list)) (=> (and (= x@53 nil) (= x nil)) (intersect2 x@53 x y))))
(assert (forall ((x@53 list) (x list) (y list) (z Nat) (xs list) (x@54 Bool) (x@55 list) (x@56 list)) (=> (and (= x@53 (ite x@54 (cons z x@55) x@56)) (= x (cons z xs)) (elem x@54 z y) (intersect2 x@55 xs y) (intersect2 x@56 xs y)) (intersect2 x@53 x y))))
(assert (forall ((x Nat) (y list) (z list)) (forall ((x@57 Bool) (x@58 Bool) (x@60 Bool) (x@59 list)) (=> (and (not x@60) x@57 (elem x@57 x y) x@58 (elem x@58 x z) (elem x@60 x x@59) (intersect2 x@59 y z)) false))))
(check-sat)
(get-info :reason-unknown)
