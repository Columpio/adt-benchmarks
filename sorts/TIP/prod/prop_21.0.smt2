(set-logic ALL)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-sort Nat@0 0)
(declare-fun Z@0 () Nat@0)
(declare-fun S@0 (Nat@0) Nat@0)
(declare-fun diseqNat@0@16 (Nat@0 Nat@0) Bool)
(assert (forall ((x@433 Nat@0)) (diseqNat@0@16 Z@0 (S@0 x@433))))
(assert (forall ((x@434 Nat@0)) (diseqNat@0@16 (S@0 x@434) Z@0)))
(assert (forall ((x@435 Nat@0) (x@436 Nat@0)) (=> (diseqNat@0@16 x@435 x@436) (diseqNat@0@16 (S@0 x@435) (S@0 x@436)))))
(declare-sort list 0)
(declare-fun nil () list)
(declare-fun cons (Nat@0 list) list)
(declare-fun diseqlist@15 (list list) Bool)
(assert (forall ((x@437 Nat@0) (x@438 list)) (diseqlist@15 nil (cons x@437 x@438))))
(assert (forall ((x@439 Nat@0) (x@440 list)) (diseqlist@15 (cons x@439 x@440) nil)))
(assert (forall ((x@441 Nat@0) (x@442 list) (x@443 Nat@0) (x@444 list)) (=> (diseqNat@0@16 x@441 x@443) (diseqlist@15 (cons x@441 x@442) (cons x@443 x@444)))))
(assert (forall ((x@441 Nat@0) (x@442 list) (x@443 Nat@0) (x@444 list)) (=> (diseqlist@15 x@442 x@444) (diseqlist@15 (cons x@441 x@442) (cons x@443 x@444)))))
(declare-sort Nat 0)
(declare-fun Z () Nat)
(declare-fun S (Nat) Nat)
(declare-fun diseqNat@13 (Nat Nat) Bool)
(assert (forall ((x@445 Nat)) (diseqNat@13 Z (S x@445))))
(assert (forall ((x@446 Nat)) (diseqNat@13 (S x@446) Z)))
(assert (forall ((x@447 Nat) (x@448 Nat)) (=> (diseqNat@13 x@447 x@448) (diseqNat@13 (S x@447) (S x@448)))))
(declare-fun length (Nat list) Bool)
(assert (forall ((x@422 Nat) (x list)) (=> (and (= x@422 Z) (= x nil)) (length x@422 x))))
(assert (forall ((x@422 Nat) (x list) (y Nat@0) (xs list) (x@423 Nat)) (=> (and (= x@422 (S x@423)) (= x (cons y xs)) (length x@423 xs)) (length x@422 x))))
(declare-fun ++ (list list list) Bool)
(assert (forall ((x@424 list) (x list) (y list)) (=> (and (= x@424 y) (= x nil)) (++ x@424 x y))))
(assert (forall ((x@424 list) (x list) (y list) (z Nat@0) (xs list) (x@425 list)) (=> (and (= x@424 (cons z x@425)) (= x (cons z xs)) (++ x@425 xs y)) (++ x@424 x y))))
(declare-fun rotate (list Nat list) Bool)
(assert (forall ((x@426 list) (x Nat) (y list)) (=> (and (= x@426 y) (= x Z)) (rotate x@426 x y))))
(assert (forall ((x@426 list) (x Nat) (y list) (z Nat)) (=> (and (= x@426 nil) (= x (S z)) (= y nil)) (rotate x@426 x y))))
(assert (forall ((x@426 list) (x Nat) (y list) (z Nat) (x2 Nat@0) (x3 list) (x@428 list) (x@427 list)) (=> (and (= x@426 x@428) (= x (S z)) (= y (cons x2 x3)) (rotate x@428 z x@427) (++ x@427 x3 (cons x2 nil))) (rotate x@426 x y))))
(assert (forall ((x list) (y list)) (forall ((x@432 list) (x@431 Nat) (x@430 list) (x@429 list)) (=> (and (diseqlist@15 x@432 x@429) (rotate x@432 x@431 x@430) (length x@431 x) (++ x@430 x y) (++ x@429 y x)) false))))
(check-sat)
(get-info :reason-unknown)
