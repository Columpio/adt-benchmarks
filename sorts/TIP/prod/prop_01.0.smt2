(set-logic ALL)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-sort Nat@0 0)
(declare-fun Z@0 () Nat@0)
(declare-fun S@0 (Nat@0) Nat@0)
(declare-fun diseqNat@0@36 (Nat@0 Nat@0) Bool)
(assert (forall ((x@935 Nat@0)) (diseqNat@0@36 Z@0 (S@0 x@935))))
(assert (forall ((x@936 Nat@0)) (diseqNat@0@36 (S@0 x@936) Z@0)))
(assert (forall ((x@937 Nat@0) (x@938 Nat@0)) (=> (diseqNat@0@36 x@937 x@938) (diseqNat@0@36 (S@0 x@937) (S@0 x@938)))))
(declare-sort Nat 0)
(declare-fun Z () Nat)
(declare-fun S (Nat) Nat)
(declare-fun diseqNat@27 (Nat Nat) Bool)
(assert (forall ((x@939 Nat)) (diseqNat@27 Z (S x@939))))
(assert (forall ((x@940 Nat)) (diseqNat@27 (S x@940) Z)))
(assert (forall ((x@941 Nat) (x@942 Nat)) (=> (diseqNat@27 x@941 x@942) (diseqNat@27 (S x@941) (S x@942)))))
(declare-fun double (Nat Nat) Bool)
(assert (forall ((x@929 Nat) (x Nat)) (=> (and (= x@929 Z) (= x Z)) (double x@929 x))))
(assert (forall ((x@929 Nat) (x Nat) (y Nat) (x@930 Nat)) (=> (and (= x@929 (S (S x@930))) (= x (S y)) (double x@930 y)) (double x@929 x))))
(declare-fun +2 (Nat Nat Nat) Bool)
(assert (forall ((x@931 Nat) (x Nat) (y Nat)) (=> (and (= x@931 y) (= x Z)) (+2 x@931 x y))))
(assert (forall ((x@931 Nat) (x Nat) (y Nat) (z Nat) (x@932 Nat)) (=> (and (= x@931 (S x@932)) (= x (S z)) (+2 x@932 z y)) (+2 x@931 x y))))
(assert (forall ((x Nat)) (forall ((x@934 Nat) (x@933 Nat)) (=> (and (diseqNat@27 x@934 x@933) (double x@934 x) (+2 x@933 x x)) false))))
(check-sat)
(get-info :reason-unknown)
