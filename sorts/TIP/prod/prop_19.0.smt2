(set-logic ALL)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-sort Nat@0 0)
(declare-fun Z@0 () Nat@0)
(declare-fun S@0 (Nat@0) Nat@0)
(declare-fun diseqNat@0@30 (Nat@0 Nat@0) Bool)
(assert (forall ((x@798 Nat@0)) (diseqNat@0@30 Z@0 (S@0 x@798))))
(assert (forall ((x@799 Nat@0)) (diseqNat@0@30 (S@0 x@799) Z@0)))
(assert (forall ((x@800 Nat@0) (x@801 Nat@0)) (=> (diseqNat@0@30 x@800 x@801) (diseqNat@0@30 (S@0 x@800) (S@0 x@801)))))
(declare-sort list 0)
(declare-fun nil () list)
(declare-fun cons (Nat@0 list) list)
(declare-fun diseqlist@27 (list list) Bool)
(assert (forall ((x@802 Nat@0) (x@803 list)) (diseqlist@27 nil (cons x@802 x@803))))
(assert (forall ((x@804 Nat@0) (x@805 list)) (diseqlist@27 (cons x@804 x@805) nil)))
(assert (forall ((x@806 Nat@0) (x@807 list) (x@808 Nat@0) (x@809 list)) (=> (diseqNat@0@30 x@806 x@808) (diseqlist@27 (cons x@806 x@807) (cons x@808 x@809)))))
(assert (forall ((x@806 Nat@0) (x@807 list) (x@808 Nat@0) (x@809 list)) (=> (diseqlist@27 x@807 x@809) (diseqlist@27 (cons x@806 x@807) (cons x@808 x@809)))))
(declare-fun ++ (list list list) Bool)
(assert (forall ((x@787 list) (x list) (y list)) (=> (and (= x@787 y) (= x nil)) (++ x@787 x y))))
(assert (forall ((x@787 list) (x list) (y list) (z Nat@0) (xs list) (x@788 list)) (=> (and (= x@787 (cons z x@788)) (= x (cons z xs)) (++ x@788 xs y)) (++ x@787 x y))))
(declare-fun rev (list list) Bool)
(assert (forall ((x@789 list) (x list)) (=> (and (= x@789 nil) (= x nil)) (rev x@789 x))))
(assert (forall ((x@789 list) (x list) (y Nat@0) (xs list) (x@791 list) (x@790 list)) (=> (and (= x@789 x@791) (= x (cons y xs)) (++ x@791 x@790 (cons y nil)) (rev x@790 xs)) (rev x@789 x))))
(assert (forall ((x list) (y list)) (forall ((x@797 list) (x@796 list) (x@795 list) (x@794 list) (x@793 list) (x@792 list)) (=> (and (diseqlist@27 x@797 x@794) (++ x@797 x@796 y) (rev x@796 x@795) (rev x@795 x) (rev x@794 x@793) (rev x@793 x@792) (++ x@792 x y)) false))))
(check-sat)
(get-info :reason-unknown)
