(set-logic ALL)
(set-option :produce-models true)

(declare-sort Nat 0)
(declare-fun Z () Nat)
(declare-fun S (Nat) Nat)
(declare-sort Tree 0)
(declare-fun node (Nat Tree Tree) Tree)
(declare-fun leaf () Tree)
(declare-sort Lst 0)
(declare-fun cons (Nat Lst) Lst)
(declare-fun nil () Lst)
(declare-sort Lst2 0)
(declare-fun cons2 (Nat Nat Lst2) Lst2)
(declare-fun nil2 () Lst2)
(declare-sort Queue 0)
(declare-fun queue (Lst Lst) Queue)

(declare-fun add (Nat Nat Nat) Bool)
(declare-fun lt (Nat Nat) Bool)
(declare-fun le (Nat Nat) Bool)
(declare-fun gt (Nat Nat) Bool)
(declare-fun ge (Nat Nat) Bool)
(assert (forall ((y Nat)) (add Z y y)))
(assert (forall ((x Nat) (y Nat) (z Nat)) (=> (add x y z) (add (S x) y (S z)))))
(assert (forall ((y Nat)) (lt Z (S y))))
(assert (forall ((x Nat) (y Nat)) (=> (lt x y) (lt (S x) (S y)))))
(assert (forall ((x Nat) (y Nat)) (=> (or (lt x y) (= x y)) (le x y))))
(assert (forall ((x Nat)) (gt (S x) Z)))
(assert (forall ((x Nat) (y Nat)) (=> (gt x y) (gt (S x) (S y)))))
(assert (forall ((x Nat) (y Nat)) (=> (or (gt x y) (= x y)) (ge x y))))

(declare-sort Heap 0)
(declare-fun hleaf () Heap)
(declare-fun heap (Nat Nat Heap Heap) Heap)

(declare-fun rightHeight (Heap Nat) Bool)
(assert (rightHeight hleaf Z))
(assert (forall ((k Nat) (v Nat) (l Heap) (r Heap) (rh Nat)) (=> (rightHeight r rh) (rightHeight (heap k v l r) (S rh)))))
(declare-fun rank (Heap Nat) Bool)
(assert (rank hleaf Z))
(assert (forall ((k Nat) (v Nat) (l Heap) (r Heap)) (rank (heap k v l r) k)))
(declare-fun hasLeftistProperty (Heap) Bool)
(assert (hasLeftistProperty hleaf))
(assert (forall ((k Nat) (v Nat) (l Heap) (r Heap) (rh Nat) (lh Nat))
    (=> (and (hasLeftistProperty l) (hasLeftistProperty r) (rightHeight r rh) (rightHeight l lh) (le rh lh)) (hasLeftistProperty (heap (S rh) v l r)))))
(declare-fun hsize (Heap Nat) Bool)
(assert (hsize hleaf Z))
(assert (forall ((k Nat) (v Nat) (l Heap) (r Heap) (lhrh Nat) (lh Nat) (rh Nat)) (=> (and (hsize l lh) (hsize r rh) (add lh rh lhrh)) (hsize (heap k v l r) (S lhrh)))))
(declare-fun mergea (Nat Heap Heap Heap) Bool)
(assert (forall ((v Nat) (l Heap) (r Heap) (rr Nat) (lr Nat)) (=> (and (rank r rr) (rank l lr) (le rr lr)) (mergea v l r (heap (S rr) v l r)))))
(assert (forall ((v Nat) (l Heap) (r Heap) (rr Nat) (lr Nat)) (=> (and (rank r rr) (rank l lr) (lt lr rr)) (mergea v l r (heap (S lr) v r l)))))
(declare-fun merge (Heap Heap Heap) Bool)
(assert (forall ((h Heap)) (merge h hleaf h)))
(assert (forall ((h Heap)) (merge hleaf h h)))
(assert (forall ((k1 Nat) (v1 Nat) (l1 Heap) (r1 Heap) (k2 Nat) (v2 Nat) (l2 Heap) (r2 Heap) (hh Heap) (ha Heap))
	(=> (and (lt v2 v1) (merge r1 (heap k2 v2 l2 r2) hh) (mergea v1 l1 hh ha)) (merge (heap k1 v1 l1 r1) (heap k2 v2 l2 r2) ha))))
(assert (forall ((k1 Nat) (v1 Nat) (l1 Heap) (r1 Heap) (k2 Nat) (v2 Nat) (l2 Heap) (r2 Heap) (hh Heap) (ha Heap))
	(=> (and (le v1 v2) (merge (heap k1 v1 l1 r1) r2 hh) (mergea v2 l2 hh ha)) (merge (heap k1 v1 l1 r1) (heap k2 v2 l2 r2) ha))))
(declare-fun hinsert (Heap Nat Heap) Bool)
(assert (forall ((h Heap) (n Nat) (h2 Heap)) (=> (merge (heap (S Z) n hleaf hleaf) h h2) (hinsert h n h2))))


(assert (forall ((x Heap) (n Nat) (xn Heap) (rh Nat) (v Nat) (l Heap) (r Heap))
	(=> (and (hasLeftistProperty x) (hinsert x n xn) (= xn (heap (S rh) v l r)) (hasLeftistProperty (heap rh v l r))) false))) ;;;

(check-sat)
(get-model)
