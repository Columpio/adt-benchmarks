(set-logic ALL)
(set-option :produce-models true)

(declare-sort Nat 0)
(declare-fun Z () Nat)
(declare-fun S (Nat) Nat)
(declare-sort Tree 0)
(declare-fun node (Nat Tree Tree) Tree)
(declare-fun leaf () Tree)
(declare-sort Lst 0)
(declare-fun cons (Nat Lst) Lst)
(declare-fun nil () Lst)
(declare-sort Lst2 0)
(declare-fun cons2 (Nat Nat Lst2) Lst2)
(declare-fun nil2 () Lst2)
(declare-sort Queue 0)
(declare-fun queue (Lst Lst) Queue)

(declare-fun add (Nat Nat Nat) Bool)
(declare-fun lt (Nat Nat) Bool)
(declare-fun le (Nat Nat) Bool)
(declare-fun gt (Nat Nat) Bool)
(declare-fun ge (Nat Nat) Bool)
(assert (forall ((y Nat)) (add Z y y)))
(assert (forall ((x Nat) (y Nat) (z Nat)) (=> (add x y z) (add (S x) y (S z)))))
(assert (forall ((y Nat)) (lt Z (S y))))
(assert (forall ((x Nat) (y Nat)) (=> (lt x y) (lt (S x) (S y)))))
(assert (forall ((x Nat) (y Nat)) (=> (or (lt x y) (= x y)) (le x y))))
(assert (forall ((x Nat)) (gt (S x) Z)))
(assert (forall ((x Nat) (y Nat)) (=> (gt x y) (gt (S x) (S y)))))
(assert (forall ((x Nat) (y Nat)) (=> (or (gt x y) (= x y)) (ge x y))))

(declare-fun len (Lst Nat) Bool)
(assert (len nil Z))
(assert (forall ((x Nat) (y Lst) (l Nat)) (=> (len y l) (len (cons x y) (S l)))))
(declare-fun qlen (Queue Nat) Bool)
(assert (forall ((x Lst) (y Lst) (xs Nat) (ys Nat) (s Nat)) (=> (and (len x xs) (len y ys) (add xs ys s)) (qlen (queue x y) s))))
(declare-fun append (Lst Lst Lst) Bool)
(assert (forall ((y Lst)) (append nil y y)))
(assert (forall ((c Nat) (x Lst) (y Lst) (z Lst)) (=> (append x y z) (append (cons c x) y (cons c z)))))
(declare-fun rev2 (Lst Lst Lst) Bool)
(assert (forall ((a Lst)) (rev2 nil a a)))
(assert (forall ((x Nat) (t Lst) (a Lst) (l Lst)) (=> (rev2 t (cons x a) l) (rev2 (cons x t) a l))))

(assert (forall ((x Lst) (xr Lst) (xl Nat) (xrl Nat)) (=> (and (rev2 x nil xr) (len x xl) (len xr xrl) (= (S xl) xrl)) false)))
(check-sat)
(get-model)
