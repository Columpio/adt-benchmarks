(set-logic HORN)
(declare-datatype Nat ((Z ) (S (nextnat Nat))))
(declare-fun add (Nat Nat Nat) Bool)
(declare-fun lt (Nat Nat) Bool)
(declare-fun le (Nat Nat) Bool)
(declare-fun gt (Nat Nat) Bool)
(declare-fun ge (Nat Nat) Bool)
(assert (forall ((y Nat)) (add Z y y)))
(assert (forall ((x Nat) (y Nat) (z Nat)) (=> (add x y z) (add (S x) y (S z)))))
(assert (forall ((y Nat)) (lt Z (S y))))
(assert (forall ((x Nat) (y Nat)) (=> (lt x y) (lt (S x) (S y)))))
(assert (forall ((x Nat) (y Nat)) (=> (or (lt x y) (= x y)) (le x y))))
(assert (forall ((x Nat) (y Nat)) (=> (lt y x) (gt x y))))
(assert (forall ((x Nat) (y Nat)) (=> (le y x) (ge x y))))
(declare-fun diseqNat (Nat Nat) Bool)
(assert (forall ((x Nat)) (diseqNat Z (S x))))
(assert (forall ((x Nat)) (diseqNat (S x) Z)))
(assert (forall ((x Nat) (y Nat)) (=> (diseqNat x y) (diseqNat (S x) (S y)))))
(declare-datatype Lst ((nil ) (cons (car Nat) (cdr Lst))))
(declare-fun diseqLst (Lst Lst) Bool)
(assert (forall ((c Nat) (x Lst)) (diseqLst nil (cons c x))))
(assert (forall ((c Nat) (x Lst)) (diseqLst (cons c x) nil)))
(assert (forall ((c1 Nat) (c2 Nat) (x Lst) (y Lst)) (=> (diseqNat c1 c2) (diseqLst (cons c1 x) (cons c2 y)))))
(assert (forall ((c1 Nat) (c2 Nat) (x Lst) (y Lst)) (=> (diseqLst x y) (diseqLst (cons c1 x) (cons c2 y)))))
(declare-datatype Lst2 ((nil2 ) (cons2 (car1 Nat) (car2 Nat) (cdr2 Lst2))))
(declare-fun diseqLst2 (Lst2 Lst2) Bool)
(assert (forall ((c Nat) (k Nat) (x Lst2)) (diseqLst2 nil2 (cons2 c k x))))
(assert (forall ((c Nat) (k Nat) (x Lst2)) (diseqLst2 (cons2 c k x) nil2)))
(assert (forall ((c1 Nat) (c2 Nat) (k1 Nat) (k2 Nat) (x Lst2) (y Lst2)) (=> (diseqNat c1 c2) (diseqLst2 (cons2 c1 k1 x) (cons2 c2 k2 y)))))
(assert (forall ((c1 Nat) (c2 Nat) (k1 Nat) (k2 Nat) (x Lst2) (y Lst2)) (=> (diseqNat k1 k2) (diseqLst2 (cons2 c1 k1 x) (cons2 c2 k2 y)))))
(assert (forall ((c1 Nat) (c2 Nat) (k1 Nat) (k2 Nat) (x Lst2) (y Lst2)) (=> (diseqLst2 x y) (diseqLst2 (cons2 c1 k1 x) (cons2 c2 k2 y)))))
(declare-fun zappend (Lst2 Lst2 Lst2) Bool)
(declare-fun len (Lst Nat) Bool)
(declare-fun drop (Nat Lst Lst) Bool)
(declare-fun zip (Lst Lst Lst2) Bool)
(declare-fun incorrect () Bool)
(declare-fun take (Nat Lst Lst) Bool)
(declare-fun append (Lst Lst Lst) Bool)
(assert (forall ((A Lst) (B Lst)) (=> (= A nil) (append A B B))))
(assert (forall ((A Nat) (B Lst) (C Lst) (D Lst) (E Lst) (F Lst)) (=> (and (append B E C) (= F (cons A C)) (= D (cons A B))) (append D E F))))
(assert (forall ((A Lst2) (B Lst2)) (=> (= A nil2) (zappend A B B))))
(assert (forall ((A Nat) (B Nat) (C Lst2) (D Lst2) (E Lst2) (F Lst2) (G Lst2)) (let ((a!1 (and (zappend C F D) (= E (cons2 A B C)) (= G (cons2 A B D))))) (=> a!1 (zappend E F G)))))
(assert (forall ((A Lst) (B Nat)) (=> (and (= B Z) (= A nil)) (len A B))))
(assert (forall ((A Nat) (B Lst) (C Nat) (D Lst) (E Nat)) (=> (and (len B C) (= D (cons A B)) (= E (S C)) (ge C Z)) (len D E))))
(assert (forall ((A Nat) (B Lst) (C Lst)) (=> (and (= C nil) (= B nil)) (take A B C))))
(assert (forall ((A Nat) (B Lst) (C Nat) (D Lst) (E Lst)) (=> (and (= E nil) (= C Z) (= D (cons A B))) (take C D E))))
(assert (forall ((A Nat) (B Nat) (C Lst) (D Lst) (E Nat) (F Lst) (G Lst)) (=> (and (take B C D) (= G (cons A D)) (= F (cons A C)) (= E (S B)) (diseqNat E Z)) (take E F G))))
(assert (forall ((A Nat) (B Lst) (C Lst)) (=> (and (= C nil) (= B nil)) (drop A B C))))
(assert (forall ((A Nat) (B Lst) (C Nat) (D Lst) (E Lst)) (=> (and (= D (cons A B)) (= C Z) (= E (cons A B))) (drop C D E))))
(assert (forall ((A Nat) (B Nat) (C Lst) (D Nat) (E Lst) (F Lst)) (=> (and (drop B C F) (= E (cons A C)) (= D (S B)) (diseqNat D Z)) (drop D E F))))
(assert (forall ((A Lst) (B Lst) (C Lst2)) (=> (and (= A nil) (= C nil2)) (zip A B C))))
(assert (forall ((A Nat) (B Lst) (C Lst) (D Lst) (E Lst2)) (=> (and (= C (cons A B)) (= E nil2) (= D nil)) (zip C D E))))
(assert (forall ((A Nat) (B Lst) (C Nat) (D Lst) (E Nat) (F Lst) (G Nat) (H Lst) (I Nat) (J Nat) (K Lst) (L Nat) (M Nat) (N Lst) (O Lst) (P Lst) (Q Lst2) (R Lst) (S Lst) (T Lst2)) (let ((a!1 (and (zip O P Q) (= S (cons M N)) (= T (cons2 L M Q)) (= R (cons L K)) (= S (cons J P)) (= R (cons I O)) (= S (cons G H)) (= R (cons E F)) (= S (cons C D)) (= R (cons A B))))) (=> a!1 (zip R S T)))))
(assert (forall ((A Lst) (B Lst2) (C Lst) (D Lst) (E Lst) (F Lst2) (G Lst2) (H Nat) (I Lst) (J Lst2) (K Lst) (L Nat)) (=> (and (len K L) (append K C A) (zip A D B) (zip C E F) (take H D I) (drop L D E) (zappend J F G) (len K H) (zip K I J) (ge L Z) (ge H Z) (diseqLst2 B G)) incorrect)))
(assert (=> incorrect false))
(check-sat)
