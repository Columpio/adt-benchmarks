(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@395 (Nat@0 Nat@0) Bool)
(assert (forall ((x@17172 Nat@0)) (diseqNat@0@395 Z@0 (S@0 x@17172))))
(assert (forall ((x@17173 Nat@0)) (diseqNat@0@395 (S@0 x@17173) Z@0)))
(assert (forall ((x@17174 Nat@0) (x@17175 Nat@0)) (=> (diseqNat@0@395 x@17174 x@17175) (diseqNat@0@395 (S@0 x@17174) (S@0 x@17175)))))
(declare-datatype Nat ((Z ) (S (proj1-S Nat))))
(declare-fun diseqNat@291 (Nat Nat) Bool)
(assert (forall ((x@17176 Nat)) (diseqNat@291 Z (S x@17176))))
(assert (forall ((x@17177 Nat)) (diseqNat@291 (S x@17177) Z)))
(assert (forall ((x@17178 Nat) (x@17179 Nat)) (=> (diseqNat@291 x@17178 x@17179) (diseqNat@291 (S x@17178) (S x@17179)))))
(declare-datatype list ((nil ) (cons (head Nat) (tail list))))
(declare-fun diseqlist@254 (list list) Bool)
(assert (forall ((x@17180 Nat) (x@17181 list)) (diseqlist@254 nil (cons x@17180 x@17181))))
(assert (forall ((x@17182 Nat) (x@17183 list)) (diseqlist@254 (cons x@17182 x@17183) nil)))
(assert (forall ((x@17184 Nat) (x@17185 list) (x@17186 Nat) (x@17187 list)) (=> (diseqNat@291 x@17184 x@17186) (diseqlist@254 (cons x@17184 x@17185) (cons x@17186 x@17187)))))
(assert (forall ((x@17184 Nat) (x@17185 list) (x@17186 Nat) (x@17187 list)) (=> (diseqlist@254 x@17185 x@17187) (diseqlist@254 (cons x@17184 x@17185) (cons x@17186 x@17187)))))
(declare-fun == (Bool Nat Nat) Bool)
(assert (forall ((x@17155 Bool) (x Nat) (y Nat)) (=> (and (= x@17155 true) (= x Z) (= y Z)) (== x@17155 x y))))
(assert (forall ((x@17155 Bool) (x Nat) (y Nat) (z Nat)) (=> (and (= x@17155 false) (= x Z) (= y (S z))) (== x@17155 x y))))
(assert (forall ((x@17155 Bool) (x Nat) (y Nat) (x2 Nat)) (=> (and (= x@17155 false) (= x (S x2)) (= y Z)) (== x@17155 x y))))
(assert (forall ((x@17155 Bool) (x Nat) (y Nat) (x2 Nat) (y2 Nat) (x@17156 Bool)) (=> (and (= x@17155 x@17156) (= x (S x2)) (= y (S y2)) (== x@17156 x2 y2)) (== x@17155 x y))))
(declare-fun count (Nat Nat list) Bool)
(assert (forall ((x@17157 Nat) (x Nat) (y list)) (=> (and (= x@17157 Z) (= y nil)) (count x@17157 x y))))
(assert (forall ((x@17157 Nat) (x Nat) (y list) (z Nat) (xs list) (x@17158 Bool) (x@17159 Nat) (x@17160 Nat)) (=> (and (= x@17157 (ite x@17158 (S x@17159) x@17160)) (= y (cons z xs)) (== x@17158 x z) (count x@17159 x xs) (count x@17160 x xs)) (count x@17157 x y))))
(declare-fun <=2 (Bool Nat Nat) Bool)
(assert (forall ((x@17161 Bool) (x Nat) (y Nat)) (=> (and (= x@17161 true) (= x Z)) (<=2 x@17161 x y))))
(assert (forall ((x@17161 Bool) (x Nat) (y Nat) (z Nat)) (=> (and (= x@17161 false) (= x (S z)) (= y Z)) (<=2 x@17161 x y))))
(assert (forall ((x@17161 Bool) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@17162 Bool)) (=> (and (= x@17161 x@17162) (= x (S z)) (= y (S x2)) (<=2 x@17162 z x2)) (<=2 x@17161 x y))))
(declare-fun insert2 (list Nat list) Bool)
(assert (forall ((x@17163 list) (x Nat) (y list)) (=> (and (= x@17163 (cons x nil)) (= y nil)) (insert2 x@17163 x y))))
(assert (forall ((x@17163 list) (x Nat) (y list) (z Nat) (xs list) (x@17164 Bool) (x@17165 list)) (=> (and (= x@17163 (ite x@17164 (cons x (cons z xs)) (cons z x@17165))) (= y (cons z xs)) (<=2 x@17164 x z) (insert2 x@17165 x xs)) (insert2 x@17163 x y))))
(declare-fun isort (list list) Bool)
(assert (forall ((x@17166 list) (x list)) (=> (and (= x@17166 nil) (= x nil)) (isort x@17166 x))))
(assert (forall ((x@17166 list) (x list) (y Nat) (xs list) (x@17168 list) (x@17167 list)) (=> (and (= x@17166 x@17168) (= x (cons y xs)) (insert2 x@17168 y x@17167) (isort x@17167 xs)) (isort x@17166 x))))
(assert (forall ((x Nat) (y list)) (forall ((x@17171 Nat) (x@17170 list) (x@17169 Nat)) (=> (and (diseqNat@291 x@17171 x@17169) (count x@17171 x x@17170) (isort x@17170 y) (count x@17169 x y)) false))))
(check-sat)
(get-info :reason-unknown)
