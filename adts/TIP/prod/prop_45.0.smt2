(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@403 (Nat@0 Nat@0) Bool)
(assert (forall ((x@17369 Nat@0)) (diseqNat@0@403 Z@0 (S@0 x@17369))))
(assert (forall ((x@17370 Nat@0)) (diseqNat@0@403 (S@0 x@17370) Z@0)))
(assert (forall ((x@17371 Nat@0) (x@17372 Nat@0)) (=> (diseqNat@0@403 x@17371 x@17372) (diseqNat@0@403 (S@0 x@17371) (S@0 x@17372)))))
(declare-datatype Nat ((Z ) (S (proj1-S Nat))))
(declare-fun diseqNat@296 (Nat Nat) Bool)
(assert (forall ((x@17373 Nat)) (diseqNat@296 Z (S x@17373))))
(assert (forall ((x@17374 Nat)) (diseqNat@296 (S x@17374) Z)))
(assert (forall ((x@17375 Nat) (x@17376 Nat)) (=> (diseqNat@296 x@17375 x@17376) (diseqNat@296 (S x@17375) (S x@17376)))))
(declare-datatype list ((nil ) (cons (head Nat) (tail list))))
(declare-fun diseqlist@261 (list list) Bool)
(assert (forall ((x@17377 Nat) (x@17378 list)) (diseqlist@261 nil (cons x@17377 x@17378))))
(assert (forall ((x@17379 Nat) (x@17380 list)) (diseqlist@261 (cons x@17379 x@17380) nil)))
(assert (forall ((x@17381 Nat) (x@17382 list) (x@17383 Nat) (x@17384 list)) (=> (diseqNat@296 x@17381 x@17383) (diseqlist@261 (cons x@17381 x@17382) (cons x@17383 x@17384)))))
(assert (forall ((x@17381 Nat) (x@17382 list) (x@17383 Nat) (x@17384 list)) (=> (diseqlist@261 x@17382 x@17384) (diseqlist@261 (cons x@17381 x@17382) (cons x@17383 x@17384)))))
(declare-fun barbar (Bool Bool Bool) Bool)
(assert (forall ((x@17355 Bool) (x Bool) (y Bool)) (=> (= x@17355 (ite x true y)) (barbar x@17355 x y))))
(declare-fun == (Bool Nat Nat) Bool)
(assert (forall ((x@17356 Bool) (x Nat) (y Nat)) (=> (and (= x@17356 true) (= x Z) (= y Z)) (== x@17356 x y))))
(assert (forall ((x@17356 Bool) (x Nat) (y Nat) (z Nat)) (=> (and (= x@17356 false) (= x Z) (= y (S z))) (== x@17356 x y))))
(assert (forall ((x@17356 Bool) (x Nat) (y Nat) (x2 Nat)) (=> (and (= x@17356 false) (= x (S x2)) (= y Z)) (== x@17356 x y))))
(assert (forall ((x@17356 Bool) (x Nat) (y Nat) (x2 Nat) (y2 Nat) (x@17357 Bool)) (=> (and (= x@17356 x@17357) (= x (S x2)) (= y (S y2)) (== x@17357 x2 y2)) (== x@17356 x y))))
(declare-fun elem (Bool Nat list) Bool)
(assert (forall ((x@17358 Bool) (x Nat) (y list)) (=> (and (= x@17358 false) (= y nil)) (elem x@17358 x y))))
(assert (forall ((x@17358 Bool) (x Nat) (y list) (z Nat) (xs list) (x@17361 Bool) (x@17360 Bool) (x@17359 Bool)) (=> (and (= x@17358 x@17361) (= y (cons z xs)) (barbar x@17361 x@17360 x@17359) (== x@17360 x z) (elem x@17359 x xs)) (elem x@17358 x y))))
(declare-fun <=2 (Bool Nat Nat) Bool)
(assert (forall ((x@17362 Bool) (x Nat) (y Nat)) (=> (and (= x@17362 true) (= x Z)) (<=2 x@17362 x y))))
(assert (forall ((x@17362 Bool) (x Nat) (y Nat) (z Nat)) (=> (and (= x@17362 false) (= x (S z)) (= y Z)) (<=2 x@17362 x y))))
(assert (forall ((x@17362 Bool) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@17363 Bool)) (=> (and (= x@17362 x@17363) (= x (S z)) (= y (S x2)) (<=2 x@17363 z x2)) (<=2 x@17362 x y))))
(declare-fun insert2 (list Nat list) Bool)
(assert (forall ((x@17364 list) (x Nat) (y list)) (=> (and (= x@17364 (cons x nil)) (= y nil)) (insert2 x@17364 x y))))
(assert (forall ((x@17364 list) (x Nat) (y list) (z Nat) (xs list) (x@17365 Bool) (x@17366 list)) (=> (and (= x@17364 (ite x@17365 (cons x (cons z xs)) (cons z x@17366))) (= y (cons z xs)) (<=2 x@17365 x z) (insert2 x@17366 x xs)) (insert2 x@17364 x y))))
(assert (forall ((x Nat) (y list)) (forall ((x@17368 Bool) (x@17367 list)) (=> (and (not x@17368) (elem x@17368 x x@17367) (insert2 x@17367 x y)) false))))
(check-sat)
(get-info :reason-unknown)
