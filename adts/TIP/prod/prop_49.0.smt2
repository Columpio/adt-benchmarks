(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@382 (Nat@0 Nat@0) Bool)
(assert (forall ((x@16818 Nat@0)) (diseqNat@0@382 Z@0 (S@0 x@16818))))
(assert (forall ((x@16819 Nat@0)) (diseqNat@0@382 (S@0 x@16819) Z@0)))
(assert (forall ((x@16820 Nat@0) (x@16821 Nat@0)) (=> (diseqNat@0@382 x@16820 x@16821) (diseqNat@0@382 (S@0 x@16820) (S@0 x@16821)))))
(declare-datatype Nat ((Z ) (S (proj1-S Nat))))
(declare-fun diseqNat@280 (Nat Nat) Bool)
(assert (forall ((x@16822 Nat)) (diseqNat@280 Z (S x@16822))))
(assert (forall ((x@16823 Nat)) (diseqNat@280 (S x@16823) Z)))
(assert (forall ((x@16824 Nat) (x@16825 Nat)) (=> (diseqNat@280 x@16824 x@16825) (diseqNat@280 (S x@16824) (S x@16825)))))
(declare-datatype list ((nil ) (cons (head Nat) (tail list))))
(declare-fun diseqlist@241 (list list) Bool)
(assert (forall ((x@16826 Nat) (x@16827 list)) (diseqlist@241 nil (cons x@16826 x@16827))))
(assert (forall ((x@16828 Nat) (x@16829 list)) (diseqlist@241 (cons x@16828 x@16829) nil)))
(assert (forall ((x@16830 Nat) (x@16831 list) (x@16832 Nat) (x@16833 list)) (=> (diseqNat@280 x@16830 x@16832) (diseqlist@241 (cons x@16830 x@16831) (cons x@16832 x@16833)))))
(assert (forall ((x@16830 Nat) (x@16831 list) (x@16832 Nat) (x@16833 list)) (=> (diseqlist@241 x@16831 x@16833) (diseqlist@241 (cons x@16830 x@16831) (cons x@16832 x@16833)))))
(declare-fun barbar (Bool Bool Bool) Bool)
(assert (forall ((x@16800 Bool) (x Bool) (y Bool)) (=> (= x@16800 (ite x true y)) (barbar x@16800 x y))))
(declare-fun == (Bool Nat Nat) Bool)
(assert (forall ((x@16801 Bool) (x Nat) (y Nat)) (=> (and (= x@16801 true) (= x Z) (= y Z)) (== x@16801 x y))))
(assert (forall ((x@16801 Bool) (x Nat) (y Nat) (z Nat)) (=> (and (= x@16801 false) (= x Z) (= y (S z))) (== x@16801 x y))))
(assert (forall ((x@16801 Bool) (x Nat) (y Nat) (x2 Nat)) (=> (and (= x@16801 false) (= x (S x2)) (= y Z)) (== x@16801 x y))))
(assert (forall ((x@16801 Bool) (x Nat) (y Nat) (x2 Nat) (y2 Nat) (x@16802 Bool)) (=> (and (= x@16801 x@16802) (= x (S x2)) (= y (S y2)) (== x@16802 x2 y2)) (== x@16801 x y))))
(declare-fun elem (Bool Nat list) Bool)
(assert (forall ((x@16803 Bool) (x Nat) (y list)) (=> (and (= x@16803 false) (= y nil)) (elem x@16803 x y))))
(assert (forall ((x@16803 Bool) (x Nat) (y list) (z Nat) (xs list) (x@16806 Bool) (x@16805 Bool) (x@16804 Bool)) (=> (and (= x@16803 x@16806) (= y (cons z xs)) (barbar x@16806 x@16805 x@16804) (== x@16805 x z) (elem x@16804 x xs)) (elem x@16803 x y))))
(declare-fun <=2 (Bool Nat Nat) Bool)
(assert (forall ((x@16807 Bool) (x Nat) (y Nat)) (=> (and (= x@16807 true) (= x Z)) (<=2 x@16807 x y))))
(assert (forall ((x@16807 Bool) (x Nat) (y Nat) (z Nat)) (=> (and (= x@16807 false) (= x (S z)) (= y Z)) (<=2 x@16807 x y))))
(assert (forall ((x@16807 Bool) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@16808 Bool)) (=> (and (= x@16807 x@16808) (= x (S z)) (= y (S x2)) (<=2 x@16808 z x2)) (<=2 x@16807 x y))))
(declare-fun insert2 (list Nat list) Bool)
(assert (forall ((x@16809 list) (x Nat) (y list)) (=> (and (= x@16809 (cons x nil)) (= y nil)) (insert2 x@16809 x y))))
(assert (forall ((x@16809 list) (x Nat) (y list) (z Nat) (xs list) (x@16810 Bool) (x@16811 list)) (=> (and (= x@16809 (ite x@16810 (cons x (cons z xs)) (cons z x@16811))) (= y (cons z xs)) (<=2 x@16810 x z) (insert2 x@16811 x xs)) (insert2 x@16809 x y))))
(declare-fun isort (list list) Bool)
(assert (forall ((x@16812 list) (x list)) (=> (and (= x@16812 nil) (= x nil)) (isort x@16812 x))))
(assert (forall ((x@16812 list) (x list) (y Nat) (xs list) (x@16814 list) (x@16813 list)) (=> (and (= x@16812 x@16814) (= x (cons y xs)) (insert2 x@16814 y x@16813) (isort x@16813 xs)) (isort x@16812 x))))
(assert (forall ((x Nat) (y list)) (forall ((x@16816 Bool) (x@16815 list) (x@16817 Bool)) (=> (and (not x@16817) x@16816 (elem x@16816 x x@16815) (isort x@16815 y) (elem x@16817 x y)) false))))
(check-sat)
(get-info :reason-unknown)
