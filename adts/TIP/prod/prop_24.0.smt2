(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@411 (Nat@0 Nat@0) Bool)
(assert (forall ((x@17586 Nat@0)) (diseqNat@0@411 Z@0 (S@0 x@17586))))
(assert (forall ((x@17587 Nat@0)) (diseqNat@0@411 (S@0 x@17587) Z@0)))
(assert (forall ((x@17588 Nat@0) (x@17589 Nat@0)) (=> (diseqNat@0@411 x@17588 x@17589) (diseqNat@0@411 (S@0 x@17588) (S@0 x@17589)))))
(declare-datatype Nat ((Z ) (S (proj1-S Nat))))
(declare-fun diseqNat@302 (Nat Nat) Bool)
(assert (forall ((x@17590 Nat)) (diseqNat@302 Z (S x@17590))))
(assert (forall ((x@17591 Nat)) (diseqNat@302 (S x@17591) Z)))
(assert (forall ((x@17592 Nat) (x@17593 Nat)) (=> (diseqNat@302 x@17592 x@17593) (diseqNat@302 (S x@17592) (S x@17593)))))
(declare-fun even (Bool Nat) Bool)
(assert (forall ((x@17578 Bool) (x Nat)) (=> (and (= x@17578 true) (= x Z)) (even x@17578 x))))
(assert (forall ((x@17578 Bool) (x Nat) (y Nat)) (=> (and (= x@17578 false) (= x (S y)) (= y Z)) (even x@17578 x))))
(assert (forall ((x@17578 Bool) (x Nat) (y Nat) (z Nat) (x@17579 Bool)) (=> (and (= x@17578 x@17579) (= x (S y)) (= y (S z)) (even x@17579 z)) (even x@17578 x))))
(declare-fun +2 (Nat Nat Nat) Bool)
(assert (forall ((x@17580 Nat) (x Nat) (y Nat)) (=> (and (= x@17580 y) (= x Z)) (+2 x@17580 x y))))
(assert (forall ((x@17580 Nat) (x Nat) (y Nat) (z Nat) (x@17581 Nat)) (=> (and (= x@17580 (S x@17581)) (= x (S z)) (+2 x@17581 z y)) (+2 x@17580 x y))))
(assert (forall ((x Nat) (y Nat)) (forall ((x@17585 Bool) (x@17584 Nat) (x@17583 Bool) (x@17582 Nat)) (=> (and (diseqBool@0 x@17585 x@17583) (even x@17585 x@17584) (+2 x@17584 x y) (even x@17583 x@17582) (+2 x@17582 y x)) false))))
(check-sat)
(get-info :reason-unknown)
