(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@406 (Nat@0 Nat@0) Bool)
(assert (forall ((x@17452 Nat@0)) (diseqNat@0@406 Z@0 (S@0 x@17452))))
(assert (forall ((x@17453 Nat@0)) (diseqNat@0@406 (S@0 x@17453) Z@0)))
(assert (forall ((x@17454 Nat@0) (x@17455 Nat@0)) (=> (diseqNat@0@406 x@17454 x@17455) (diseqNat@0@406 (S@0 x@17454) (S@0 x@17455)))))
(declare-datatype Nat ((Z ) (S (proj1-S Nat))))
(declare-fun diseqNat@299 (Nat Nat) Bool)
(assert (forall ((x@17456 Nat)) (diseqNat@299 Z (S x@17456))))
(assert (forall ((x@17457 Nat)) (diseqNat@299 (S x@17457) Z)))
(assert (forall ((x@17458 Nat) (x@17459 Nat)) (=> (diseqNat@299 x@17458 x@17459) (diseqNat@299 (S x@17458) (S x@17459)))))
(declare-datatype list ((nil ) (cons (head Nat) (tail list))))
(declare-fun diseqlist@263 (list list) Bool)
(assert (forall ((x@17460 Nat) (x@17461 list)) (diseqlist@263 nil (cons x@17460 x@17461))))
(assert (forall ((x@17462 Nat) (x@17463 list)) (diseqlist@263 (cons x@17462 x@17463) nil)))
(assert (forall ((x@17464 Nat) (x@17465 list) (x@17466 Nat) (x@17467 list)) (=> (diseqNat@299 x@17464 x@17466) (diseqlist@263 (cons x@17464 x@17465) (cons x@17466 x@17467)))))
(assert (forall ((x@17464 Nat) (x@17465 list) (x@17466 Nat) (x@17467 list)) (=> (diseqlist@263 x@17465 x@17467) (diseqlist@263 (cons x@17464 x@17465) (cons x@17466 x@17467)))))
(declare-fun barbar (Bool Bool Bool) Bool)
(assert (forall ((x@17434 Bool) (x Bool) (y Bool)) (=> (= x@17434 (ite x true y)) (barbar x@17434 x y))))
(declare-fun == (Bool Nat Nat) Bool)
(assert (forall ((x@17435 Bool) (x Nat) (y Nat)) (=> (and (= x@17435 true) (= x Z) (= y Z)) (== x@17435 x y))))
(assert (forall ((x@17435 Bool) (x Nat) (y Nat) (z Nat)) (=> (and (= x@17435 false) (= x Z) (= y (S z))) (== x@17435 x y))))
(assert (forall ((x@17435 Bool) (x Nat) (y Nat) (x2 Nat)) (=> (and (= x@17435 false) (= x (S x2)) (= y Z)) (== x@17435 x y))))
(assert (forall ((x@17435 Bool) (x Nat) (y Nat) (x2 Nat) (y2 Nat) (x@17436 Bool)) (=> (and (= x@17435 x@17436) (= x (S x2)) (= y (S y2)) (== x@17436 x2 y2)) (== x@17435 x y))))
(declare-fun elem (Bool Nat list) Bool)
(assert (forall ((x@17437 Bool) (x Nat) (y list)) (=> (and (= x@17437 false) (= y nil)) (elem x@17437 x y))))
(assert (forall ((x@17437 Bool) (x Nat) (y list) (z Nat) (xs list) (x@17440 Bool) (x@17439 Bool) (x@17438 Bool)) (=> (and (= x@17437 x@17440) (= y (cons z xs)) (barbar x@17440 x@17439 x@17438) (== x@17439 x z) (elem x@17438 x xs)) (elem x@17437 x y))))
(declare-fun <=2 (Bool Nat Nat) Bool)
(assert (forall ((x@17441 Bool) (x Nat) (y Nat)) (=> (and (= x@17441 true) (= x Z)) (<=2 x@17441 x y))))
(assert (forall ((x@17441 Bool) (x Nat) (y Nat) (z Nat)) (=> (and (= x@17441 false) (= x (S z)) (= y Z)) (<=2 x@17441 x y))))
(assert (forall ((x@17441 Bool) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@17442 Bool)) (=> (and (= x@17441 x@17442) (= x (S z)) (= y (S x2)) (<=2 x@17442 z x2)) (<=2 x@17441 x y))))
(declare-fun insert2 (list Nat list) Bool)
(assert (forall ((x@17443 list) (x Nat) (y list)) (=> (and (= x@17443 (cons x nil)) (= y nil)) (insert2 x@17443 x y))))
(assert (forall ((x@17443 list) (x Nat) (y list) (z Nat) (xs list) (x@17444 Bool) (x@17445 list)) (=> (and (= x@17443 (ite x@17444 (cons x (cons z xs)) (cons z x@17445))) (= y (cons z xs)) (<=2 x@17444 x z) (insert2 x@17445 x xs)) (insert2 x@17443 x y))))
(declare-fun /= (Bool Nat Nat) Bool)
(assert (forall ((x@17446 Bool) (x Nat) (y Nat) (x@17447 Bool)) (=> (and (diseqBool@0 x@17446 x@17447) (== x@17447 x y)) (/= x@17446 x y))))
(assert (forall ((x Nat) (y Nat) (z list)) (forall ((x@17448 Bool) (x@17451 Bool) (x@17450 list) (x@17449 Bool)) (=> (and (diseqBool@0 x@17451 x@17449) x@17448 (/= x@17448 x y) (elem x@17451 x x@17450) (insert2 x@17450 y z) (elem x@17449 x z)) false))))
(check-sat)
(get-info :reason-unknown)
