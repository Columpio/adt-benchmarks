(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@394 (Nat@0 Nat@0) Bool)
(assert (forall ((x@17143 Nat@0)) (diseqNat@0@394 Z@0 (S@0 x@17143))))
(assert (forall ((x@17144 Nat@0)) (diseqNat@0@394 (S@0 x@17144) Z@0)))
(assert (forall ((x@17145 Nat@0) (x@17146 Nat@0)) (=> (diseqNat@0@394 x@17145 x@17146) (diseqNat@0@394 (S@0 x@17145) (S@0 x@17146)))))
(declare-datatype list ((nil ) (cons (head Nat@0) (tail list))))
(declare-fun diseqlist@253 (list list) Bool)
(assert (forall ((x@17147 Nat@0) (x@17148 list)) (diseqlist@253 nil (cons x@17147 x@17148))))
(assert (forall ((x@17149 Nat@0) (x@17150 list)) (diseqlist@253 (cons x@17149 x@17150) nil)))
(assert (forall ((x@17151 Nat@0) (x@17152 list) (x@17153 Nat@0) (x@17154 list)) (=> (diseqNat@0@394 x@17151 x@17153) (diseqlist@253 (cons x@17151 x@17152) (cons x@17153 x@17154)))))
(assert (forall ((x@17151 Nat@0) (x@17152 list) (x@17153 Nat@0) (x@17154 list)) (=> (diseqlist@253 x@17152 x@17154) (diseqlist@253 (cons x@17151 x@17152) (cons x@17153 x@17154)))))
(declare-fun ++ (list list list) Bool)
(assert (forall ((x@17130 list) (x list) (y list)) (=> (and (= x@17130 y) (= x nil)) (++ x@17130 x y))))
(assert (forall ((x@17130 list) (x list) (y list) (z Nat@0) (xs list) (x@17131 list)) (=> (and (= x@17130 (cons z x@17131)) (= x (cons z xs)) (++ x@17131 xs y)) (++ x@17130 x y))))
(declare-fun rev (list list) Bool)
(assert (forall ((x@17132 list) (x list)) (=> (and (= x@17132 nil) (= x nil)) (rev x@17132 x))))
(assert (forall ((x@17132 list) (x list) (y Nat@0) (xs list) (x@17134 list) (x@17133 list)) (=> (and (= x@17132 x@17134) (= x (cons y xs)) (++ x@17134 x@17133 (cons y nil)) (rev x@17133 xs)) (rev x@17132 x))))
(assert (forall ((x list) (y list)) (forall ((x@17142 list) (x@17141 list) (x@17140 list) (x@17139 list) (x@17138 list) (x@17137 list) (x@17136 list) (x@17135 list)) (=> (and (diseqlist@253 x@17142 x@17139) (rev x@17142 x@17141) (rev x@17141 x@17140) (++ x@17140 x y) (++ x@17139 x@17138 x@17136) (rev x@17138 x@17137) (rev x@17137 x) (rev x@17136 x@17135) (rev x@17135 y)) false))))
(check-sat)
(get-info :reason-unknown)
