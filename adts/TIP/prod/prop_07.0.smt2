(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@408 (Nat@0 Nat@0) Bool)
(assert (forall ((x@17502 Nat@0)) (diseqNat@0@408 Z@0 (S@0 x@17502))))
(assert (forall ((x@17503 Nat@0)) (diseqNat@0@408 (S@0 x@17503) Z@0)))
(assert (forall ((x@17504 Nat@0) (x@17505 Nat@0)) (=> (diseqNat@0@408 x@17504 x@17505) (diseqNat@0@408 (S@0 x@17504) (S@0 x@17505)))))
(declare-datatype list ((nil ) (cons (head Nat@0) (tail list))))
(declare-fun diseqlist@265 (list list) Bool)
(assert (forall ((x@17506 Nat@0) (x@17507 list)) (diseqlist@265 nil (cons x@17506 x@17507))))
(assert (forall ((x@17508 Nat@0) (x@17509 list)) (diseqlist@265 (cons x@17508 x@17509) nil)))
(assert (forall ((x@17510 Nat@0) (x@17511 list) (x@17512 Nat@0) (x@17513 list)) (=> (diseqNat@0@408 x@17510 x@17512) (diseqlist@265 (cons x@17510 x@17511) (cons x@17512 x@17513)))))
(assert (forall ((x@17510 Nat@0) (x@17511 list) (x@17512 Nat@0) (x@17513 list)) (=> (diseqlist@265 x@17511 x@17513) (diseqlist@265 (cons x@17510 x@17511) (cons x@17512 x@17513)))))
(declare-datatype Nat ((Z ) (S (proj1-S Nat))))
(declare-fun diseqNat@300 (Nat Nat) Bool)
(assert (forall ((x@17514 Nat)) (diseqNat@300 Z (S x@17514))))
(assert (forall ((x@17515 Nat)) (diseqNat@300 (S x@17515) Z)))
(assert (forall ((x@17516 Nat) (x@17517 Nat)) (=> (diseqNat@300 x@17516 x@17517) (diseqNat@300 (S x@17516) (S x@17517)))))
(declare-fun qrev (list list list) Bool)
(assert (forall ((x@17491 list) (x list) (y list)) (=> (and (= x@17491 y) (= x nil)) (qrev x@17491 x y))))
(assert (forall ((x@17491 list) (x list) (y list) (z Nat@0) (xs list) (x@17492 list)) (=> (and (= x@17491 x@17492) (= x (cons z xs)) (qrev x@17492 xs (cons z y))) (qrev x@17491 x y))))
(declare-fun length (Nat list) Bool)
(assert (forall ((x@17493 Nat) (x list)) (=> (and (= x@17493 Z) (= x nil)) (length x@17493 x))))
(assert (forall ((x@17493 Nat) (x list) (y Nat@0) (xs list) (x@17494 Nat)) (=> (and (= x@17493 (S x@17494)) (= x (cons y xs)) (length x@17494 xs)) (length x@17493 x))))
(declare-fun +2 (Nat Nat Nat) Bool)
(assert (forall ((x@17495 Nat) (x Nat) (y Nat)) (=> (and (= x@17495 y) (= x Z)) (+2 x@17495 x y))))
(assert (forall ((x@17495 Nat) (x Nat) (y Nat) (z Nat) (x@17496 Nat)) (=> (and (= x@17495 (S x@17496)) (= x (S z)) (+2 x@17496 z y)) (+2 x@17495 x y))))
(assert (forall ((x list) (y list)) (forall ((x@17501 Nat) (x@17500 list) (x@17499 Nat) (x@17498 Nat) (x@17497 Nat)) (=> (and (diseqNat@300 x@17501 x@17499) (length x@17501 x@17500) (qrev x@17500 x y) (+2 x@17499 x@17498 x@17497) (length x@17498 x) (length x@17497 y)) false))))
(check-sat)
(get-info :reason-unknown)
