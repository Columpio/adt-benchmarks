(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@389 (Nat@0 Nat@0) Bool)
(assert (forall ((x@16999 Nat@0)) (diseqNat@0@389 Z@0 (S@0 x@16999))))
(assert (forall ((x@17000 Nat@0)) (diseqNat@0@389 (S@0 x@17000) Z@0)))
(assert (forall ((x@17001 Nat@0) (x@17002 Nat@0)) (=> (diseqNat@0@389 x@17001 x@17002) (diseqNat@0@389 (S@0 x@17001) (S@0 x@17002)))))
(declare-datatype Nat ((Z ) (S (proj1-S Nat))))
(declare-fun diseqNat@286 (Nat Nat) Bool)
(assert (forall ((x@17003 Nat)) (diseqNat@286 Z (S x@17003))))
(assert (forall ((x@17004 Nat)) (diseqNat@286 (S x@17004) Z)))
(assert (forall ((x@17005 Nat) (x@17006 Nat)) (=> (diseqNat@286 x@17005 x@17006) (diseqNat@286 (S x@17005) (S x@17006)))))
(declare-datatype list ((nil ) (cons (head Nat) (tail list))))
(declare-fun diseqlist@248 (list list) Bool)
(assert (forall ((x@17007 Nat) (x@17008 list)) (diseqlist@248 nil (cons x@17007 x@17008))))
(assert (forall ((x@17009 Nat) (x@17010 list)) (diseqlist@248 (cons x@17009 x@17010) nil)))
(assert (forall ((x@17011 Nat) (x@17012 list) (x@17013 Nat) (x@17014 list)) (=> (diseqNat@286 x@17011 x@17013) (diseqlist@248 (cons x@17011 x@17012) (cons x@17013 x@17014)))))
(assert (forall ((x@17011 Nat) (x@17012 list) (x@17013 Nat) (x@17014 list)) (=> (diseqlist@248 x@17012 x@17014) (diseqlist@248 (cons x@17011 x@17012) (cons x@17013 x@17014)))))
(declare-fun <=2 (Bool Nat Nat) Bool)
(assert (forall ((x@16984 Bool) (x Nat) (y Nat)) (=> (and (= x@16984 true) (= x Z)) (<=2 x@16984 x y))))
(assert (forall ((x@16984 Bool) (x Nat) (y Nat) (z Nat)) (=> (and (= x@16984 false) (= x (S z)) (= y Z)) (<=2 x@16984 x y))))
(assert (forall ((x@16984 Bool) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@16985 Bool)) (=> (and (= x@16984 x@16985) (= x (S z)) (= y (S x2)) (<=2 x@16985 z x2)) (<=2 x@16984 x y))))
(declare-fun insert2 (list Nat list) Bool)
(assert (forall ((x@16986 list) (x Nat) (y list)) (=> (and (= x@16986 (cons x nil)) (= y nil)) (insert2 x@16986 x y))))
(assert (forall ((x@16986 list) (x Nat) (y list) (z Nat) (xs list) (x@16987 Bool) (x@16988 list)) (=> (and (= x@16986 (ite x@16987 (cons x (cons z xs)) (cons z x@16988))) (= y (cons z xs)) (<=2 x@16987 x z) (insert2 x@16988 x xs)) (insert2 x@16986 x y))))
(declare-fun isort (list list) Bool)
(assert (forall ((x@16989 list) (x list)) (=> (and (= x@16989 nil) (= x nil)) (isort x@16989 x))))
(assert (forall ((x@16989 list) (x list) (y Nat) (xs list) (x@16991 list) (x@16990 list)) (=> (and (= x@16989 x@16991) (= x (cons y xs)) (insert2 x@16991 y x@16990) (isort x@16990 xs)) (isort x@16989 x))))
(declare-fun && (Bool Bool Bool) Bool)
(assert (forall ((x@16992 Bool) (x Bool) (y Bool)) (=> (= x@16992 (ite x y false)) (&& x@16992 x y))))
(declare-fun sorted (Bool list) Bool)
(assert (forall ((x@16993 Bool) (x list)) (=> (and (= x@16993 true) (= x nil)) (sorted x@16993 x))))
(assert (forall ((x@16993 Bool) (x list) (y Nat) (z list)) (=> (and (= x@16993 true) (= x (cons y z)) (= z nil)) (sorted x@16993 x))))
(assert (forall ((x@16993 Bool) (x list) (y Nat) (z list) (y2 Nat) (xs list) (x@16996 Bool) (x@16995 Bool) (x@16994 Bool)) (=> (and (= x@16993 x@16996) (= x (cons y z)) (= z (cons y2 xs)) (&& x@16996 x@16995 x@16994) (<=2 x@16995 y y2) (sorted x@16994 (cons y2 xs))) (sorted x@16993 x))))
(assert (forall ((x list)) (forall ((x@16998 Bool) (x@16997 list)) (=> (and (not x@16998) (sorted x@16998 x@16997) (isort x@16997 x)) false))))
(check-sat)
(get-info :reason-unknown)
