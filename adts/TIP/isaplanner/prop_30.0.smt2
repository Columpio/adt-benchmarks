(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@681 (Nat@0 Nat@0) Bool)
(assert (forall ((x@31522 Nat@0)) (diseqNat@0@681 Z@0 (S@0 x@31522))))
(assert (forall ((x@31523 Nat@0)) (diseqNat@0@681 (S@0 x@31523) Z@0)))
(assert (forall ((x@31524 Nat@0) (x@31525 Nat@0)) (=> (diseqNat@0@681 x@31524 x@31525) (diseqNat@0@681 (S@0 x@31524) (S@0 x@31525)))))
(declare-datatype Nat ((Z ) (S (proj1-S Nat))))
(declare-fun diseqNat@489 (Nat Nat) Bool)
(assert (forall ((x@31526 Nat)) (diseqNat@489 Z (S x@31526))))
(assert (forall ((x@31527 Nat)) (diseqNat@489 (S x@31527) Z)))
(assert (forall ((x@31528 Nat) (x@31529 Nat)) (=> (diseqNat@489 x@31528 x@31529) (diseqNat@489 (S x@31528) (S x@31529)))))
(declare-datatype list ((nil ) (cons (head Nat) (tail list))))
(declare-fun diseqlist@420 (list list) Bool)
(assert (forall ((x@31530 Nat) (x@31531 list)) (diseqlist@420 nil (cons x@31530 x@31531))))
(assert (forall ((x@31532 Nat) (x@31533 list)) (diseqlist@420 (cons x@31532 x@31533) nil)))
(assert (forall ((x@31534 Nat) (x@31535 list) (x@31536 Nat) (x@31537 list)) (=> (diseqNat@489 x@31534 x@31536) (diseqlist@420 (cons x@31534 x@31535) (cons x@31536 x@31537)))))
(assert (forall ((x@31534 Nat) (x@31535 list) (x@31536 Nat) (x@31537 list)) (=> (diseqlist@420 x@31535 x@31537) (diseqlist@420 (cons x@31534 x@31535) (cons x@31536 x@31537)))))
(declare-fun == (Bool Nat Nat) Bool)
(assert (forall ((x@31510 Bool) (x Nat) (y Nat)) (=> (and (= x@31510 true) (= x Z) (= y Z)) (== x@31510 x y))))
(assert (forall ((x@31510 Bool) (x Nat) (y Nat) (z Nat)) (=> (and (= x@31510 false) (= x Z) (= y (S z))) (== x@31510 x y))))
(assert (forall ((x@31510 Bool) (x Nat) (y Nat) (x2 Nat)) (=> (and (= x@31510 false) (= x (S x2)) (= y Z)) (== x@31510 x y))))
(assert (forall ((x@31510 Bool) (x Nat) (y Nat) (x2 Nat) (y2 Nat) (x@31511 Bool)) (=> (and (= x@31510 x@31511) (= x (S x2)) (= y (S y2)) (== x@31511 x2 y2)) (== x@31510 x y))))
(declare-fun elem (Bool Nat list) Bool)
(assert (forall ((x@31512 Bool) (x Nat) (y list)) (=> (and (= x@31512 false) (= y nil)) (elem x@31512 x y))))
(assert (forall ((x@31512 Bool) (x Nat) (y list) (z Nat) (xs list) (x@31513 Bool) (x@31514 Bool)) (=> (and (= x@31512 (ite x@31513 true x@31514)) (= y (cons z xs)) (== x@31513 x z) (elem x@31514 x xs)) (elem x@31512 x y))))
(declare-fun <2 (Bool Nat Nat) Bool)
(assert (forall ((x@31515 Bool) (x Nat) (y Nat)) (=> (and (= x@31515 false) (= y Z)) (<2 x@31515 x y))))
(assert (forall ((x@31515 Bool) (x Nat) (y Nat) (z Nat)) (=> (and (= x@31515 true) (= y (S z)) (= x Z)) (<2 x@31515 x y))))
(assert (forall ((x@31515 Bool) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@31516 Bool)) (=> (and (= x@31515 x@31516) (= y (S z)) (= x (S x2)) (<2 x@31516 x2 z)) (<2 x@31515 x y))))
(declare-fun ins (list Nat list) Bool)
(assert (forall ((x@31517 list) (x Nat) (y list)) (=> (and (= x@31517 (cons x nil)) (= y nil)) (ins x@31517 x y))))
(assert (forall ((x@31517 list) (x Nat) (y list) (z Nat) (xs list) (x@31518 Bool) (x@31519 list)) (=> (and (= x@31517 (ite x@31518 (cons x (cons z xs)) (cons z x@31519))) (= y (cons z xs)) (<2 x@31518 x z) (ins x@31519 x xs)) (ins x@31517 x y))))
(assert (forall ((x Nat) (xs list)) (forall ((x@31521 Bool) (x@31520 list)) (=> (and (not x@31521) (elem x@31521 x x@31520) (ins x@31520 x xs)) false))))
(check-sat)
(get-info :reason-unknown)
