(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@719 (Nat@0 Nat@0) Bool)
(assert (forall ((x@32468 Nat@0)) (diseqNat@0@719 Z@0 (S@0 x@32468))))
(assert (forall ((x@32469 Nat@0)) (diseqNat@0@719 (S@0 x@32469) Z@0)))
(assert (forall ((x@32470 Nat@0) (x@32471 Nat@0)) (=> (diseqNat@0@719 x@32470 x@32471) (diseqNat@0@719 (S@0 x@32470) (S@0 x@32471)))))
(declare-datatype Nat ((Z ) (S (proj1-S Nat))))
(declare-fun diseqNat@525 (Nat Nat) Bool)
(assert (forall ((x@32472 Nat)) (diseqNat@525 Z (S x@32472))))
(assert (forall ((x@32473 Nat)) (diseqNat@525 (S x@32473) Z)))
(assert (forall ((x@32474 Nat) (x@32475 Nat)) (=> (diseqNat@525 x@32474 x@32475) (diseqNat@525 (S x@32474) (S x@32475)))))
(declare-datatype list ((nil ) (cons (head Nat) (tail list))))
(declare-fun diseqlist@446 (list list) Bool)
(assert (forall ((x@32476 Nat) (x@32477 list)) (diseqlist@446 nil (cons x@32476 x@32477))))
(assert (forall ((x@32478 Nat) (x@32479 list)) (diseqlist@446 (cons x@32478 x@32479) nil)))
(assert (forall ((x@32480 Nat) (x@32481 list) (x@32482 Nat) (x@32483 list)) (=> (diseqNat@525 x@32480 x@32482) (diseqlist@446 (cons x@32480 x@32481) (cons x@32482 x@32483)))))
(assert (forall ((x@32480 Nat) (x@32481 list) (x@32482 Nat) (x@32483 list)) (=> (diseqlist@446 x@32481 x@32483) (diseqlist@446 (cons x@32480 x@32481) (cons x@32482 x@32483)))))
(declare-fun last (Nat list) Bool)
(assert (forall ((x@32464 Nat) (x list)) (=> (and (= x@32464 Z) (= x nil)) (last x@32464 x))))
(assert (forall ((x@32464 Nat) (x list) (y Nat) (z list)) (=> (and (= x@32464 y) (= x (cons y z)) (= z nil)) (last x@32464 x))))
(assert (forall ((x@32464 Nat) (x list) (y Nat) (z list) (x2 Nat) (x3 list) (x@32465 Nat)) (=> (and (= x@32464 x@32465) (= x (cons y z)) (= z (cons x2 x3)) (last x@32465 (cons x2 x3))) (last x@32464 x))))
(assert (forall ((xs list) (x Nat)) (forall ((x@32467 Nat) (x@32466 Nat)) (=> false false))))
(check-sat)
(get-info :reason-unknown)
