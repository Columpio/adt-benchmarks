(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@677 (Nat@0 Nat@0) Bool)
(assert (forall ((x@31410 Nat@0)) (diseqNat@0@677 Z@0 (S@0 x@31410))))
(assert (forall ((x@31411 Nat@0)) (diseqNat@0@677 (S@0 x@31411) Z@0)))
(assert (forall ((x@31412 Nat@0) (x@31413 Nat@0)) (=> (diseqNat@0@677 x@31412 x@31413) (diseqNat@0@677 (S@0 x@31412) (S@0 x@31413)))))
(declare-datatype list ((nil ) (cons (head Nat@0) (tail list))))
(declare-fun diseqlist@417 (list list) Bool)
(assert (forall ((x@31414 Nat@0) (x@31415 list)) (diseqlist@417 nil (cons x@31414 x@31415))))
(assert (forall ((x@31416 Nat@0) (x@31417 list)) (diseqlist@417 (cons x@31416 x@31417) nil)))
(assert (forall ((x@31418 Nat@0) (x@31419 list) (x@31420 Nat@0) (x@31421 list)) (=> (diseqNat@0@677 x@31418 x@31420) (diseqlist@417 (cons x@31418 x@31419) (cons x@31420 x@31421)))))
(assert (forall ((x@31418 Nat@0) (x@31419 list) (x@31420 Nat@0) (x@31421 list)) (=> (diseqlist@417 x@31419 x@31421) (diseqlist@417 (cons x@31418 x@31419) (cons x@31420 x@31421)))))
(declare-datatype Nat ((Z ) (S (proj1-S Nat))))
(declare-fun diseqNat@486 (Nat Nat) Bool)
(assert (forall ((x@31422 Nat)) (diseqNat@486 Z (S x@31422))))
(assert (forall ((x@31423 Nat)) (diseqNat@486 (S x@31423) Z)))
(assert (forall ((x@31424 Nat) (x@31425 Nat)) (=> (diseqNat@486 x@31424 x@31425) (diseqNat@486 (S x@31424) (S x@31425)))))
(declare-fun drop (list Nat list) Bool)
(assert (forall ((x@31402 list) (x Nat) (y list)) (=> (and (= x@31402 y) (= x Z)) (drop x@31402 x y))))
(assert (forall ((x@31402 list) (x Nat) (y list) (z Nat)) (=> (and (= x@31402 nil) (= x (S z)) (= y nil)) (drop x@31402 x y))))
(assert (forall ((x@31402 list) (x Nat) (y list) (z Nat) (x2 Nat@0) (x3 list) (x@31403 list)) (=> (and (= x@31402 x@31403) (= x (S z)) (= y (cons x2 x3)) (drop x@31403 z x3)) (drop x@31402 x y))))
(declare-fun +2 (Nat Nat Nat) Bool)
(assert (forall ((x@31404 Nat) (x Nat) (y Nat)) (=> (and (= x@31404 y) (= x Z)) (+2 x@31404 x y))))
(assert (forall ((x@31404 Nat) (x Nat) (y Nat) (z Nat) (x@31405 Nat)) (=> (and (= x@31404 (S x@31405)) (= x (S z)) (+2 x@31405 z y)) (+2 x@31404 x y))))
(assert (forall ((n Nat) (m Nat) (xs list)) (forall ((x@31409 list) (x@31408 list) (x@31407 list) (x@31406 Nat)) (=> (and (diseqlist@417 x@31409 x@31407) (drop x@31409 n x@31408) (drop x@31408 m xs) (drop x@31407 x@31406 xs) (+2 x@31406 n m)) false))))
(check-sat)
(get-info :reason-unknown)
