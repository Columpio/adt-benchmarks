(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@708 (Nat@0 Nat@0) Bool)
(assert (forall ((x@32192 Nat@0)) (diseqNat@0@708 Z@0 (S@0 x@32192))))
(assert (forall ((x@32193 Nat@0)) (diseqNat@0@708 (S@0 x@32193) Z@0)))
(assert (forall ((x@32194 Nat@0) (x@32195 Nat@0)) (=> (diseqNat@0@708 x@32194 x@32195) (diseqNat@0@708 (S@0 x@32194) (S@0 x@32195)))))
(declare-datatype pair ((pair2 (proj1-pair Nat@0) (proj2-pair Nat@0))))
(declare-fun diseqpair@63 (pair pair) Bool)
(assert (forall ((x@32196 Nat@0) (x@32197 Nat@0) (x@32198 Nat@0) (x@32199 Nat@0)) (=> (diseqNat@0@708 x@32196 x@32198) (diseqpair@63 (pair2 x@32196 x@32197) (pair2 x@32198 x@32199)))))
(assert (forall ((x@32196 Nat@0) (x@32197 Nat@0) (x@32198 Nat@0) (x@32199 Nat@0)) (=> (diseqNat@0@708 x@32197 x@32199) (diseqpair@63 (pair2 x@32196 x@32197) (pair2 x@32198 x@32199)))))
(declare-datatype list2 ((nil2 ) (cons2 (head2 Nat@0) (tail2 list2))))
(declare-fun diseqlist2@53 (list2 list2) Bool)
(assert (forall ((x@32200 Nat@0) (x@32201 list2)) (diseqlist2@53 nil2 (cons2 x@32200 x@32201))))
(assert (forall ((x@32202 Nat@0) (x@32203 list2)) (diseqlist2@53 (cons2 x@32202 x@32203) nil2)))
(assert (forall ((x@32204 Nat@0) (x@32205 list2) (x@32206 Nat@0) (x@32207 list2)) (=> (diseqNat@0@708 x@32204 x@32206) (diseqlist2@53 (cons2 x@32204 x@32205) (cons2 x@32206 x@32207)))))
(assert (forall ((x@32204 Nat@0) (x@32205 list2) (x@32206 Nat@0) (x@32207 list2)) (=> (diseqlist2@53 x@32205 x@32207) (diseqlist2@53 (cons2 x@32204 x@32205) (cons2 x@32206 x@32207)))))
(declare-datatype list ((nil ) (cons (head pair) (tail list))))
(declare-fun diseqlist@439 (list list) Bool)
(assert (forall ((x@32208 pair) (x@32209 list)) (diseqlist@439 nil (cons x@32208 x@32209))))
(assert (forall ((x@32210 pair) (x@32211 list)) (diseqlist@439 (cons x@32210 x@32211) nil)))
(assert (forall ((x@32212 pair) (x@32213 list) (x@32214 pair) (x@32215 list)) (=> (diseqpair@63 x@32212 x@32214) (diseqlist@439 (cons x@32212 x@32213) (cons x@32214 x@32215)))))
(assert (forall ((x@32212 pair) (x@32213 list) (x@32214 pair) (x@32215 list)) (=> (diseqlist@439 x@32213 x@32215) (diseqlist@439 (cons x@32212 x@32213) (cons x@32214 x@32215)))))
(declare-fun zip (list list2 list2) Bool)
(assert (forall ((x@32188 list) (x list2) (y list2)) (=> (and (= x@32188 nil) (= x nil2)) (zip x@32188 x y))))
(assert (forall ((x@32188 list) (x list2) (y list2) (z Nat@0) (x2 list2)) (=> (and (= x@32188 nil) (= x (cons2 z x2)) (= y nil2)) (zip x@32188 x y))))
(assert (forall ((x@32188 list) (x list2) (y list2) (z Nat@0) (x2 list2) (x3 Nat@0) (x4 list2) (x@32189 list)) (=> (and (= x@32188 (cons (pair2 z x3) x@32189)) (= x (cons2 z x2)) (= y (cons2 x3 x4)) (zip x@32189 x2 x4)) (zip x@32188 x y))))
(assert (forall ((x Nat@0) (y Nat@0) (xs list2) (ys list2)) (forall ((x@32191 list) (x@32190 list)) (=> (and (diseqlist@439 x@32191 (cons (pair2 x y) x@32190)) (zip x@32191 (cons2 x xs) (cons2 y ys)) (zip x@32190 xs ys)) false))))
(check-sat)
(get-info :reason-unknown)
