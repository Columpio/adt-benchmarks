(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@709 (Nat@0 Nat@0) Bool)
(assert (forall ((x@32227 Nat@0)) (diseqNat@0@709 Z@0 (S@0 x@32227))))
(assert (forall ((x@32228 Nat@0)) (diseqNat@0@709 (S@0 x@32228) Z@0)))
(assert (forall ((x@32229 Nat@0) (x@32230 Nat@0)) (=> (diseqNat@0@709 x@32229 x@32230) (diseqNat@0@709 (S@0 x@32229) (S@0 x@32230)))))
(declare-datatype pair ((pair2 (proj1-pair Nat@0) (proj2-pair Nat@0))))
(declare-fun diseqpair@64 (pair pair) Bool)
(assert (forall ((x@32231 Nat@0) (x@32232 Nat@0) (x@32233 Nat@0) (x@32234 Nat@0)) (=> (diseqNat@0@709 x@32231 x@32233) (diseqpair@64 (pair2 x@32231 x@32232) (pair2 x@32233 x@32234)))))
(assert (forall ((x@32231 Nat@0) (x@32232 Nat@0) (x@32233 Nat@0) (x@32234 Nat@0)) (=> (diseqNat@0@709 x@32232 x@32234) (diseqpair@64 (pair2 x@32231 x@32232) (pair2 x@32233 x@32234)))))
(declare-datatype list2 ((nil2 ) (cons2 (head2 Nat@0) (tail2 list2))))
(declare-fun diseqlist2@54 (list2 list2) Bool)
(assert (forall ((x@32235 Nat@0) (x@32236 list2)) (diseqlist2@54 nil2 (cons2 x@32235 x@32236))))
(assert (forall ((x@32237 Nat@0) (x@32238 list2)) (diseqlist2@54 (cons2 x@32237 x@32238) nil2)))
(assert (forall ((x@32239 Nat@0) (x@32240 list2) (x@32241 Nat@0) (x@32242 list2)) (=> (diseqNat@0@709 x@32239 x@32241) (diseqlist2@54 (cons2 x@32239 x@32240) (cons2 x@32241 x@32242)))))
(assert (forall ((x@32239 Nat@0) (x@32240 list2) (x@32241 Nat@0) (x@32242 list2)) (=> (diseqlist2@54 x@32240 x@32242) (diseqlist2@54 (cons2 x@32239 x@32240) (cons2 x@32241 x@32242)))))
(declare-datatype list ((nil ) (cons (head pair) (tail list))))
(declare-fun diseqlist@440 (list list) Bool)
(assert (forall ((x@32243 pair) (x@32244 list)) (diseqlist@440 nil (cons x@32243 x@32244))))
(assert (forall ((x@32245 pair) (x@32246 list)) (diseqlist@440 (cons x@32245 x@32246) nil)))
(assert (forall ((x@32247 pair) (x@32248 list) (x@32249 pair) (x@32250 list)) (=> (diseqpair@64 x@32247 x@32249) (diseqlist@440 (cons x@32247 x@32248) (cons x@32249 x@32250)))))
(assert (forall ((x@32247 pair) (x@32248 list) (x@32249 pair) (x@32250 list)) (=> (diseqlist@440 x@32248 x@32250) (diseqlist@440 (cons x@32247 x@32248) (cons x@32249 x@32250)))))
(declare-datatype Nat ((Z ) (S (proj1-S Nat))))
(declare-fun diseqNat@515 (Nat Nat) Bool)
(assert (forall ((x@32251 Nat)) (diseqNat@515 Z (S x@32251))))
(assert (forall ((x@32252 Nat)) (diseqNat@515 (S x@32252) Z)))
(assert (forall ((x@32253 Nat) (x@32254 Nat)) (=> (diseqNat@515 x@32253 x@32254) (diseqNat@515 (S x@32253) (S x@32254)))))
(declare-fun zip (list list2 list2) Bool)
(assert (forall ((x@32216 list) (x list2) (y list2)) (=> (and (= x@32216 nil) (= x nil2)) (zip x@32216 x y))))
(assert (forall ((x@32216 list) (x list2) (y list2) (z Nat@0) (x2 list2)) (=> (and (= x@32216 nil) (= x (cons2 z x2)) (= y nil2)) (zip x@32216 x y))))
(assert (forall ((x@32216 list) (x list2) (y list2) (z Nat@0) (x2 list2) (x3 Nat@0) (x4 list2) (x@32217 list)) (=> (and (= x@32216 (cons (pair2 z x3) x@32217)) (= x (cons2 z x2)) (= y (cons2 x3 x4)) (zip x@32217 x2 x4)) (zip x@32216 x y))))
(declare-fun drop2 (list2 Nat list2) Bool)
(assert (forall ((x@32218 list2) (x Nat) (y list2)) (=> (and (= x@32218 y) (= x Z)) (drop2 x@32218 x y))))
(assert (forall ((x@32218 list2) (x Nat) (y list2) (z Nat)) (=> (and (= x@32218 nil2) (= x (S z)) (= y nil2)) (drop2 x@32218 x y))))
(assert (forall ((x@32218 list2) (x Nat) (y list2) (z Nat) (x2 Nat@0) (x3 list2) (x@32219 list2)) (=> (and (= x@32218 x@32219) (= x (S z)) (= y (cons2 x2 x3)) (drop2 x@32219 z x3)) (drop2 x@32218 x y))))
(declare-fun drop (list Nat list) Bool)
(assert (forall ((x@32220 list) (x Nat) (y list)) (=> (and (= x@32220 y) (= x Z)) (drop x@32220 x y))))
(assert (forall ((x@32220 list) (x Nat) (y list) (z Nat)) (=> (and (= x@32220 nil) (= x (S z)) (= y nil)) (drop x@32220 x y))))
(assert (forall ((x@32220 list) (x Nat) (y list) (z Nat) (x2 pair) (x3 list) (x@32221 list)) (=> (and (= x@32220 x@32221) (= x (S z)) (= y (cons x2 x3)) (drop x@32221 z x3)) (drop x@32220 x y))))
(assert (forall ((n Nat) (xs list2) (ys list2)) (forall ((x@32226 list) (x@32225 list) (x@32224 list) (x@32223 list2) (x@32222 list2)) (=> (and (diseqlist@440 x@32226 x@32224) (drop x@32226 n x@32225) (zip x@32225 xs ys) (zip x@32224 x@32223 x@32222) (drop2 x@32223 n xs) (drop2 x@32222 n ys)) false))))
(check-sat)
(get-info :reason-unknown)
