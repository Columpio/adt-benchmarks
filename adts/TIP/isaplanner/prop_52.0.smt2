(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@723 (Nat@0 Nat@0) Bool)
(assert (forall ((x@32568 Nat@0)) (diseqNat@0@723 Z@0 (S@0 x@32568))))
(assert (forall ((x@32569 Nat@0)) (diseqNat@0@723 (S@0 x@32569) Z@0)))
(assert (forall ((x@32570 Nat@0) (x@32571 Nat@0)) (=> (diseqNat@0@723 x@32570 x@32571) (diseqNat@0@723 (S@0 x@32570) (S@0 x@32571)))))
(declare-datatype Nat ((Z ) (S (proj1-S Nat))))
(declare-fun diseqNat@529 (Nat Nat) Bool)
(assert (forall ((x@32572 Nat)) (diseqNat@529 Z (S x@32572))))
(assert (forall ((x@32573 Nat)) (diseqNat@529 (S x@32573) Z)))
(assert (forall ((x@32574 Nat) (x@32575 Nat)) (=> (diseqNat@529 x@32574 x@32575) (diseqNat@529 (S x@32574) (S x@32575)))))
(declare-datatype list ((nil ) (cons (head Nat) (tail list))))
(declare-fun diseqlist@450 (list list) Bool)
(assert (forall ((x@32576 Nat) (x@32577 list)) (diseqlist@450 nil (cons x@32576 x@32577))))
(assert (forall ((x@32578 Nat) (x@32579 list)) (diseqlist@450 (cons x@32578 x@32579) nil)))
(assert (forall ((x@32580 Nat) (x@32581 list) (x@32582 Nat) (x@32583 list)) (=> (diseqNat@529 x@32580 x@32582) (diseqlist@450 (cons x@32580 x@32581) (cons x@32582 x@32583)))))
(assert (forall ((x@32580 Nat) (x@32581 list) (x@32582 Nat) (x@32583 list)) (=> (diseqlist@450 x@32581 x@32583) (diseqlist@450 (cons x@32580 x@32581) (cons x@32582 x@32583)))))
(declare-fun == (Bool Nat Nat) Bool)
(assert (forall ((x@32554 Bool) (x Nat) (y Nat)) (=> (and (= x@32554 true) (= x Z) (= y Z)) (== x@32554 x y))))
(assert (forall ((x@32554 Bool) (x Nat) (y Nat) (z Nat)) (=> (and (= x@32554 false) (= x Z) (= y (S z))) (== x@32554 x y))))
(assert (forall ((x@32554 Bool) (x Nat) (y Nat) (x2 Nat)) (=> (and (= x@32554 false) (= x (S x2)) (= y Z)) (== x@32554 x y))))
(assert (forall ((x@32554 Bool) (x Nat) (y Nat) (x2 Nat) (y2 Nat) (x@32555 Bool)) (=> (and (= x@32554 x@32555) (= x (S x2)) (= y (S y2)) (== x@32555 x2 y2)) (== x@32554 x y))))
(declare-fun count (Nat Nat list) Bool)
(assert (forall ((x@32556 Nat) (x Nat) (y list)) (=> (and (= x@32556 Z) (= y nil)) (count x@32556 x y))))
(assert (forall ((x@32556 Nat) (x Nat) (y list) (z Nat) (ys list) (x@32557 Bool) (x@32558 Nat) (x@32559 Nat)) (=> (and (= x@32556 (ite x@32557 (S x@32558) x@32559)) (= y (cons z ys)) (== x@32557 x z) (count x@32558 x ys) (count x@32559 x ys)) (count x@32556 x y))))
(declare-fun ++ (list list list) Bool)
(assert (forall ((x@32560 list) (x list) (y list)) (=> (and (= x@32560 y) (= x nil)) (++ x@32560 x y))))
(assert (forall ((x@32560 list) (x list) (y list) (z Nat) (xs list) (x@32561 list)) (=> (and (= x@32560 (cons z x@32561)) (= x (cons z xs)) (++ x@32561 xs y)) (++ x@32560 x y))))
(declare-fun rev (list list) Bool)
(assert (forall ((x@32562 list) (x list)) (=> (and (= x@32562 nil) (= x nil)) (rev x@32562 x))))
(assert (forall ((x@32562 list) (x list) (y Nat) (xs list) (x@32564 list) (x@32563 list)) (=> (and (= x@32562 x@32564) (= x (cons y xs)) (++ x@32564 x@32563 (cons y nil)) (rev x@32563 xs)) (rev x@32562 x))))
(assert (forall ((n Nat) (xs list)) (forall ((x@32567 Nat) (x@32566 Nat) (x@32565 list)) (=> (and (diseqNat@529 x@32567 x@32566) (count x@32567 n xs) (count x@32566 n x@32565) (rev x@32565 xs)) false))))
(check-sat)
(get-info :reason-unknown)
