(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@713 (Nat@0 Nat@0) Bool)
(assert (forall ((x@32331 Nat@0)) (diseqNat@0@713 Z@0 (S@0 x@32331))))
(assert (forall ((x@32332 Nat@0)) (diseqNat@0@713 (S@0 x@32332) Z@0)))
(assert (forall ((x@32333 Nat@0) (x@32334 Nat@0)) (=> (diseqNat@0@713 x@32333 x@32334) (diseqNat@0@713 (S@0 x@32333) (S@0 x@32334)))))
(declare-datatype Tree ((Leaf ) (Node (proj1-Node Tree) (proj2-Node Nat@0) (proj3-Node Tree))))
(declare-fun diseqTree@13 (Tree Tree) Bool)
(assert (forall ((x@32335 Tree) (x@32336 Nat@0) (x@32337 Tree)) (diseqTree@13 Leaf (Node x@32335 x@32336 x@32337))))
(assert (forall ((x@32338 Tree) (x@32339 Nat@0) (x@32340 Tree)) (diseqTree@13 (Node x@32338 x@32339 x@32340) Leaf)))
(assert (forall ((x@32341 Tree) (x@32342 Nat@0) (x@32343 Tree) (x@32344 Tree) (x@32345 Nat@0) (x@32346 Tree)) (=> (diseqTree@13 x@32341 x@32344) (diseqTree@13 (Node x@32341 x@32342 x@32343) (Node x@32344 x@32345 x@32346)))))
(assert (forall ((x@32341 Tree) (x@32342 Nat@0) (x@32343 Tree) (x@32344 Tree) (x@32345 Nat@0) (x@32346 Tree)) (=> (diseqNat@0@713 x@32342 x@32345) (diseqTree@13 (Node x@32341 x@32342 x@32343) (Node x@32344 x@32345 x@32346)))))
(assert (forall ((x@32341 Tree) (x@32342 Nat@0) (x@32343 Tree) (x@32344 Tree) (x@32345 Nat@0) (x@32346 Tree)) (=> (diseqTree@13 x@32343 x@32346) (diseqTree@13 (Node x@32341 x@32342 x@32343) (Node x@32344 x@32345 x@32346)))))
(declare-datatype Nat ((Z ) (S (proj1-S Nat))))
(declare-fun diseqNat@519 (Nat Nat) Bool)
(assert (forall ((x@32347 Nat)) (diseqNat@519 Z (S x@32347))))
(assert (forall ((x@32348 Nat)) (diseqNat@519 (S x@32348) Z)))
(assert (forall ((x@32349 Nat) (x@32350 Nat)) (=> (diseqNat@519 x@32349 x@32350) (diseqNat@519 (S x@32349) (S x@32350)))))
(declare-fun mirror (Tree Tree) Bool)
(assert (forall ((x@32319 Tree) (x Tree)) (=> (and (= x@32319 Leaf) (= x Leaf)) (mirror x@32319 x))))
(assert (forall ((x@32319 Tree) (x Tree) (l Tree) (y Nat@0) (r Tree) (x@32321 Tree) (x@32320 Tree)) (=> (and (= x@32319 (Node x@32321 y x@32320)) (= x (Node l y r)) (mirror x@32321 r) (mirror x@32320 l)) (mirror x@32319 x))))
(declare-fun max (Nat Nat Nat) Bool)
(assert (forall ((x@32322 Nat) (x Nat) (y Nat)) (=> (and (= x@32322 y) (= x Z)) (max x@32322 x y))))
(assert (forall ((x@32322 Nat) (x Nat) (y Nat) (z Nat)) (=> (and (= x@32322 (S z)) (= x (S z)) (= y Z)) (max x@32322 x y))))
(assert (forall ((x@32322 Nat) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@32323 Nat)) (=> (and (= x@32322 (S x@32323)) (= x (S z)) (= y (S x2)) (max x@32323 z x2)) (max x@32322 x y))))
(declare-fun height (Nat Tree) Bool)
(assert (forall ((x@32324 Nat) (x Tree)) (=> (and (= x@32324 Z) (= x Leaf)) (height x@32324 x))))
(assert (forall ((x@32324 Nat) (x Tree) (l Tree) (y Nat@0) (r Tree) (x@32327 Nat) (x@32326 Nat) (x@32325 Nat)) (=> (and (= x@32324 (S x@32327)) (= x (Node l y r)) (max x@32327 x@32326 x@32325) (height x@32326 l) (height x@32325 r)) (height x@32324 x))))
(assert (forall ((a1 Tree)) (forall ((x@32330 Nat) (x@32329 Tree) (x@32328 Nat)) (=> (and (diseqNat@519 x@32330 x@32328) (height x@32330 x@32329) (mirror x@32329 a1) (height x@32328 a1)) false))))
(check-sat)
(get-info :reason-unknown)
