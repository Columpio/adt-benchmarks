(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@670 (Nat@0 Nat@0) Bool)
(assert (forall ((x@31233 Nat@0)) (diseqNat@0@670 Z@0 (S@0 x@31233))))
(assert (forall ((x@31234 Nat@0)) (diseqNat@0@670 (S@0 x@31234) Z@0)))
(assert (forall ((x@31235 Nat@0) (x@31236 Nat@0)) (=> (diseqNat@0@670 x@31235 x@31236) (diseqNat@0@670 (S@0 x@31235) (S@0 x@31236)))))
(declare-datatype Nat ((Z ) (S (proj1-S Nat))))
(declare-fun diseqNat@481 (Nat Nat) Bool)
(assert (forall ((x@31237 Nat)) (diseqNat@481 Z (S x@31237))))
(assert (forall ((x@31238 Nat)) (diseqNat@481 (S x@31238) Z)))
(assert (forall ((x@31239 Nat) (x@31240 Nat)) (=> (diseqNat@481 x@31239 x@31240) (diseqNat@481 (S x@31239) (S x@31240)))))
(declare-fun <2 (Bool Nat Nat) Bool)
(assert (forall ((x@31227 Bool) (x Nat) (y Nat)) (=> (and (= x@31227 false) (= y Z)) (<2 x@31227 x y))))
(assert (forall ((x@31227 Bool) (x Nat) (y Nat) (z Nat)) (=> (and (= x@31227 true) (= y (S z)) (= x Z)) (<2 x@31227 x y))))
(assert (forall ((x@31227 Bool) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@31228 Bool)) (=> (and (= x@31227 x@31228) (= y (S z)) (= x (S x2)) (<2 x@31228 x2 z)) (<2 x@31227 x y))))
(declare-fun +2 (Nat Nat Nat) Bool)
(assert (forall ((x@31229 Nat) (x Nat) (y Nat)) (=> (and (= x@31229 y) (= x Z)) (+2 x@31229 x y))))
(assert (forall ((x@31229 Nat) (x Nat) (y Nat) (z Nat) (x@31230 Nat)) (=> (and (= x@31229 (S x@31230)) (= x (S z)) (+2 x@31230 z y)) (+2 x@31229 x y))))
(assert (forall ((i Nat) (m Nat)) (forall ((x@31232 Bool) (x@31231 Nat)) (=> (and (not x@31232) (<2 x@31232 i (S x@31231)) (+2 x@31231 m i)) false))))
(check-sat)
(get-info :reason-unknown)
