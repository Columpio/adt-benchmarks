(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@741 (Nat@0 Nat@0) Bool)
(assert (forall ((x@32969 Nat@0)) (diseqNat@0@741 Z@0 (S@0 x@32969))))
(assert (forall ((x@32970 Nat@0)) (diseqNat@0@741 (S@0 x@32970) Z@0)))
(assert (forall ((x@32971 Nat@0) (x@32972 Nat@0)) (=> (diseqNat@0@741 x@32971 x@32972) (diseqNat@0@741 (S@0 x@32971) (S@0 x@32972)))))
(declare-datatype Nat ((Z ) (S (proj1-S Nat))))
(declare-fun diseqNat@547 (Nat Nat) Bool)
(assert (forall ((x@32973 Nat)) (diseqNat@547 Z (S x@32973))))
(assert (forall ((x@32974 Nat)) (diseqNat@547 (S x@32974) Z)))
(assert (forall ((x@32975 Nat) (x@32976 Nat)) (=> (diseqNat@547 x@32975 x@32976) (diseqNat@547 (S x@32975) (S x@32976)))))
(declare-fun max (Nat Nat Nat) Bool)
(assert (forall ((x@32963 Nat) (x Nat) (y Nat)) (=> (and (= x@32963 y) (= x Z)) (max x@32963 x y))))
(assert (forall ((x@32963 Nat) (x Nat) (y Nat) (z Nat)) (=> (and (= x@32963 (S z)) (= x (S z)) (= y Z)) (max x@32963 x y))))
(assert (forall ((x@32963 Nat) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@32964 Nat)) (=> (and (= x@32963 (S x@32964)) (= x (S z)) (= y (S x2)) (max x@32964 z x2)) (max x@32963 x y))))
(assert (forall ((a Nat) (b Nat) (c Nat)) (forall ((x@32968 Nat) (x@32967 Nat) (x@32966 Nat) (x@32965 Nat)) (=> (and (diseqNat@547 x@32968 x@32966) (max x@32968 x@32967 c) (max x@32967 a b) (max x@32966 a x@32965) (max x@32965 b c)) false))))
(check-sat)
(get-info :reason-unknown)
