(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@693 (Nat@0 Nat@0) Bool)
(assert (forall ((x@31892 Nat@0)) (diseqNat@0@693 Z@0 (S@0 x@31892))))
(assert (forall ((x@31893 Nat@0)) (diseqNat@0@693 (S@0 x@31893) Z@0)))
(assert (forall ((x@31894 Nat@0) (x@31895 Nat@0)) (=> (diseqNat@0@693 x@31894 x@31895) (diseqNat@0@693 (S@0 x@31894) (S@0 x@31895)))))
(declare-datatype Nat ((Z ) (S (proj1-S Nat))))
(declare-fun diseqNat@500 (Nat Nat) Bool)
(assert (forall ((x@31896 Nat)) (diseqNat@500 Z (S x@31896))))
(assert (forall ((x@31897 Nat)) (diseqNat@500 (S x@31897) Z)))
(assert (forall ((x@31898 Nat) (x@31899 Nat)) (=> (diseqNat@500 x@31898 x@31899) (diseqNat@500 (S x@31898) (S x@31899)))))
(declare-fun <=2 (Bool Nat Nat) Bool)
(assert (forall ((x@31886 Bool) (x Nat) (y Nat)) (=> (and (= x@31886 true) (= x Z)) (<=2 x@31886 x y))))
(assert (forall ((x@31886 Bool) (x Nat) (y Nat) (z Nat)) (=> (and (= x@31886 false) (= x (S z)) (= y Z)) (<=2 x@31886 x y))))
(assert (forall ((x@31886 Bool) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@31887 Bool)) (=> (and (= x@31886 x@31887) (= x (S z)) (= y (S x2)) (<=2 x@31887 z x2)) (<=2 x@31886 x y))))
(declare-fun +2 (Nat Nat Nat) Bool)
(assert (forall ((x@31888 Nat) (x Nat) (y Nat)) (=> (and (= x@31888 y) (= x Z)) (+2 x@31888 x y))))
(assert (forall ((x@31888 Nat) (x Nat) (y Nat) (z Nat) (x@31889 Nat)) (=> (and (= x@31888 (S x@31889)) (= x (S z)) (+2 x@31889 z y)) (+2 x@31888 x y))))
(assert (forall ((n Nat) (m Nat)) (forall ((x@31891 Bool) (x@31890 Nat)) (=> (and (not x@31891) (<=2 x@31891 n x@31890) (+2 x@31890 n m)) false))))
(check-sat)
(get-info :reason-unknown)
