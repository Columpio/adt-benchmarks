(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@679 (Nat@0 Nat@0) Bool)
(assert (forall ((x@31470 Nat@0)) (diseqNat@0@679 Z@0 (S@0 x@31470))))
(assert (forall ((x@31471 Nat@0)) (diseqNat@0@679 (S@0 x@31471) Z@0)))
(assert (forall ((x@31472 Nat@0) (x@31473 Nat@0)) (=> (diseqNat@0@679 x@31472 x@31473) (diseqNat@0@679 (S@0 x@31472) (S@0 x@31473)))))
(declare-datatype pair ((pair2 (proj1-pair Nat@0) (proj2-pair Nat@0))))
(declare-fun diseqpair@59 (pair pair) Bool)
(assert (forall ((x@31474 Nat@0) (x@31475 Nat@0) (x@31476 Nat@0) (x@31477 Nat@0)) (=> (diseqNat@0@679 x@31474 x@31476) (diseqpair@59 (pair2 x@31474 x@31475) (pair2 x@31476 x@31477)))))
(assert (forall ((x@31474 Nat@0) (x@31475 Nat@0) (x@31476 Nat@0) (x@31477 Nat@0)) (=> (diseqNat@0@679 x@31475 x@31477) (diseqpair@59 (pair2 x@31474 x@31475) (pair2 x@31476 x@31477)))))
(declare-datatype list2 ((nil2 ) (cons2 (head2 Nat@0) (tail2 list2))))
(declare-fun diseqlist2@49 (list2 list2) Bool)
(assert (forall ((x@31478 Nat@0) (x@31479 list2)) (diseqlist2@49 nil2 (cons2 x@31478 x@31479))))
(assert (forall ((x@31480 Nat@0) (x@31481 list2)) (diseqlist2@49 (cons2 x@31480 x@31481) nil2)))
(assert (forall ((x@31482 Nat@0) (x@31483 list2) (x@31484 Nat@0) (x@31485 list2)) (=> (diseqNat@0@679 x@31482 x@31484) (diseqlist2@49 (cons2 x@31482 x@31483) (cons2 x@31484 x@31485)))))
(assert (forall ((x@31482 Nat@0) (x@31483 list2) (x@31484 Nat@0) (x@31485 list2)) (=> (diseqlist2@49 x@31483 x@31485) (diseqlist2@49 (cons2 x@31482 x@31483) (cons2 x@31484 x@31485)))))
(declare-datatype list ((nil ) (cons (head pair) (tail list))))
(declare-fun diseqlist@419 (list list) Bool)
(assert (forall ((x@31486 pair) (x@31487 list)) (diseqlist@419 nil (cons x@31486 x@31487))))
(assert (forall ((x@31488 pair) (x@31489 list)) (diseqlist@419 (cons x@31488 x@31489) nil)))
(assert (forall ((x@31490 pair) (x@31491 list) (x@31492 pair) (x@31493 list)) (=> (diseqpair@59 x@31490 x@31492) (diseqlist@419 (cons x@31490 x@31491) (cons x@31492 x@31493)))))
(assert (forall ((x@31490 pair) (x@31491 list) (x@31492 pair) (x@31493 list)) (=> (diseqlist@419 x@31491 x@31493) (diseqlist@419 (cons x@31490 x@31491) (cons x@31492 x@31493)))))
(declare-datatype Nat ((Z ) (S (proj1-S Nat))))
(declare-fun diseqNat@487 (Nat Nat) Bool)
(assert (forall ((x@31494 Nat)) (diseqNat@487 Z (S x@31494))))
(assert (forall ((x@31495 Nat)) (diseqNat@487 (S x@31495) Z)))
(assert (forall ((x@31496 Nat) (x@31497 Nat)) (=> (diseqNat@487 x@31496 x@31497) (diseqNat@487 (S x@31496) (S x@31497)))))
(declare-fun zip (list list2 list2) Bool)
(assert (forall ((x@31449 list) (x list2) (y list2)) (=> (and (= x@31449 nil) (= x nil2)) (zip x@31449 x y))))
(assert (forall ((x@31449 list) (x list2) (y list2) (z Nat@0) (x2 list2)) (=> (and (= x@31449 nil) (= x (cons2 z x2)) (= y nil2)) (zip x@31449 x y))))
(assert (forall ((x@31449 list) (x list2) (y list2) (z Nat@0) (x2 list2) (x3 Nat@0) (x4 list2) (x@31450 list)) (=> (and (= x@31449 (cons (pair2 z x3) x@31450)) (= x (cons2 z x2)) (= y (cons2 x3 x4)) (zip x@31450 x2 x4)) (zip x@31449 x y))))
(declare-fun len (Nat list2) Bool)
(assert (forall ((x@31451 Nat) (x list2)) (=> (and (= x@31451 Z) (= x nil2)) (len x@31451 x))))
(assert (forall ((x@31451 Nat) (x list2) (y Nat@0) (xs list2) (x@31452 Nat)) (=> (and (= x@31451 (S x@31452)) (= x (cons2 y xs)) (len x@31452 xs)) (len x@31451 x))))
(declare-fun ++2 (list2 list2 list2) Bool)
(assert (forall ((x@31453 list2) (x list2) (y list2)) (=> (and (= x@31453 y) (= x nil2)) (++2 x@31453 x y))))
(assert (forall ((x@31453 list2) (x list2) (y list2) (z Nat@0) (xs list2) (x@31454 list2)) (=> (and (= x@31453 (cons2 z x@31454)) (= x (cons2 z xs)) (++2 x@31454 xs y)) (++2 x@31453 x y))))
(declare-fun rev2 (list2 list2) Bool)
(assert (forall ((x@31455 list2) (x list2)) (=> (and (= x@31455 nil2) (= x nil2)) (rev2 x@31455 x))))
(assert (forall ((x@31455 list2) (x list2) (y Nat@0) (xs list2) (x@31457 list2) (x@31456 list2)) (=> (and (= x@31455 x@31457) (= x (cons2 y xs)) (++2 x@31457 x@31456 (cons2 y nil2)) (rev2 x@31456 xs)) (rev2 x@31455 x))))
(declare-fun ++ (list list list) Bool)
(assert (forall ((x@31458 list) (x list) (y list)) (=> (and (= x@31458 y) (= x nil)) (++ x@31458 x y))))
(assert (forall ((x@31458 list) (x list) (y list) (z pair) (xs list) (x@31459 list)) (=> (and (= x@31458 (cons z x@31459)) (= x (cons z xs)) (++ x@31459 xs y)) (++ x@31458 x y))))
(declare-fun rev (list list) Bool)
(assert (forall ((x@31460 list) (x list)) (=> (and (= x@31460 nil) (= x nil)) (rev x@31460 x))))
(assert (forall ((x@31460 list) (x list) (y pair) (xs list) (x@31462 list) (x@31461 list)) (=> (and (= x@31460 x@31462) (= x (cons y xs)) (++ x@31462 x@31461 (cons y nil)) (rev x@31461 xs)) (rev x@31460 x))))
(assert (forall ((xs list2) (ys list2)) (forall ((x@31464 Nat) (x@31463 Nat) (x@31469 list) (x@31468 list2) (x@31467 list2) (x@31466 list) (x@31465 list)) (=> (and (diseqlist@419 x@31469 x@31466) (= x@31464 x@31463) (len x@31464 xs) (len x@31463 ys) (zip x@31469 x@31468 x@31467) (rev2 x@31468 xs) (rev2 x@31467 ys) (rev x@31466 x@31465) (zip x@31465 xs ys)) false))))
(check-sat)
(get-info :reason-unknown)
