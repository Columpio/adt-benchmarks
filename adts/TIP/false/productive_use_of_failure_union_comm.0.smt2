(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@445 (Nat@0 Nat@0) Bool)
(assert (forall ((x@19655 Nat@0)) (diseqNat@0@445 Z@0 (S@0 x@19655))))
(assert (forall ((x@19656 Nat@0)) (diseqNat@0@445 (S@0 x@19656) Z@0)))
(assert (forall ((x@19657 Nat@0) (x@19658 Nat@0)) (=> (diseqNat@0@445 x@19657 x@19658) (diseqNat@0@445 (S@0 x@19657) (S@0 x@19658)))))
(declare-datatype Nat ((S (proj1-S Nat)) (Z )))
(declare-fun diseqNat@321 (Nat Nat) Bool)
(assert (forall ((x@19659 Nat)) (diseqNat@321 (S x@19659) Z)))
(assert (forall ((x@19660 Nat)) (diseqNat@321 Z (S x@19660))))
(assert (forall ((x@19661 Nat) (x@19662 Nat)) (=> (diseqNat@321 x@19661 x@19662) (diseqNat@321 (S x@19661) (S x@19662)))))
(declare-datatype list ((nil ) (cons (head Nat) (tail list))))
(declare-fun diseqlist@296 (list list) Bool)
(assert (forall ((x@19663 Nat) (x@19664 list)) (diseqlist@296 nil (cons x@19663 x@19664))))
(assert (forall ((x@19665 Nat) (x@19666 list)) (diseqlist@296 (cons x@19665 x@19666) nil)))
(assert (forall ((x@19667 Nat) (x@19668 list) (x@19669 Nat) (x@19670 list)) (=> (diseqNat@321 x@19667 x@19669) (diseqlist@296 (cons x@19667 x@19668) (cons x@19669 x@19670)))))
(assert (forall ((x@19667 Nat) (x@19668 list) (x@19669 Nat) (x@19670 list)) (=> (diseqlist@296 x@19668 x@19670) (diseqlist@296 (cons x@19667 x@19668) (cons x@19669 x@19670)))))
(declare-fun eqNat (Bool Nat Nat) Bool)
(assert (forall ((x@19642 Bool) (x Nat) (y Nat) (z Nat) (y2 Nat) (x@19643 Bool)) (=> (and (= x@19642 x@19643) (= x (S z)) (= y (S y2)) (eqNat x@19643 z y2)) (eqNat x@19642 x y))))
(assert (forall ((x@19642 Bool) (x Nat) (y Nat) (z Nat)) (=> (and (= x@19642 false) (= x (S z)) (= y Z)) (eqNat x@19642 x y))))
(assert (forall ((x@19642 Bool) (x Nat) (y Nat) (x2 Nat)) (=> (and (= x@19642 false) (= x Z) (= y (S x2))) (eqNat x@19642 x y))))
(assert (forall ((x@19642 Bool) (x Nat) (y Nat)) (=> (and (= x@19642 true) (= x Z) (= y Z)) (eqNat x@19642 x y))))
(declare-fun barbar (Bool Bool Bool) Bool)
(assert (forall ((x@19644 Bool) (x Bool) (y Bool)) (=> (= x@19644 (ite x true y)) (barbar x@19644 x y))))
(declare-fun elem (Bool Nat list) Bool)
(assert (forall ((x@19645 Bool) (x Nat) (y list)) (=> (and (= x@19645 false) (= y nil)) (elem x@19645 x y))))
(assert (forall ((x@19645 Bool) (x Nat) (y list) (z Nat) (xs list) (x@19648 Bool) (x@19647 Bool) (x@19646 Bool)) (=> (and (= x@19645 x@19648) (= y (cons z xs)) (barbar x@19648 x@19647 x@19646) (eqNat x@19647 x z) (elem x@19646 x xs)) (elem x@19645 x y))))
(declare-fun union2 (list list list) Bool)
(assert (forall ((x@19649 list) (x list) (y list)) (=> (and (= x@19649 y) (= x nil)) (union2 x@19649 x y))))
(assert (forall ((x@19649 list) (x list) (y list) (z Nat) (xs list) (x@19650 Bool) (x@19651 list) (x@19652 list)) (=> (and (= x@19649 (ite x@19650 x@19651 (cons z x@19652))) (= x (cons z xs)) (elem x@19650 z y) (union2 x@19651 xs y) (union2 x@19652 xs y)) (union2 x@19649 x y))))
(assert (forall ((xs list) (ys list)) (forall ((x@19654 list) (x@19653 list)) (=> (and (diseqlist@296 x@19654 x@19653) (union2 x@19654 xs ys) (union2 x@19653 ys xs)) false))))
(check-sat)
(get-info :reason-unknown)
