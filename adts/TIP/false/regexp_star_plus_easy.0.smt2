(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@436 (Nat@0 Nat@0) Bool)
(assert (forall ((x@18679 Nat@0)) (diseqNat@0@436 Z@0 (S@0 x@18679))))
(assert (forall ((x@18680 Nat@0)) (diseqNat@0@436 (S@0 x@18680) Z@0)))
(assert (forall ((x@18681 Nat@0) (x@18682 Nat@0)) (=> (diseqNat@0@436 x@18681 x@18682) (diseqNat@0@436 (S@0 x@18681) (S@0 x@18682)))))
(declare-datatype T ((A ) (B ) (C )))
(declare-fun diseqT@40 (T T) Bool)
(assert (diseqT@40 A B))
(assert (diseqT@40 A C))
(assert (diseqT@40 B A))
(assert (diseqT@40 C A))
(assert (diseqT@40 B C))
(assert (diseqT@40 C B))
(declare-datatype list ((nil ) (cons (head T) (tail list))))
(declare-fun diseqlist@287 (list list) Bool)
(assert (forall ((x@18683 T) (x@18684 list)) (diseqlist@287 nil (cons x@18683 x@18684))))
(assert (forall ((x@18685 T) (x@18686 list)) (diseqlist@287 (cons x@18685 x@18686) nil)))
(assert (forall ((x@18687 T) (x@18688 list) (x@18689 T) (x@18690 list)) (=> (diseqT@40 x@18687 x@18689) (diseqlist@287 (cons x@18687 x@18688) (cons x@18689 x@18690)))))
(assert (forall ((x@18687 T) (x@18688 list) (x@18689 T) (x@18690 list)) (=> (diseqlist@287 x@18688 x@18690) (diseqlist@287 (cons x@18687 x@18688) (cons x@18689 x@18690)))))
(declare-datatype R ((Nil ) (Eps ) (Atom (proj1-Atom T)) (|:+:| (|proj1-:+:| R) (|proj2-:+:| R)) (|:>:| (|proj1-:>:| R) (|proj2-:>:| R)) (Star (proj1-Star R))))
(declare-fun diseqR@55 (R R) Bool)
(assert (diseqR@55 Nil Eps))
(assert (forall ((x@18691 T)) (diseqR@55 Nil (Atom x@18691))))
(assert (forall ((x@18692 R) (x@18693 R)) (diseqR@55 Nil (|:+:| x@18692 x@18693))))
(assert (forall ((x@18694 R) (x@18695 R)) (diseqR@55 Nil (|:>:| x@18694 x@18695))))
(assert (forall ((x@18696 R)) (diseqR@55 Nil (Star x@18696))))
(assert (diseqR@55 Eps Nil))
(assert (forall ((x@18697 T)) (diseqR@55 (Atom x@18697) Nil)))
(assert (forall ((x@18698 R) (x@18699 R)) (diseqR@55 (|:+:| x@18698 x@18699) Nil)))
(assert (forall ((x@18700 R) (x@18701 R)) (diseqR@55 (|:>:| x@18700 x@18701) Nil)))
(assert (forall ((x@18702 R)) (diseqR@55 (Star x@18702) Nil)))
(assert (forall ((x@18703 T)) (diseqR@55 Eps (Atom x@18703))))
(assert (forall ((x@18704 R) (x@18705 R)) (diseqR@55 Eps (|:+:| x@18704 x@18705))))
(assert (forall ((x@18706 R) (x@18707 R)) (diseqR@55 Eps (|:>:| x@18706 x@18707))))
(assert (forall ((x@18708 R)) (diseqR@55 Eps (Star x@18708))))
(assert (forall ((x@18709 T)) (diseqR@55 (Atom x@18709) Eps)))
(assert (forall ((x@18710 R) (x@18711 R)) (diseqR@55 (|:+:| x@18710 x@18711) Eps)))
(assert (forall ((x@18712 R) (x@18713 R)) (diseqR@55 (|:>:| x@18712 x@18713) Eps)))
(assert (forall ((x@18714 R)) (diseqR@55 (Star x@18714) Eps)))
(assert (forall ((x@18715 T) (x@18716 R) (x@18717 R)) (diseqR@55 (Atom x@18715) (|:+:| x@18716 x@18717))))
(assert (forall ((x@18718 T) (x@18719 R) (x@18720 R)) (diseqR@55 (Atom x@18718) (|:>:| x@18719 x@18720))))
(assert (forall ((x@18721 T) (x@18722 R)) (diseqR@55 (Atom x@18721) (Star x@18722))))
(assert (forall ((x@18723 R) (x@18724 R) (x@18725 T)) (diseqR@55 (|:+:| x@18723 x@18724) (Atom x@18725))))
(assert (forall ((x@18726 R) (x@18727 R) (x@18728 T)) (diseqR@55 (|:>:| x@18726 x@18727) (Atom x@18728))))
(assert (forall ((x@18729 R) (x@18730 T)) (diseqR@55 (Star x@18729) (Atom x@18730))))
(assert (forall ((x@18731 R) (x@18732 R) (x@18733 R) (x@18734 R)) (diseqR@55 (|:+:| x@18731 x@18732) (|:>:| x@18733 x@18734))))
(assert (forall ((x@18735 R) (x@18736 R) (x@18737 R)) (diseqR@55 (|:+:| x@18735 x@18736) (Star x@18737))))
(assert (forall ((x@18738 R) (x@18739 R) (x@18740 R) (x@18741 R)) (diseqR@55 (|:>:| x@18738 x@18739) (|:+:| x@18740 x@18741))))
(assert (forall ((x@18742 R) (x@18743 R) (x@18744 R)) (diseqR@55 (Star x@18742) (|:+:| x@18743 x@18744))))
(assert (forall ((x@18745 R) (x@18746 R) (x@18747 R)) (diseqR@55 (|:>:| x@18745 x@18746) (Star x@18747))))
(assert (forall ((x@18748 R) (x@18749 R) (x@18750 R)) (diseqR@55 (Star x@18748) (|:>:| x@18749 x@18750))))
(assert (forall ((x@18751 T) (x@18752 T)) (=> (diseqT@40 x@18751 x@18752) (diseqR@55 (Atom x@18751) (Atom x@18752)))))
(assert (forall ((x@18753 R) (x@18754 R) (x@18755 R) (x@18756 R)) (=> (diseqR@55 x@18753 x@18755) (diseqR@55 (|:+:| x@18753 x@18754) (|:+:| x@18755 x@18756)))))
(assert (forall ((x@18753 R) (x@18754 R) (x@18755 R) (x@18756 R)) (=> (diseqR@55 x@18754 x@18756) (diseqR@55 (|:+:| x@18753 x@18754) (|:+:| x@18755 x@18756)))))
(assert (forall ((x@18757 R) (x@18758 R) (x@18759 R) (x@18760 R)) (=> (diseqR@55 x@18757 x@18759) (diseqR@55 (|:>:| x@18757 x@18758) (|:>:| x@18759 x@18760)))))
(assert (forall ((x@18757 R) (x@18758 R) (x@18759 R) (x@18760 R)) (=> (diseqR@55 x@18758 x@18760) (diseqR@55 (|:>:| x@18757 x@18758) (|:>:| x@18759 x@18760)))))
(assert (forall ((x@18761 R) (x@18762 R)) (=> (diseqR@55 x@18761 x@18762) (diseqR@55 (Star x@18761) (Star x@18762)))))
(declare-fun eps (Bool R) Bool)
(assert (forall ((x@18660 Bool) (x R) (x@18658 R)) (=> (and (= x@18660 false) (= x x@18658)) (eps x@18660 x))))
(assert (forall ((x@18660 Bool) (x R)) (=> (and (= x@18660 true) (= x Eps)) (eps x@18660 x))))
(assert (forall ((x@18660 Bool) (x R) (p R) (q R) (x@18662 Bool) (x@18661 Bool)) (=> (and (= x@18660 (or x@18662 x@18661)) (= x (|:+:| p q)) (eps x@18662 p) (eps x@18661 q)) (eps x@18660 x))))
(assert (forall ((x@18660 Bool) (x R) (r R) (q2 R) (x@18664 Bool) (x@18663 Bool)) (=> (and (= x@18660 (and x@18664 x@18663)) (= x (|:>:| r q2)) (eps x@18664 r) (eps x@18663 q2)) (eps x@18660 x))))
(assert (forall ((x@18660 Bool) (x R) (y R)) (=> (and (= x@18660 true) (= x (Star y))) (eps x@18660 x))))
(declare-fun step (R R T) Bool)
(assert (forall ((x@18665 R) (x R) (y T) (x@18659 R)) (=> (and (= x@18665 Nil) (= x x@18659)) (step x@18665 x y))))
(assert (forall ((x@18665 R) (x R) (y T) (b T)) (=> (and (= x@18665 (ite (= b y) Eps Nil)) (= x (Atom b))) (step x@18665 x y))))
(assert (forall ((x@18665 R) (x R) (y T) (p R) (q R) (x@18667 R) (x@18666 R)) (=> (and (= x@18665 (|:+:| x@18667 x@18666)) (= x (|:+:| p q)) (step x@18667 p y) (step x@18666 q y)) (step x@18665 x y))))
(assert (forall ((x@18665 R) (x R) (y T) (r R) (q2 R) (x@18668 Bool) (x@18670 R) (x@18669 R) (x@18671 R)) (=> (and (= x@18665 (ite x@18668 (|:+:| (|:>:| x@18670 q2) x@18669) (|:+:| (|:>:| x@18671 q2) Nil))) (= x (|:>:| r q2)) (eps x@18668 r) (step x@18670 r y) (step x@18669 q2 y) (step x@18671 r y)) (step x@18665 x y))))
(assert (forall ((x@18665 R) (x R) (y T) (p2 R) (x@18672 R)) (=> (and (= x@18665 (|:>:| x@18672 (Star p2))) (= x (Star p2)) (step x@18672 p2 y)) (step x@18665 x y))))
(declare-fun rec (Bool R list) Bool)
(assert (forall ((x@18673 Bool) (x R) (y list) (x@18674 Bool)) (=> (and (= x@18673 x@18674) (= y nil) (eps x@18674 x)) (rec x@18673 x y))))
(assert (forall ((x@18673 Bool) (x R) (y list) (z T) (xs list) (x@18676 Bool) (x@18675 R)) (=> (and (= x@18673 x@18676) (= y (cons z xs)) (rec x@18676 x@18675 xs) (step x@18675 x z)) (rec x@18673 x y))))
(assert (forall ((p R) (q R) (a T) (b T)) (forall ((x@18678 Bool) (x@18677 Bool)) (=> (and (diseqBool@0 x@18678 x@18677) (rec x@18678 (Star (|:+:| p q)) (cons a (cons b nil))) (rec x@18677 (|:+:| (Star p) (Star q)) (cons a (cons b nil)))) false))))
(check-sat)
(get-info :reason-unknown)
