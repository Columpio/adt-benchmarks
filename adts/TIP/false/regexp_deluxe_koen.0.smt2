(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@468 (Nat@0 Nat@0) Bool)
(assert (forall ((x@21768 Nat@0)) (diseqNat@0@468 Z@0 (S@0 x@21768))))
(assert (forall ((x@21769 Nat@0)) (diseqNat@0@468 (S@0 x@21769) Z@0)))
(assert (forall ((x@21770 Nat@0) (x@21771 Nat@0)) (=> (diseqNat@0@468 x@21770 x@21771) (diseqNat@0@468 (S@0 x@21770) (S@0 x@21771)))))
(declare-datatype T ((A ) (B ) (C )))
(declare-fun diseqT@61 (T T) Bool)
(assert (diseqT@61 A B))
(assert (diseqT@61 A C))
(assert (diseqT@61 B A))
(assert (diseqT@61 C A))
(assert (diseqT@61 B C))
(assert (diseqT@61 C B))
(declare-datatype list ((nil ) (cons (head T) (tail list))))
(declare-fun diseqlist@319 (list list) Bool)
(assert (forall ((x@21772 T) (x@21773 list)) (diseqlist@319 nil (cons x@21772 x@21773))))
(assert (forall ((x@21774 T) (x@21775 list)) (diseqlist@319 (cons x@21774 x@21775) nil)))
(assert (forall ((x@21776 T) (x@21777 list) (x@21778 T) (x@21779 list)) (=> (diseqT@61 x@21776 x@21778) (diseqlist@319 (cons x@21776 x@21777) (cons x@21778 x@21779)))))
(assert (forall ((x@21776 T) (x@21777 list) (x@21778 T) (x@21779 list)) (=> (diseqlist@319 x@21777 x@21779) (diseqlist@319 (cons x@21776 x@21777) (cons x@21778 x@21779)))))
(declare-datatype R ((Nil ) (Eps ) (Atom (proj1-Atom T)) (|:+:| (|proj1-:+:| R) (|proj2-:+:| R)) (|:&:| (|proj1-:&:| R) (|proj2-:&:| R)) (|:>:| (|proj1-:>:| R) (|proj2-:>:| R)) (Star (proj1-Star R))))
(declare-fun diseqR@76 (R R) Bool)
(assert (diseqR@76 Nil Eps))
(assert (forall ((x@21780 T)) (diseqR@76 Nil (Atom x@21780))))
(assert (forall ((x@21781 R) (x@21782 R)) (diseqR@76 Nil (|:+:| x@21781 x@21782))))
(assert (forall ((x@21783 R) (x@21784 R)) (diseqR@76 Nil (|:&:| x@21783 x@21784))))
(assert (forall ((x@21785 R) (x@21786 R)) (diseqR@76 Nil (|:>:| x@21785 x@21786))))
(assert (forall ((x@21787 R)) (diseqR@76 Nil (Star x@21787))))
(assert (diseqR@76 Eps Nil))
(assert (forall ((x@21788 T)) (diseqR@76 (Atom x@21788) Nil)))
(assert (forall ((x@21789 R) (x@21790 R)) (diseqR@76 (|:+:| x@21789 x@21790) Nil)))
(assert (forall ((x@21791 R) (x@21792 R)) (diseqR@76 (|:&:| x@21791 x@21792) Nil)))
(assert (forall ((x@21793 R) (x@21794 R)) (diseqR@76 (|:>:| x@21793 x@21794) Nil)))
(assert (forall ((x@21795 R)) (diseqR@76 (Star x@21795) Nil)))
(assert (forall ((x@21796 T)) (diseqR@76 Eps (Atom x@21796))))
(assert (forall ((x@21797 R) (x@21798 R)) (diseqR@76 Eps (|:+:| x@21797 x@21798))))
(assert (forall ((x@21799 R) (x@21800 R)) (diseqR@76 Eps (|:&:| x@21799 x@21800))))
(assert (forall ((x@21801 R) (x@21802 R)) (diseqR@76 Eps (|:>:| x@21801 x@21802))))
(assert (forall ((x@21803 R)) (diseqR@76 Eps (Star x@21803))))
(assert (forall ((x@21804 T)) (diseqR@76 (Atom x@21804) Eps)))
(assert (forall ((x@21805 R) (x@21806 R)) (diseqR@76 (|:+:| x@21805 x@21806) Eps)))
(assert (forall ((x@21807 R) (x@21808 R)) (diseqR@76 (|:&:| x@21807 x@21808) Eps)))
(assert (forall ((x@21809 R) (x@21810 R)) (diseqR@76 (|:>:| x@21809 x@21810) Eps)))
(assert (forall ((x@21811 R)) (diseqR@76 (Star x@21811) Eps)))
(assert (forall ((x@21812 T) (x@21813 R) (x@21814 R)) (diseqR@76 (Atom x@21812) (|:+:| x@21813 x@21814))))
(assert (forall ((x@21815 T) (x@21816 R) (x@21817 R)) (diseqR@76 (Atom x@21815) (|:&:| x@21816 x@21817))))
(assert (forall ((x@21818 T) (x@21819 R) (x@21820 R)) (diseqR@76 (Atom x@21818) (|:>:| x@21819 x@21820))))
(assert (forall ((x@21821 T) (x@21822 R)) (diseqR@76 (Atom x@21821) (Star x@21822))))
(assert (forall ((x@21823 R) (x@21824 R) (x@21825 T)) (diseqR@76 (|:+:| x@21823 x@21824) (Atom x@21825))))
(assert (forall ((x@21826 R) (x@21827 R) (x@21828 T)) (diseqR@76 (|:&:| x@21826 x@21827) (Atom x@21828))))
(assert (forall ((x@21829 R) (x@21830 R) (x@21831 T)) (diseqR@76 (|:>:| x@21829 x@21830) (Atom x@21831))))
(assert (forall ((x@21832 R) (x@21833 T)) (diseqR@76 (Star x@21832) (Atom x@21833))))
(assert (forall ((x@21834 R) (x@21835 R) (x@21836 R) (x@21837 R)) (diseqR@76 (|:+:| x@21834 x@21835) (|:&:| x@21836 x@21837))))
(assert (forall ((x@21838 R) (x@21839 R) (x@21840 R) (x@21841 R)) (diseqR@76 (|:+:| x@21838 x@21839) (|:>:| x@21840 x@21841))))
(assert (forall ((x@21842 R) (x@21843 R) (x@21844 R)) (diseqR@76 (|:+:| x@21842 x@21843) (Star x@21844))))
(assert (forall ((x@21845 R) (x@21846 R) (x@21847 R) (x@21848 R)) (diseqR@76 (|:&:| x@21845 x@21846) (|:+:| x@21847 x@21848))))
(assert (forall ((x@21849 R) (x@21850 R) (x@21851 R) (x@21852 R)) (diseqR@76 (|:>:| x@21849 x@21850) (|:+:| x@21851 x@21852))))
(assert (forall ((x@21853 R) (x@21854 R) (x@21855 R)) (diseqR@76 (Star x@21853) (|:+:| x@21854 x@21855))))
(assert (forall ((x@21856 R) (x@21857 R) (x@21858 R) (x@21859 R)) (diseqR@76 (|:&:| x@21856 x@21857) (|:>:| x@21858 x@21859))))
(assert (forall ((x@21860 R) (x@21861 R) (x@21862 R)) (diseqR@76 (|:&:| x@21860 x@21861) (Star x@21862))))
(assert (forall ((x@21863 R) (x@21864 R) (x@21865 R) (x@21866 R)) (diseqR@76 (|:>:| x@21863 x@21864) (|:&:| x@21865 x@21866))))
(assert (forall ((x@21867 R) (x@21868 R) (x@21869 R)) (diseqR@76 (Star x@21867) (|:&:| x@21868 x@21869))))
(assert (forall ((x@21870 R) (x@21871 R) (x@21872 R)) (diseqR@76 (|:>:| x@21870 x@21871) (Star x@21872))))
(assert (forall ((x@21873 R) (x@21874 R) (x@21875 R)) (diseqR@76 (Star x@21873) (|:>:| x@21874 x@21875))))
(assert (forall ((x@21876 T) (x@21877 T)) (=> (diseqT@61 x@21876 x@21877) (diseqR@76 (Atom x@21876) (Atom x@21877)))))
(assert (forall ((x@21878 R) (x@21879 R) (x@21880 R) (x@21881 R)) (=> (diseqR@76 x@21878 x@21880) (diseqR@76 (|:+:| x@21878 x@21879) (|:+:| x@21880 x@21881)))))
(assert (forall ((x@21878 R) (x@21879 R) (x@21880 R) (x@21881 R)) (=> (diseqR@76 x@21879 x@21881) (diseqR@76 (|:+:| x@21878 x@21879) (|:+:| x@21880 x@21881)))))
(assert (forall ((x@21882 R) (x@21883 R) (x@21884 R) (x@21885 R)) (=> (diseqR@76 x@21882 x@21884) (diseqR@76 (|:&:| x@21882 x@21883) (|:&:| x@21884 x@21885)))))
(assert (forall ((x@21882 R) (x@21883 R) (x@21884 R) (x@21885 R)) (=> (diseqR@76 x@21883 x@21885) (diseqR@76 (|:&:| x@21882 x@21883) (|:&:| x@21884 x@21885)))))
(assert (forall ((x@21886 R) (x@21887 R) (x@21888 R) (x@21889 R)) (=> (diseqR@76 x@21886 x@21888) (diseqR@76 (|:>:| x@21886 x@21887) (|:>:| x@21888 x@21889)))))
(assert (forall ((x@21886 R) (x@21887 R) (x@21888 R) (x@21889 R)) (=> (diseqR@76 x@21887 x@21889) (diseqR@76 (|:>:| x@21886 x@21887) (|:>:| x@21888 x@21889)))))
(assert (forall ((x@21890 R) (x@21891 R)) (=> (diseqR@76 x@21890 x@21891) (diseqR@76 (Star x@21890) (Star x@21891)))))
(declare-fun x.>. (R R R) Bool)
(assert (forall ((x@21737 R) (x R) (y R) (x@21727 R) (x@21728 R) (x@21729 R) (x@21730 R)) (=> (and (= x@21737 (|:>:| x y)) (= x x@21727) (= y x@21728) (= x x@21729) (= y x@21730)) (x.>. x@21737 x y))))
(assert (forall ((x@21737 R) (x R) (y R) (x@21727 R) (x@21728 R) (x@21729 R)) (=> (and (= x@21737 x) (= x x@21727) (= y x@21728) (= x x@21729) (= y Eps)) (x.>. x@21737 x y))))
(assert (forall ((x@21737 R) (x R) (y R) (x@21727 R) (x@21728 R)) (=> (and (= x@21737 y) (= x x@21727) (= y x@21728) (= x Eps)) (x.>. x@21737 x y))))
(assert (forall ((x@21737 R) (x R) (y R) (x@21727 R)) (=> (and (= x@21737 Nil) (= x x@21727) (= y Nil)) (x.>. x@21737 x y))))
(assert (forall ((x@21737 R) (x R) (y R)) (=> (and (= x@21737 Nil) (= x Nil)) (x.>. x@21737 x y))))
(declare-fun x.+. (R R R) Bool)
(assert (forall ((x@21738 R) (x R) (y R) (x@21731 R) (x@21732 R)) (=> (and (= x@21738 (|:+:| x y)) (= x x@21731) (= y x@21732)) (x.+. x@21738 x y))))
(assert (forall ((x@21738 R) (x R) (y R) (x@21731 R)) (=> (and (= x@21738 x) (= x x@21731) (= y Nil)) (x.+. x@21738 x y))))
(assert (forall ((x@21738 R) (x R) (y R)) (=> (and (= x@21738 y) (= x Nil)) (x.+. x@21738 x y))))
(declare-fun eps (Bool R) Bool)
(assert (forall ((x@21739 Bool) (x R) (x@21733 R)) (=> (and (= x@21739 false) (= x x@21733)) (eps x@21739 x))))
(assert (forall ((x@21739 Bool) (x R)) (=> (and (= x@21739 true) (= x Eps)) (eps x@21739 x))))
(assert (forall ((x@21739 Bool) (x R) (p R) (q R) (x@21741 Bool) (x@21740 Bool)) (=> (and (= x@21739 (or x@21741 x@21740)) (= x (|:+:| p q)) (eps x@21741 p) (eps x@21740 q)) (eps x@21739 x))))
(assert (forall ((x@21739 Bool) (x R) (r R) (q2 R) (x@21743 Bool) (x@21742 Bool)) (=> (and (= x@21739 (and x@21743 x@21742)) (= x (|:&:| r q2)) (eps x@21743 r) (eps x@21742 q2)) (eps x@21739 x))))
(assert (forall ((x@21739 Bool) (x R) (p2 R) (q3 R) (x@21745 Bool) (x@21744 Bool)) (=> (and (= x@21739 (and x@21745 x@21744)) (= x (|:>:| p2 q3)) (eps x@21745 p2) (eps x@21744 q3)) (eps x@21739 x))))
(assert (forall ((x@21739 Bool) (x R) (y R)) (=> (and (= x@21739 true) (= x (Star y))) (eps x@21739 x))))
(declare-fun step (R R T) Bool)
(assert (forall ((x@21746 R) (x R) (y T) (x@21734 R)) (=> (and (= x@21746 Nil) (= x x@21734)) (step x@21746 x y))))
(assert (forall ((x@21746 R) (x R) (y T) (b T)) (=> (and (= x@21746 (ite (= b y) Eps Nil)) (= x (Atom b))) (step x@21746 x y))))
(assert (forall ((x@21746 R) (x R) (y T) (p R) (q R) (x@21749 R) (x@21748 R) (x@21747 R)) (=> (and (= x@21746 x@21749) (= x (|:+:| p q)) (x.+. x@21749 x@21748 x@21747) (step x@21748 p y) (step x@21747 q y)) (step x@21746 x y))))
(assert (forall ((x@21746 R) (x R) (y T) (r R) (q2 R) (wild1 R) (x@21750 R) (x@21735 R) (wild2 R) (x@21751 R) (x@21736 R)) (=> (and (= x@21746 (|:&:| wild1 wild2)) (= x (|:&:| r q2)) (= wild1 x@21750) (step x@21750 r y) (= wild1 x@21735) (= wild2 x@21751) (step x@21751 q2 y) (= wild2 x@21736)) (step x@21746 x y))))
(assert (forall ((x@21746 R) (x R) (y T) (r R) (q2 R) (wild1 R) (x@21750 R) (x@21735 R) (wild2 R) (x@21751 R)) (=> (and (= x@21746 Nil) (= x (|:&:| r q2)) (= wild1 x@21750) (step x@21750 r y) (= wild1 x@21735) (= wild2 x@21751) (step x@21751 q2 y) (= wild2 Nil)) (step x@21746 x y))))
(assert (forall ((x@21746 R) (x R) (y T) (r R) (q2 R) (wild1 R) (x@21750 R)) (=> (and (= x@21746 Nil) (= x (|:&:| r q2)) (= wild1 x@21750) (step x@21750 r y) (= wild1 Nil)) (step x@21746 x y))))
(assert (forall ((x@21746 R) (x R) (y T) (p2 R) (q3 R) (x@21752 Bool) (x@21756 R) (x@21755 R) (x@21754 R) (x@21753 R) (x@21759 R) (x@21758 R) (x@21757 R)) (=> (and (= x@21746 (ite x@21752 x@21756 x@21759)) (= x (|:>:| p2 q3)) (eps x@21752 p2) (x.+. x@21756 x@21755 x@21753) (x.>. x@21755 x@21754 q3) (step x@21754 p2 y) (step x@21753 q3 y) (x.+. x@21759 x@21758 Nil) (x.>. x@21758 x@21757 q3) (step x@21757 p2 y)) (step x@21746 x y))))
(assert (forall ((x@21746 R) (x R) (y T) (p3 R) (x@21761 R) (x@21760 R)) (=> (and (= x@21746 x@21761) (= x (Star p3)) (x.>. x@21761 x@21760 (Star p3)) (step x@21760 p3 y)) (step x@21746 x y))))
(declare-fun rec (Bool R list) Bool)
(assert (forall ((x@21762 Bool) (x R) (y list) (x@21763 Bool)) (=> (and (= x@21762 x@21763) (= y nil) (eps x@21763 x)) (rec x@21762 x y))))
(assert (forall ((x@21762 Bool) (x R) (y list) (z T) (xs list) (x@21765 Bool) (x@21764 R)) (=> (and (= x@21762 x@21765) (= y (cons z xs)) (rec x@21765 x@21764 xs) (step x@21764 x z)) (rec x@21762 x y))))
(assert (forall ((p R) (q R) (s list)) (forall ((x@21767 Bool) (x@21766 Bool)) (=> (and (diseqBool@0 x@21767 x@21766) (rec x@21767 (|:>:| p q) s) (rec x@21766 (|:>:| q p) s)) false))))
(check-sat)
(get-info :reason-unknown)
