(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@471 (Nat@0 Nat@0) Bool)
(assert (forall ((x@22149 Nat@0)) (diseqNat@0@471 Z@0 (S@0 x@22149))))
(assert (forall ((x@22150 Nat@0)) (diseqNat@0@471 (S@0 x@22150) Z@0)))
(assert (forall ((x@22151 Nat@0) (x@22152 Nat@0)) (=> (diseqNat@0@471 x@22151 x@22152) (diseqNat@0@471 (S@0 x@22151) (S@0 x@22152)))))
(declare-datatype Nat ((S (proj1-S Nat)) (Z )))
(declare-fun diseqNat@330 (Nat Nat) Bool)
(assert (forall ((x@22153 Nat)) (diseqNat@330 (S x@22153) Z)))
(assert (forall ((x@22154 Nat)) (diseqNat@330 Z (S x@22154))))
(assert (forall ((x@22155 Nat) (x@22156 Nat)) (=> (diseqNat@330 x@22155 x@22156) (diseqNat@330 (S x@22155) (S x@22156)))))
(declare-datatype list ((nil ) (cons (head Nat) (tail list))))
(declare-fun diseqlist@322 (list list) Bool)
(assert (forall ((x@22157 Nat) (x@22158 list)) (diseqlist@322 nil (cons x@22157 x@22158))))
(assert (forall ((x@22159 Nat) (x@22160 list)) (diseqlist@322 (cons x@22159 x@22160) nil)))
(assert (forall ((x@22161 Nat) (x@22162 list) (x@22163 Nat) (x@22164 list)) (=> (diseqNat@330 x@22161 x@22163) (diseqlist@322 (cons x@22161 x@22162) (cons x@22163 x@22164)))))
(assert (forall ((x@22161 Nat) (x@22162 list) (x@22163 Nat) (x@22164 list)) (=> (diseqlist@322 x@22162 x@22164) (diseqlist@322 (cons x@22161 x@22162) (cons x@22163 x@22164)))))
(declare-fun ++ (list list list) Bool)
(assert (forall ((x@22140 list) (x list) (y list)) (=> (and (= x@22140 y) (= x nil)) (++ x@22140 x y))))
(assert (forall ((x@22140 list) (x list) (y list) (z Nat) (xs list) (x@22141 list)) (=> (and (= x@22140 (cons z x@22141)) (= x (cons z xs)) (++ x@22141 xs y)) (++ x@22140 x y))))
(declare-fun rotate (list Nat list) Bool)
(assert (forall ((x@22142 list) (x Nat) (y list) (z Nat)) (=> (and (= x@22142 nil) (= x (S z)) (= y nil)) (rotate x@22142 x y))))
(assert (forall ((x@22142 list) (x Nat) (y list) (z Nat) (x2 Nat) (x3 list) (x@22144 list) (x@22143 list)) (=> (and (= x@22142 x@22144) (= x (S z)) (= y (cons x2 x3)) (rotate x@22144 z x@22143) (++ x@22143 x3 (cons x2 nil))) (rotate x@22142 x y))))
(assert (forall ((x@22142 list) (x Nat) (y list)) (=> (and (= x@22142 y) (= x Z)) (rotate x@22142 x y))))
(assert (forall ((n Nat) (m Nat) (xs list)) (forall ((x@22148 list) (x@22147 list) (x@22146 list) (x@22145 list)) (=> (and (diseqlist@322 x@22148 x@22146) (rotate x@22148 n x@22147) (rotate x@22147 m xs) (rotate x@22146 m x@22145) (rotate x@22145 n xs)) false))))
(check-sat)
(get-info :reason-unknown)
