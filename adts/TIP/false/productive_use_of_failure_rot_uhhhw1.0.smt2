(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@446 (Nat@0 Nat@0) Bool)
(assert (forall ((x@19682 Nat@0)) (diseqNat@0@446 Z@0 (S@0 x@19682))))
(assert (forall ((x@19683 Nat@0)) (diseqNat@0@446 (S@0 x@19683) Z@0)))
(assert (forall ((x@19684 Nat@0) (x@19685 Nat@0)) (=> (diseqNat@0@446 x@19684 x@19685) (diseqNat@0@446 (S@0 x@19684) (S@0 x@19685)))))
(declare-datatype Nat ((S (proj1-S Nat)) (Z )))
(declare-fun diseqNat@322 (Nat Nat) Bool)
(assert (forall ((x@19686 Nat)) (diseqNat@322 (S x@19686) Z)))
(assert (forall ((x@19687 Nat)) (diseqNat@322 Z (S x@19687))))
(assert (forall ((x@19688 Nat) (x@19689 Nat)) (=> (diseqNat@322 x@19688 x@19689) (diseqNat@322 (S x@19688) (S x@19689)))))
(declare-datatype list ((nil ) (cons (head Nat) (tail list))))
(declare-fun diseqlist@297 (list list) Bool)
(assert (forall ((x@19690 Nat) (x@19691 list)) (diseqlist@297 nil (cons x@19690 x@19691))))
(assert (forall ((x@19692 Nat) (x@19693 list)) (diseqlist@297 (cons x@19692 x@19693) nil)))
(assert (forall ((x@19694 Nat) (x@19695 list) (x@19696 Nat) (x@19697 list)) (=> (diseqNat@322 x@19694 x@19696) (diseqlist@297 (cons x@19694 x@19695) (cons x@19696 x@19697)))))
(assert (forall ((x@19694 Nat) (x@19695 list) (x@19696 Nat) (x@19697 list)) (=> (diseqlist@297 x@19695 x@19697) (diseqlist@297 (cons x@19694 x@19695) (cons x@19696 x@19697)))))
(declare-fun length (Nat list) Bool)
(assert (forall ((x@19671 Nat) (x list)) (=> (and (= x@19671 Z) (= x nil)) (length x@19671 x))))
(assert (forall ((x@19671 Nat) (x list) (y Nat) (xs list) (x@19672 Nat)) (=> (and (= x@19671 (S x@19672)) (= x (cons y xs)) (length x@19672 xs)) (length x@19671 x))))
(declare-fun ++ (list list list) Bool)
(assert (forall ((x@19673 list) (x list) (y list)) (=> (and (= x@19673 y) (= x nil)) (++ x@19673 x y))))
(assert (forall ((x@19673 list) (x list) (y list) (z Nat) (xs list) (x@19674 list)) (=> (and (= x@19673 (cons z x@19674)) (= x (cons z xs)) (++ x@19674 xs y)) (++ x@19673 x y))))
(declare-fun rotate (list Nat list) Bool)
(assert (forall ((x@19675 list) (x Nat) (y list) (z Nat)) (=> (and (= x@19675 nil) (= x (S z)) (= y nil)) (rotate x@19675 x y))))
(assert (forall ((x@19675 list) (x Nat) (y list) (z Nat) (x2 Nat) (x3 list) (x@19677 list) (x@19676 list)) (=> (and (= x@19675 x@19677) (= x (S z)) (= y (cons x2 x3)) (rotate x@19677 z x@19676) (++ x@19676 x3 (cons x2 nil))) (rotate x@19675 x y))))
(assert (forall ((x@19675 list) (x Nat) (y list)) (=> (and (= x@19675 y) (= x Z)) (rotate x@19675 x y))))
(assert (forall ((xs list) (ys list)) (forall ((x@19681 list) (x@19680 Nat) (x@19679 list) (x@19678 list)) (=> (and (diseqlist@297 xs ys) (= x@19681 x@19678) (rotate x@19681 x@19680 x@19679) (length x@19680 xs) (++ x@19679 xs ys) (++ x@19678 xs ys)) false))))
(check-sat)
(get-info :reason-unknown)
