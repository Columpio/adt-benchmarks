(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@473 (Nat@0 Nat@0) Bool)
(assert (forall ((x@22289 Nat@0)) (diseqNat@0@473 Z@0 (S@0 x@22289))))
(assert (forall ((x@22290 Nat@0)) (diseqNat@0@473 (S@0 x@22290) Z@0)))
(assert (forall ((x@22291 Nat@0) (x@22292 Nat@0)) (=> (diseqNat@0@473 x@22291 x@22292) (diseqNat@0@473 (S@0 x@22291) (S@0 x@22292)))))
(declare-datatype T ((A ) (B ) (C )))
(declare-fun diseqT@65 (T T) Bool)
(assert (diseqT@65 A B))
(assert (diseqT@65 A C))
(assert (diseqT@65 B A))
(assert (diseqT@65 C A))
(assert (diseqT@65 B C))
(assert (diseqT@65 C B))
(declare-datatype list ((nil ) (cons (head T) (tail list))))
(declare-fun diseqlist@324 (list list) Bool)
(assert (forall ((x@22293 T) (x@22294 list)) (diseqlist@324 nil (cons x@22293 x@22294))))
(assert (forall ((x@22295 T) (x@22296 list)) (diseqlist@324 (cons x@22295 x@22296) nil)))
(assert (forall ((x@22297 T) (x@22298 list) (x@22299 T) (x@22300 list)) (=> (diseqT@65 x@22297 x@22299) (diseqlist@324 (cons x@22297 x@22298) (cons x@22299 x@22300)))))
(assert (forall ((x@22297 T) (x@22298 list) (x@22299 T) (x@22300 list)) (=> (diseqlist@324 x@22298 x@22300) (diseqlist@324 (cons x@22297 x@22298) (cons x@22299 x@22300)))))
(declare-datatype R ((Nil ) (Eps ) (Atom (proj1-Atom T)) (|:+:| (|proj1-:+:| R) (|proj2-:+:| R)) (|:>:| (|proj1-:>:| R) (|proj2-:>:| R)) (Star (proj1-Star R))))
(declare-fun diseqR@80 (R R) Bool)
(assert (diseqR@80 Nil Eps))
(assert (forall ((x@22301 T)) (diseqR@80 Nil (Atom x@22301))))
(assert (forall ((x@22302 R) (x@22303 R)) (diseqR@80 Nil (|:+:| x@22302 x@22303))))
(assert (forall ((x@22304 R) (x@22305 R)) (diseqR@80 Nil (|:>:| x@22304 x@22305))))
(assert (forall ((x@22306 R)) (diseqR@80 Nil (Star x@22306))))
(assert (diseqR@80 Eps Nil))
(assert (forall ((x@22307 T)) (diseqR@80 (Atom x@22307) Nil)))
(assert (forall ((x@22308 R) (x@22309 R)) (diseqR@80 (|:+:| x@22308 x@22309) Nil)))
(assert (forall ((x@22310 R) (x@22311 R)) (diseqR@80 (|:>:| x@22310 x@22311) Nil)))
(assert (forall ((x@22312 R)) (diseqR@80 (Star x@22312) Nil)))
(assert (forall ((x@22313 T)) (diseqR@80 Eps (Atom x@22313))))
(assert (forall ((x@22314 R) (x@22315 R)) (diseqR@80 Eps (|:+:| x@22314 x@22315))))
(assert (forall ((x@22316 R) (x@22317 R)) (diseqR@80 Eps (|:>:| x@22316 x@22317))))
(assert (forall ((x@22318 R)) (diseqR@80 Eps (Star x@22318))))
(assert (forall ((x@22319 T)) (diseqR@80 (Atom x@22319) Eps)))
(assert (forall ((x@22320 R) (x@22321 R)) (diseqR@80 (|:+:| x@22320 x@22321) Eps)))
(assert (forall ((x@22322 R) (x@22323 R)) (diseqR@80 (|:>:| x@22322 x@22323) Eps)))
(assert (forall ((x@22324 R)) (diseqR@80 (Star x@22324) Eps)))
(assert (forall ((x@22325 T) (x@22326 R) (x@22327 R)) (diseqR@80 (Atom x@22325) (|:+:| x@22326 x@22327))))
(assert (forall ((x@22328 T) (x@22329 R) (x@22330 R)) (diseqR@80 (Atom x@22328) (|:>:| x@22329 x@22330))))
(assert (forall ((x@22331 T) (x@22332 R)) (diseqR@80 (Atom x@22331) (Star x@22332))))
(assert (forall ((x@22333 R) (x@22334 R) (x@22335 T)) (diseqR@80 (|:+:| x@22333 x@22334) (Atom x@22335))))
(assert (forall ((x@22336 R) (x@22337 R) (x@22338 T)) (diseqR@80 (|:>:| x@22336 x@22337) (Atom x@22338))))
(assert (forall ((x@22339 R) (x@22340 T)) (diseqR@80 (Star x@22339) (Atom x@22340))))
(assert (forall ((x@22341 R) (x@22342 R) (x@22343 R) (x@22344 R)) (diseqR@80 (|:+:| x@22341 x@22342) (|:>:| x@22343 x@22344))))
(assert (forall ((x@22345 R) (x@22346 R) (x@22347 R)) (diseqR@80 (|:+:| x@22345 x@22346) (Star x@22347))))
(assert (forall ((x@22348 R) (x@22349 R) (x@22350 R) (x@22351 R)) (diseqR@80 (|:>:| x@22348 x@22349) (|:+:| x@22350 x@22351))))
(assert (forall ((x@22352 R) (x@22353 R) (x@22354 R)) (diseqR@80 (Star x@22352) (|:+:| x@22353 x@22354))))
(assert (forall ((x@22355 R) (x@22356 R) (x@22357 R)) (diseqR@80 (|:>:| x@22355 x@22356) (Star x@22357))))
(assert (forall ((x@22358 R) (x@22359 R) (x@22360 R)) (diseqR@80 (Star x@22358) (|:>:| x@22359 x@22360))))
(assert (forall ((x@22361 T) (x@22362 T)) (=> (diseqT@65 x@22361 x@22362) (diseqR@80 (Atom x@22361) (Atom x@22362)))))
(assert (forall ((x@22363 R) (x@22364 R) (x@22365 R) (x@22366 R)) (=> (diseqR@80 x@22363 x@22365) (diseqR@80 (|:+:| x@22363 x@22364) (|:+:| x@22365 x@22366)))))
(assert (forall ((x@22363 R) (x@22364 R) (x@22365 R) (x@22366 R)) (=> (diseqR@80 x@22364 x@22366) (diseqR@80 (|:+:| x@22363 x@22364) (|:+:| x@22365 x@22366)))))
(assert (forall ((x@22367 R) (x@22368 R) (x@22369 R) (x@22370 R)) (=> (diseqR@80 x@22367 x@22369) (diseqR@80 (|:>:| x@22367 x@22368) (|:>:| x@22369 x@22370)))))
(assert (forall ((x@22367 R) (x@22368 R) (x@22369 R) (x@22370 R)) (=> (diseqR@80 x@22368 x@22370) (diseqR@80 (|:>:| x@22367 x@22368) (|:>:| x@22369 x@22370)))))
(assert (forall ((x@22371 R) (x@22372 R)) (=> (diseqR@80 x@22371 x@22372) (diseqR@80 (Star x@22371) (Star x@22372)))))
(declare-fun eps (Bool R) Bool)
(assert (forall ((x@22271 Bool) (x R) (x@22269 R)) (=> (and (= x@22271 false) (= x x@22269)) (eps x@22271 x))))
(assert (forall ((x@22271 Bool) (x R)) (=> (and (= x@22271 true) (= x Eps)) (eps x@22271 x))))
(assert (forall ((x@22271 Bool) (x R) (p R) (q R) (x@22273 Bool) (x@22272 Bool)) (=> (and (= x@22271 (or x@22273 x@22272)) (= x (|:+:| p q)) (eps x@22273 p) (eps x@22272 q)) (eps x@22271 x))))
(assert (forall ((x@22271 Bool) (x R) (r R) (q2 R) (x@22275 Bool) (x@22274 Bool)) (=> (and (= x@22271 (and x@22275 x@22274)) (= x (|:>:| r q2)) (eps x@22275 r) (eps x@22274 q2)) (eps x@22271 x))))
(assert (forall ((x@22271 Bool) (x R) (y R)) (=> (and (= x@22271 true) (= x (Star y))) (eps x@22271 x))))
(declare-fun step (R R T) Bool)
(assert (forall ((x@22276 R) (x R) (y T) (x@22270 R)) (=> (and (= x@22276 Nil) (= x x@22270)) (step x@22276 x y))))
(assert (forall ((x@22276 R) (x R) (y T) (b T)) (=> (and (= x@22276 (ite (= b y) Eps Nil)) (= x (Atom b))) (step x@22276 x y))))
(assert (forall ((x@22276 R) (x R) (y T) (p R) (q R) (x@22278 R) (x@22277 R)) (=> (and (= x@22276 (|:+:| x@22278 x@22277)) (= x (|:+:| p q)) (step x@22278 p y) (step x@22277 q y)) (step x@22276 x y))))
(assert (forall ((x@22276 R) (x R) (y T) (r R) (q2 R) (x@22279 Bool) (x@22281 R) (x@22280 R) (x@22282 R)) (=> (and (= x@22276 (ite x@22279 (|:+:| (|:>:| x@22281 q2) x@22280) (|:+:| (|:>:| x@22282 q2) Nil))) (= x (|:>:| r q2)) (eps x@22279 r) (step x@22281 r y) (step x@22280 q2 y) (step x@22282 r y)) (step x@22276 x y))))
(assert (forall ((x@22276 R) (x R) (y T) (p2 R) (x@22283 R)) (=> (and (= x@22276 (|:>:| x@22283 (Star p2))) (= x (Star p2)) (step x@22283 p2 y)) (step x@22276 x y))))
(declare-fun rec (Bool R list) Bool)
(assert (forall ((x@22284 Bool) (x R) (y list) (x@22285 Bool)) (=> (and (= x@22284 x@22285) (= y nil) (eps x@22285 x)) (rec x@22284 x y))))
(assert (forall ((x@22284 Bool) (x R) (y list) (z T) (xs list) (x@22287 Bool) (x@22286 R)) (=> (and (= x@22284 x@22287) (= y (cons z xs)) (rec x@22287 x@22286 xs) (step x@22286 x z)) (rec x@22284 x y))))
(assert (forall ((p R)) (forall ((x@22288 Bool)) (=> (and x@22288 (rec x@22288 p (cons A (cons B (cons B (cons A nil)))))) false))))
(check-sat)
(get-info :reason-unknown)
