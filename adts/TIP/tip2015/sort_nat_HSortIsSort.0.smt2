(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@554 (Nat@0 Nat@0) Bool)
(assert (forall ((x@26260 Nat@0)) (diseqNat@0@554 Z@0 (S@0 x@26260))))
(assert (forall ((x@26261 Nat@0)) (diseqNat@0@554 (S@0 x@26261) Z@0)))
(assert (forall ((x@26262 Nat@0) (x@26263 Nat@0)) (=> (diseqNat@0@554 x@26262 x@26263) (diseqNat@0@554 (S@0 x@26262) (S@0 x@26263)))))
(declare-datatype Nat ((zero ) (succ (p Nat))))
(declare-fun diseqNat@386 (Nat Nat) Bool)
(assert (forall ((x@26264 Nat)) (diseqNat@386 zero (succ x@26264))))
(assert (forall ((x@26265 Nat)) (diseqNat@386 (succ x@26265) zero)))
(assert (forall ((x@26266 Nat) (x@26267 Nat)) (=> (diseqNat@386 x@26266 x@26267) (diseqNat@386 (succ x@26266) (succ x@26267)))))
(declare-datatype list ((nil ) (cons (head Nat) (tail list))))
(declare-fun diseqlist@363 (list list) Bool)
(assert (forall ((x@26268 Nat) (x@26269 list)) (diseqlist@363 nil (cons x@26268 x@26269))))
(assert (forall ((x@26270 Nat) (x@26271 list)) (diseqlist@363 (cons x@26270 x@26271) nil)))
(assert (forall ((x@26272 Nat) (x@26273 list) (x@26274 Nat) (x@26275 list)) (=> (diseqNat@386 x@26272 x@26274) (diseqlist@363 (cons x@26272 x@26273) (cons x@26274 x@26275)))))
(assert (forall ((x@26272 Nat) (x@26273 list) (x@26274 Nat) (x@26275 list)) (=> (diseqlist@363 x@26273 x@26275) (diseqlist@363 (cons x@26272 x@26273) (cons x@26274 x@26275)))))
(declare-datatype Heap ((Node (proj1-Node Heap) (proj2-Node Nat) (proj3-Node Heap)) (Nil )))
(declare-fun diseqHeap@8 (Heap Heap) Bool)
(assert (forall ((x@26276 Heap) (x@26277 Nat) (x@26278 Heap)) (diseqHeap@8 (Node x@26276 x@26277 x@26278) Nil)))
(assert (forall ((x@26279 Heap) (x@26280 Nat) (x@26281 Heap)) (diseqHeap@8 Nil (Node x@26279 x@26280 x@26281))))
(assert (forall ((x@26282 Heap) (x@26283 Nat) (x@26284 Heap) (x@26285 Heap) (x@26286 Nat) (x@26287 Heap)) (=> (diseqHeap@8 x@26282 x@26285) (diseqHeap@8 (Node x@26282 x@26283 x@26284) (Node x@26285 x@26286 x@26287)))))
(assert (forall ((x@26282 Heap) (x@26283 Nat) (x@26284 Heap) (x@26285 Heap) (x@26286 Nat) (x@26287 Heap)) (=> (diseqNat@386 x@26283 x@26286) (diseqHeap@8 (Node x@26282 x@26283 x@26284) (Node x@26285 x@26286 x@26287)))))
(assert (forall ((x@26282 Heap) (x@26283 Nat) (x@26284 Heap) (x@26285 Heap) (x@26286 Nat) (x@26287 Heap)) (=> (diseqHeap@8 x@26284 x@26287) (diseqHeap@8 (Node x@26282 x@26283 x@26284) (Node x@26285 x@26286 x@26287)))))
(declare-datatype list2 ((nil2 ) (cons2 (head2 Heap) (tail2 list2))))
(declare-fun diseqlist2@40 (list2 list2) Bool)
(assert (forall ((x@26288 Heap) (x@26289 list2)) (diseqlist2@40 nil2 (cons2 x@26288 x@26289))))
(assert (forall ((x@26290 Heap) (x@26291 list2)) (diseqlist2@40 (cons2 x@26290 x@26291) nil2)))
(assert (forall ((x@26292 Heap) (x@26293 list2) (x@26294 Heap) (x@26295 list2)) (=> (diseqHeap@8 x@26292 x@26294) (diseqlist2@40 (cons2 x@26292 x@26293) (cons2 x@26294 x@26295)))))
(assert (forall ((x@26292 Heap) (x@26293 list2) (x@26294 Heap) (x@26295 list2)) (=> (diseqlist2@40 x@26293 x@26295) (diseqlist2@40 (cons2 x@26292 x@26293) (cons2 x@26294 x@26295)))))
(declare-fun toHeap (list2 list) Bool)
(assert (forall ((x@26229 list2) (x list)) (=> (and (= x@26229 nil2) (= x nil)) (toHeap x@26229 x))))
(assert (forall ((x@26229 list2) (x list) (y Nat) (z list) (x@26230 list2)) (=> (and (= x@26229 (cons2 (Node Nil y Nil) x@26230)) (= x (cons y z)) (toHeap x@26230 z)) (toHeap x@26229 x))))
(declare-fun leq (Bool Nat Nat) Bool)
(assert (forall ((x@26231 Bool) (x Nat) (y Nat)) (=> (and (= x@26231 true) (= x zero)) (leq x@26231 x y))))
(assert (forall ((x@26231 Bool) (x Nat) (y Nat) (z Nat)) (=> (and (= x@26231 false) (= x (succ z)) (= y zero)) (leq x@26231 x y))))
(assert (forall ((x@26231 Bool) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@26232 Bool)) (=> (and (= x@26231 x@26232) (= x (succ z)) (= y (succ x2)) (leq x@26232 z x2)) (leq x@26231 x y))))
(declare-fun insert2 (list Nat list) Bool)
(assert (forall ((x@26233 list) (x Nat) (y list)) (=> (and (= x@26233 (cons x nil)) (= y nil)) (insert2 x@26233 x y))))
(assert (forall ((x@26233 list) (x Nat) (y list) (z Nat) (xs list) (x@26234 Bool) (x@26235 list)) (=> (and (= x@26233 (ite x@26234 (cons x (cons z xs)) (cons z x@26235))) (= y (cons z xs)) (leq x@26234 x z) (insert2 x@26235 x xs)) (insert2 x@26233 x y))))
(declare-fun isort (list list) Bool)
(assert (forall ((x@26236 list) (x list)) (=> (and (= x@26236 nil) (= x nil)) (isort x@26236 x))))
(assert (forall ((x@26236 list) (x list) (y Nat) (xs list) (x@26238 list) (x@26237 list)) (=> (and (= x@26236 x@26238) (= x (cons y xs)) (insert2 x@26238 y x@26237) (isort x@26237 xs)) (isort x@26236 x))))
(declare-fun hmerge (Heap Heap Heap) Bool)
(assert (forall ((x@26239 Heap) (x Heap) (y Heap) (z Heap) (x2 Nat) (x3 Heap) (x4 Heap) (x5 Nat) (x6 Heap) (x@26240 Bool) (x@26241 Heap) (x@26242 Heap)) (=> (and (= x@26239 (ite x@26240 (Node x@26241 x2 z) (Node x@26242 x5 x4))) (= x (Node z x2 x3)) (= y (Node x4 x5 x6)) (leq x@26240 x2 x5) (hmerge x@26241 x3 (Node x4 x5 x6)) (hmerge x@26242 (Node z x2 x3) x6)) (hmerge x@26239 x y))))
(assert (forall ((x@26239 Heap) (x Heap) (y Heap) (z Heap) (x2 Nat) (x3 Heap)) (=> (and (= x@26239 (Node z x2 x3)) (= x (Node z x2 x3)) (= y Nil)) (hmerge x@26239 x y))))
(assert (forall ((x@26239 Heap) (x Heap) (y Heap)) (=> (and (= x@26239 y) (= x Nil)) (hmerge x@26239 x y))))
(declare-fun hpairwise (list2 list2) Bool)
(assert (forall ((x@26243 list2) (x list2)) (=> (and (= x@26243 nil2) (= x nil2)) (hpairwise x@26243 x))))
(assert (forall ((x@26243 list2) (x list2) (q Heap) (y list2)) (=> (and (= x@26243 (cons2 q nil2)) (= x (cons2 q y)) (= y nil2)) (hpairwise x@26243 x))))
(assert (forall ((x@26243 list2) (x list2) (q Heap) (y list2) (r Heap) (qs list2) (x@26245 Heap) (x@26244 list2)) (=> (and (= x@26243 (cons2 x@26245 x@26244)) (= x (cons2 q y)) (= y (cons2 r qs)) (hmerge x@26245 q r) (hpairwise x@26244 qs)) (hpairwise x@26243 x))))
(declare-fun hmerging (Heap list2) Bool)
(assert (forall ((x@26246 Heap) (x list2)) (=> (and (= x@26246 Nil) (= x nil2)) (hmerging x@26246 x))))
(assert (forall ((x@26246 Heap) (x list2) (q Heap) (y list2)) (=> (and (= x@26246 q) (= x (cons2 q y)) (= y nil2)) (hmerging x@26246 x))))
(assert (forall ((x@26246 Heap) (x list2) (q Heap) (y list2) (z Heap) (x2 list2) (x@26248 Heap) (x@26247 list2)) (=> (and (= x@26246 x@26248) (= x (cons2 q y)) (= y (cons2 z x2)) (hmerging x@26248 x@26247) (hpairwise x@26247 (cons2 q (cons2 z x2)))) (hmerging x@26246 x))))
(declare-fun toHeap2 (Heap list) Bool)
(assert (forall ((x@26249 Heap) (x list) (x@26251 Heap) (x@26250 list2)) (=> (and (= x@26249 x@26251) (hmerging x@26251 x@26250) (toHeap x@26250 x)) (toHeap2 x@26249 x))))
(declare-fun toList (list Heap) Bool)
(assert (forall ((x@26252 list) (x Heap) (q Heap) (y Nat) (r Heap) (x@26254 list) (x@26253 Heap)) (=> (and (= x@26252 (cons y x@26254)) (= x (Node q y r)) (toList x@26254 x@26253) (hmerge x@26253 q r)) (toList x@26252 x))))
(assert (forall ((x@26252 list) (x Heap)) (=> (and (= x@26252 nil) (= x Nil)) (toList x@26252 x))))
(declare-fun hsort (list list) Bool)
(assert (forall ((x@26255 list) (x list) (x@26257 list) (x@26256 Heap)) (=> (and (= x@26255 x@26257) (toList x@26257 x@26256) (toHeap2 x@26256 x)) (hsort x@26255 x))))
(assert (forall ((xs list)) (forall ((x@26259 list) (x@26258 list)) (=> (and (diseqlist@363 x@26259 x@26258) (hsort x@26259 xs) (isort x@26258 xs)) false))))
(check-sat)
(get-info :reason-unknown)
