(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@648 (Nat@0 Nat@0) Bool)
(assert (forall ((x@30533 Nat@0)) (diseqNat@0@648 Z@0 (S@0 x@30533))))
(assert (forall ((x@30534 Nat@0)) (diseqNat@0@648 (S@0 x@30534) Z@0)))
(assert (forall ((x@30535 Nat@0) (x@30536 Nat@0)) (=> (diseqNat@0@648 x@30535 x@30536) (diseqNat@0@648 (S@0 x@30535) (S@0 x@30536)))))
(declare-datatype Nat ((zero ) (succ (p Nat))))
(declare-fun diseqNat@463 (Nat Nat) Bool)
(assert (forall ((x@30537 Nat)) (diseqNat@463 zero (succ x@30537))))
(assert (forall ((x@30538 Nat)) (diseqNat@463 (succ x@30538) zero)))
(assert (forall ((x@30539 Nat) (x@30540 Nat)) (=> (diseqNat@463 x@30539 x@30540) (diseqNat@463 (succ x@30539) (succ x@30540)))))
(declare-fun plus (Nat Nat Nat) Bool)
(assert (forall ((x@30492 Nat) (x Nat) (y Nat)) (=> (and (= x@30492 y) (= x zero)) (plus x@30492 x y))))
(assert (forall ((x@30492 Nat) (x Nat) (y Nat) (z Nat) (x@30493 Nat)) (=> (and (= x@30492 (succ x@30493)) (= x (succ z)) (plus x@30493 z y)) (plus x@30492 x y))))
(declare-fun times (Nat Nat Nat) Bool)
(assert (forall ((x@30494 Nat) (x Nat) (y Nat)) (=> (and (= x@30494 zero) (= x zero)) (times x@30494 x y))))
(assert (forall ((x@30494 Nat) (x Nat) (y Nat) (z Nat) (x@30496 Nat) (x@30495 Nat)) (=> (and (= x@30494 x@30496) (= x (succ z)) (plus x@30496 y x@30495) (times x@30495 z y)) (times x@30494 x y))))
(declare-fun acc_plus (Nat Nat Nat) Bool)
(assert (forall ((x@30497 Nat) (x Nat) (y Nat)) (=> (and (= x@30497 y) (= x zero)) (acc_plus x@30497 x y))))
(assert (forall ((x@30497 Nat) (x Nat) (y Nat) (z Nat) (x@30498 Nat)) (=> (and (= x@30497 x@30498) (= x (succ z)) (acc_plus x@30498 z (succ y))) (acc_plus x@30497 x y))))
(declare-fun acc_alt_mul (Nat Nat Nat) Bool)
(assert (forall ((x@30499 Nat) (x Nat) (y Nat)) (=> (and (= x@30499 zero) (= x zero)) (acc_alt_mul x@30499 x y))))
(assert (forall ((x@30499 Nat) (x Nat) (y Nat) (z Nat)) (=> (and (= x@30499 zero) (= x (succ z)) (= y zero)) (acc_alt_mul x@30499 x y))))
(assert (forall ((x@30499 Nat) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@30502 Nat) (x@30501 Nat) (x@30500 Nat)) (=> (and (= x@30499 x@30502) (= x (succ z)) (= y (succ x2)) (acc_plus x@30502 (succ z) x@30501) (acc_plus x@30501 x2 x@30500) (acc_alt_mul x@30500 z x2)) (acc_alt_mul x@30499 x y))))
(assert (forall ((x Nat) (y Nat)) (forall ((x@30504 Nat) (x@30503 Nat)) (=> (and (diseqNat@463 x@30504 x@30503) (acc_alt_mul x@30504 x y) (times x@30503 x y)) false))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@30508 Nat) (x@30507 Nat) (x@30506 Nat) (x@30505 Nat)) (=> (and (diseqNat@463 x@30508 x@30506) (times x@30508 x x@30507) (times x@30507 y z) (times x@30506 x@30505 z) (times x@30505 x y)) false))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@30512 Nat) (x@30511 Nat) (x@30510 Nat) (x@30509 Nat)) (=> (and (diseqNat@463 x@30512 x@30510) (plus x@30512 x x@30511) (plus x@30511 y z) (plus x@30510 x@30509 z) (plus x@30509 x y)) false))))
(assert (forall ((x Nat) (y Nat)) (forall ((x@30514 Nat) (x@30513 Nat)) (=> (and (diseqNat@463 x@30514 x@30513) (times x@30514 x y) (times x@30513 y x)) false))))
(assert (forall ((x Nat) (y Nat)) (forall ((x@30516 Nat) (x@30515 Nat)) (=> (and (diseqNat@463 x@30516 x@30515) (plus x@30516 x y) (plus x@30515 y x)) false))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@30521 Nat) (x@30520 Nat) (x@30519 Nat) (x@30518 Nat) (x@30517 Nat)) (=> (and (diseqNat@463 x@30521 x@30519) (times x@30521 x x@30520) (plus x@30520 y z) (plus x@30519 x@30518 x@30517) (times x@30518 x y) (times x@30517 x z)) false))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@30526 Nat) (x@30525 Nat) (x@30524 Nat) (x@30523 Nat) (x@30522 Nat)) (=> (and (diseqNat@463 x@30526 x@30524) (times x@30526 x@30525 z) (plus x@30525 x y) (plus x@30524 x@30523 x@30522) (times x@30523 x z) (times x@30522 y z)) false))))
(assert (forall ((x Nat)) (forall ((x@30527 Nat)) (=> (and (diseqNat@463 x@30527 x) (times x@30527 x (succ zero))) false))))
(assert (forall ((x Nat)) (forall ((x@30528 Nat)) (=> (and (diseqNat@463 x@30528 x) (times x@30528 (succ zero) x)) false))))
(assert (forall ((x Nat)) (forall ((x@30529 Nat)) (=> (and (diseqNat@463 x@30529 x) (plus x@30529 x zero)) false))))
(assert (forall ((x Nat)) (forall ((x@30530 Nat)) (=> (and (diseqNat@463 x@30530 x) (plus x@30530 zero x)) false))))
(assert (forall ((x Nat)) (forall ((x@30531 Nat)) (=> (and (diseqNat@463 x@30531 zero) (times x@30531 x zero)) false))))
(assert (forall ((x Nat)) (forall ((x@30532 Nat)) (=> (and (diseqNat@463 x@30532 zero) (times x@30532 zero x)) false))))
(check-sat)
(get-info :reason-unknown)
