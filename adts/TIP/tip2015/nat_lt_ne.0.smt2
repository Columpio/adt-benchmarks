(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@658 (Nat@0 Nat@0) Bool)
(assert (forall ((x@30802 Nat@0)) (diseqNat@0@658 Z@0 (S@0 x@30802))))
(assert (forall ((x@30803 Nat@0)) (diseqNat@0@658 (S@0 x@30803) Z@0)))
(assert (forall ((x@30804 Nat@0) (x@30805 Nat@0)) (=> (diseqNat@0@658 x@30804 x@30805) (diseqNat@0@658 (S@0 x@30804) (S@0 x@30805)))))
(declare-datatype Nat ((zero ) (succ (p Nat))))
(declare-fun diseqNat@473 (Nat Nat) Bool)
(assert (forall ((x@30806 Nat)) (diseqNat@473 zero (succ x@30806))))
(assert (forall ((x@30807 Nat)) (diseqNat@473 (succ x@30807) zero)))
(assert (forall ((x@30808 Nat) (x@30809 Nat)) (=> (diseqNat@473 x@30808 x@30809) (diseqNat@473 (succ x@30808) (succ x@30809)))))
(declare-fun lt (Bool Nat Nat) Bool)
(assert (forall ((x@30799 Bool) (x Nat) (y Nat)) (=> (and (= x@30799 false) (= y zero)) (lt x@30799 x y))))
(assert (forall ((x@30799 Bool) (x Nat) (y Nat) (z Nat)) (=> (and (= x@30799 true) (= y (succ z)) (= x zero)) (lt x@30799 x y))))
(assert (forall ((x@30799 Bool) (x Nat) (y Nat) (z Nat) (n Nat) (x@30800 Bool)) (=> (and (= x@30799 x@30800) (= y (succ z)) (= x (succ n)) (lt x@30800 n z)) (lt x@30799 x y))))
(assert (forall ((x Nat) (y Nat)) (forall ((x@30801 Bool)) (=> (and (= x y) x@30801 (lt x@30801 y x)) false))))
(check-sat)
(get-info :reason-unknown)
