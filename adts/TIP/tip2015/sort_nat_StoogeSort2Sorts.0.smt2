(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@545 (Nat@0 Nat@0) Bool)
(assert (forall ((x@25919 Nat@0)) (diseqNat@0@545 Z@0 (S@0 x@25919))))
(assert (forall ((x@25920 Nat@0)) (diseqNat@0@545 (S@0 x@25920) Z@0)))
(assert (forall ((x@25921 Nat@0) (x@25922 Nat@0)) (=> (diseqNat@0@545 x@25921 x@25922) (diseqNat@0@545 (S@0 x@25921) (S@0 x@25922)))))
(declare-datatype Nat ((zero ) (succ (p Nat))))
(declare-fun diseqNat@378 (Nat Nat) Bool)
(assert (forall ((x@25923 Nat)) (diseqNat@378 zero (succ x@25923))))
(assert (forall ((x@25924 Nat)) (diseqNat@378 (succ x@25924) zero)))
(assert (forall ((x@25925 Nat) (x@25926 Nat)) (=> (diseqNat@378 x@25925 x@25926) (diseqNat@378 (succ x@25925) (succ x@25926)))))
(declare-datatype list ((nil ) (cons (head Nat) (tail list))))
(declare-fun diseqlist@360 (list list) Bool)
(assert (forall ((x@25927 Nat) (x@25928 list)) (diseqlist@360 nil (cons x@25927 x@25928))))
(assert (forall ((x@25929 Nat) (x@25930 list)) (diseqlist@360 (cons x@25929 x@25930) nil)))
(assert (forall ((x@25931 Nat) (x@25932 list) (x@25933 Nat) (x@25934 list)) (=> (diseqNat@378 x@25931 x@25933) (diseqlist@360 (cons x@25931 x@25932) (cons x@25933 x@25934)))))
(assert (forall ((x@25931 Nat) (x@25932 list) (x@25933 Nat) (x@25934 list)) (=> (diseqlist@360 x@25932 x@25934) (diseqlist@360 (cons x@25931 x@25932) (cons x@25933 x@25934)))))
(declare-datatype pair ((pair2 (proj1-pair list) (proj2-pair list))))
(declare-fun diseqpair@45 (pair pair) Bool)
(assert (forall ((x@25935 list) (x@25936 list) (x@25937 list) (x@25938 list)) (=> (diseqlist@360 x@25935 x@25937) (diseqpair@45 (pair2 x@25935 x@25936) (pair2 x@25937 x@25938)))))
(assert (forall ((x@25935 list) (x@25936 list) (x@25937 list) (x@25938 list)) (=> (diseqlist@360 x@25936 x@25938) (diseqpair@45 (pair2 x@25935 x@25936) (pair2 x@25937 x@25938)))))
(declare-fun take (list Nat list) Bool)
(assert (forall ((x@25839 list) (x Nat) (y list)) (=> (and (= x@25839 nil) (= x zero)) (take x@25839 x y))))
(assert (forall ((x@25839 list) (x Nat) (y list) (z Nat)) (=> (and (= x@25839 nil) (= x (succ z)) (= y nil)) (take x@25839 x y))))
(assert (forall ((x@25839 list) (x Nat) (y list) (z Nat) (z2 Nat) (xs list) (x@25840 list)) (=> (and (= x@25839 (cons z2 x@25840)) (= x (succ z)) (= y (cons z2 xs)) (take x@25840 z xs)) (take x@25839 x y))))
(declare-fun plus (Nat Nat Nat) Bool)
(assert (forall ((x@25841 Nat) (x Nat) (y Nat)) (=> (and (= x@25841 y) (= x zero)) (plus x@25841 x y))))
(assert (forall ((x@25841 Nat) (x Nat) (y Nat) (z Nat) (x@25842 Nat)) (=> (and (= x@25841 (succ x@25842)) (= x (succ z)) (plus x@25842 z y)) (plus x@25841 x y))))
(declare-fun times (Nat Nat Nat) Bool)
(assert (forall ((x@25843 Nat) (x Nat) (y Nat)) (=> (and (= x@25843 zero) (= x zero)) (times x@25843 x y))))
(assert (forall ((x@25843 Nat) (x Nat) (y Nat) (z Nat) (x@25845 Nat) (x@25844 Nat)) (=> (and (= x@25843 x@25845) (= x (succ z)) (plus x@25845 y x@25844) (times x@25844 z y)) (times x@25843 x y))))
(declare-fun minus (Nat Nat Nat) Bool)
(assert (forall ((x@25846 Nat) (x Nat) (y Nat)) (=> (and (= x@25846 zero) (= x zero)) (minus x@25846 x y))))
(assert (forall ((x@25846 Nat) (x Nat) (y Nat) (z Nat)) (=> (and (= x@25846 zero) (= x (succ z)) (= y zero)) (minus x@25846 x y))))
(assert (forall ((x@25846 Nat) (x Nat) (y Nat) (z Nat) (y2 Nat) (x@25847 Nat)) (=> (and (= x@25846 x@25847) (= x (succ z)) (= y (succ y2)) (minus x@25847 z y2)) (minus x@25846 x y))))
(declare-fun lt (Bool Nat Nat) Bool)
(assert (forall ((x@25848 Bool) (x Nat) (y Nat)) (=> (and (= x@25848 false) (= y zero)) (lt x@25848 x y))))
(assert (forall ((x@25848 Bool) (x Nat) (y Nat) (z Nat)) (=> (and (= x@25848 true) (= y (succ z)) (= x zero)) (lt x@25848 x y))))
(assert (forall ((x@25848 Bool) (x Nat) (y Nat) (z Nat) (n Nat) (x@25849 Bool)) (=> (and (= x@25848 x@25849) (= y (succ z)) (= x (succ n)) (lt x@25849 n z)) (lt x@25848 x y))))
(declare-fun leq (Bool Nat Nat) Bool)
(assert (forall ((x@25850 Bool) (x Nat) (y Nat)) (=> (and (= x@25850 true) (= x zero)) (leq x@25850 x y))))
(assert (forall ((x@25850 Bool) (x Nat) (y Nat) (z Nat)) (=> (and (= x@25850 false) (= x (succ z)) (= y zero)) (leq x@25850 x y))))
(assert (forall ((x@25850 Bool) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@25851 Bool)) (=> (and (= x@25850 x@25851) (= x (succ z)) (= y (succ x2)) (leq x@25851 z x2)) (leq x@25850 x y))))
(declare-fun ordered (Bool list) Bool)
(assert (forall ((x@25852 Bool) (x list)) (=> (and (= x@25852 true) (= x nil)) (ordered x@25852 x))))
(assert (forall ((x@25852 Bool) (x list) (y Nat) (z list)) (=> (and (= x@25852 true) (= x (cons y z)) (= z nil)) (ordered x@25852 x))))
(assert (forall ((x@25852 Bool) (x list) (y Nat) (z list) (y2 Nat) (xs list) (x@25854 Bool) (x@25853 Bool)) (=> (and (= x@25852 (and x@25854 x@25853)) (= x (cons y z)) (= z (cons y2 xs)) (leq x@25854 y y2) (ordered x@25853 (cons y2 xs))) (ordered x@25852 x))))
(declare-fun sort2 (list Nat Nat) Bool)
(assert (forall ((x@25855 list) (x Nat) (y Nat) (x@25856 Bool)) (=> (and (= x@25855 (ite x@25856 (cons x (cons y nil)) (cons y (cons x nil)))) (leq x@25856 x y)) (sort2 x@25855 x y))))
(declare-fun length (Nat list) Bool)
(assert (forall ((x@25857 Nat) (x list)) (=> (and (= x@25857 zero) (= x nil)) (length x@25857 x))))
(assert (forall ((x@25857 Nat) (x list) (y Nat) (l list) (x@25859 Nat) (x@25858 Nat)) (=> (and (= x@25857 x@25859) (= x (cons y l)) (plus x@25859 (succ zero) x@25858) (length x@25858 l)) (length x@25857 x))))
(declare-fun idiv (Nat Nat Nat) Bool)
(assert (forall ((x@25860 Nat) (x Nat) (y Nat) (x@25861 Bool) (x@25863 Nat) (x@25862 Nat)) (=> (and (= x@25860 (ite x@25861 zero (succ x@25863))) (lt x@25861 x y) (idiv x@25863 x@25862 y) (minus x@25862 x y)) (idiv x@25860 x y))))
(declare-fun drop (list Nat list) Bool)
(assert (forall ((x@25864 list) (x Nat) (y list)) (=> (and (= x@25864 y) (= x zero)) (drop x@25864 x y))))
(assert (forall ((x@25864 list) (x Nat) (y list) (z Nat)) (=> (and (= x@25864 nil) (= x (succ z)) (= y nil)) (drop x@25864 x y))))
(assert (forall ((x@25864 list) (x Nat) (y list) (z Nat) (z2 Nat) (xs1 list) (x@25865 list)) (=> (and (= x@25864 x@25865) (= x (succ z)) (= y (cons z2 xs1)) (drop x@25865 z xs1)) (drop x@25864 x y))))
(declare-fun splitAt (pair Nat list) Bool)
(assert (forall ((x@25866 pair) (x Nat) (y list) (x@25868 list) (x@25867 list)) (=> (and (= x@25866 (pair2 x@25868 x@25867)) (take x@25868 x y) (drop x@25867 x y)) (splitAt x@25866 x y))))
(declare-fun ++ (list list list) Bool)
(assert (forall ((x@25869 list) (x list) (y list)) (=> (and (= x@25869 y) (= x nil)) (++ x@25869 x y))))
(assert (forall ((x@25869 list) (x list) (y list) (z Nat) (xs list) (x@25870 list)) (=> (and (= x@25869 (cons z x@25870)) (= x (cons z xs)) (++ x@25870 xs y)) (++ x@25869 x y))))
(declare-fun stooge2sort2 (list list) Bool)
(declare-fun stoogesort2 (list list) Bool)
(declare-fun stooge2sort1 (list list) Bool)
(assert (forall ((x@25871 list) (x list) (x@25875 pair) (x@25874 Nat) (x@25873 Nat) (x@25872 Nat) (ys1 list) (zs list) (x@25877 list) (x@25876 list)) (=> (and (= x@25871 x@25877) (= x@25875 (pair2 ys1 zs)) (splitAt x@25875 x@25874 x) (idiv x@25874 (succ x@25873) (succ (succ (succ zero)))) (times x@25873 (succ (succ zero)) x@25872) (length x@25872 x) (++ x@25877 x@25876 zs) (stoogesort2 x@25876 ys1)) (stooge2sort2 x@25871 x))))
(assert (forall ((x@25878 list) (x list)) (=> (and (= x@25878 nil) (= x nil)) (stoogesort2 x@25878 x))))
(assert (forall ((x@25878 list) (x list) (y Nat) (z list)) (=> (and (= x@25878 (cons y nil)) (= x (cons y z)) (= z nil)) (stoogesort2 x@25878 x))))
(assert (forall ((x@25878 list) (x list) (y Nat) (z list) (y2 Nat) (x2 list) (x@25879 list)) (=> (and (= x@25878 x@25879) (= x (cons y z)) (= z (cons y2 x2)) (= x2 nil) (sort2 x@25879 y y2)) (stoogesort2 x@25878 x))))
(assert (forall ((x@25878 list) (x list) (y Nat) (z list) (y2 Nat) (x2 list) (x3 Nat) (x4 list) (x@25882 list) (x@25881 list) (x@25880 list)) (=> (and (= x@25878 x@25882) (= x (cons y z)) (= z (cons y2 x2)) (= x2 (cons x3 x4)) (stooge2sort2 x@25882 x@25881) (stooge2sort1 x@25881 x@25880) (stooge2sort2 x@25880 (cons y (cons y2 (cons x3 x4))))) (stoogesort2 x@25878 x))))
(assert (forall ((x@25883 list) (x list) (x@25886 pair) (x@25885 Nat) (x@25884 Nat) (ys1 list) (zs list) (x@25888 list) (x@25887 list)) (=> (and (= x@25883 x@25888) (= x@25886 (pair2 ys1 zs)) (splitAt x@25886 x@25885 x) (idiv x@25885 x@25884 (succ (succ (succ zero)))) (length x@25884 x) (++ x@25888 ys1 x@25887) (stoogesort2 x@25887 zs)) (stooge2sort1 x@25883 x))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@25892 Nat) (x@25891 Nat) (x@25890 Nat) (x@25889 Nat)) (=> (and (diseqNat@378 x@25892 x@25890) (times x@25892 x x@25891) (times x@25891 y z) (times x@25890 x@25889 z) (times x@25889 x y)) false))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@25896 Nat) (x@25895 Nat) (x@25894 Nat) (x@25893 Nat)) (=> (and (diseqNat@378 x@25896 x@25894) (plus x@25896 x x@25895) (plus x@25895 y z) (plus x@25894 x@25893 z) (plus x@25893 x y)) false))))
(assert (forall ((x Nat) (y Nat)) (forall ((x@25898 Nat) (x@25897 Nat)) (=> (and (diseqNat@378 x@25898 x@25897) (times x@25898 x y) (times x@25897 y x)) false))))
(assert (forall ((x Nat) (y Nat)) (forall ((x@25900 Nat) (x@25899 Nat)) (=> (and (diseqNat@378 x@25900 x@25899) (plus x@25900 x y) (plus x@25899 y x)) false))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@25905 Nat) (x@25904 Nat) (x@25903 Nat) (x@25902 Nat) (x@25901 Nat)) (=> (and (diseqNat@378 x@25905 x@25903) (times x@25905 x x@25904) (plus x@25904 y z) (plus x@25903 x@25902 x@25901) (times x@25902 x y) (times x@25901 x z)) false))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@25910 Nat) (x@25909 Nat) (x@25908 Nat) (x@25907 Nat) (x@25906 Nat)) (=> (and (diseqNat@378 x@25910 x@25908) (times x@25910 x@25909 z) (plus x@25909 x y) (plus x@25908 x@25907 x@25906) (times x@25907 x z) (times x@25906 y z)) false))))
(assert (forall ((x Nat)) (forall ((x@25911 Nat)) (=> (and (diseqNat@378 x@25911 x) (times x@25911 x (succ zero))) false))))
(assert (forall ((x Nat)) (forall ((x@25912 Nat)) (=> (and (diseqNat@378 x@25912 x) (times x@25912 (succ zero) x)) false))))
(assert (forall ((x Nat)) (forall ((x@25913 Nat)) (=> (and (diseqNat@378 x@25913 x) (plus x@25913 x zero)) false))))
(assert (forall ((x Nat)) (forall ((x@25914 Nat)) (=> (and (diseqNat@378 x@25914 x) (plus x@25914 zero x)) false))))
(assert (forall ((x Nat)) (forall ((x@25915 Nat)) (=> (and (diseqNat@378 x@25915 zero) (times x@25915 x zero)) false))))
(assert (forall ((x Nat)) (forall ((x@25916 Nat)) (=> (and (diseqNat@378 x@25916 zero) (times x@25916 zero x)) false))))
(assert (forall ((xs list)) (forall ((x@25918 Bool) (x@25917 list)) (=> (and (not x@25918) (ordered x@25918 x@25917) (stoogesort2 x@25917 xs)) false))))
(check-sat)
(get-info :reason-unknown)
