(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@593 (Nat@0 Nat@0) Bool)
(assert (forall ((x@27824 Nat@0)) (diseqNat@0@593 Z@0 (S@0 x@27824))))
(assert (forall ((x@27825 Nat@0)) (diseqNat@0@593 (S@0 x@27825) Z@0)))
(assert (forall ((x@27826 Nat@0) (x@27827 Nat@0)) (=> (diseqNat@0@593 x@27826 x@27827) (diseqNat@0@593 (S@0 x@27826) (S@0 x@27827)))))
(declare-datatype Sign ((Pos ) (Neg )))
(declare-fun diseqSign@10 (Sign Sign) Bool)
(assert (diseqSign@10 Pos Neg))
(assert (diseqSign@10 Neg Pos))
(declare-datatype Nat ((zero ) (succ (p Nat))))
(declare-fun diseqNat@419 (Nat Nat) Bool)
(assert (forall ((x@27828 Nat)) (diseqNat@419 zero (succ x@27828))))
(assert (forall ((x@27829 Nat)) (diseqNat@419 (succ x@27829) zero)))
(assert (forall ((x@27830 Nat) (x@27831 Nat)) (=> (diseqNat@419 x@27830 x@27831) (diseqNat@419 (succ x@27830) (succ x@27831)))))
(declare-datatype Integer ((P (proj1-P Nat)) (N (proj1-N Nat))))
(declare-fun diseqInteger@19 (Integer Integer) Bool)
(assert (forall ((x@27832 Nat) (x@27833 Nat)) (diseqInteger@19 (P x@27832) (N x@27833))))
(assert (forall ((x@27834 Nat) (x@27835 Nat)) (diseqInteger@19 (N x@27834) (P x@27835))))
(assert (forall ((x@27836 Nat) (x@27837 Nat)) (=> (diseqNat@419 x@27836 x@27837) (diseqInteger@19 (P x@27836) (P x@27837)))))
(assert (forall ((x@27838 Nat) (x@27839 Nat)) (=> (diseqNat@419 x@27838 x@27839) (diseqInteger@19 (N x@27838) (N x@27839)))))
(declare-fun toInteger (Integer Sign Nat) Bool)
(assert (forall ((x@27773 Integer) (x Sign) (y Nat)) (=> (and (= x@27773 (P y)) (= x Pos)) (toInteger x@27773 x y))))
(assert (forall ((x@27773 Integer) (x Sign) (y Nat)) (=> (and (= x@27773 (P zero)) (= x Neg) (= y zero)) (toInteger x@27773 x y))))
(assert (forall ((x@27773 Integer) (x Sign) (y Nat) (z Nat)) (=> (and (= x@27773 (N z)) (= x Neg) (= y (succ z))) (toInteger x@27773 x y))))
(declare-fun sign (Sign Integer) Bool)
(assert (forall ((x@27774 Sign) (x Integer) (y Nat)) (=> (and (= x@27774 Pos) (= x (P y))) (sign x@27774 x))))
(assert (forall ((x@27774 Sign) (x Integer) (z Nat)) (=> (and (= x@27774 Neg) (= x (N z))) (sign x@27774 x))))
(declare-fun plus (Nat Nat Nat) Bool)
(assert (forall ((x@27775 Nat) (x Nat) (y Nat)) (=> (and (= x@27775 y) (= x zero)) (plus x@27775 x y))))
(assert (forall ((x@27775 Nat) (x Nat) (y Nat) (z Nat) (x@27776 Nat)) (=> (and (= x@27775 (succ x@27776)) (= x (succ z)) (plus x@27776 z y)) (plus x@27775 x y))))
(declare-fun times2 (Nat Nat Nat) Bool)
(assert (forall ((x@27777 Nat) (x Nat) (y Nat)) (=> (and (= x@27777 zero) (= x zero)) (times2 x@27777 x y))))
(assert (forall ((x@27777 Nat) (x Nat) (y Nat) (z Nat) (x@27779 Nat) (x@27778 Nat)) (=> (and (= x@27777 x@27779) (= x (succ z)) (plus x@27779 y x@27778) (times2 x@27778 z y)) (times2 x@27777 x y))))
(declare-fun opposite (Sign Sign) Bool)
(assert (forall ((x@27780 Sign) (x Sign)) (=> (and (= x@27780 Neg) (= x Pos)) (opposite x@27780 x))))
(assert (forall ((x@27780 Sign) (x Sign)) (=> (and (= x@27780 Pos) (= x Neg)) (opposite x@27780 x))))
(declare-fun timesSign (Sign Sign Sign) Bool)
(assert (forall ((x@27781 Sign) (x Sign) (y Sign)) (=> (and (= x@27781 y) (= x Pos)) (timesSign x@27781 x y))))
(assert (forall ((x@27781 Sign) (x Sign) (y Sign) (x@27782 Sign)) (=> (and (= x@27781 x@27782) (= x Neg) (opposite x@27782 y)) (timesSign x@27781 x y))))
(declare-fun one (Integer) Bool)
(assert (forall ((x@27783 Integer)) (=> (= x@27783 (P (succ zero))) (one x@27783))))
(declare-fun absVal (Nat Integer) Bool)
(assert (forall ((x@27784 Nat) (x Integer) (n Nat)) (=> (and (= x@27784 n) (= x (P n))) (absVal x@27784 x))))
(assert (forall ((x@27784 Nat) (x Integer) (m Nat) (x@27785 Nat)) (=> (and (= x@27784 x@27785) (= x (N m)) (plus x@27785 (succ zero) m)) (absVal x@27784 x))))
(declare-fun times (Integer Integer Integer) Bool)
(assert (forall ((x@27786 Integer) (x Integer) (y Integer) (x@27793 Integer) (x@27792 Sign) (x@27791 Sign) (x@27790 Sign) (x@27789 Nat) (x@27788 Nat) (x@27787 Nat)) (=> (and (= x@27786 x@27793) (toInteger x@27793 x@27792 x@27789) (timesSign x@27792 x@27791 x@27790) (sign x@27791 x) (sign x@27790 y) (times2 x@27789 x@27788 x@27787) (absVal x@27788 x) (absVal x@27787 y)) (times x@27786 x y))))
(assert (forall ((x Integer)) (forall ((x@27795 Integer) (x@27794 Integer)) (=> (and (diseqInteger@19 x x@27795) (times x@27795 x@27794 x) (one x@27794)) false))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@27799 Nat) (x@27798 Nat) (x@27797 Nat) (x@27796 Nat)) (=> (and (diseqNat@419 x@27799 x@27797) (times2 x@27799 x x@27798) (times2 x@27798 y z) (times2 x@27797 x@27796 z) (times2 x@27796 x y)) false))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@27803 Nat) (x@27802 Nat) (x@27801 Nat) (x@27800 Nat)) (=> (and (diseqNat@419 x@27803 x@27801) (plus x@27803 x x@27802) (plus x@27802 y z) (plus x@27801 x@27800 z) (plus x@27800 x y)) false))))
(assert (forall ((x Nat) (y Nat)) (forall ((x@27805 Nat) (x@27804 Nat)) (=> (and (diseqNat@419 x@27805 x@27804) (times2 x@27805 x y) (times2 x@27804 y x)) false))))
(assert (forall ((x Nat) (y Nat)) (forall ((x@27807 Nat) (x@27806 Nat)) (=> (and (diseqNat@419 x@27807 x@27806) (plus x@27807 x y) (plus x@27806 y x)) false))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@27812 Nat) (x@27811 Nat) (x@27810 Nat) (x@27809 Nat) (x@27808 Nat)) (=> (and (diseqNat@419 x@27812 x@27810) (times2 x@27812 x x@27811) (plus x@27811 y z) (plus x@27810 x@27809 x@27808) (times2 x@27809 x y) (times2 x@27808 x z)) false))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@27817 Nat) (x@27816 Nat) (x@27815 Nat) (x@27814 Nat) (x@27813 Nat)) (=> (and (diseqNat@419 x@27817 x@27815) (times2 x@27817 x@27816 z) (plus x@27816 x y) (plus x@27815 x@27814 x@27813) (times2 x@27814 x z) (times2 x@27813 y z)) false))))
(assert (forall ((x Nat)) (forall ((x@27818 Nat)) (=> (and (diseqNat@419 x@27818 x) (times2 x@27818 x (succ zero))) false))))
(assert (forall ((x Nat)) (forall ((x@27819 Nat)) (=> (and (diseqNat@419 x@27819 x) (times2 x@27819 (succ zero) x)) false))))
(assert (forall ((x Nat)) (forall ((x@27820 Nat)) (=> (and (diseqNat@419 x@27820 x) (plus x@27820 x zero)) false))))
(assert (forall ((x Nat)) (forall ((x@27821 Nat)) (=> (and (diseqNat@419 x@27821 x) (plus x@27821 zero x)) false))))
(assert (forall ((x Nat)) (forall ((x@27822 Nat)) (=> (and (diseqNat@419 x@27822 zero) (times2 x@27822 x zero)) false))))
(assert (forall ((x Nat)) (forall ((x@27823 Nat)) (=> (and (diseqNat@419 x@27823 zero) (times2 x@27823 zero x)) false))))
(check-sat)
(get-info :reason-unknown)
