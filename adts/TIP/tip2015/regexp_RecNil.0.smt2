(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@630 (Nat@0 Nat@0) Bool)
(assert (forall ((x@29730 Nat@0)) (diseqNat@0@630 Z@0 (S@0 x@29730))))
(assert (forall ((x@29731 Nat@0)) (diseqNat@0@630 (S@0 x@29731) Z@0)))
(assert (forall ((x@29732 Nat@0) (x@29733 Nat@0)) (=> (diseqNat@0@630 x@29732 x@29733) (diseqNat@0@630 (S@0 x@29732) (S@0 x@29733)))))
(declare-datatype A ((X ) (Y )))
(declare-fun diseqA@28 (A A) Bool)
(assert (diseqA@28 X Y))
(assert (diseqA@28 Y X))
(declare-datatype R ((Nil ) (Eps ) (Atom (proj1-Atom A)) (Plus (proj1-Plus R) (proj2-Plus R)) (Seq (proj1-Seq R) (proj2-Seq R)) (Star (proj1-Star R))))
(declare-fun diseqR@96 (R R) Bool)
(assert (diseqR@96 Nil Eps))
(assert (forall ((x@29734 A)) (diseqR@96 Nil (Atom x@29734))))
(assert (forall ((x@29735 R) (x@29736 R)) (diseqR@96 Nil (Plus x@29735 x@29736))))
(assert (forall ((x@29737 R) (x@29738 R)) (diseqR@96 Nil (Seq x@29737 x@29738))))
(assert (forall ((x@29739 R)) (diseqR@96 Nil (Star x@29739))))
(assert (diseqR@96 Eps Nil))
(assert (forall ((x@29740 A)) (diseqR@96 (Atom x@29740) Nil)))
(assert (forall ((x@29741 R) (x@29742 R)) (diseqR@96 (Plus x@29741 x@29742) Nil)))
(assert (forall ((x@29743 R) (x@29744 R)) (diseqR@96 (Seq x@29743 x@29744) Nil)))
(assert (forall ((x@29745 R)) (diseqR@96 (Star x@29745) Nil)))
(assert (forall ((x@29746 A)) (diseqR@96 Eps (Atom x@29746))))
(assert (forall ((x@29747 R) (x@29748 R)) (diseqR@96 Eps (Plus x@29747 x@29748))))
(assert (forall ((x@29749 R) (x@29750 R)) (diseqR@96 Eps (Seq x@29749 x@29750))))
(assert (forall ((x@29751 R)) (diseqR@96 Eps (Star x@29751))))
(assert (forall ((x@29752 A)) (diseqR@96 (Atom x@29752) Eps)))
(assert (forall ((x@29753 R) (x@29754 R)) (diseqR@96 (Plus x@29753 x@29754) Eps)))
(assert (forall ((x@29755 R) (x@29756 R)) (diseqR@96 (Seq x@29755 x@29756) Eps)))
(assert (forall ((x@29757 R)) (diseqR@96 (Star x@29757) Eps)))
(assert (forall ((x@29758 A) (x@29759 R) (x@29760 R)) (diseqR@96 (Atom x@29758) (Plus x@29759 x@29760))))
(assert (forall ((x@29761 A) (x@29762 R) (x@29763 R)) (diseqR@96 (Atom x@29761) (Seq x@29762 x@29763))))
(assert (forall ((x@29764 A) (x@29765 R)) (diseqR@96 (Atom x@29764) (Star x@29765))))
(assert (forall ((x@29766 R) (x@29767 R) (x@29768 A)) (diseqR@96 (Plus x@29766 x@29767) (Atom x@29768))))
(assert (forall ((x@29769 R) (x@29770 R) (x@29771 A)) (diseqR@96 (Seq x@29769 x@29770) (Atom x@29771))))
(assert (forall ((x@29772 R) (x@29773 A)) (diseqR@96 (Star x@29772) (Atom x@29773))))
(assert (forall ((x@29774 R) (x@29775 R) (x@29776 R) (x@29777 R)) (diseqR@96 (Plus x@29774 x@29775) (Seq x@29776 x@29777))))
(assert (forall ((x@29778 R) (x@29779 R) (x@29780 R)) (diseqR@96 (Plus x@29778 x@29779) (Star x@29780))))
(assert (forall ((x@29781 R) (x@29782 R) (x@29783 R) (x@29784 R)) (diseqR@96 (Seq x@29781 x@29782) (Plus x@29783 x@29784))))
(assert (forall ((x@29785 R) (x@29786 R) (x@29787 R)) (diseqR@96 (Star x@29785) (Plus x@29786 x@29787))))
(assert (forall ((x@29788 R) (x@29789 R) (x@29790 R)) (diseqR@96 (Seq x@29788 x@29789) (Star x@29790))))
(assert (forall ((x@29791 R) (x@29792 R) (x@29793 R)) (diseqR@96 (Star x@29791) (Seq x@29792 x@29793))))
(assert (forall ((x@29794 A) (x@29795 A)) (=> (diseqA@28 x@29794 x@29795) (diseqR@96 (Atom x@29794) (Atom x@29795)))))
(assert (forall ((x@29796 R) (x@29797 R) (x@29798 R) (x@29799 R)) (=> (diseqR@96 x@29796 x@29798) (diseqR@96 (Plus x@29796 x@29797) (Plus x@29798 x@29799)))))
(assert (forall ((x@29796 R) (x@29797 R) (x@29798 R) (x@29799 R)) (=> (diseqR@96 x@29797 x@29799) (diseqR@96 (Plus x@29796 x@29797) (Plus x@29798 x@29799)))))
(assert (forall ((x@29800 R) (x@29801 R) (x@29802 R) (x@29803 R)) (=> (diseqR@96 x@29800 x@29802) (diseqR@96 (Seq x@29800 x@29801) (Seq x@29802 x@29803)))))
(assert (forall ((x@29800 R) (x@29801 R) (x@29802 R) (x@29803 R)) (=> (diseqR@96 x@29801 x@29803) (diseqR@96 (Seq x@29800 x@29801) (Seq x@29802 x@29803)))))
(assert (forall ((x@29804 R) (x@29805 R)) (=> (diseqR@96 x@29804 x@29805) (diseqR@96 (Star x@29804) (Star x@29805)))))
(declare-datatype list ((nil ) (cons (head A) (tail list))))
(declare-fun diseqlist@395 (list list) Bool)
(assert (forall ((x@29806 A) (x@29807 list)) (diseqlist@395 nil (cons x@29806 x@29807))))
(assert (forall ((x@29808 A) (x@29809 list)) (diseqlist@395 (cons x@29808 x@29809) nil)))
(assert (forall ((x@29810 A) (x@29811 list) (x@29812 A) (x@29813 list)) (=> (diseqA@28 x@29810 x@29812) (diseqlist@395 (cons x@29810 x@29811) (cons x@29812 x@29813)))))
(assert (forall ((x@29810 A) (x@29811 list) (x@29812 A) (x@29813 list)) (=> (diseqlist@395 x@29811 x@29813) (diseqlist@395 (cons x@29810 x@29811) (cons x@29812 x@29813)))))
(declare-fun seq (R R R) Bool)
(assert (forall ((x@29702 R) (x R) (y R) (x@29694 R) (x@29695 R) (x@29696 R) (x@29697 R)) (=> (and (= x@29702 (Seq x y)) (= x x@29694) (= y x@29695) (= x x@29696) (= y x@29697)) (seq x@29702 x y))))
(assert (forall ((x@29702 R) (x R) (y R) (x@29694 R) (x@29695 R) (x@29696 R)) (=> (and (= x@29702 x) (= x x@29694) (= y x@29695) (= x x@29696) (= y Eps)) (seq x@29702 x y))))
(assert (forall ((x@29702 R) (x R) (y R) (x@29694 R) (x@29695 R)) (=> (and (= x@29702 y) (= x x@29694) (= y x@29695) (= x Eps)) (seq x@29702 x y))))
(assert (forall ((x@29702 R) (x R) (y R) (x@29694 R)) (=> (and (= x@29702 Nil) (= x x@29694) (= y Nil)) (seq x@29702 x y))))
(assert (forall ((x@29702 R) (x R) (y R)) (=> (and (= x@29702 Nil) (= x Nil)) (seq x@29702 x y))))
(declare-fun plus (R R R) Bool)
(assert (forall ((x@29703 R) (x R) (y R) (x@29698 R) (x@29699 R)) (=> (and (= x@29703 (Plus x y)) (= x x@29698) (= y x@29699)) (plus x@29703 x y))))
(assert (forall ((x@29703 R) (x R) (y R) (x@29698 R)) (=> (and (= x@29703 x) (= x x@29698) (= y Nil)) (plus x@29703 x y))))
(assert (forall ((x@29703 R) (x R) (y R)) (=> (and (= x@29703 y) (= x Nil)) (plus x@29703 x y))))
(declare-fun eqA (Bool A A) Bool)
(assert (forall ((x@29704 Bool) (x A) (y A)) (=> (and (= x@29704 true) (= x X) (= y X)) (eqA x@29704 x y))))
(assert (forall ((x@29704 Bool) (x A) (y A)) (=> (and (= x@29704 false) (= x X) (= y Y)) (eqA x@29704 x y))))
(assert (forall ((x@29704 Bool) (x A) (y A)) (=> (and (= x@29704 false) (= x Y) (= y X)) (eqA x@29704 x y))))
(assert (forall ((x@29704 Bool) (x A) (y A)) (=> (and (= x@29704 true) (= x Y) (= y Y)) (eqA x@29704 x y))))
(declare-fun eps (Bool R) Bool)
(assert (forall ((x@29705 Bool) (x R) (x@29700 R)) (=> (and (= x@29705 false) (= x x@29700)) (eps x@29705 x))))
(assert (forall ((x@29705 Bool) (x R)) (=> (and (= x@29705 true) (= x Eps)) (eps x@29705 x))))
(assert (forall ((x@29705 Bool) (x R) (p R) (q R) (x@29707 Bool) (x@29706 Bool)) (=> (and (= x@29705 (or x@29707 x@29706)) (= x (Plus p q)) (eps x@29707 p) (eps x@29706 q)) (eps x@29705 x))))
(assert (forall ((x@29705 Bool) (x R) (r R) (q2 R) (x@29709 Bool) (x@29708 Bool)) (=> (and (= x@29705 (and x@29709 x@29708)) (= x (Seq r q2)) (eps x@29709 r) (eps x@29708 q2)) (eps x@29705 x))))
(assert (forall ((x@29705 Bool) (x R) (y R)) (=> (and (= x@29705 true) (= x (Star y))) (eps x@29705 x))))
(declare-fun epsR (R R) Bool)
(assert (forall ((x@29710 R) (x R) (x@29711 Bool)) (=> (and (= x@29710 (ite x@29711 Eps Nil)) (eps x@29711 x)) (epsR x@29710 x))))
(declare-fun step (R R A) Bool)
(assert (forall ((x@29712 R) (x R) (y A) (x@29701 R)) (=> (and (= x@29712 Nil) (= x x@29701)) (step x@29712 x y))))
(assert (forall ((x@29712 R) (x R) (y A) (a A) (x@29713 Bool)) (=> (and (= x@29712 (ite x@29713 Eps Nil)) (= x (Atom a)) (eqA x@29713 a y)) (step x@29712 x y))))
(assert (forall ((x@29712 R) (x R) (y A) (p R) (q R) (x@29716 R) (x@29715 R) (x@29714 R)) (=> (and (= x@29712 x@29716) (= x (Plus p q)) (plus x@29716 x@29715 x@29714) (step x@29715 p y) (step x@29714 q y)) (step x@29712 x y))))
(assert (forall ((x@29712 R) (x R) (y A) (r R) (q2 R) (x@29722 R) (x@29721 R) (x@29720 R) (x@29719 R) (x@29718 R) (x@29717 R)) (=> (and (= x@29712 x@29722) (= x (Seq r q2)) (plus x@29722 x@29721 x@29719) (seq x@29721 x@29720 q2) (step x@29720 r y) (seq x@29719 x@29718 x@29717) (epsR x@29718 r) (step x@29717 q2 y)) (step x@29712 x y))))
(assert (forall ((x@29712 R) (x R) (y A) (p2 R) (x@29724 R) (x@29723 R)) (=> (and (= x@29712 x@29724) (= x (Star p2)) (seq x@29724 x@29723 (Star p2)) (step x@29723 p2 y)) (step x@29712 x y))))
(declare-fun recognise (Bool R list) Bool)
(assert (forall ((x@29725 Bool) (x R) (y list) (x@29726 Bool)) (=> (and (= x@29725 x@29726) (= y nil) (eps x@29726 x)) (recognise x@29725 x y))))
(assert (forall ((x@29725 Bool) (x R) (y list) (z A) (xs list) (x@29728 Bool) (x@29727 R)) (=> (and (= x@29725 x@29728) (= y (cons z xs)) (recognise x@29728 x@29727 xs) (step x@29727 x z)) (recognise x@29725 x y))))
(assert (forall ((s list)) (forall ((x@29729 Bool)) (=> (and x@29729 (recognise x@29729 Nil s)) false))))
(check-sat)
(get-info :reason-unknown)
