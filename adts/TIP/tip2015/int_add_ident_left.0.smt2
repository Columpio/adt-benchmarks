(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@638 (Nat@0 Nat@0) Bool)
(assert (forall ((x@30193 Nat@0)) (diseqNat@0@638 Z@0 (S@0 x@30193))))
(assert (forall ((x@30194 Nat@0)) (diseqNat@0@638 (S@0 x@30194) Z@0)))
(assert (forall ((x@30195 Nat@0) (x@30196 Nat@0)) (=> (diseqNat@0@638 x@30195 x@30196) (diseqNat@0@638 (S@0 x@30195) (S@0 x@30196)))))
(declare-datatype Nat ((zero ) (succ (p Nat))))
(declare-fun diseqNat@453 (Nat Nat) Bool)
(assert (forall ((x@30197 Nat)) (diseqNat@453 zero (succ x@30197))))
(assert (forall ((x@30198 Nat)) (diseqNat@453 (succ x@30198) zero)))
(assert (forall ((x@30199 Nat) (x@30200 Nat)) (=> (diseqNat@453 x@30199 x@30200) (diseqNat@453 (succ x@30199) (succ x@30200)))))
(declare-datatype Integer ((P (proj1-P Nat)) (N (proj1-N Nat))))
(declare-fun diseqInteger@22 (Integer Integer) Bool)
(assert (forall ((x@30201 Nat) (x@30202 Nat)) (diseqInteger@22 (P x@30201) (N x@30202))))
(assert (forall ((x@30203 Nat) (x@30204 Nat)) (diseqInteger@22 (N x@30203) (P x@30204))))
(assert (forall ((x@30205 Nat) (x@30206 Nat)) (=> (diseqNat@453 x@30205 x@30206) (diseqInteger@22 (P x@30205) (P x@30206)))))
(assert (forall ((x@30207 Nat) (x@30208 Nat)) (=> (diseqNat@453 x@30207 x@30208) (diseqInteger@22 (N x@30207) (N x@30208)))))
(declare-fun zero2 (Integer) Bool)
(assert (forall ((x@30170 Integer)) (=> (= x@30170 (P zero)) (zero2 x@30170))))
(declare-fun plus2 (Nat Nat Nat) Bool)
(assert (forall ((x@30171 Nat) (x Nat) (y Nat)) (=> (and (= x@30171 y) (= x zero)) (plus2 x@30171 x y))))
(assert (forall ((x@30171 Nat) (x Nat) (y Nat) (z Nat) (x@30172 Nat)) (=> (and (= x@30171 (succ x@30172)) (= x (succ z)) (plus2 x@30172 z y)) (plus2 x@30171 x y))))
(declare-fun |-2| (Integer Nat Nat) Bool)
(assert (forall ((x@30173 Integer) (x Nat) (y Nat) (fail Integer)) (=> (and (= x@30173 (P zero)) (= fail (P x)) (= y zero) (= x zero) (= y zero)) (|-2| x@30173 x y))))
(assert (forall ((x@30173 Integer) (x Nat) (y Nat) (fail Integer) (x4 Nat)) (=> (and (= x@30173 fail) (= fail (P x)) (= y zero) (= x zero) (= y (succ x4))) (|-2| x@30173 x y))))
(assert (forall ((x@30173 Integer) (x Nat) (y Nat) (fail Integer) (x3 Nat)) (=> (and (= x@30173 fail) (= fail (P x)) (= y zero) (= x (succ x3))) (|-2| x@30173 x y))))
(assert (forall ((x@30173 Integer) (x Nat) (y Nat) (fail Integer) (z Nat)) (=> (and (= x@30173 (P zero)) (= fail (N (succ z))) (= y (succ z)) (= x zero) (= x zero) (= y zero)) (|-2| x@30173 x y))))
(assert (forall ((x@30173 Integer) (x Nat) (y Nat) (fail Integer) (z Nat) (x4 Nat)) (=> (and (= x@30173 fail) (= fail (N (succ z))) (= y (succ z)) (= x zero) (= x zero) (= y (succ x4))) (|-2| x@30173 x y))))
(assert (forall ((x@30173 Integer) (x Nat) (y Nat) (fail Integer) (z Nat) (x3 Nat)) (=> (and (= x@30173 fail) (= fail (N (succ z))) (= y (succ z)) (= x zero) (= x (succ x3))) (|-2| x@30173 x y))))
(assert (forall ((x@30173 Integer) (x Nat) (y Nat) (fail Integer) (z Nat) (x2 Nat) (x@30174 Integer)) (=> (and (= x@30173 (P zero)) (= fail x@30174) (= y (succ z)) (= x (succ x2)) (|-2| x@30174 x2 z) (= x zero) (= y zero)) (|-2| x@30173 x y))))
(assert (forall ((x@30173 Integer) (x Nat) (y Nat) (fail Integer) (z Nat) (x2 Nat) (x@30174 Integer) (x4 Nat)) (=> (and (= x@30173 fail) (= fail x@30174) (= y (succ z)) (= x (succ x2)) (|-2| x@30174 x2 z) (= x zero) (= y (succ x4))) (|-2| x@30173 x y))))
(assert (forall ((x@30173 Integer) (x Nat) (y Nat) (fail Integer) (z Nat) (x2 Nat) (x@30174 Integer) (x3 Nat)) (=> (and (= x@30173 fail) (= fail x@30174) (= y (succ z)) (= x (succ x2)) (|-2| x@30174 x2 z) (= x (succ x3))) (|-2| x@30173 x y))))
(declare-fun plus (Integer Integer Integer) Bool)
(assert (forall ((x@30175 Integer) (x Integer) (y Integer) (m Nat) (n Nat) (x@30176 Nat)) (=> (and (= x@30175 (P x@30176)) (= x (P m)) (= y (P n)) (plus2 x@30176 m n)) (plus x@30175 x y))))
(assert (forall ((x@30175 Integer) (x Integer) (y Integer) (m Nat) (o Nat) (x@30178 Integer) (x@30177 Nat)) (=> (and (= x@30175 x@30178) (= x (P m)) (= y (N o)) (|-2| x@30178 m x@30177) (plus2 x@30177 (succ zero) o)) (plus x@30175 x y))))
(assert (forall ((x@30175 Integer) (x Integer) (y Integer) (m2 Nat) (n2 Nat) (x@30180 Integer) (x@30179 Nat)) (=> (and (= x@30175 x@30180) (= x (N m2)) (= y (P n2)) (|-2| x@30180 n2 x@30179) (plus2 x@30179 (succ zero) m2)) (plus x@30175 x y))))
(assert (forall ((x@30175 Integer) (x Integer) (y Integer) (m2 Nat) (n3 Nat) (x@30182 Nat) (x@30181 Nat)) (=> (and (= x@30175 (N x@30182)) (= x (N m2)) (= y (N n3)) (plus2 x@30182 x@30181 n3) (plus2 x@30181 (succ zero) m2)) (plus x@30175 x y))))
(assert (forall ((x Integer)) (forall ((x@30184 Integer) (x@30183 Integer)) (=> (and (diseqInteger@22 x x@30184) (plus x@30184 x@30183 x) (zero2 x@30183)) false))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@30188 Nat) (x@30187 Nat) (x@30186 Nat) (x@30185 Nat)) (=> (and (diseqNat@453 x@30188 x@30186) (plus2 x@30188 x x@30187) (plus2 x@30187 y z) (plus2 x@30186 x@30185 z) (plus2 x@30185 x y)) false))))
(assert (forall ((x Nat) (y Nat)) (forall ((x@30190 Nat) (x@30189 Nat)) (=> (and (diseqNat@453 x@30190 x@30189) (plus2 x@30190 x y) (plus2 x@30189 y x)) false))))
(assert (forall ((x Nat)) (forall ((x@30191 Nat)) (=> (and (diseqNat@453 x@30191 x) (plus2 x@30191 x zero)) false))))
(assert (forall ((x Nat)) (forall ((x@30192 Nat)) (=> (and (diseqNat@453 x@30192 x) (plus2 x@30192 zero x)) false))))
(check-sat)
(get-info :reason-unknown)
