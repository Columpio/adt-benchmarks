(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@484 (Nat@0 Nat@0) Bool)
(assert (forall ((x@23079 Nat@0)) (diseqNat@0@484 Z@0 (S@0 x@23079))))
(assert (forall ((x@23080 Nat@0)) (diseqNat@0@484 (S@0 x@23080) Z@0)))
(assert (forall ((x@23081 Nat@0) (x@23082 Nat@0)) (=> (diseqNat@0@484 x@23081 x@23082) (diseqNat@0@484 (S@0 x@23081) (S@0 x@23082)))))
(declare-datatype Nat ((zero ) (succ (p Nat))))
(declare-fun diseqNat@336 (Nat Nat) Bool)
(assert (forall ((x@23083 Nat)) (diseqNat@336 zero (succ x@23083))))
(assert (forall ((x@23084 Nat)) (diseqNat@336 (succ x@23084) zero)))
(assert (forall ((x@23085 Nat) (x@23086 Nat)) (=> (diseqNat@336 x@23085 x@23086) (diseqNat@336 (succ x@23085) (succ x@23086)))))
(declare-fun plus (Nat Nat Nat) Bool)
(assert (forall ((x@23062 Nat) (x Nat) (y Nat)) (=> (and (= x@23062 y) (= x zero)) (plus x@23062 x y))))
(assert (forall ((x@23062 Nat) (x Nat) (y Nat) (z Nat) (x@23063 Nat)) (=> (and (= x@23062 (succ x@23063)) (= x (succ z)) (plus x@23063 z y)) (plus x@23062 x y))))
(declare-fun add3 (Nat Nat Nat Nat) Bool)
(assert (forall ((x@23064 Nat) (x Nat) (y Nat) (z Nat)) (=> (and (= x@23064 z) (= x zero) (= y zero)) (add3 x@23064 x y z))))
(assert (forall ((x@23064 Nat) (x Nat) (y Nat) (z Nat) (x3 Nat) (x@23066 Nat) (x@23065 Nat)) (=> (and (= x@23064 x@23066) (= x zero) (= y (succ x3)) (plus x@23066 (succ zero) x@23065) (add3 x@23065 zero x3 z)) (add3 x@23064 x y z))))
(assert (forall ((x@23064 Nat) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@23068 Nat) (x@23067 Nat)) (=> (and (= x@23064 x@23068) (= x (succ x2)) (plus x@23068 (succ zero) x@23067) (add3 x@23067 x2 y z)) (add3 x@23064 x y z))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@23070 Nat) (x@23069 Nat)) (=> (and (diseqNat@336 x@23070 x@23069) (add3 x@23070 x y z) (add3 x@23069 x z y)) false))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@23074 Nat) (x@23073 Nat) (x@23072 Nat) (x@23071 Nat)) (=> (and (diseqNat@336 x@23074 x@23072) (plus x@23074 x x@23073) (plus x@23073 y z) (plus x@23072 x@23071 z) (plus x@23071 x y)) false))))
(assert (forall ((x Nat) (y Nat)) (forall ((x@23076 Nat) (x@23075 Nat)) (=> (and (diseqNat@336 x@23076 x@23075) (plus x@23076 x y) (plus x@23075 y x)) false))))
(assert (forall ((x Nat)) (forall ((x@23077 Nat)) (=> (and (diseqNat@336 x@23077 x) (plus x@23077 x zero)) false))))
(assert (forall ((x Nat)) (forall ((x@23078 Nat)) (=> (and (diseqNat@336 x@23078 x) (plus x@23078 zero x)) false))))
(check-sat)
(get-info :reason-unknown)
