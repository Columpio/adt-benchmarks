(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@609 (Nat@0 Nat@0) Bool)
(assert (forall ((x@28574 Nat@0)) (diseqNat@0@609 Z@0 (S@0 x@28574))))
(assert (forall ((x@28575 Nat@0)) (diseqNat@0@609 (S@0 x@28575) Z@0)))
(assert (forall ((x@28576 Nat@0) (x@28577 Nat@0)) (=> (diseqNat@0@609 x@28576 x@28577) (diseqNat@0@609 (S@0 x@28576) (S@0 x@28577)))))
(declare-datatype Nat ((zero ) (succ (p Nat))))
(declare-fun diseqNat@431 (Nat Nat) Bool)
(assert (forall ((x@28578 Nat)) (diseqNat@431 zero (succ x@28578))))
(assert (forall ((x@28579 Nat)) (diseqNat@431 (succ x@28579) zero)))
(assert (forall ((x@28580 Nat) (x@28581 Nat)) (=> (diseqNat@431 x@28580 x@28581) (diseqNat@431 (succ x@28580) (succ x@28581)))))
(declare-fun plus (Nat Nat Nat) Bool)
(assert (forall ((x@28537 Nat) (x Nat) (y Nat)) (=> (and (= x@28537 y) (= x zero)) (plus x@28537 x y))))
(assert (forall ((x@28537 Nat) (x Nat) (y Nat) (z Nat) (x@28538 Nat)) (=> (and (= x@28537 (succ x@28538)) (= x (succ z)) (plus x@28538 z y)) (plus x@28537 x y))))
(declare-fun times (Nat Nat Nat) Bool)
(assert (forall ((x@28539 Nat) (x Nat) (y Nat)) (=> (and (= x@28539 zero) (= x zero)) (times x@28539 x y))))
(assert (forall ((x@28539 Nat) (x Nat) (y Nat) (z Nat) (x@28541 Nat) (x@28540 Nat)) (=> (and (= x@28539 x@28541) (= x (succ z)) (plus x@28541 y x@28540) (times x@28540 z y)) (times x@28539 x y))))
(declare-fun formula-pow (Nat Nat Nat) Bool)
(assert (forall ((x@28542 Nat) (x Nat) (y Nat)) (=> (and (= x@28542 (succ zero)) (= y zero)) (formula-pow x@28542 x y))))
(assert (forall ((x@28542 Nat) (x Nat) (y Nat) (z Nat) (x@28544 Nat) (x@28543 Nat)) (=> (and (= x@28542 x@28544) (= y (succ z)) (times x@28544 x x@28543) (formula-pow x@28543 x z)) (formula-pow x@28542 x y))))
(assert (forall ((x Nat)) (forall ((x@28545 Nat)) (=> (and (diseqNat@431 x@28545 (succ zero)) (formula-pow x@28545 (succ zero) x)) false))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@28549 Nat) (x@28548 Nat) (x@28547 Nat) (x@28546 Nat)) (=> (and (diseqNat@431 x@28549 x@28547) (times x@28549 x x@28548) (times x@28548 y z) (times x@28547 x@28546 z) (times x@28546 x y)) false))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@28553 Nat) (x@28552 Nat) (x@28551 Nat) (x@28550 Nat)) (=> (and (diseqNat@431 x@28553 x@28551) (plus x@28553 x x@28552) (plus x@28552 y z) (plus x@28551 x@28550 z) (plus x@28550 x y)) false))))
(assert (forall ((x Nat) (y Nat)) (forall ((x@28555 Nat) (x@28554 Nat)) (=> (and (diseqNat@431 x@28555 x@28554) (times x@28555 x y) (times x@28554 y x)) false))))
(assert (forall ((x Nat) (y Nat)) (forall ((x@28557 Nat) (x@28556 Nat)) (=> (and (diseqNat@431 x@28557 x@28556) (plus x@28557 x y) (plus x@28556 y x)) false))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@28562 Nat) (x@28561 Nat) (x@28560 Nat) (x@28559 Nat) (x@28558 Nat)) (=> (and (diseqNat@431 x@28562 x@28560) (times x@28562 x x@28561) (plus x@28561 y z) (plus x@28560 x@28559 x@28558) (times x@28559 x y) (times x@28558 x z)) false))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@28567 Nat) (x@28566 Nat) (x@28565 Nat) (x@28564 Nat) (x@28563 Nat)) (=> (and (diseqNat@431 x@28567 x@28565) (times x@28567 x@28566 z) (plus x@28566 x y) (plus x@28565 x@28564 x@28563) (times x@28564 x z) (times x@28563 y z)) false))))
(assert (forall ((x Nat)) (forall ((x@28568 Nat)) (=> (and (diseqNat@431 x@28568 x) (times x@28568 x (succ zero))) false))))
(assert (forall ((x Nat)) (forall ((x@28569 Nat)) (=> (and (diseqNat@431 x@28569 x) (times x@28569 (succ zero) x)) false))))
(assert (forall ((x Nat)) (forall ((x@28570 Nat)) (=> (and (diseqNat@431 x@28570 x) (plus x@28570 x zero)) false))))
(assert (forall ((x Nat)) (forall ((x@28571 Nat)) (=> (and (diseqNat@431 x@28571 x) (plus x@28571 zero x)) false))))
(assert (forall ((x Nat)) (forall ((x@28572 Nat)) (=> (and (diseqNat@431 x@28572 zero) (times x@28572 x zero)) false))))
(assert (forall ((x Nat)) (forall ((x@28573 Nat)) (=> (and (diseqNat@431 x@28573 zero) (times x@28573 zero x)) false))))
(check-sat)
(get-info :reason-unknown)
