(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@496 (Nat@0 Nat@0) Bool)
(assert (forall ((x@23586 Nat@0)) (diseqNat@0@496 Z@0 (S@0 x@23586))))
(assert (forall ((x@23587 Nat@0)) (diseqNat@0@496 (S@0 x@23587) Z@0)))
(assert (forall ((x@23588 Nat@0) (x@23589 Nat@0)) (=> (diseqNat@0@496 x@23588 x@23589) (diseqNat@0@496 (S@0 x@23588) (S@0 x@23589)))))
(declare-datatype A ((X ) (Y )))
(declare-fun diseqA@18 (A A) Bool)
(assert (diseqA@18 X Y))
(assert (diseqA@18 Y X))
(declare-datatype R ((Nil ) (Eps ) (Atom (proj1-Atom A)) (Plus (proj1-Plus R) (proj2-Plus R)) (Seq (proj1-Seq R) (proj2-Seq R)) (Star (proj1-Star R))))
(declare-fun diseqR@86 (R R) Bool)
(assert (diseqR@86 Nil Eps))
(assert (forall ((x@23590 A)) (diseqR@86 Nil (Atom x@23590))))
(assert (forall ((x@23591 R) (x@23592 R)) (diseqR@86 Nil (Plus x@23591 x@23592))))
(assert (forall ((x@23593 R) (x@23594 R)) (diseqR@86 Nil (Seq x@23593 x@23594))))
(assert (forall ((x@23595 R)) (diseqR@86 Nil (Star x@23595))))
(assert (diseqR@86 Eps Nil))
(assert (forall ((x@23596 A)) (diseqR@86 (Atom x@23596) Nil)))
(assert (forall ((x@23597 R) (x@23598 R)) (diseqR@86 (Plus x@23597 x@23598) Nil)))
(assert (forall ((x@23599 R) (x@23600 R)) (diseqR@86 (Seq x@23599 x@23600) Nil)))
(assert (forall ((x@23601 R)) (diseqR@86 (Star x@23601) Nil)))
(assert (forall ((x@23602 A)) (diseqR@86 Eps (Atom x@23602))))
(assert (forall ((x@23603 R) (x@23604 R)) (diseqR@86 Eps (Plus x@23603 x@23604))))
(assert (forall ((x@23605 R) (x@23606 R)) (diseqR@86 Eps (Seq x@23605 x@23606))))
(assert (forall ((x@23607 R)) (diseqR@86 Eps (Star x@23607))))
(assert (forall ((x@23608 A)) (diseqR@86 (Atom x@23608) Eps)))
(assert (forall ((x@23609 R) (x@23610 R)) (diseqR@86 (Plus x@23609 x@23610) Eps)))
(assert (forall ((x@23611 R) (x@23612 R)) (diseqR@86 (Seq x@23611 x@23612) Eps)))
(assert (forall ((x@23613 R)) (diseqR@86 (Star x@23613) Eps)))
(assert (forall ((x@23614 A) (x@23615 R) (x@23616 R)) (diseqR@86 (Atom x@23614) (Plus x@23615 x@23616))))
(assert (forall ((x@23617 A) (x@23618 R) (x@23619 R)) (diseqR@86 (Atom x@23617) (Seq x@23618 x@23619))))
(assert (forall ((x@23620 A) (x@23621 R)) (diseqR@86 (Atom x@23620) (Star x@23621))))
(assert (forall ((x@23622 R) (x@23623 R) (x@23624 A)) (diseqR@86 (Plus x@23622 x@23623) (Atom x@23624))))
(assert (forall ((x@23625 R) (x@23626 R) (x@23627 A)) (diseqR@86 (Seq x@23625 x@23626) (Atom x@23627))))
(assert (forall ((x@23628 R) (x@23629 A)) (diseqR@86 (Star x@23628) (Atom x@23629))))
(assert (forall ((x@23630 R) (x@23631 R) (x@23632 R) (x@23633 R)) (diseqR@86 (Plus x@23630 x@23631) (Seq x@23632 x@23633))))
(assert (forall ((x@23634 R) (x@23635 R) (x@23636 R)) (diseqR@86 (Plus x@23634 x@23635) (Star x@23636))))
(assert (forall ((x@23637 R) (x@23638 R) (x@23639 R) (x@23640 R)) (diseqR@86 (Seq x@23637 x@23638) (Plus x@23639 x@23640))))
(assert (forall ((x@23641 R) (x@23642 R) (x@23643 R)) (diseqR@86 (Star x@23641) (Plus x@23642 x@23643))))
(assert (forall ((x@23644 R) (x@23645 R) (x@23646 R)) (diseqR@86 (Seq x@23644 x@23645) (Star x@23646))))
(assert (forall ((x@23647 R) (x@23648 R) (x@23649 R)) (diseqR@86 (Star x@23647) (Seq x@23648 x@23649))))
(assert (forall ((x@23650 A) (x@23651 A)) (=> (diseqA@18 x@23650 x@23651) (diseqR@86 (Atom x@23650) (Atom x@23651)))))
(assert (forall ((x@23652 R) (x@23653 R) (x@23654 R) (x@23655 R)) (=> (diseqR@86 x@23652 x@23654) (diseqR@86 (Plus x@23652 x@23653) (Plus x@23654 x@23655)))))
(assert (forall ((x@23652 R) (x@23653 R) (x@23654 R) (x@23655 R)) (=> (diseqR@86 x@23653 x@23655) (diseqR@86 (Plus x@23652 x@23653) (Plus x@23654 x@23655)))))
(assert (forall ((x@23656 R) (x@23657 R) (x@23658 R) (x@23659 R)) (=> (diseqR@86 x@23656 x@23658) (diseqR@86 (Seq x@23656 x@23657) (Seq x@23658 x@23659)))))
(assert (forall ((x@23656 R) (x@23657 R) (x@23658 R) (x@23659 R)) (=> (diseqR@86 x@23657 x@23659) (diseqR@86 (Seq x@23656 x@23657) (Seq x@23658 x@23659)))))
(assert (forall ((x@23660 R) (x@23661 R)) (=> (diseqR@86 x@23660 x@23661) (diseqR@86 (Star x@23660) (Star x@23661)))))
(declare-datatype list ((nil ) (cons (head A) (tail list))))
(declare-fun diseqlist@338 (list list) Bool)
(assert (forall ((x@23662 A) (x@23663 list)) (diseqlist@338 nil (cons x@23662 x@23663))))
(assert (forall ((x@23664 A) (x@23665 list)) (diseqlist@338 (cons x@23664 x@23665) nil)))
(assert (forall ((x@23666 A) (x@23667 list) (x@23668 A) (x@23669 list)) (=> (diseqA@18 x@23666 x@23668) (diseqlist@338 (cons x@23666 x@23667) (cons x@23668 x@23669)))))
(assert (forall ((x@23666 A) (x@23667 list) (x@23668 A) (x@23669 list)) (=> (diseqlist@338 x@23667 x@23669) (diseqlist@338 (cons x@23666 x@23667) (cons x@23668 x@23669)))))
(declare-fun seq (R R R) Bool)
(assert (forall ((x@23557 R) (x R) (y R) (x@23549 R) (x@23550 R) (x@23551 R) (x@23552 R)) (=> (and (= x@23557 (Seq x y)) (= x x@23549) (= y x@23550) (= x x@23551) (= y x@23552)) (seq x@23557 x y))))
(assert (forall ((x@23557 R) (x R) (y R) (x@23549 R) (x@23550 R) (x@23551 R)) (=> (and (= x@23557 x) (= x x@23549) (= y x@23550) (= x x@23551) (= y Eps)) (seq x@23557 x y))))
(assert (forall ((x@23557 R) (x R) (y R) (x@23549 R) (x@23550 R)) (=> (and (= x@23557 y) (= x x@23549) (= y x@23550) (= x Eps)) (seq x@23557 x y))))
(assert (forall ((x@23557 R) (x R) (y R) (x@23549 R)) (=> (and (= x@23557 Nil) (= x x@23549) (= y Nil)) (seq x@23557 x y))))
(assert (forall ((x@23557 R) (x R) (y R)) (=> (and (= x@23557 Nil) (= x Nil)) (seq x@23557 x y))))
(declare-fun plus (R R R) Bool)
(assert (forall ((x@23558 R) (x R) (y R) (x@23553 R) (x@23554 R)) (=> (and (= x@23558 (Plus x y)) (= x x@23553) (= y x@23554)) (plus x@23558 x y))))
(assert (forall ((x@23558 R) (x R) (y R) (x@23553 R)) (=> (and (= x@23558 x) (= x x@23553) (= y Nil)) (plus x@23558 x y))))
(assert (forall ((x@23558 R) (x R) (y R)) (=> (and (= x@23558 y) (= x Nil)) (plus x@23558 x y))))
(declare-fun eqA (Bool A A) Bool)
(assert (forall ((x@23559 Bool) (x A) (y A)) (=> (and (= x@23559 true) (= x X) (= y X)) (eqA x@23559 x y))))
(assert (forall ((x@23559 Bool) (x A) (y A)) (=> (and (= x@23559 false) (= x X) (= y Y)) (eqA x@23559 x y))))
(assert (forall ((x@23559 Bool) (x A) (y A)) (=> (and (= x@23559 false) (= x Y) (= y X)) (eqA x@23559 x y))))
(assert (forall ((x@23559 Bool) (x A) (y A)) (=> (and (= x@23559 true) (= x Y) (= y Y)) (eqA x@23559 x y))))
(declare-fun eps (Bool R) Bool)
(assert (forall ((x@23560 Bool) (x R) (x@23555 R)) (=> (and (= x@23560 false) (= x x@23555)) (eps x@23560 x))))
(assert (forall ((x@23560 Bool) (x R)) (=> (and (= x@23560 true) (= x Eps)) (eps x@23560 x))))
(assert (forall ((x@23560 Bool) (x R) (p R) (q R) (x@23562 Bool) (x@23561 Bool)) (=> (and (= x@23560 (or x@23562 x@23561)) (= x (Plus p q)) (eps x@23562 p) (eps x@23561 q)) (eps x@23560 x))))
(assert (forall ((x@23560 Bool) (x R) (r R) (q2 R) (x@23564 Bool) (x@23563 Bool)) (=> (and (= x@23560 (and x@23564 x@23563)) (= x (Seq r q2)) (eps x@23564 r) (eps x@23563 q2)) (eps x@23560 x))))
(assert (forall ((x@23560 Bool) (x R) (y R)) (=> (and (= x@23560 true) (= x (Star y))) (eps x@23560 x))))
(declare-fun epsR (R R) Bool)
(assert (forall ((x@23565 R) (x R) (x@23566 Bool)) (=> (and (= x@23565 (ite x@23566 Eps Nil)) (eps x@23566 x)) (epsR x@23565 x))))
(declare-fun step (R R A) Bool)
(assert (forall ((x@23567 R) (x R) (y A) (x@23556 R)) (=> (and (= x@23567 Nil) (= x x@23556)) (step x@23567 x y))))
(assert (forall ((x@23567 R) (x R) (y A) (a A) (x@23568 Bool)) (=> (and (= x@23567 (ite x@23568 Eps Nil)) (= x (Atom a)) (eqA x@23568 a y)) (step x@23567 x y))))
(assert (forall ((x@23567 R) (x R) (y A) (p R) (q R) (x@23571 R) (x@23570 R) (x@23569 R)) (=> (and (= x@23567 x@23571) (= x (Plus p q)) (plus x@23571 x@23570 x@23569) (step x@23570 p y) (step x@23569 q y)) (step x@23567 x y))))
(assert (forall ((x@23567 R) (x R) (y A) (r R) (q2 R) (x@23577 R) (x@23576 R) (x@23575 R) (x@23574 R) (x@23573 R) (x@23572 R)) (=> (and (= x@23567 x@23577) (= x (Seq r q2)) (plus x@23577 x@23576 x@23574) (seq x@23576 x@23575 q2) (step x@23575 r y) (seq x@23574 x@23573 x@23572) (epsR x@23573 r) (step x@23572 q2 y)) (step x@23567 x y))))
(assert (forall ((x@23567 R) (x R) (y A) (p2 R) (x@23579 R) (x@23578 R)) (=> (and (= x@23567 x@23579) (= x (Star p2)) (seq x@23579 x@23578 (Star p2)) (step x@23578 p2 y)) (step x@23567 x y))))
(declare-fun recognise (Bool R list) Bool)
(assert (forall ((x@23580 Bool) (x R) (y list) (x@23581 Bool)) (=> (and (= x@23580 x@23581) (= y nil) (eps x@23581 x)) (recognise x@23580 x y))))
(assert (forall ((x@23580 Bool) (x R) (y list) (z A) (xs list) (x@23583 Bool) (x@23582 R)) (=> (and (= x@23580 x@23583) (= y (cons z xs)) (recognise x@23583 x@23582 xs) (step x@23582 x z)) (recognise x@23580 x y))))
(assert (forall ((p R) (s list)) (forall ((x@23585 Bool) (x@23584 Bool)) (=> (and (diseqBool@0 x@23585 x@23584) (recognise x@23585 (Star p) s) (recognise x@23584 (Plus Eps (Seq p (Star p))) s)) false))))
(check-sat)
(get-info :reason-unknown)
