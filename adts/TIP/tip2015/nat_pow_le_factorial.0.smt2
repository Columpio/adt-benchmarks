(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@580 (Nat@0 Nat@0) Bool)
(assert (forall ((x@27209 Nat@0)) (diseqNat@0@580 Z@0 (S@0 x@27209))))
(assert (forall ((x@27210 Nat@0)) (diseqNat@0@580 (S@0 x@27210) Z@0)))
(assert (forall ((x@27211 Nat@0) (x@27212 Nat@0)) (=> (diseqNat@0@580 x@27211 x@27212) (diseqNat@0@580 (S@0 x@27211) (S@0 x@27212)))))
(declare-datatype Nat ((zero ) (succ (p Nat))))
(declare-fun diseqNat@410 (Nat Nat) Bool)
(assert (forall ((x@27213 Nat)) (diseqNat@410 zero (succ x@27213))))
(assert (forall ((x@27214 Nat)) (diseqNat@410 (succ x@27214) zero)))
(assert (forall ((x@27215 Nat) (x@27216 Nat)) (=> (diseqNat@410 x@27215 x@27216) (diseqNat@410 (succ x@27215) (succ x@27216)))))
(declare-fun plus (Nat Nat Nat) Bool)
(assert (forall ((x@27163 Nat) (x Nat) (y Nat)) (=> (and (= x@27163 y) (= x zero)) (plus x@27163 x y))))
(assert (forall ((x@27163 Nat) (x Nat) (y Nat) (z Nat) (x@27164 Nat)) (=> (and (= x@27163 (succ x@27164)) (= x (succ z)) (plus x@27164 z y)) (plus x@27163 x y))))
(declare-fun times (Nat Nat Nat) Bool)
(assert (forall ((x@27165 Nat) (x Nat) (y Nat)) (=> (and (= x@27165 zero) (= x zero)) (times x@27165 x y))))
(assert (forall ((x@27165 Nat) (x Nat) (y Nat) (z Nat) (x@27167 Nat) (x@27166 Nat)) (=> (and (= x@27165 x@27167) (= x (succ z)) (plus x@27167 y x@27166) (times x@27166 z y)) (times x@27165 x y))))
(declare-fun lt (Bool Nat Nat) Bool)
(assert (forall ((x@27168 Bool) (x Nat) (y Nat)) (=> (and (= x@27168 false) (= y zero)) (lt x@27168 x y))))
(assert (forall ((x@27168 Bool) (x Nat) (y Nat) (z Nat)) (=> (and (= x@27168 true) (= y (succ z)) (= x zero)) (lt x@27168 x y))))
(assert (forall ((x@27168 Bool) (x Nat) (y Nat) (z Nat) (n Nat) (x@27169 Bool)) (=> (and (= x@27168 x@27169) (= y (succ z)) (= x (succ n)) (lt x@27169 n z)) (lt x@27168 x y))))
(declare-fun formula-pow (Nat Nat Nat) Bool)
(assert (forall ((x@27170 Nat) (x Nat) (y Nat)) (=> (and (= x@27170 (succ zero)) (= y zero)) (formula-pow x@27170 x y))))
(assert (forall ((x@27170 Nat) (x Nat) (y Nat) (z Nat) (x@27172 Nat) (x@27171 Nat)) (=> (and (= x@27170 x@27172) (= y (succ z)) (times x@27172 x x@27171) (formula-pow x@27171 x z)) (formula-pow x@27170 x y))))
(declare-fun factorial (Nat Nat) Bool)
(assert (forall ((x@27173 Nat) (x Nat)) (=> (and (= x@27173 (succ zero)) (= x zero)) (factorial x@27173 x))))
(assert (forall ((x@27173 Nat) (x Nat) (y Nat) (x@27175 Nat) (x@27174 Nat)) (=> (and (= x@27173 x@27175) (= x (succ y)) (times x@27175 (succ y) x@27174) (factorial x@27174 y)) (factorial x@27173 x))))
(assert (forall ((n Nat)) (forall ((x@27180 Bool) (x@27179 Nat) (x@27178 Nat) (x@27177 Nat) (x@27176 Nat)) (=> (and (not x@27180) (lt x@27180 x@27179 x@27177) (formula-pow x@27179 (succ (succ zero)) x@27178) (plus x@27178 (succ (succ (succ (succ zero)))) n) (factorial x@27177 x@27176) (plus x@27176 (succ (succ (succ (succ zero)))) n)) false))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@27184 Nat) (x@27183 Nat) (x@27182 Nat) (x@27181 Nat)) (=> (and (diseqNat@410 x@27184 x@27182) (times x@27184 x x@27183) (times x@27183 y z) (times x@27182 x@27181 z) (times x@27181 x y)) false))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@27188 Nat) (x@27187 Nat) (x@27186 Nat) (x@27185 Nat)) (=> (and (diseqNat@410 x@27188 x@27186) (plus x@27188 x x@27187) (plus x@27187 y z) (plus x@27186 x@27185 z) (plus x@27185 x y)) false))))
(assert (forall ((x Nat) (y Nat)) (forall ((x@27190 Nat) (x@27189 Nat)) (=> (and (diseqNat@410 x@27190 x@27189) (times x@27190 x y) (times x@27189 y x)) false))))
(assert (forall ((x Nat) (y Nat)) (forall ((x@27192 Nat) (x@27191 Nat)) (=> (and (diseqNat@410 x@27192 x@27191) (plus x@27192 x y) (plus x@27191 y x)) false))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@27197 Nat) (x@27196 Nat) (x@27195 Nat) (x@27194 Nat) (x@27193 Nat)) (=> (and (diseqNat@410 x@27197 x@27195) (times x@27197 x x@27196) (plus x@27196 y z) (plus x@27195 x@27194 x@27193) (times x@27194 x y) (times x@27193 x z)) false))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@27202 Nat) (x@27201 Nat) (x@27200 Nat) (x@27199 Nat) (x@27198 Nat)) (=> (and (diseqNat@410 x@27202 x@27200) (times x@27202 x@27201 z) (plus x@27201 x y) (plus x@27200 x@27199 x@27198) (times x@27199 x z) (times x@27198 y z)) false))))
(assert (forall ((x Nat)) (forall ((x@27203 Nat)) (=> (and (diseqNat@410 x@27203 x) (times x@27203 x (succ zero))) false))))
(assert (forall ((x Nat)) (forall ((x@27204 Nat)) (=> (and (diseqNat@410 x@27204 x) (times x@27204 (succ zero) x)) false))))
(assert (forall ((x Nat)) (forall ((x@27205 Nat)) (=> (and (diseqNat@410 x@27205 x) (plus x@27205 x zero)) false))))
(assert (forall ((x Nat)) (forall ((x@27206 Nat)) (=> (and (diseqNat@410 x@27206 x) (plus x@27206 zero x)) false))))
(assert (forall ((x Nat)) (forall ((x@27207 Nat)) (=> (and (diseqNat@410 x@27207 zero) (times x@27207 x zero)) false))))
(assert (forall ((x Nat)) (forall ((x@27208 Nat)) (=> (and (diseqNat@410 x@27208 zero) (times x@27208 zero x)) false))))
(check-sat)
(get-info :reason-unknown)
