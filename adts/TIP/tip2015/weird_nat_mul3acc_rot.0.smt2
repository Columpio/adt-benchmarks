(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@625 (Nat@0 Nat@0) Bool)
(assert (forall ((x@29417 Nat@0)) (diseqNat@0@625 Z@0 (S@0 x@29417))))
(assert (forall ((x@29418 Nat@0)) (diseqNat@0@625 (S@0 x@29418) Z@0)))
(assert (forall ((x@29419 Nat@0) (x@29420 Nat@0)) (=> (diseqNat@0@625 x@29419 x@29420) (diseqNat@0@625 (S@0 x@29419) (S@0 x@29420)))))
(declare-datatype Nat ((zero ) (succ (p Nat))))
(declare-fun diseqNat@444 (Nat Nat) Bool)
(assert (forall ((x@29421 Nat)) (diseqNat@444 zero (succ x@29421))))
(assert (forall ((x@29422 Nat)) (diseqNat@444 (succ x@29422) zero)))
(assert (forall ((x@29423 Nat) (x@29424 Nat)) (=> (diseqNat@444 x@29423 x@29424) (diseqNat@444 (succ x@29423) (succ x@29424)))))
(declare-fun plus (Nat Nat Nat) Bool)
(assert (forall ((x@29393 Nat) (x Nat) (y Nat)) (=> (and (= x@29393 y) (= x zero)) (plus x@29393 x y))))
(assert (forall ((x@29393 Nat) (x Nat) (y Nat) (z Nat) (x@29394 Nat)) (=> (and (= x@29393 (succ x@29394)) (= x (succ z)) (plus x@29394 z y)) (plus x@29393 x y))))
(declare-fun add3acc (Nat Nat Nat Nat) Bool)
(assert (forall ((x@29395 Nat) (x Nat) (y Nat) (z Nat)) (=> (and (= x@29395 z) (= x zero) (= y zero)) (add3acc x@29395 x y z))))
(assert (forall ((x@29395 Nat) (x Nat) (y Nat) (z Nat) (x3 Nat) (x@29396 Nat)) (=> (and (= x@29395 x@29396) (= x zero) (= y (succ x3)) (add3acc x@29396 zero x3 (succ z))) (add3acc x@29395 x y z))))
(assert (forall ((x@29395 Nat) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@29397 Nat)) (=> (and (= x@29395 x@29397) (= x (succ x2)) (add3acc x@29397 x2 (succ y) z)) (add3acc x@29395 x y z))))
(declare-fun mul3acc (Nat Nat Nat Nat) Bool)
(assert (forall ((x@29398 Nat) (x Nat) (y Nat) (z Nat)) (=> (and (= x@29398 zero) (= x zero)) (mul3acc x@29398 x y z))))
(assert (forall ((x@29398 Nat) (x Nat) (y Nat) (z Nat) (x2 Nat)) (=> (and (= x@29398 zero) (= x (succ x2)) (= y zero)) (mul3acc x@29398 x y z))))
(assert (forall ((x@29398 Nat) (x Nat) (y Nat) (z Nat) (x2 Nat) (x3 Nat)) (=> (and (= x@29398 zero) (= x (succ x2)) (= y (succ x3)) (= z zero)) (mul3acc x@29398 x y z))))
(assert (forall ((x@29398 Nat) (x Nat) (y Nat) (z Nat) (x2 Nat) (x3 Nat) (x4 Nat) (fail Nat) (x@29406 Nat) (x@29405 Nat) (x@29404 Nat) (x@29403 Nat) (x@29402 Nat) (x@29401 Nat) (x@29400 Nat) (x@29399 Nat)) (=> (and (= x@29398 (ite (= x2 zero) (ite (= x3 zero) (ite (= x4 zero) (succ zero) fail) fail) fail)) (= x (succ x2)) (= y (succ x3)) (= z (succ x4)) (= fail x@29406) (plus x@29406 (succ zero) x@29405) (add3acc x@29405 x@29404 x@29403 x@29399) (mul3acc x@29404 x2 x3 x4) (add3acc x@29403 x@29402 x@29401 x@29400) (mul3acc x@29402 (succ zero) x3 x4) (mul3acc x@29401 x2 (succ zero) x4) (mul3acc x@29400 x2 x3 (succ zero)) (add3acc x@29399 (succ x2) (succ x3) (succ x4))) (mul3acc x@29398 x y z))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@29408 Nat) (x@29407 Nat)) (=> (and (diseqNat@444 x@29408 x@29407) (mul3acc x@29408 x y z) (mul3acc x@29407 y x z)) false))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@29412 Nat) (x@29411 Nat) (x@29410 Nat) (x@29409 Nat)) (=> (and (diseqNat@444 x@29412 x@29410) (plus x@29412 x x@29411) (plus x@29411 y z) (plus x@29410 x@29409 z) (plus x@29409 x y)) false))))
(assert (forall ((x Nat) (y Nat)) (forall ((x@29414 Nat) (x@29413 Nat)) (=> (and (diseqNat@444 x@29414 x@29413) (plus x@29414 x y) (plus x@29413 y x)) false))))
(assert (forall ((x Nat)) (forall ((x@29415 Nat)) (=> (and (diseqNat@444 x@29415 x) (plus x@29415 x zero)) false))))
(assert (forall ((x Nat)) (forall ((x@29416 Nat)) (=> (and (diseqNat@444 x@29416 x) (plus x@29416 zero x)) false))))
(check-sat)
(get-info :reason-unknown)
