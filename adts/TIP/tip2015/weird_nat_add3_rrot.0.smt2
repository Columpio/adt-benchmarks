(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@591 (Nat@0 Nat@0) Bool)
(assert (forall ((x@27705 Nat@0)) (diseqNat@0@591 Z@0 (S@0 x@27705))))
(assert (forall ((x@27706 Nat@0)) (diseqNat@0@591 (S@0 x@27706) Z@0)))
(assert (forall ((x@27707 Nat@0) (x@27708 Nat@0)) (=> (diseqNat@0@591 x@27707 x@27708) (diseqNat@0@591 (S@0 x@27707) (S@0 x@27708)))))
(declare-datatype Nat ((zero ) (succ (p Nat))))
(declare-fun diseqNat@417 (Nat Nat) Bool)
(assert (forall ((x@27709 Nat)) (diseqNat@417 zero (succ x@27709))))
(assert (forall ((x@27710 Nat)) (diseqNat@417 (succ x@27710) zero)))
(assert (forall ((x@27711 Nat) (x@27712 Nat)) (=> (diseqNat@417 x@27711 x@27712) (diseqNat@417 (succ x@27711) (succ x@27712)))))
(declare-fun plus (Nat Nat Nat) Bool)
(assert (forall ((x@27688 Nat) (x Nat) (y Nat)) (=> (and (= x@27688 y) (= x zero)) (plus x@27688 x y))))
(assert (forall ((x@27688 Nat) (x Nat) (y Nat) (z Nat) (x@27689 Nat)) (=> (and (= x@27688 (succ x@27689)) (= x (succ z)) (plus x@27689 z y)) (plus x@27688 x y))))
(declare-fun add3 (Nat Nat Nat Nat) Bool)
(assert (forall ((x@27690 Nat) (x Nat) (y Nat) (z Nat)) (=> (and (= x@27690 z) (= x zero) (= y zero)) (add3 x@27690 x y z))))
(assert (forall ((x@27690 Nat) (x Nat) (y Nat) (z Nat) (x3 Nat) (x@27692 Nat) (x@27691 Nat)) (=> (and (= x@27690 x@27692) (= x zero) (= y (succ x3)) (plus x@27692 (succ zero) x@27691) (add3 x@27691 zero x3 z)) (add3 x@27690 x y z))))
(assert (forall ((x@27690 Nat) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@27694 Nat) (x@27693 Nat)) (=> (and (= x@27690 x@27694) (= x (succ x2)) (plus x@27694 (succ zero) x@27693) (add3 x@27693 x2 y z)) (add3 x@27690 x y z))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@27696 Nat) (x@27695 Nat)) (=> (and (diseqNat@417 x@27696 x@27695) (add3 x@27696 x y z) (add3 x@27695 z x y)) false))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@27700 Nat) (x@27699 Nat) (x@27698 Nat) (x@27697 Nat)) (=> (and (diseqNat@417 x@27700 x@27698) (plus x@27700 x x@27699) (plus x@27699 y z) (plus x@27698 x@27697 z) (plus x@27697 x y)) false))))
(assert (forall ((x Nat) (y Nat)) (forall ((x@27702 Nat) (x@27701 Nat)) (=> (and (diseqNat@417 x@27702 x@27701) (plus x@27702 x y) (plus x@27701 y x)) false))))
(assert (forall ((x Nat)) (forall ((x@27703 Nat)) (=> (and (diseqNat@417 x@27703 x) (plus x@27703 x zero)) false))))
(assert (forall ((x Nat)) (forall ((x@27704 Nat)) (=> (and (diseqNat@417 x@27704 x) (plus x@27704 zero x)) false))))
(check-sat)
(get-info :reason-unknown)
