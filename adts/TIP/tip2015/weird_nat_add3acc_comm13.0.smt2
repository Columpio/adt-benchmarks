(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@527 (Nat@0 Nat@0) Bool)
(assert (forall ((x@24962 Nat@0)) (diseqNat@0@527 Z@0 (S@0 x@24962))))
(assert (forall ((x@24963 Nat@0)) (diseqNat@0@527 (S@0 x@24963) Z@0)))
(assert (forall ((x@24964 Nat@0) (x@24965 Nat@0)) (=> (diseqNat@0@527 x@24964 x@24965) (diseqNat@0@527 (S@0 x@24964) (S@0 x@24965)))))
(declare-datatype Nat ((zero ) (succ (p Nat))))
(declare-fun diseqNat@366 (Nat Nat) Bool)
(assert (forall ((x@24966 Nat)) (diseqNat@366 zero (succ x@24966))))
(assert (forall ((x@24967 Nat)) (diseqNat@366 (succ x@24967) zero)))
(assert (forall ((x@24968 Nat) (x@24969 Nat)) (=> (diseqNat@366 x@24968 x@24969) (diseqNat@366 (succ x@24968) (succ x@24969)))))
(declare-fun add3acc (Nat Nat Nat Nat) Bool)
(assert (forall ((x@24957 Nat) (x Nat) (y Nat) (z Nat)) (=> (and (= x@24957 z) (= x zero) (= y zero)) (add3acc x@24957 x y z))))
(assert (forall ((x@24957 Nat) (x Nat) (y Nat) (z Nat) (x3 Nat) (x@24958 Nat)) (=> (and (= x@24957 x@24958) (= x zero) (= y (succ x3)) (add3acc x@24958 zero x3 (succ z))) (add3acc x@24957 x y z))))
(assert (forall ((x@24957 Nat) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@24959 Nat)) (=> (and (= x@24957 x@24959) (= x (succ x2)) (add3acc x@24959 x2 (succ y) z)) (add3acc x@24957 x y z))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@24961 Nat) (x@24960 Nat)) (=> (and (diseqNat@366 x@24961 x@24960) (add3acc x@24961 x y z) (add3acc x@24960 z y x)) false))))
(check-sat)
(get-info :reason-unknown)
