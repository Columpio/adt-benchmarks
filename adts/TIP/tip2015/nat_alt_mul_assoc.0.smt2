(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@524 (Nat@0 Nat@0) Bool)
(assert (forall ((x@24903 Nat@0)) (diseqNat@0@524 Z@0 (S@0 x@24903))))
(assert (forall ((x@24904 Nat@0)) (diseqNat@0@524 (S@0 x@24904) Z@0)))
(assert (forall ((x@24905 Nat@0) (x@24906 Nat@0)) (=> (diseqNat@0@524 x@24905 x@24906) (diseqNat@0@524 (S@0 x@24905) (S@0 x@24906)))))
(declare-datatype Nat ((zero ) (succ (p Nat))))
(declare-fun diseqNat@363 (Nat Nat) Bool)
(assert (forall ((x@24907 Nat)) (diseqNat@363 zero (succ x@24907))))
(assert (forall ((x@24908 Nat)) (diseqNat@363 (succ x@24908) zero)))
(assert (forall ((x@24909 Nat) (x@24910 Nat)) (=> (diseqNat@363 x@24909 x@24910) (diseqNat@363 (succ x@24909) (succ x@24910)))))
(declare-fun plus (Nat Nat Nat) Bool)
(assert (forall ((x@24884 Nat) (x Nat) (y Nat)) (=> (and (= x@24884 y) (= x zero)) (plus x@24884 x y))))
(assert (forall ((x@24884 Nat) (x Nat) (y Nat) (z Nat) (x@24885 Nat)) (=> (and (= x@24884 (succ x@24885)) (= x (succ z)) (plus x@24885 z y)) (plus x@24884 x y))))
(declare-fun alt_mul (Nat Nat Nat) Bool)
(assert (forall ((x@24886 Nat) (x Nat) (y Nat)) (=> (and (= x@24886 zero) (= x zero)) (alt_mul x@24886 x y))))
(assert (forall ((x@24886 Nat) (x Nat) (y Nat) (z Nat)) (=> (and (= x@24886 zero) (= x (succ z)) (= y zero)) (alt_mul x@24886 x y))))
(assert (forall ((x@24886 Nat) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@24890 Nat) (x@24889 Nat) (x@24888 Nat) (x@24887 Nat)) (=> (and (= x@24886 x@24890) (= x (succ z)) (= y (succ x2)) (plus x@24890 x@24889 x2) (plus x@24889 x@24888 z) (plus x@24888 (succ zero) x@24887) (alt_mul x@24887 z x2)) (alt_mul x@24886 x y))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@24894 Nat) (x@24893 Nat) (x@24892 Nat) (x@24891 Nat)) (=> (and (diseqNat@363 x@24894 x@24892) (alt_mul x@24894 x x@24893) (alt_mul x@24893 y z) (alt_mul x@24892 x@24891 z) (alt_mul x@24891 x y)) false))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@24898 Nat) (x@24897 Nat) (x@24896 Nat) (x@24895 Nat)) (=> (and (diseqNat@363 x@24898 x@24896) (plus x@24898 x x@24897) (plus x@24897 y z) (plus x@24896 x@24895 z) (plus x@24895 x y)) false))))
(assert (forall ((x Nat) (y Nat)) (forall ((x@24900 Nat) (x@24899 Nat)) (=> (and (diseqNat@363 x@24900 x@24899) (plus x@24900 x y) (plus x@24899 y x)) false))))
(assert (forall ((x Nat)) (forall ((x@24901 Nat)) (=> (and (diseqNat@363 x@24901 x) (plus x@24901 x zero)) false))))
(assert (forall ((x Nat)) (forall ((x@24902 Nat)) (=> (and (diseqNat@363 x@24902 x) (plus x@24902 zero x)) false))))
(check-sat)
(get-info :reason-unknown)
