(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@574 (Nat@0 Nat@0) Bool)
(assert (forall ((x@26994 Nat@0)) (diseqNat@0@574 Z@0 (S@0 x@26994))))
(assert (forall ((x@26995 Nat@0)) (diseqNat@0@574 (S@0 x@26995) Z@0)))
(assert (forall ((x@26996 Nat@0) (x@26997 Nat@0)) (=> (diseqNat@0@574 x@26996 x@26997) (diseqNat@0@574 (S@0 x@26996) (S@0 x@26997)))))
(declare-datatype Nat ((zero ) (succ (p Nat))))
(declare-fun diseqNat@404 (Nat Nat) Bool)
(assert (forall ((x@26998 Nat)) (diseqNat@404 zero (succ x@26998))))
(assert (forall ((x@26999 Nat)) (diseqNat@404 (succ x@26999) zero)))
(assert (forall ((x@27000 Nat) (x@27001 Nat)) (=> (diseqNat@404 x@27000 x@27001) (diseqNat@404 (succ x@27000) (succ x@27001)))))
(declare-datatype Integer ((P (proj1-P Nat)) (N (proj1-N Nat))))
(declare-fun diseqInteger@16 (Integer Integer) Bool)
(assert (forall ((x@27002 Nat) (x@27003 Nat)) (diseqInteger@16 (P x@27002) (N x@27003))))
(assert (forall ((x@27004 Nat) (x@27005 Nat)) (diseqInteger@16 (N x@27004) (P x@27005))))
(assert (forall ((x@27006 Nat) (x@27007 Nat)) (=> (diseqNat@404 x@27006 x@27007) (diseqInteger@16 (P x@27006) (P x@27007)))))
(assert (forall ((x@27008 Nat) (x@27009 Nat)) (=> (diseqNat@404 x@27008 x@27009) (diseqInteger@16 (N x@27008) (N x@27009)))))
(declare-fun zero2 (Integer) Bool)
(assert (forall ((x@26971 Integer)) (=> (= x@26971 (P zero)) (zero2 x@26971))))
(declare-fun plus2 (Nat Nat Nat) Bool)
(assert (forall ((x@26972 Nat) (x Nat) (y Nat)) (=> (and (= x@26972 y) (= x zero)) (plus2 x@26972 x y))))
(assert (forall ((x@26972 Nat) (x Nat) (y Nat) (z Nat) (x@26973 Nat)) (=> (and (= x@26972 (succ x@26973)) (= x (succ z)) (plus2 x@26973 z y)) (plus2 x@26972 x y))))
(declare-fun |-2| (Integer Nat Nat) Bool)
(assert (forall ((x@26974 Integer) (x Nat) (y Nat) (fail Integer)) (=> (and (= x@26974 (P zero)) (= fail (P x)) (= y zero) (= x zero) (= y zero)) (|-2| x@26974 x y))))
(assert (forall ((x@26974 Integer) (x Nat) (y Nat) (fail Integer) (x4 Nat)) (=> (and (= x@26974 fail) (= fail (P x)) (= y zero) (= x zero) (= y (succ x4))) (|-2| x@26974 x y))))
(assert (forall ((x@26974 Integer) (x Nat) (y Nat) (fail Integer) (x3 Nat)) (=> (and (= x@26974 fail) (= fail (P x)) (= y zero) (= x (succ x3))) (|-2| x@26974 x y))))
(assert (forall ((x@26974 Integer) (x Nat) (y Nat) (fail Integer) (z Nat)) (=> (and (= x@26974 (P zero)) (= fail (N (succ z))) (= y (succ z)) (= x zero) (= x zero) (= y zero)) (|-2| x@26974 x y))))
(assert (forall ((x@26974 Integer) (x Nat) (y Nat) (fail Integer) (z Nat) (x4 Nat)) (=> (and (= x@26974 fail) (= fail (N (succ z))) (= y (succ z)) (= x zero) (= x zero) (= y (succ x4))) (|-2| x@26974 x y))))
(assert (forall ((x@26974 Integer) (x Nat) (y Nat) (fail Integer) (z Nat) (x3 Nat)) (=> (and (= x@26974 fail) (= fail (N (succ z))) (= y (succ z)) (= x zero) (= x (succ x3))) (|-2| x@26974 x y))))
(assert (forall ((x@26974 Integer) (x Nat) (y Nat) (fail Integer) (z Nat) (x2 Nat) (x@26975 Integer)) (=> (and (= x@26974 (P zero)) (= fail x@26975) (= y (succ z)) (= x (succ x2)) (|-2| x@26975 x2 z) (= x zero) (= y zero)) (|-2| x@26974 x y))))
(assert (forall ((x@26974 Integer) (x Nat) (y Nat) (fail Integer) (z Nat) (x2 Nat) (x@26975 Integer) (x4 Nat)) (=> (and (= x@26974 fail) (= fail x@26975) (= y (succ z)) (= x (succ x2)) (|-2| x@26975 x2 z) (= x zero) (= y (succ x4))) (|-2| x@26974 x y))))
(assert (forall ((x@26974 Integer) (x Nat) (y Nat) (fail Integer) (z Nat) (x2 Nat) (x@26975 Integer) (x3 Nat)) (=> (and (= x@26974 fail) (= fail x@26975) (= y (succ z)) (= x (succ x2)) (|-2| x@26975 x2 z) (= x (succ x3))) (|-2| x@26974 x y))))
(declare-fun plus (Integer Integer Integer) Bool)
(assert (forall ((x@26976 Integer) (x Integer) (y Integer) (m Nat) (n Nat) (x@26977 Nat)) (=> (and (= x@26976 (P x@26977)) (= x (P m)) (= y (P n)) (plus2 x@26977 m n)) (plus x@26976 x y))))
(assert (forall ((x@26976 Integer) (x Integer) (y Integer) (m Nat) (o Nat) (x@26979 Integer) (x@26978 Nat)) (=> (and (= x@26976 x@26979) (= x (P m)) (= y (N o)) (|-2| x@26979 m x@26978) (plus2 x@26978 (succ zero) o)) (plus x@26976 x y))))
(assert (forall ((x@26976 Integer) (x Integer) (y Integer) (m2 Nat) (n2 Nat) (x@26981 Integer) (x@26980 Nat)) (=> (and (= x@26976 x@26981) (= x (N m2)) (= y (P n2)) (|-2| x@26981 n2 x@26980) (plus2 x@26980 (succ zero) m2)) (plus x@26976 x y))))
(assert (forall ((x@26976 Integer) (x Integer) (y Integer) (m2 Nat) (n3 Nat) (x@26983 Nat) (x@26982 Nat)) (=> (and (= x@26976 (N x@26983)) (= x (N m2)) (= y (N n3)) (plus2 x@26983 x@26982 n3) (plus2 x@26982 (succ zero) m2)) (plus x@26976 x y))))
(assert (forall ((x Integer)) (forall ((x@26985 Integer) (x@26984 Integer)) (=> (and (diseqInteger@16 x x@26985) (plus x@26985 x x@26984) (zero2 x@26984)) false))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@26989 Nat) (x@26988 Nat) (x@26987 Nat) (x@26986 Nat)) (=> (and (diseqNat@404 x@26989 x@26987) (plus2 x@26989 x x@26988) (plus2 x@26988 y z) (plus2 x@26987 x@26986 z) (plus2 x@26986 x y)) false))))
(assert (forall ((x Nat) (y Nat)) (forall ((x@26991 Nat) (x@26990 Nat)) (=> (and (diseqNat@404 x@26991 x@26990) (plus2 x@26991 x y) (plus2 x@26990 y x)) false))))
(assert (forall ((x Nat)) (forall ((x@26992 Nat)) (=> (and (diseqNat@404 x@26992 x) (plus2 x@26992 x zero)) false))))
(assert (forall ((x Nat)) (forall ((x@26993 Nat)) (=> (and (diseqNat@404 x@26993 x) (plus2 x@26993 zero x)) false))))
(check-sat)
(get-info :reason-unknown)
