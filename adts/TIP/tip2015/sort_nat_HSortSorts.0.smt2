(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@652 (Nat@0 Nat@0) Bool)
(assert (forall ((x@30622 Nat@0)) (diseqNat@0@652 Z@0 (S@0 x@30622))))
(assert (forall ((x@30623 Nat@0)) (diseqNat@0@652 (S@0 x@30623) Z@0)))
(assert (forall ((x@30624 Nat@0) (x@30625 Nat@0)) (=> (diseqNat@0@652 x@30624 x@30625) (diseqNat@0@652 (S@0 x@30624) (S@0 x@30625)))))
(declare-datatype Nat ((zero ) (succ (p Nat))))
(declare-fun diseqNat@467 (Nat Nat) Bool)
(assert (forall ((x@30626 Nat)) (diseqNat@467 zero (succ x@30626))))
(assert (forall ((x@30627 Nat)) (diseqNat@467 (succ x@30627) zero)))
(assert (forall ((x@30628 Nat) (x@30629 Nat)) (=> (diseqNat@467 x@30628 x@30629) (diseqNat@467 (succ x@30628) (succ x@30629)))))
(declare-datatype list ((nil ) (cons (head Nat) (tail list))))
(declare-fun diseqlist@402 (list list) Bool)
(assert (forall ((x@30630 Nat) (x@30631 list)) (diseqlist@402 nil (cons x@30630 x@30631))))
(assert (forall ((x@30632 Nat) (x@30633 list)) (diseqlist@402 (cons x@30632 x@30633) nil)))
(assert (forall ((x@30634 Nat) (x@30635 list) (x@30636 Nat) (x@30637 list)) (=> (diseqNat@467 x@30634 x@30636) (diseqlist@402 (cons x@30634 x@30635) (cons x@30636 x@30637)))))
(assert (forall ((x@30634 Nat) (x@30635 list) (x@30636 Nat) (x@30637 list)) (=> (diseqlist@402 x@30635 x@30637) (diseqlist@402 (cons x@30634 x@30635) (cons x@30636 x@30637)))))
(declare-datatype Heap ((Node (proj1-Node Heap) (proj2-Node Nat) (proj3-Node Heap)) (Nil )))
(declare-fun diseqHeap@11 (Heap Heap) Bool)
(assert (forall ((x@30638 Heap) (x@30639 Nat) (x@30640 Heap)) (diseqHeap@11 (Node x@30638 x@30639 x@30640) Nil)))
(assert (forall ((x@30641 Heap) (x@30642 Nat) (x@30643 Heap)) (diseqHeap@11 Nil (Node x@30641 x@30642 x@30643))))
(assert (forall ((x@30644 Heap) (x@30645 Nat) (x@30646 Heap) (x@30647 Heap) (x@30648 Nat) (x@30649 Heap)) (=> (diseqHeap@11 x@30644 x@30647) (diseqHeap@11 (Node x@30644 x@30645 x@30646) (Node x@30647 x@30648 x@30649)))))
(assert (forall ((x@30644 Heap) (x@30645 Nat) (x@30646 Heap) (x@30647 Heap) (x@30648 Nat) (x@30649 Heap)) (=> (diseqNat@467 x@30645 x@30648) (diseqHeap@11 (Node x@30644 x@30645 x@30646) (Node x@30647 x@30648 x@30649)))))
(assert (forall ((x@30644 Heap) (x@30645 Nat) (x@30646 Heap) (x@30647 Heap) (x@30648 Nat) (x@30649 Heap)) (=> (diseqHeap@11 x@30646 x@30649) (diseqHeap@11 (Node x@30644 x@30645 x@30646) (Node x@30647 x@30648 x@30649)))))
(declare-datatype list2 ((nil2 ) (cons2 (head2 Heap) (tail2 list2))))
(declare-fun diseqlist2@46 (list2 list2) Bool)
(assert (forall ((x@30650 Heap) (x@30651 list2)) (diseqlist2@46 nil2 (cons2 x@30650 x@30651))))
(assert (forall ((x@30652 Heap) (x@30653 list2)) (diseqlist2@46 (cons2 x@30652 x@30653) nil2)))
(assert (forall ((x@30654 Heap) (x@30655 list2) (x@30656 Heap) (x@30657 list2)) (=> (diseqHeap@11 x@30654 x@30656) (diseqlist2@46 (cons2 x@30654 x@30655) (cons2 x@30656 x@30657)))))
(assert (forall ((x@30654 Heap) (x@30655 list2) (x@30656 Heap) (x@30657 list2)) (=> (diseqlist2@46 x@30655 x@30657) (diseqlist2@46 (cons2 x@30654 x@30655) (cons2 x@30656 x@30657)))))
(declare-fun toHeap (list2 list) Bool)
(assert (forall ((x@30594 list2) (x list)) (=> (and (= x@30594 nil2) (= x nil)) (toHeap x@30594 x))))
(assert (forall ((x@30594 list2) (x list) (y Nat) (z list) (x@30595 list2)) (=> (and (= x@30594 (cons2 (Node Nil y Nil) x@30595)) (= x (cons y z)) (toHeap x@30595 z)) (toHeap x@30594 x))))
(declare-fun leq (Bool Nat Nat) Bool)
(assert (forall ((x@30596 Bool) (x Nat) (y Nat)) (=> (and (= x@30596 true) (= x zero)) (leq x@30596 x y))))
(assert (forall ((x@30596 Bool) (x Nat) (y Nat) (z Nat)) (=> (and (= x@30596 false) (= x (succ z)) (= y zero)) (leq x@30596 x y))))
(assert (forall ((x@30596 Bool) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@30597 Bool)) (=> (and (= x@30596 x@30597) (= x (succ z)) (= y (succ x2)) (leq x@30597 z x2)) (leq x@30596 x y))))
(declare-fun ordered (Bool list) Bool)
(assert (forall ((x@30598 Bool) (x list)) (=> (and (= x@30598 true) (= x nil)) (ordered x@30598 x))))
(assert (forall ((x@30598 Bool) (x list) (y Nat) (z list)) (=> (and (= x@30598 true) (= x (cons y z)) (= z nil)) (ordered x@30598 x))))
(assert (forall ((x@30598 Bool) (x list) (y Nat) (z list) (y2 Nat) (xs list) (x@30600 Bool) (x@30599 Bool)) (=> (and (= x@30598 (and x@30600 x@30599)) (= x (cons y z)) (= z (cons y2 xs)) (leq x@30600 y y2) (ordered x@30599 (cons y2 xs))) (ordered x@30598 x))))
(declare-fun hmerge (Heap Heap Heap) Bool)
(assert (forall ((x@30601 Heap) (x Heap) (y Heap) (z Heap) (x2 Nat) (x3 Heap) (x4 Heap) (x5 Nat) (x6 Heap) (x@30602 Bool) (x@30603 Heap) (x@30604 Heap)) (=> (and (= x@30601 (ite x@30602 (Node x@30603 x2 z) (Node x@30604 x5 x4))) (= x (Node z x2 x3)) (= y (Node x4 x5 x6)) (leq x@30602 x2 x5) (hmerge x@30603 x3 (Node x4 x5 x6)) (hmerge x@30604 (Node z x2 x3) x6)) (hmerge x@30601 x y))))
(assert (forall ((x@30601 Heap) (x Heap) (y Heap) (z Heap) (x2 Nat) (x3 Heap)) (=> (and (= x@30601 (Node z x2 x3)) (= x (Node z x2 x3)) (= y Nil)) (hmerge x@30601 x y))))
(assert (forall ((x@30601 Heap) (x Heap) (y Heap)) (=> (and (= x@30601 y) (= x Nil)) (hmerge x@30601 x y))))
(declare-fun hpairwise (list2 list2) Bool)
(assert (forall ((x@30605 list2) (x list2)) (=> (and (= x@30605 nil2) (= x nil2)) (hpairwise x@30605 x))))
(assert (forall ((x@30605 list2) (x list2) (q Heap) (y list2)) (=> (and (= x@30605 (cons2 q nil2)) (= x (cons2 q y)) (= y nil2)) (hpairwise x@30605 x))))
(assert (forall ((x@30605 list2) (x list2) (q Heap) (y list2) (r Heap) (qs list2) (x@30607 Heap) (x@30606 list2)) (=> (and (= x@30605 (cons2 x@30607 x@30606)) (= x (cons2 q y)) (= y (cons2 r qs)) (hmerge x@30607 q r) (hpairwise x@30606 qs)) (hpairwise x@30605 x))))
(declare-fun hmerging (Heap list2) Bool)
(assert (forall ((x@30608 Heap) (x list2)) (=> (and (= x@30608 Nil) (= x nil2)) (hmerging x@30608 x))))
(assert (forall ((x@30608 Heap) (x list2) (q Heap) (y list2)) (=> (and (= x@30608 q) (= x (cons2 q y)) (= y nil2)) (hmerging x@30608 x))))
(assert (forall ((x@30608 Heap) (x list2) (q Heap) (y list2) (z Heap) (x2 list2) (x@30610 Heap) (x@30609 list2)) (=> (and (= x@30608 x@30610) (= x (cons2 q y)) (= y (cons2 z x2)) (hmerging x@30610 x@30609) (hpairwise x@30609 (cons2 q (cons2 z x2)))) (hmerging x@30608 x))))
(declare-fun toHeap2 (Heap list) Bool)
(assert (forall ((x@30611 Heap) (x list) (x@30613 Heap) (x@30612 list2)) (=> (and (= x@30611 x@30613) (hmerging x@30613 x@30612) (toHeap x@30612 x)) (toHeap2 x@30611 x))))
(declare-fun toList (list Heap) Bool)
(assert (forall ((x@30614 list) (x Heap) (q Heap) (y Nat) (r Heap) (x@30616 list) (x@30615 Heap)) (=> (and (= x@30614 (cons y x@30616)) (= x (Node q y r)) (toList x@30616 x@30615) (hmerge x@30615 q r)) (toList x@30614 x))))
(assert (forall ((x@30614 list) (x Heap)) (=> (and (= x@30614 nil) (= x Nil)) (toList x@30614 x))))
(declare-fun hsort (list list) Bool)
(assert (forall ((x@30617 list) (x list) (x@30619 list) (x@30618 Heap)) (=> (and (= x@30617 x@30619) (toList x@30619 x@30618) (toHeap2 x@30618 x)) (hsort x@30617 x))))
(assert (forall ((xs list)) (forall ((x@30621 Bool) (x@30620 list)) (=> (and (not x@30621) (ordered x@30621 x@30620) (hsort x@30620 xs)) false))))
(check-sat)
(get-info :reason-unknown)
