(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@565 (Nat@0 Nat@0) Bool)
(assert (forall ((x@26621 Nat@0)) (diseqNat@0@565 Z@0 (S@0 x@26621))))
(assert (forall ((x@26622 Nat@0)) (diseqNat@0@565 (S@0 x@26622) Z@0)))
(assert (forall ((x@26623 Nat@0) (x@26624 Nat@0)) (=> (diseqNat@0@565 x@26623 x@26624) (diseqNat@0@565 (S@0 x@26623) (S@0 x@26624)))))
(declare-datatype Nat ((zero ) (succ (p Nat))))
(declare-fun diseqNat@395 (Nat Nat) Bool)
(assert (forall ((x@26625 Nat)) (diseqNat@395 zero (succ x@26625))))
(assert (forall ((x@26626 Nat)) (diseqNat@395 (succ x@26626) zero)))
(assert (forall ((x@26627 Nat) (x@26628 Nat)) (=> (diseqNat@395 x@26627 x@26628) (diseqNat@395 (succ x@26627) (succ x@26628)))))
(declare-fun lt (Bool Nat Nat) Bool)
(assert (forall ((x@26616 Bool) (x Nat) (y Nat)) (=> (and (= x@26616 false) (= y zero)) (lt x@26616 x y))))
(assert (forall ((x@26616 Bool) (x Nat) (y Nat) (z Nat)) (=> (and (= x@26616 true) (= y (succ z)) (= x zero)) (lt x@26616 x y))))
(assert (forall ((x@26616 Bool) (x Nat) (y Nat) (z Nat) (n Nat) (x@26617 Bool)) (=> (and (= x@26616 x@26617) (= y (succ z)) (= x (succ n)) (lt x@26617 n z)) (lt x@26616 x y))))
(declare-fun gt (Bool Nat Nat) Bool)
(assert (forall ((x@26618 Bool) (x Nat) (y Nat) (x@26619 Bool)) (=> (and (= x@26618 x@26619) (lt x@26619 y x)) (gt x@26618 x y))))
(assert (forall ((x Nat)) (forall ((x@26620 Bool)) (=> (and x@26620 (gt x@26620 x x)) false))))
(check-sat)
(get-info :reason-unknown)
