(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@626 (Nat@0 Nat@0) Bool)
(assert (forall ((x@29462 Nat@0)) (diseqNat@0@626 Z@0 (S@0 x@29462))))
(assert (forall ((x@29463 Nat@0)) (diseqNat@0@626 (S@0 x@29463) Z@0)))
(assert (forall ((x@29464 Nat@0) (x@29465 Nat@0)) (=> (diseqNat@0@626 x@29464 x@29465) (diseqNat@0@626 (S@0 x@29464) (S@0 x@29465)))))
(declare-datatype A ((X ) (Y )))
(declare-fun diseqA@27 (A A) Bool)
(assert (diseqA@27 X Y))
(assert (diseqA@27 Y X))
(declare-datatype R ((Nil ) (Eps ) (Atom (proj1-Atom A)) (Plus (proj1-Plus R) (proj2-Plus R)) (Seq (proj1-Seq R) (proj2-Seq R)) (Star (proj1-Star R))))
(declare-fun diseqR@95 (R R) Bool)
(assert (diseqR@95 Nil Eps))
(assert (forall ((x@29466 A)) (diseqR@95 Nil (Atom x@29466))))
(assert (forall ((x@29467 R) (x@29468 R)) (diseqR@95 Nil (Plus x@29467 x@29468))))
(assert (forall ((x@29469 R) (x@29470 R)) (diseqR@95 Nil (Seq x@29469 x@29470))))
(assert (forall ((x@29471 R)) (diseqR@95 Nil (Star x@29471))))
(assert (diseqR@95 Eps Nil))
(assert (forall ((x@29472 A)) (diseqR@95 (Atom x@29472) Nil)))
(assert (forall ((x@29473 R) (x@29474 R)) (diseqR@95 (Plus x@29473 x@29474) Nil)))
(assert (forall ((x@29475 R) (x@29476 R)) (diseqR@95 (Seq x@29475 x@29476) Nil)))
(assert (forall ((x@29477 R)) (diseqR@95 (Star x@29477) Nil)))
(assert (forall ((x@29478 A)) (diseqR@95 Eps (Atom x@29478))))
(assert (forall ((x@29479 R) (x@29480 R)) (diseqR@95 Eps (Plus x@29479 x@29480))))
(assert (forall ((x@29481 R) (x@29482 R)) (diseqR@95 Eps (Seq x@29481 x@29482))))
(assert (forall ((x@29483 R)) (diseqR@95 Eps (Star x@29483))))
(assert (forall ((x@29484 A)) (diseqR@95 (Atom x@29484) Eps)))
(assert (forall ((x@29485 R) (x@29486 R)) (diseqR@95 (Plus x@29485 x@29486) Eps)))
(assert (forall ((x@29487 R) (x@29488 R)) (diseqR@95 (Seq x@29487 x@29488) Eps)))
(assert (forall ((x@29489 R)) (diseqR@95 (Star x@29489) Eps)))
(assert (forall ((x@29490 A) (x@29491 R) (x@29492 R)) (diseqR@95 (Atom x@29490) (Plus x@29491 x@29492))))
(assert (forall ((x@29493 A) (x@29494 R) (x@29495 R)) (diseqR@95 (Atom x@29493) (Seq x@29494 x@29495))))
(assert (forall ((x@29496 A) (x@29497 R)) (diseqR@95 (Atom x@29496) (Star x@29497))))
(assert (forall ((x@29498 R) (x@29499 R) (x@29500 A)) (diseqR@95 (Plus x@29498 x@29499) (Atom x@29500))))
(assert (forall ((x@29501 R) (x@29502 R) (x@29503 A)) (diseqR@95 (Seq x@29501 x@29502) (Atom x@29503))))
(assert (forall ((x@29504 R) (x@29505 A)) (diseqR@95 (Star x@29504) (Atom x@29505))))
(assert (forall ((x@29506 R) (x@29507 R) (x@29508 R) (x@29509 R)) (diseqR@95 (Plus x@29506 x@29507) (Seq x@29508 x@29509))))
(assert (forall ((x@29510 R) (x@29511 R) (x@29512 R)) (diseqR@95 (Plus x@29510 x@29511) (Star x@29512))))
(assert (forall ((x@29513 R) (x@29514 R) (x@29515 R) (x@29516 R)) (diseqR@95 (Seq x@29513 x@29514) (Plus x@29515 x@29516))))
(assert (forall ((x@29517 R) (x@29518 R) (x@29519 R)) (diseqR@95 (Star x@29517) (Plus x@29518 x@29519))))
(assert (forall ((x@29520 R) (x@29521 R) (x@29522 R)) (diseqR@95 (Seq x@29520 x@29521) (Star x@29522))))
(assert (forall ((x@29523 R) (x@29524 R) (x@29525 R)) (diseqR@95 (Star x@29523) (Seq x@29524 x@29525))))
(assert (forall ((x@29526 A) (x@29527 A)) (=> (diseqA@27 x@29526 x@29527) (diseqR@95 (Atom x@29526) (Atom x@29527)))))
(assert (forall ((x@29528 R) (x@29529 R) (x@29530 R) (x@29531 R)) (=> (diseqR@95 x@29528 x@29530) (diseqR@95 (Plus x@29528 x@29529) (Plus x@29530 x@29531)))))
(assert (forall ((x@29528 R) (x@29529 R) (x@29530 R) (x@29531 R)) (=> (diseqR@95 x@29529 x@29531) (diseqR@95 (Plus x@29528 x@29529) (Plus x@29530 x@29531)))))
(assert (forall ((x@29532 R) (x@29533 R) (x@29534 R) (x@29535 R)) (=> (diseqR@95 x@29532 x@29534) (diseqR@95 (Seq x@29532 x@29533) (Seq x@29534 x@29535)))))
(assert (forall ((x@29532 R) (x@29533 R) (x@29534 R) (x@29535 R)) (=> (diseqR@95 x@29533 x@29535) (diseqR@95 (Seq x@29532 x@29533) (Seq x@29534 x@29535)))))
(assert (forall ((x@29536 R) (x@29537 R)) (=> (diseqR@95 x@29536 x@29537) (diseqR@95 (Star x@29536) (Star x@29537)))))
(declare-datatype list ((nil ) (cons (head A) (tail list))))
(declare-fun diseqlist@392 (list list) Bool)
(assert (forall ((x@29538 A) (x@29539 list)) (diseqlist@392 nil (cons x@29538 x@29539))))
(assert (forall ((x@29540 A) (x@29541 list)) (diseqlist@392 (cons x@29540 x@29541) nil)))
(assert (forall ((x@29542 A) (x@29543 list) (x@29544 A) (x@29545 list)) (=> (diseqA@27 x@29542 x@29544) (diseqlist@392 (cons x@29542 x@29543) (cons x@29544 x@29545)))))
(assert (forall ((x@29542 A) (x@29543 list) (x@29544 A) (x@29545 list)) (=> (diseqlist@392 x@29543 x@29545) (diseqlist@392 (cons x@29542 x@29543) (cons x@29544 x@29545)))))
(declare-fun seq (R R R) Bool)
(assert (forall ((x@29433 R) (x R) (y R) (x@29425 R) (x@29426 R) (x@29427 R) (x@29428 R)) (=> (and (= x@29433 (Seq x y)) (= x x@29425) (= y x@29426) (= x x@29427) (= y x@29428)) (seq x@29433 x y))))
(assert (forall ((x@29433 R) (x R) (y R) (x@29425 R) (x@29426 R) (x@29427 R)) (=> (and (= x@29433 x) (= x x@29425) (= y x@29426) (= x x@29427) (= y Eps)) (seq x@29433 x y))))
(assert (forall ((x@29433 R) (x R) (y R) (x@29425 R) (x@29426 R)) (=> (and (= x@29433 y) (= x x@29425) (= y x@29426) (= x Eps)) (seq x@29433 x y))))
(assert (forall ((x@29433 R) (x R) (y R) (x@29425 R)) (=> (and (= x@29433 Nil) (= x x@29425) (= y Nil)) (seq x@29433 x y))))
(assert (forall ((x@29433 R) (x R) (y R)) (=> (and (= x@29433 Nil) (= x Nil)) (seq x@29433 x y))))
(declare-fun plus (R R R) Bool)
(assert (forall ((x@29434 R) (x R) (y R) (x@29429 R) (x@29430 R)) (=> (and (= x@29434 (Plus x y)) (= x x@29429) (= y x@29430)) (plus x@29434 x y))))
(assert (forall ((x@29434 R) (x R) (y R) (x@29429 R)) (=> (and (= x@29434 x) (= x x@29429) (= y Nil)) (plus x@29434 x y))))
(assert (forall ((x@29434 R) (x R) (y R)) (=> (and (= x@29434 y) (= x Nil)) (plus x@29434 x y))))
(declare-fun eqA (Bool A A) Bool)
(assert (forall ((x@29435 Bool) (x A) (y A)) (=> (and (= x@29435 true) (= x X) (= y X)) (eqA x@29435 x y))))
(assert (forall ((x@29435 Bool) (x A) (y A)) (=> (and (= x@29435 false) (= x X) (= y Y)) (eqA x@29435 x y))))
(assert (forall ((x@29435 Bool) (x A) (y A)) (=> (and (= x@29435 false) (= x Y) (= y X)) (eqA x@29435 x y))))
(assert (forall ((x@29435 Bool) (x A) (y A)) (=> (and (= x@29435 true) (= x Y) (= y Y)) (eqA x@29435 x y))))
(declare-fun eps (Bool R) Bool)
(assert (forall ((x@29436 Bool) (x R) (x@29431 R)) (=> (and (= x@29436 false) (= x x@29431)) (eps x@29436 x))))
(assert (forall ((x@29436 Bool) (x R)) (=> (and (= x@29436 true) (= x Eps)) (eps x@29436 x))))
(assert (forall ((x@29436 Bool) (x R) (p R) (q R) (x@29438 Bool) (x@29437 Bool)) (=> (and (= x@29436 (or x@29438 x@29437)) (= x (Plus p q)) (eps x@29438 p) (eps x@29437 q)) (eps x@29436 x))))
(assert (forall ((x@29436 Bool) (x R) (r R) (q2 R) (x@29440 Bool) (x@29439 Bool)) (=> (and (= x@29436 (and x@29440 x@29439)) (= x (Seq r q2)) (eps x@29440 r) (eps x@29439 q2)) (eps x@29436 x))))
(assert (forall ((x@29436 Bool) (x R) (y R)) (=> (and (= x@29436 true) (= x (Star y))) (eps x@29436 x))))
(declare-fun epsR (R R) Bool)
(assert (forall ((x@29441 R) (x R) (x@29442 Bool)) (=> (and (= x@29441 (ite x@29442 Eps Nil)) (eps x@29442 x)) (epsR x@29441 x))))
(declare-fun step (R R A) Bool)
(assert (forall ((x@29443 R) (x R) (y A) (x@29432 R)) (=> (and (= x@29443 Nil) (= x x@29432)) (step x@29443 x y))))
(assert (forall ((x@29443 R) (x R) (y A) (a A) (x@29444 Bool)) (=> (and (= x@29443 (ite x@29444 Eps Nil)) (= x (Atom a)) (eqA x@29444 a y)) (step x@29443 x y))))
(assert (forall ((x@29443 R) (x R) (y A) (p R) (q R) (x@29447 R) (x@29446 R) (x@29445 R)) (=> (and (= x@29443 x@29447) (= x (Plus p q)) (plus x@29447 x@29446 x@29445) (step x@29446 p y) (step x@29445 q y)) (step x@29443 x y))))
(assert (forall ((x@29443 R) (x R) (y A) (r R) (q2 R) (x@29453 R) (x@29452 R) (x@29451 R) (x@29450 R) (x@29449 R) (x@29448 R)) (=> (and (= x@29443 x@29453) (= x (Seq r q2)) (plus x@29453 x@29452 x@29450) (seq x@29452 x@29451 q2) (step x@29451 r y) (seq x@29450 x@29449 x@29448) (epsR x@29449 r) (step x@29448 q2 y)) (step x@29443 x y))))
(assert (forall ((x@29443 R) (x R) (y A) (p2 R) (x@29455 R) (x@29454 R)) (=> (and (= x@29443 x@29455) (= x (Star p2)) (seq x@29455 x@29454 (Star p2)) (step x@29454 p2 y)) (step x@29443 x y))))
(declare-fun recognise (Bool R list) Bool)
(assert (forall ((x@29456 Bool) (x R) (y list) (x@29457 Bool)) (=> (and (= x@29456 x@29457) (= y nil) (eps x@29457 x)) (recognise x@29456 x y))))
(assert (forall ((x@29456 Bool) (x R) (y list) (z A) (xs list) (x@29459 Bool) (x@29458 R)) (=> (and (= x@29456 x@29459) (= y (cons z xs)) (recognise x@29459 x@29458 xs) (step x@29458 x z)) (recognise x@29456 x y))))
(assert (forall ((p R) (s list)) (forall ((x@29461 Bool) (x@29460 Bool)) (=> (and (diseqBool@0 x@29461 x@29460) (recognise x@29461 (Plus p p) s) (recognise x@29460 p s)) false))))
(check-sat)
(get-info :reason-unknown)
