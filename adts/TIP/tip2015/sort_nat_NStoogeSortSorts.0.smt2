(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@520 (Nat@0 Nat@0) Bool)
(assert (forall ((x@24750 Nat@0)) (diseqNat@0@520 Z@0 (S@0 x@24750))))
(assert (forall ((x@24751 Nat@0)) (diseqNat@0@520 (S@0 x@24751) Z@0)))
(assert (forall ((x@24752 Nat@0) (x@24753 Nat@0)) (=> (diseqNat@0@520 x@24752 x@24753) (diseqNat@0@520 (S@0 x@24752) (S@0 x@24753)))))
(declare-datatype Nat ((zero ) (succ (p Nat))))
(declare-fun diseqNat@361 (Nat Nat) Bool)
(assert (forall ((x@24754 Nat)) (diseqNat@361 zero (succ x@24754))))
(assert (forall ((x@24755 Nat)) (diseqNat@361 (succ x@24755) zero)))
(assert (forall ((x@24756 Nat) (x@24757 Nat)) (=> (diseqNat@361 x@24756 x@24757) (diseqNat@361 (succ x@24756) (succ x@24757)))))
(declare-datatype list ((nil ) (cons (head Nat) (tail list))))
(declare-fun diseqlist@350 (list list) Bool)
(assert (forall ((x@24758 Nat) (x@24759 list)) (diseqlist@350 nil (cons x@24758 x@24759))))
(assert (forall ((x@24760 Nat) (x@24761 list)) (diseqlist@350 (cons x@24760 x@24761) nil)))
(assert (forall ((x@24762 Nat) (x@24763 list) (x@24764 Nat) (x@24765 list)) (=> (diseqNat@361 x@24762 x@24764) (diseqlist@350 (cons x@24762 x@24763) (cons x@24764 x@24765)))))
(assert (forall ((x@24762 Nat) (x@24763 list) (x@24764 Nat) (x@24765 list)) (=> (diseqlist@350 x@24763 x@24765) (diseqlist@350 (cons x@24762 x@24763) (cons x@24764 x@24765)))))
(declare-datatype pair ((pair2 (proj1-pair list) (proj2-pair list))))
(declare-fun diseqpair@42 (pair pair) Bool)
(assert (forall ((x@24766 list) (x@24767 list) (x@24768 list) (x@24769 list)) (=> (diseqlist@350 x@24766 x@24768) (diseqpair@42 (pair2 x@24766 x@24767) (pair2 x@24768 x@24769)))))
(assert (forall ((x@24766 list) (x@24767 list) (x@24768 list) (x@24769 list)) (=> (diseqlist@350 x@24767 x@24769) (diseqpair@42 (pair2 x@24766 x@24767) (pair2 x@24768 x@24769)))))
(declare-fun take (list Nat list) Bool)
(assert (forall ((x@24691 list) (x Nat) (y list)) (=> (and (= x@24691 nil) (= x zero)) (take x@24691 x y))))
(assert (forall ((x@24691 list) (x Nat) (y list) (z Nat)) (=> (and (= x@24691 nil) (= x (succ z)) (= y nil)) (take x@24691 x y))))
(assert (forall ((x@24691 list) (x Nat) (y list) (z Nat) (z2 Nat) (xs list) (x@24692 list)) (=> (and (= x@24691 (cons z2 x@24692)) (= x (succ z)) (= y (cons z2 xs)) (take x@24692 z xs)) (take x@24691 x y))))
(declare-fun plus (Nat Nat Nat) Bool)
(assert (forall ((x@24693 Nat) (x Nat) (y Nat)) (=> (and (= x@24693 y) (= x zero)) (plus x@24693 x y))))
(assert (forall ((x@24693 Nat) (x Nat) (y Nat) (z Nat) (x@24694 Nat)) (=> (and (= x@24693 (succ x@24694)) (= x (succ z)) (plus x@24694 z y)) (plus x@24693 x y))))
(declare-fun minus (Nat Nat Nat) Bool)
(assert (forall ((x@24695 Nat) (x Nat) (y Nat)) (=> (and (= x@24695 zero) (= x zero)) (minus x@24695 x y))))
(assert (forall ((x@24695 Nat) (x Nat) (y Nat) (z Nat)) (=> (and (= x@24695 zero) (= x (succ z)) (= y zero)) (minus x@24695 x y))))
(assert (forall ((x@24695 Nat) (x Nat) (y Nat) (z Nat) (y2 Nat) (x@24696 Nat)) (=> (and (= x@24695 x@24696) (= x (succ z)) (= y (succ y2)) (minus x@24696 z y2)) (minus x@24695 x y))))
(declare-fun third (Nat Nat) Bool)
(assert (forall ((x@24697 Nat) (x Nat)) (=> (and (= x@24697 (ite (= x (succ (succ zero))) zero (ite (= x (succ zero)) zero zero))) (= x zero)) (third x@24697 x))))
(assert (forall ((x@24697 Nat) (x Nat) (y Nat) (x@24700 Nat) (x@24699 Nat) (x@24698 Nat)) (=> (and (= x@24697 (ite (= x (succ (succ zero))) zero (ite (= x (succ zero)) zero x@24700))) (= x (succ y)) (plus x@24700 (succ zero) x@24699) (third x@24699 x@24698) (minus x@24698 (succ y) (succ (succ (succ zero))))) (third x@24697 x))))
(declare-fun leq (Bool Nat Nat) Bool)
(assert (forall ((x@24701 Bool) (x Nat) (y Nat)) (=> (and (= x@24701 true) (= x zero)) (leq x@24701 x y))))
(assert (forall ((x@24701 Bool) (x Nat) (y Nat) (z Nat)) (=> (and (= x@24701 false) (= x (succ z)) (= y zero)) (leq x@24701 x y))))
(assert (forall ((x@24701 Bool) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@24702 Bool)) (=> (and (= x@24701 x@24702) (= x (succ z)) (= y (succ x2)) (leq x@24702 z x2)) (leq x@24701 x y))))
(declare-fun ordered (Bool list) Bool)
(assert (forall ((x@24703 Bool) (x list)) (=> (and (= x@24703 true) (= x nil)) (ordered x@24703 x))))
(assert (forall ((x@24703 Bool) (x list) (y Nat) (z list)) (=> (and (= x@24703 true) (= x (cons y z)) (= z nil)) (ordered x@24703 x))))
(assert (forall ((x@24703 Bool) (x list) (y Nat) (z list) (y2 Nat) (xs list) (x@24705 Bool) (x@24704 Bool)) (=> (and (= x@24703 (and x@24705 x@24704)) (= x (cons y z)) (= z (cons y2 xs)) (leq x@24705 y y2) (ordered x@24704 (cons y2 xs))) (ordered x@24703 x))))
(declare-fun sort2 (list Nat Nat) Bool)
(assert (forall ((x@24706 list) (x Nat) (y Nat) (x@24707 Bool)) (=> (and (= x@24706 (ite x@24707 (cons x (cons y nil)) (cons y (cons x nil)))) (leq x@24707 x y)) (sort2 x@24706 x y))))
(declare-fun length (Nat list) Bool)
(assert (forall ((x@24708 Nat) (x list)) (=> (and (= x@24708 zero) (= x nil)) (length x@24708 x))))
(assert (forall ((x@24708 Nat) (x list) (y Nat) (l list) (x@24710 Nat) (x@24709 Nat)) (=> (and (= x@24708 x@24710) (= x (cons y l)) (plus x@24710 (succ zero) x@24709) (length x@24709 l)) (length x@24708 x))))
(declare-fun drop (list Nat list) Bool)
(assert (forall ((x@24711 list) (x Nat) (y list)) (=> (and (= x@24711 y) (= x zero)) (drop x@24711 x y))))
(assert (forall ((x@24711 list) (x Nat) (y list) (z Nat)) (=> (and (= x@24711 nil) (= x (succ z)) (= y nil)) (drop x@24711 x y))))
(assert (forall ((x@24711 list) (x Nat) (y list) (z Nat) (z2 Nat) (xs1 list) (x@24712 list)) (=> (and (= x@24711 x@24712) (= x (succ z)) (= y (cons z2 xs1)) (drop x@24712 z xs1)) (drop x@24711 x y))))
(declare-fun splitAt (pair Nat list) Bool)
(assert (forall ((x@24713 pair) (x Nat) (y list) (x@24715 list) (x@24714 list)) (=> (and (= x@24713 (pair2 x@24715 x@24714)) (take x@24715 x y) (drop x@24714 x y)) (splitAt x@24713 x y))))
(declare-fun ++ (list list list) Bool)
(assert (forall ((x@24716 list) (x list) (y list)) (=> (and (= x@24716 y) (= x nil)) (++ x@24716 x y))))
(assert (forall ((x@24716 list) (x list) (y list) (z Nat) (xs list) (x@24717 list)) (=> (and (= x@24716 (cons z x@24717)) (= x (cons z xs)) (++ x@24717 xs y)) (++ x@24716 x y))))
(declare-fun reverse (list list) Bool)
(assert (forall ((x@24718 list) (x list)) (=> (and (= x@24718 nil) (= x nil)) (reverse x@24718 x))))
(assert (forall ((x@24718 list) (x list) (y Nat) (xs list) (x@24720 list) (x@24719 list)) (=> (and (= x@24718 x@24720) (= x (cons y xs)) (++ x@24720 x@24719 (cons y nil)) (reverse x@24719 xs)) (reverse x@24718 x))))
(declare-fun nstooge1sort2 (list list) Bool)
(declare-fun nstoogesort (list list) Bool)
(declare-fun nstooge1sort1 (list list) Bool)
(assert (forall ((x@24721 list) (x list) (x@24725 pair) (x@24724 Nat) (x@24723 Nat) (x@24722 list) (ys1 list) (zs1 list) (x@24728 list) (x@24727 list) (x@24726 list)) (=> (and (= x@24721 x@24728) (= x@24725 (pair2 ys1 zs1)) (splitAt x@24725 x@24724 x@24722) (third x@24724 x@24723) (length x@24723 x) (reverse x@24722 x) (++ x@24728 x@24727 x@24726) (nstoogesort x@24727 zs1) (reverse x@24726 ys1)) (nstooge1sort2 x@24721 x))))
(assert (forall ((x@24729 list) (x list)) (=> (and (= x@24729 nil) (= x nil)) (nstoogesort x@24729 x))))
(assert (forall ((x@24729 list) (x list) (y Nat) (z list)) (=> (and (= x@24729 (cons y nil)) (= x (cons y z)) (= z nil)) (nstoogesort x@24729 x))))
(assert (forall ((x@24729 list) (x list) (y Nat) (z list) (y2 Nat) (x2 list) (x@24730 list)) (=> (and (= x@24729 x@24730) (= x (cons y z)) (= z (cons y2 x2)) (= x2 nil) (sort2 x@24730 y y2)) (nstoogesort x@24729 x))))
(assert (forall ((x@24729 list) (x list) (y Nat) (z list) (y2 Nat) (x2 list) (x3 Nat) (x4 list) (x@24733 list) (x@24732 list) (x@24731 list)) (=> (and (= x@24729 x@24733) (= x (cons y z)) (= z (cons y2 x2)) (= x2 (cons x3 x4)) (nstooge1sort2 x@24733 x@24732) (nstooge1sort1 x@24732 x@24731) (nstooge1sort2 x@24731 (cons y (cons y2 (cons x3 x4))))) (nstoogesort x@24729 x))))
(assert (forall ((x@24734 list) (x list) (x@24737 pair) (x@24736 Nat) (x@24735 Nat) (ys1 list) (zs list) (x@24739 list) (x@24738 list)) (=> (and (= x@24734 x@24739) (= x@24737 (pair2 ys1 zs)) (splitAt x@24737 x@24736 x) (third x@24736 x@24735) (length x@24735 x) (++ x@24739 ys1 x@24738) (nstoogesort x@24738 zs)) (nstooge1sort1 x@24734 x))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@24743 Nat) (x@24742 Nat) (x@24741 Nat) (x@24740 Nat)) (=> (and (diseqNat@361 x@24743 x@24741) (plus x@24743 x x@24742) (plus x@24742 y z) (plus x@24741 x@24740 z) (plus x@24740 x y)) false))))
(assert (forall ((x Nat) (y Nat)) (forall ((x@24745 Nat) (x@24744 Nat)) (=> (and (diseqNat@361 x@24745 x@24744) (plus x@24745 x y) (plus x@24744 y x)) false))))
(assert (forall ((x Nat)) (forall ((x@24746 Nat)) (=> (and (diseqNat@361 x@24746 x) (plus x@24746 x zero)) false))))
(assert (forall ((x Nat)) (forall ((x@24747 Nat)) (=> (and (diseqNat@361 x@24747 x) (plus x@24747 zero x)) false))))
(assert (forall ((xs list)) (forall ((x@24749 Bool) (x@24748 list)) (=> (and (not x@24749) (ordered x@24749 x@24748) (nstoogesort x@24748 xs)) false))))
(check-sat)
(get-info :reason-unknown)
