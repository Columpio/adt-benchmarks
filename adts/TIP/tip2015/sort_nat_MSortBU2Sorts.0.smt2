(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@487 (Nat@0 Nat@0) Bool)
(assert (forall ((x@23165 Nat@0)) (diseqNat@0@487 Z@0 (S@0 x@23165))))
(assert (forall ((x@23166 Nat@0)) (diseqNat@0@487 (S@0 x@23166) Z@0)))
(assert (forall ((x@23167 Nat@0) (x@23168 Nat@0)) (=> (diseqNat@0@487 x@23167 x@23168) (diseqNat@0@487 (S@0 x@23167) (S@0 x@23168)))))
(declare-datatype Nat ((zero ) (succ (p Nat))))
(declare-fun diseqNat@339 (Nat Nat) Bool)
(assert (forall ((x@23169 Nat)) (diseqNat@339 zero (succ x@23169))))
(assert (forall ((x@23170 Nat)) (diseqNat@339 (succ x@23170) zero)))
(assert (forall ((x@23171 Nat) (x@23172 Nat)) (=> (diseqNat@339 x@23171 x@23172) (diseqNat@339 (succ x@23171) (succ x@23172)))))
(declare-datatype list2 ((nil2 ) (cons2 (head2 Nat) (tail2 list2))))
(declare-fun diseqlist2@37 (list2 list2) Bool)
(assert (forall ((x@23173 Nat) (x@23174 list2)) (diseqlist2@37 nil2 (cons2 x@23173 x@23174))))
(assert (forall ((x@23175 Nat) (x@23176 list2)) (diseqlist2@37 (cons2 x@23175 x@23176) nil2)))
(assert (forall ((x@23177 Nat) (x@23178 list2) (x@23179 Nat) (x@23180 list2)) (=> (diseqNat@339 x@23177 x@23179) (diseqlist2@37 (cons2 x@23177 x@23178) (cons2 x@23179 x@23180)))))
(assert (forall ((x@23177 Nat) (x@23178 list2) (x@23179 Nat) (x@23180 list2)) (=> (diseqlist2@37 x@23178 x@23180) (diseqlist2@37 (cons2 x@23177 x@23178) (cons2 x@23179 x@23180)))))
(declare-datatype list ((nil ) (cons (head list2) (tail list))))
(declare-fun diseqlist@334 (list list) Bool)
(assert (forall ((x@23181 list2) (x@23182 list)) (diseqlist@334 nil (cons x@23181 x@23182))))
(assert (forall ((x@23183 list2) (x@23184 list)) (diseqlist@334 (cons x@23183 x@23184) nil)))
(assert (forall ((x@23185 list2) (x@23186 list) (x@23187 list2) (x@23188 list)) (=> (diseqlist2@37 x@23185 x@23187) (diseqlist@334 (cons x@23185 x@23186) (cons x@23187 x@23188)))))
(assert (forall ((x@23185 list2) (x@23186 list) (x@23187 list2) (x@23188 list)) (=> (diseqlist@334 x@23186 x@23188) (diseqlist@334 (cons x@23185 x@23186) (cons x@23187 x@23188)))))
(declare-fun leq (Bool Nat Nat) Bool)
(assert (forall ((x@23141 Bool) (x Nat) (y Nat)) (=> (and (= x@23141 true) (= x zero)) (leq x@23141 x y))))
(assert (forall ((x@23141 Bool) (x Nat) (y Nat) (z Nat)) (=> (and (= x@23141 false) (= x (succ z)) (= y zero)) (leq x@23141 x y))))
(assert (forall ((x@23141 Bool) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@23142 Bool)) (=> (and (= x@23141 x@23142) (= x (succ z)) (= y (succ x2)) (leq x@23142 z x2)) (leq x@23141 x y))))
(declare-fun lmerge (list2 list2 list2) Bool)
(assert (forall ((x@23143 list2) (x list2) (y list2)) (=> (and (= x@23143 y) (= x nil2)) (lmerge x@23143 x y))))
(assert (forall ((x@23143 list2) (x list2) (y list2) (z Nat) (x2 list2)) (=> (and (= x@23143 (cons2 z x2)) (= x (cons2 z x2)) (= y nil2)) (lmerge x@23143 x y))))
(assert (forall ((x@23143 list2) (x list2) (y list2) (z Nat) (x2 list2) (x3 Nat) (x4 list2) (x@23144 Bool) (x@23145 list2) (x@23146 list2)) (=> (and (= x@23143 (ite x@23144 (cons2 z x@23145) (cons2 x3 x@23146))) (= x (cons2 z x2)) (= y (cons2 x3 x4)) (leq x@23144 z x3) (lmerge x@23145 x2 (cons2 x3 x4)) (lmerge x@23146 (cons2 z x2) x4)) (lmerge x@23143 x y))))
(declare-fun pairwise (list list) Bool)
(assert (forall ((x@23147 list) (x list)) (=> (and (= x@23147 nil) (= x nil)) (pairwise x@23147 x))))
(assert (forall ((x@23147 list) (x list) (xs list2) (y list)) (=> (and (= x@23147 (cons xs nil)) (= x (cons xs y)) (= y nil)) (pairwise x@23147 x))))
(assert (forall ((x@23147 list) (x list) (xs list2) (y list) (ys list2) (xss list) (x@23149 list2) (x@23148 list)) (=> (and (= x@23147 (cons x@23149 x@23148)) (= x (cons xs y)) (= y (cons ys xss)) (lmerge x@23149 xs ys) (pairwise x@23148 xss)) (pairwise x@23147 x))))
(declare-fun mergingbu2 (list2 list) Bool)
(assert (forall ((x@23150 list2) (x list)) (=> (and (= x@23150 nil2) (= x nil)) (mergingbu2 x@23150 x))))
(assert (forall ((x@23150 list2) (x list) (xs list2) (y list)) (=> (and (= x@23150 xs) (= x (cons xs y)) (= y nil)) (mergingbu2 x@23150 x))))
(assert (forall ((x@23150 list2) (x list) (xs list2) (y list) (z list2) (x2 list) (x@23152 list2) (x@23151 list)) (=> (and (= x@23150 x@23152) (= x (cons xs y)) (= y (cons z x2)) (mergingbu2 x@23152 x@23151) (pairwise x@23151 (cons xs (cons z x2)))) (mergingbu2 x@23150 x))))
(declare-fun ordered (Bool list2) Bool)
(assert (forall ((x@23153 Bool) (x list2)) (=> (and (= x@23153 true) (= x nil2)) (ordered x@23153 x))))
(assert (forall ((x@23153 Bool) (x list2) (y Nat) (z list2)) (=> (and (= x@23153 true) (= x (cons2 y z)) (= z nil2)) (ordered x@23153 x))))
(assert (forall ((x@23153 Bool) (x list2) (y Nat) (z list2) (y2 Nat) (xs list2) (x@23155 Bool) (x@23154 Bool)) (=> (and (= x@23153 (and x@23155 x@23154)) (= x (cons2 y z)) (= z (cons2 y2 xs)) (leq x@23155 y y2) (ordered x@23154 (cons2 y2 xs))) (ordered x@23153 x))))
(declare-fun risers (list list2) Bool)
(assert (forall ((x@23156 list) (x list2)) (=> (and (= x@23156 nil) (= x nil2)) (risers x@23156 x))))
(assert (forall ((x@23156 list) (x list2) (y Nat) (z list2)) (=> (and (= x@23156 (cons (cons2 y nil2) nil)) (= x (cons2 y z)) (= z nil2)) (risers x@23156 x))))
(assert (forall ((x@23156 list) (x list2) (y Nat) (z list2) (y2 Nat) (xs list2) (x@23157 Bool) (x@23158 list) (x@23159 list)) (=> (and (= x@23156 (ite x@23157 nil (cons (cons2 y nil2) x@23159))) (= x (cons2 y z)) (= z (cons2 y2 xs)) (leq x@23157 y y2) (= x@23158 nil) (risers x@23158 (cons2 y2 xs)) (risers x@23159 (cons2 y2 xs))) (risers x@23156 x))))
(assert (forall ((x@23156 list) (x list2) (y Nat) (z list2) (y2 Nat) (xs list2) (x@23157 Bool) (x@23158 list) (ys list2) (yss list) (x@23159 list)) (=> (and (= x@23156 (ite x@23157 (cons (cons2 y ys) yss) (cons (cons2 y nil2) x@23159))) (= x (cons2 y z)) (= z (cons2 y2 xs)) (leq x@23157 y y2) (= x@23158 (cons ys yss)) (risers x@23158 (cons2 y2 xs)) (risers x@23159 (cons2 y2 xs))) (risers x@23156 x))))
(declare-fun msortbu2 (list2 list2) Bool)
(assert (forall ((x@23160 list2) (x list2) (x@23162 list2) (x@23161 list)) (=> (and (= x@23160 x@23162) (mergingbu2 x@23162 x@23161) (risers x@23161 x)) (msortbu2 x@23160 x))))
(assert (forall ((xs list2)) (forall ((x@23164 Bool) (x@23163 list2)) (=> (and (not x@23164) (ordered x@23164 x@23163) (msortbu2 x@23163 xs)) false))))
(check-sat)
(get-info :reason-unknown)
