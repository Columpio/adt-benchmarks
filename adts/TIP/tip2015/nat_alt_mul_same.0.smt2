(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@488 (Nat@0 Nat@0) Bool)
(assert (forall ((x@23229 Nat@0)) (diseqNat@0@488 Z@0 (S@0 x@23229))))
(assert (forall ((x@23230 Nat@0)) (diseqNat@0@488 (S@0 x@23230) Z@0)))
(assert (forall ((x@23231 Nat@0) (x@23232 Nat@0)) (=> (diseqNat@0@488 x@23231 x@23232) (diseqNat@0@488 (S@0 x@23231) (S@0 x@23232)))))
(declare-datatype Nat ((zero ) (succ (p Nat))))
(declare-fun diseqNat@340 (Nat Nat) Bool)
(assert (forall ((x@23233 Nat)) (diseqNat@340 zero (succ x@23233))))
(assert (forall ((x@23234 Nat)) (diseqNat@340 (succ x@23234) zero)))
(assert (forall ((x@23235 Nat) (x@23236 Nat)) (=> (diseqNat@340 x@23235 x@23236) (diseqNat@340 (succ x@23235) (succ x@23236)))))
(declare-fun plus (Nat Nat Nat) Bool)
(assert (forall ((x@23189 Nat) (x Nat) (y Nat)) (=> (and (= x@23189 y) (= x zero)) (plus x@23189 x y))))
(assert (forall ((x@23189 Nat) (x Nat) (y Nat) (z Nat) (x@23190 Nat)) (=> (and (= x@23189 (succ x@23190)) (= x (succ z)) (plus x@23190 z y)) (plus x@23189 x y))))
(declare-fun times (Nat Nat Nat) Bool)
(assert (forall ((x@23191 Nat) (x Nat) (y Nat)) (=> (and (= x@23191 zero) (= x zero)) (times x@23191 x y))))
(assert (forall ((x@23191 Nat) (x Nat) (y Nat) (z Nat) (x@23193 Nat) (x@23192 Nat)) (=> (and (= x@23191 x@23193) (= x (succ z)) (plus x@23193 y x@23192) (times x@23192 z y)) (times x@23191 x y))))
(declare-fun alt_mul (Nat Nat Nat) Bool)
(assert (forall ((x@23194 Nat) (x Nat) (y Nat)) (=> (and (= x@23194 zero) (= x zero)) (alt_mul x@23194 x y))))
(assert (forall ((x@23194 Nat) (x Nat) (y Nat) (z Nat)) (=> (and (= x@23194 zero) (= x (succ z)) (= y zero)) (alt_mul x@23194 x y))))
(assert (forall ((x@23194 Nat) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@23198 Nat) (x@23197 Nat) (x@23196 Nat) (x@23195 Nat)) (=> (and (= x@23194 x@23198) (= x (succ z)) (= y (succ x2)) (plus x@23198 x@23197 x2) (plus x@23197 x@23196 z) (plus x@23196 (succ zero) x@23195) (alt_mul x@23195 z x2)) (alt_mul x@23194 x y))))
(assert (forall ((x Nat) (y Nat)) (forall ((x@23200 Nat) (x@23199 Nat)) (=> (and (diseqNat@340 x@23200 x@23199) (alt_mul x@23200 x y) (times x@23199 x y)) false))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@23204 Nat) (x@23203 Nat) (x@23202 Nat) (x@23201 Nat)) (=> (and (diseqNat@340 x@23204 x@23202) (times x@23204 x x@23203) (times x@23203 y z) (times x@23202 x@23201 z) (times x@23201 x y)) false))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@23208 Nat) (x@23207 Nat) (x@23206 Nat) (x@23205 Nat)) (=> (and (diseqNat@340 x@23208 x@23206) (plus x@23208 x x@23207) (plus x@23207 y z) (plus x@23206 x@23205 z) (plus x@23205 x y)) false))))
(assert (forall ((x Nat) (y Nat)) (forall ((x@23210 Nat) (x@23209 Nat)) (=> (and (diseqNat@340 x@23210 x@23209) (times x@23210 x y) (times x@23209 y x)) false))))
(assert (forall ((x Nat) (y Nat)) (forall ((x@23212 Nat) (x@23211 Nat)) (=> (and (diseqNat@340 x@23212 x@23211) (plus x@23212 x y) (plus x@23211 y x)) false))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@23217 Nat) (x@23216 Nat) (x@23215 Nat) (x@23214 Nat) (x@23213 Nat)) (=> (and (diseqNat@340 x@23217 x@23215) (times x@23217 x x@23216) (plus x@23216 y z) (plus x@23215 x@23214 x@23213) (times x@23214 x y) (times x@23213 x z)) false))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@23222 Nat) (x@23221 Nat) (x@23220 Nat) (x@23219 Nat) (x@23218 Nat)) (=> (and (diseqNat@340 x@23222 x@23220) (times x@23222 x@23221 z) (plus x@23221 x y) (plus x@23220 x@23219 x@23218) (times x@23219 x z) (times x@23218 y z)) false))))
(assert (forall ((x Nat)) (forall ((x@23223 Nat)) (=> (and (diseqNat@340 x@23223 x) (times x@23223 x (succ zero))) false))))
(assert (forall ((x Nat)) (forall ((x@23224 Nat)) (=> (and (diseqNat@340 x@23224 x) (times x@23224 (succ zero) x)) false))))
(assert (forall ((x Nat)) (forall ((x@23225 Nat)) (=> (and (diseqNat@340 x@23225 x) (plus x@23225 x zero)) false))))
(assert (forall ((x Nat)) (forall ((x@23226 Nat)) (=> (and (diseqNat@340 x@23226 x) (plus x@23226 zero x)) false))))
(assert (forall ((x Nat)) (forall ((x@23227 Nat)) (=> (and (diseqNat@340 x@23227 zero) (times x@23227 x zero)) false))))
(assert (forall ((x Nat)) (forall ((x@23228 Nat)) (=> (and (diseqNat@340 x@23228 zero) (times x@23228 zero x)) false))))
(check-sat)
(get-info :reason-unknown)
