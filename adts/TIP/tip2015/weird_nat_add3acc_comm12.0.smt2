(set-logic HORN)
(declare-fun diseqBool@0 (Bool Bool) Bool)
(assert (diseqBool@0 true false))
(assert (diseqBool@0 false true))
(declare-datatype Nat@0 ((Z@0 ) (S@0 (unS@0 Nat@0))))
(declare-fun diseqNat@0@548 (Nat@0 Nat@0) Bool)
(assert (forall ((x@25982 Nat@0)) (diseqNat@0@548 Z@0 (S@0 x@25982))))
(assert (forall ((x@25983 Nat@0)) (diseqNat@0@548 (S@0 x@25983) Z@0)))
(assert (forall ((x@25984 Nat@0) (x@25985 Nat@0)) (=> (diseqNat@0@548 x@25984 x@25985) (diseqNat@0@548 (S@0 x@25984) (S@0 x@25985)))))
(declare-datatype Nat ((zero ) (succ (p Nat))))
(declare-fun diseqNat@380 (Nat Nat) Bool)
(assert (forall ((x@25986 Nat)) (diseqNat@380 zero (succ x@25986))))
(assert (forall ((x@25987 Nat)) (diseqNat@380 (succ x@25987) zero)))
(assert (forall ((x@25988 Nat) (x@25989 Nat)) (=> (diseqNat@380 x@25988 x@25989) (diseqNat@380 (succ x@25988) (succ x@25989)))))
(declare-fun add3acc (Nat Nat Nat Nat) Bool)
(assert (forall ((x@25977 Nat) (x Nat) (y Nat) (z Nat)) (=> (and (= x@25977 z) (= x zero) (= y zero)) (add3acc x@25977 x y z))))
(assert (forall ((x@25977 Nat) (x Nat) (y Nat) (z Nat) (x3 Nat) (x@25978 Nat)) (=> (and (= x@25977 x@25978) (= x zero) (= y (succ x3)) (add3acc x@25978 zero x3 (succ z))) (add3acc x@25977 x y z))))
(assert (forall ((x@25977 Nat) (x Nat) (y Nat) (z Nat) (x2 Nat) (x@25979 Nat)) (=> (and (= x@25977 x@25979) (= x (succ x2)) (add3acc x@25979 x2 (succ y) z)) (add3acc x@25977 x y z))))
(assert (forall ((x Nat) (y Nat) (z Nat)) (forall ((x@25981 Nat) (x@25980 Nat)) (=> (and (diseqNat@380 x@25981 x@25980) (add3acc x@25981 x y z) (add3acc x@25980 y x z)) false))))
(check-sat)
(get-info :reason-unknown)
